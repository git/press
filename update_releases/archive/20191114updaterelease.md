2019-11-14 Cumulative Update Release
====================================

The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 12.1, 11.6, 10.11, 9.6.16, 9.5.20,
and 9.4.25. This release fixes over 50 bugs reported over the last three months.

PostgreSQL 9.4 EOL Approaching
------------------------------

PostgreSQL 9.4 will stop receiving fixes on February 13, 2020, which is the next
planned cumulative update release. We suggest that you make plans to upgrade to
a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Bug Fixes and Improvements
--------------------------

This update also fixes over 50 bugs that were reported in the last several
months. Some of these issues affect only version 12, but may also affect all
supported versions.

Some of these fixes include:

* Fix crash that occurs when `ALTER TABLE` adds a column without a default value
along with other changes that require a table rewrite
* Several fixes for `REINDEX CONCURRENTLY`.
* Fix for `VACUUM` that would cause it to fail under a specific case involving a
still-running transaction.
* Fix for a memory leak that could occur when `VACUUM` runs on a GiST index.
* Fix for an error that occurred when running `CLUSTER` on an expression index.
* Fix failure for `SET CONSTRAINTS ... DEFERRED` on partitioned tables.
* Several fixes for the creation and dropping of indexes on partitioned tables.
* Fix for partition-wise joins that could lead to planner failures.
* Ensure that offset expressions in WINDOW clauses are processed when a query's
expressions are manipulated.
* Fix misbehavior of `bitshiftright()` where it failed to zero out padding space
in the last byte if the bit string length is not a multiple of 8. For how to
correct your data, please see the "Updating" section.
* Ensure an empty string that is evaluated by the `position()` functions returns
1, as per the SQL standard.
* Fix for a parallel query failure when it is unable to request a background
worker.
* Fix crash triggered by a case involving a `BEFORE UPDATE` trigger.
* Display the correct error when a query tries to access a TOAST table.
* Allow encoding conversion to succeed on strings with output up to 1GB.
Previously there was hard limit of 0.25GB on the input string.
* Ensure that temporary WAL and history files are removed at the end of archive
recovery.
* Avoid failure in archive recovery if `recovery_min_apply_delay` is enabled.
* Ignore `restore_command`, `recovery_end_command`, and
`recovery_min_apply_delay` settings during crash recovery.
* Several fixes for logical replication, including a failure when the publisher
and subscriber had different REPLICA IDENTITY columns set.
* Correctly timestamp replication messages for logical decoding, which in the
broken case would lead to `pg_stat_subscription.last_msg_send_time` set to
`NULL`.
* Several fixes for libpq, including one that improves PostgreSQL 12
compatibility.
* Several `pg_upgrade` fixes.
* Fix how a parallel restore handles foreign key constraints on partitioned
tables to ensure they are not created too soon.
* `pg_dump` now outputs similarly named triggers and RLS policies in order based
on table name, instead of OID.
* Fix `pg_rewind` to not update the contents of `pg_control` when using the
`--dry-run` option.

This update also contains tzdata release 2019c for DST law changes in Fiji and
Norfolk Island.  Historical corrections for Alberta, Austria, Belgium, British
Columbia, Cambodia, Hong Kong, Indiana (Perry County), Kaliningrad, Kentucky,
Michigan, Norfolk Island, South Korea, and Turkey.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/current/release.html).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

If you have inconsistent data as a result of saving the output of
`bitshiftright()` in a table, it's possible to fix it with a query similar to:

    UPDATE mytab SET bitcol = ~(~bitcol) WHERE bitcol != ~(~bitcol);

**NOTE**: PostgreSQL 9.4 will stop receiving fixes on February 13, 2020. Please
see our [versioning policy](https://www.postgresql.org/support/versioning/) for
more information.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/current/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
