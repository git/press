The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 14.2, 13.6, 12.10, 11.15, and 10.20. This
release fixes over 55 bugs reported over the last three months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Bug Fixes and Improvements
--------------------------

This update fixes over 55 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 14. Some of these issues may also
affect other supported versions of PostgreSQL.

Included in this release:

* Fix for a low probability scenario of index corruption when a HOT
([heap-only tuple](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/access/heap/README.HOT;hb=HEAD))
chain changes state during `VACUUM`. Encountering this issue is unlikely, but if
you are concerned, please consider
[reindexing](https://www.postgresql.org/docs/current/sql-reindex.html).
* Fix for using [`REINDEX CONCURRENTLY`](https://www.postgresql.org/docs/current/sql-reindex.html)
on [TOAST](https://www.postgresql.org/docs/current/storage-toast.html) table
indexes to prevent corruption. You can fix any TOAST indexes by
[reindexing](https://www.postgresql.org/docs/current/sql-reindex.html) them
again.
* The [`psql`](https://www.postgresql.org/docs/current/app-psql.html)
`\password` command now defaults to setting the password for the role defined
by `CURRENT_USER`. Additionally, the role name is now included in the password
prompt.
* Build extended statistics for partitioned tables. If you previously added
extended statistics to a partitioned table, you should run
[`ANALYZE`](https://www.postgresql.org/docs/current/sql-analyze.html) on those
tables.
As [`autovacuum`](https://www.postgresql.org/docs/current/routine-vacuuming.html#AUTOVACUUM)
currently does not process partitioned tables, you must periodically run
`ANALYZE` on any partitioned tables to update their statistics.
* Fix crash with [`ALTER STATISTICS`](https://www.postgresql.org/docs/current/sql-alterstatistics.html)
when the statistics object is dropped concurrently.
* Fix crash with [multiranges](https://www.postgresql.org/docs/current/rangetypes.html)
when extracting variable-length data types.
* Several fixes to the query planner that lead to incorrect query results.
* Several fixes for query plan [memoization](https://www.postgresql.org/docs/current/runtime-config-query.html#GUC-ENABLE-MEMOIZE).
* Fix startup of a physical replica to tolerate transaction ID wraparound.
* When using logical replication, avoid duplicate transmission of a
partitioned table's data when the publication includes both the child and parent
tables.
* Disallow altering data type of a partitioned table's columns when the
partitioned table's row type is used as a composite type elsewhere.
* Disallow [`ALTER TABLE ... DROP NOT NULL`](https://www.postgresql.org/docs/current/sql-altertable.html)
for a column that is part of a replica identity index.
* Several fixes for caching that correct logical replication behavior and
improve performance.
* Fix memory leak when updating expression indexes.
* Avoid leaking memory during [`REASSIGN OWNED BY`](https://www.postgresql.org/docs/current/sql-reassign-owned.html)
operations that reassign ownership of many objects.
* Fix display of whole-row variables appearing in `INSERT ... VALUES` rules.
* Fix race condition that could lead to failure to localize error messages that
are reported early in multi-threaded use of libpq or ecpglib.
* Fix [`psql`](https://www.postgresql.org/docs/current/app-psql.html) `\d`
command for identifying parent triggers.
* Fix failures on Windows when using the terminal as data source or
destination. This affected the psql `\copy` command and using
[`pg_recvlogical`](https://www.postgresql.org/docs/current/app-pgrecvlogical.html)
with `-f -`.
* Fix the [`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html)
`--inserts` and `--column-inserts` modes to handle tables that contain both
generated and dropped columns.
* Fix edge cases in how [`postgres_fdw`](https://www.postgresql.org/docs/current/postgres-fdw.html)
handles asynchronous queries. These errors could lead to crashes or incorrect
results when attempting to run parallel scans of foreign tables.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
