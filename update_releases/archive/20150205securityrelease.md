The PostgreSQL Global Development Group has released an important update with fixes for multiple security issues to all supported versions of the PostgreSQL database system, which includes minor versions 9.4.1, 9.3.6, 9.2.10, 9.1.15, and 9.0.19. This update includes both security fixes and fixes for issues discovered since the last release.  In particular for the 9.4 update, there is a change to the way unicode strings are escaped for the JSON and JSONB data types.

All users should update their PostgreSQL installation at the next opportunity.

Security Fixes
--------------

This update fixes multiple security issues reported in PostgreSQL over the past few months.  All of these issues require prior authentication, and some require additional conditions, and as such are not considered generally urgent.  However, users should examine the list of security holes patched below in case they are particularly vulnerable.

* [CVE-2015-0241](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0241) Buffer overruns in "to_char" functions.
* [CVE-2015-0242](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0242) Buffer overrun in replacement printf family of functions.
* [CVE-2015-0243](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0243) Memory errors in functions in the pgcrypto extension.
* [CVE-2015-0244](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0244) An error in extended protocol message reading.
* [CVE-2014-8161](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-8161) Constraint violation errors can cause display of values in columns which the user would not normally have rights to see.

This update also fixes the previously reported problem that, during regression testing on Windows, the test postmaster process was vulnerable to unauthorized connections.  This vulnerability was fixed on non-Windows platforms in the prior update releases.

More information about these issues, as well as older patched issues, is available on the PostgreSQL Security Page.

JSON and JSONB Unicode Escapes
------------------------------

The handling of Unicode escape strings for JSON and JSONB in PostgreSQL 9.4.0 has been changed in a way which may break compatibility for some users.  To fix some inconsistencies, type JSONB no longer accepts the escape sequence "\u0000".  Type JSON accepts "\u0000" only in contexts where it does not need to be converted to de-escaped form.  See the release notes for more detail.

Other Fixes and Improvements
----------------------------

In addition to the above, more than 60 reported issues have been fixed in this cumulative update release.  Some of them affect only version 9.4, but many of them fix problems present in older versions.  These fixes include:

* Cope with the non-ASCII Norwegian Windows locale name.
* Avoid data corruption when databases are moved to new tablespaces and back again.
* Ensure that UNLOGGED tables are correctly copied during ALTER DATABASE operations.
* Avoid deadlocks when locking recently modified rows.
* Fix two SELECT FOR UPDATE query issues.
* Prevent false negative for shortest-first regular expression matches.
* Fix false positives and negatives in tsquery contains operator.
* Fix namespace handling in xpath().
* Prevent row-producing functions from creating empty column names.
* Make autovacuum use per-table cost_limit and cost_delay settings.
* When autovacuum=off, limit autovacuum work to wraparound prevention only.
* Multiple fixes for logical decoding in 9.4.
* Fix transient errors on hot standby queries due to page replacement.
* Prevent duplicate WAL file archiving at end of recovery or standby promotion.
* Prevent deadlock in parallel restore of schema-only dump.

In addition to the fixes above, the following contrib modules and extensions have had bugs fixed in this release: pg_upgrade, auto_explain, hstore, pageinspect, pgcrypto, pg_test_fsync, tablefunc, and xml2.  Also, multiple functions across several contrib modules have been modified with the correct level of volatility.  There are also multiple cleanup fixes based on minor issues found by the Coverity Scan static analyzer.

This update also contains many changes to PostgreSQL's timezone files.  This includes an update to tzdata release 2015a, with updates to Chile, Mexico, Caicos Islands, and Fiji.  PostgreSQL now takes date into account when assigning an offset based on a timezone abbreviation for historically changeable timezones.  We have also done a general cleanup on timezone abbreviations, and added "CST" as an abbreviation for China Standard Time.

As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.  Users who have skipped multiple update releases may need to perform additional post-update steps; see the Release Notes for details.

Links:
  * [Download](http://www.postgresql.org/download)
  * [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
  * [Security Page](http://www.postgresql.org/support/security/)
