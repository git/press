2017-05-11 Security Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.6.3, 9.5.7, 9.4.12, 9.3.17, and 9.2.21. This release fixes three security issues.  It also patches a number of other bugs reported over the last three months.  Users who use the PGREQUIRESSL environment variable to control connections, and users who rely on security isolation between database users when using foreign servers, should update as soon as possible.  Other users should plan to update at the next convenient downtime.

Security Issues
---------------

Three security vulnerabilities have been closed by this release:

* [CVE-2017-7484](https://access.redhat.com/security/cve/CVE-2017-7484): selectivity estimators bypass SELECT privilege checks
* [CVE-2017-7485](https://access.redhat.com/security/cve/CVE-2017-7485): libpq ignores PGREQUIRESSL environment variable
* [CVE-2017-7486](https://access.redhat.com/security/cve/CVE-2017-7486): pg_user_mappings view discloses foreign server passwords

The fix for CVE-2017-7486 applies to new databases, see the release notes for the procedure to apply the fix to an existing database.

Any user relying on the PGREQUIRESSL environment variable is encouraged to use the sslmode connection string option,
as use of PGREQUIRESSL is deprecated.  CVE-2017-7485 does not affect the 9.2 series.  For more information on these issues
and how they affect backwards-compatibility, see the Release Notes.

Bug Fixes and Improvements
--------------------------

This update also fixes a number of bugs reported in the last few months.  Some of these issues affect only the 9.6 series, but many
affect all supported versions.  There are more than 90 fixes in this release, including:

* Fix to ensure consistent behavior for RLS policies
* Fix ALTER TABLE ... VALIDATE CONSTRAINT to not recurse to child tables when the constraint is marked NO INHERIT
* Fix incorrect support for certain box operators in SP-GiST which could yield incorrect results
* Fixes for handling query cancellation
* Skip tablespace privilege checks when ALTER TABLE ... ALTER COLUMN TYPE rebuilds an existing index
* Fix possibly-invalid initial snapshot during logical decoding
* Fix possible corruption of init forks of unlogged indexes
* Several fixes to postmaster, including checks for when running as a Windows service
* Several planner fixes, among others assorted minor fixes in planning of parallel queries
* Avoid possible crashes in walsender and some index-only scans on GiST index
* Fix cancelling of pg_stop_backup() when attempting to stop a non-exclusive backup
* Updates to ecpg to support COMMIT PREPARED and ROLLBACK PREPARED
* Several fixes for pg_dump/pg_restore, among others to handle privileges for procedural languages and when using --clean option
* Several fixes for contrib modules, such as dblink, pg_trgm and postgres_fdw
* Fixes to MSVC builds, such as using correct daylight-savings rules for POSIX-style time zone names and supporting Tcl 8.6
* Several performance improvements
* Fix cursor_to_xml() to produce valid output with tableforest = false
* Fix roundoff problems in float8_timestamptz() and make_interval()
* Fix pgbench to handle the combination of --connect and --rate options correctly
* Fixes to commandline tools such as pg_upgrade and pg_basebackup
* Several fixes to VACUUM and CLUSTER

Users of replication tools based on logical decoding, as well as users of unlogged indexes, should consult the release notes
for potential extra steps during the upgrade.

This update also contains tzdata release 2017b with updates for DST law changes in Chile, Haiti, and Mongolia, plus historical corrections for Ecuador, Kazakhstan, Liberia, and Spain.  Switch to numeric abbreviations for numerous time zones in South America, the Pacific and Indian oceans, and some Asian and Middle Eastern countries. The timezone library is synchronized with IANA release tzcode2017b.

EOL Warning for Version 9.2
---------------------------

PostgreSQL version 9.2 will be End-of-Life in September 2017.  The project expects to only release one, or two, more updates for that version.  We urge users to start planning an upgrade to a later version of PostgreSQL as soon as possible. See our Versioning Policy for more information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.

After update, users of replication tools based on logical decoding, as well as users of unlogged indexes, should consult the release notes
for potential extra steps during the upgrade.  See the Release Notes for more details.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Links:
* [Download](https://www.postgresql.org/download)
* [Release Notes](https://www.postgresql.org/docs/current/static/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
