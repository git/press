The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 16.2, 15.6, 14.11, 13.14, and 12.18.
This release fixes one security vulnerability and over 65 bugs reported
over the last several months.

If you use GIN indexes, you may need to reindex after updating to this release.
Please see the [release notes](https://www.postgresql.org/docs/release/) for
more information.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Security Issues
---------------

### [CVE-2024-0985](https://www.postgresql.org/support/security/CVE-2024-0985/): PostgreSQL non-owner `REFRESH MATERIALIZED VIEW CONCURRENTLY` executes arbitrary SQL

CVSS v3 Base Score: [8.0](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:H/A:H)

Supported, Vulnerable Versions: 12 - 15.

One step of a concurrent refresh command was run under weak security
restrictions. If a materialized view's owner could persuade a superuser or
other high-privileged user to perform a concurrent refresh on that view,
the view's owner could control code executed with the privileges of the user
running `REFRESH`. The fix for the vulnerability makes is so that all
user-determined code is run as the view's owner, as expected.

The PostgreSQL project thanks Pedro Gallegos for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 65 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 16. Some of these issues may also
affect other supported versions of PostgreSQL.

* Fix memory leak when performing JIT inlining that could lead to out-of-memory
conditions.
* Several query planner fixes.
* Align `MERGE` behavior with `UPDATE` when updating a partition key column and
skip firing `AFTER UPDATE ROW` trigger and other post-update actions.
* Fix problems with duplicate token names in `ALTER TEXT SEARCH CONFIGURATION ... MAPPING`
commands.
* Fix `DROP ROLE` with duplicate role names.
* Properly lock the associated table during DROP STATISTICS to prevent errors if
`ANALYZE` is running concurrently.
* Fix function volatility checking for `GENERATED` and `DEFAULT` expressions.
* Ensure collation matches when matching an existing index to a new partitioned
index.
* Avoid failure if a child index is dropped concurrently with `REINDEX INDEX` on a
partitioned index.
* Fix for locking during cleanup of GIN indexes. For this case, if multiple
processes tried to clean the same GIN index page, there was a chance of index
corruption. If you believe you were affected by this issue, reindex your GIN
indexes after installing this update.
* Avoid failure with partitioned SP-GiST indexes.
* Several ownership fixes for large objects.
* In `EXPLAIN (BUFFERS)`, change name of I/O timing data "shared/local" to
"shared".
* Ensure durability of the `CREATE DATABASE` command if a system crash occurred
during or shortly after execution.
* Add more logging messages when starting and ending recovery from a backup.
* Revert a change that made the walreceiver process unresponsive to `SIGTERM`
while waiting for a replication connection to be established.
* Several fixes for logical replication.
* Fix incompatibility with OpenSSL 3.2.
* Fix PL/pgSQL to allow `CREATE FUNCTION`/`CREATE PROCEDURE` SQL commands that
use SQL-standard function bodies.
* Fix for error handling in [libpq pipeline mode](https://www.postgresql.org/docs/current/libpq-pipeline-mode.html).
* Ensure `initdb` always uncomments `postgresql.conf` entries for the
`lc_` family of parameters.
* In `pg_dump`, don't dump RLS policies or security labels for extension member
objects.

This release also updates time zone data files to tzdata release 2024a for DST
law changes in Greenland, Kazakhstan, and Palestine, plus corrections for the
Antarctic stations Casey and Vostok. Also historical corrections for Vietnam,
Toronto, and Miquelon.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

If you use GIN indexes, you may need to reindex after updating to this release.
Please see the [release notes](https://www.postgresql.org/docs/release/) for
more information.

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [PostgreSQL 16 Release Announcement](https://www.postgresql.org/about/press/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
