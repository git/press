2015-06-12 Update Release
=========================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.4.4, 9.3.9, 9.2.13, 9.1.18 and 9.0.22.  This release primarily fixes issues not successfully fixed in prior releases. It should be applied as soon as possible by any user who installed the May or June update releases. Other users should apply at the next available downtime.

Crash Recovery Fixes
---------------------

Earlier update releases attempted to fix an issue in PostgreSQL 9.3 and 9.4 with "multixact wraparound", but failed to account for issues doing multixact cleanup during crash recovery.  This could cause servers to be unable to restart after a crash.  As such, all users of 9.3 and 9.4 should apply this update as soon as possible.

Servers previously upgraded to PostgreSQL 9.3 using pg_upgrade, even those servers now running PostgreSQL 9.4 due to another upgrade, may experience
an immediate autovacuum of all tables after applying this update.  For large databases, consider a controlled manual VACUUM, before updating, to better
regulate the performance consequences of this critical maintenance.  Please see the [release notes](http://www.postgresql.org/docs/9.4/static/release-9-3-9.html) for details.

Other Fixes and Improvements
----------------------------

In addition to the above, a few other minor issues were patched in this release.  These fixes include:

* Prevent failure to invalidate relation cache init file
* Avoid deadlock between new sessions and CREATE/DROP DATABASE
* Improve query planning for semi-joins and anti-joins

Cumulative Releases
-------------------

All PostgreSQL update releases are cumulative.  As this update release fixes a number of problems inadvertently introduced by fixes in earlier update releases, we strongly urge users to apply this update, rather than installing less recent updates that have known issues.  As this update release closes multiple known bugs with multixact handling, the PostgreSQL Project does not anticipate additional update releases soon.

Updating
--------

As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries. Users who have skipped multiple update releases may need to perform additional post-update steps; see the Release Notes for details.  See also the above note for users who used pg_upgrade with PostgreSQL version 9.3.

Links:
  * [Download](http://www.postgresql.org/download)
  * [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
