The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 16.1, 15.5, 14.10, 13.13, 12.17, and 11.22
This release fixes three security vulnerabilities and over 55 bugs reported
over the last several months.

This release includes fixes for indexes where in certain cases, we advise
reindexing. Please see the "Updating" section for more details.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 11 EOL Notice
------------------------

**This is the final release of PostgreSQL 11**. PostgreSQL 11 is now end-of-life
and will no longer receive security and bug fixes. If you are
running PostgreSQL 11 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2023-5868](https://www.postgresql.org/support/security/CVE-2023-5868/): Memory disclosure in aggregate function calls

CVSS v3 Base Score: [4.3](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N)

Supported, Vulnerable Versions: 11 - 16.  The security team typically does not
test unsupported versions, but this problem is quite old.

Certain aggregate function calls receiving "unknown"-type arguments could
disclose bytes of server memory from the end of the "unknown"-type value to the
next zero byte. One typically gets an "unknown"-type value via a string literal
having no type designation. We have not confirmed or ruled out viability of
attacks that arrange for presence of notable, confidential information in 
disclosed bytes.

The PostgreSQL project thanks Jingzhou Fu for reporting this problem.

### [CVE-2023-5869](https://www.postgresql.org/support/security/CVE-2023-5869/): Buffer overrun from integer overflow in array modification

CVSS v3 Base Score: [8.8](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H)

Supported, Vulnerable Versions: 11 - 16.  The security team typically does not
test unsupported versions, but this problem is quite old.

While modifying certain SQL array values, missing overflow checks let
authenticated database users write arbitrary bytes to a memory area that
facilitates arbitrary code execution. Missing overflow checks also let
authenticated database users read a wide area of server memory. The
[CVE-2021-32027](https://www.postgresql.org/support/security/CVE-2021-32027/)
fix covered some attacks of this description, but it missed others.

The PostgreSQL project thanks Pedro Gallegos for reporting this problem.

### [CVE-2023-5870](https://www.postgresql.org/support/security/CVE-2023-5870/): Role `pg_cancel_backend` can signal certain superuser processes

CVSS v3 Base Score: [2.2](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:H/PR:H/UI:N/S:U/C:N/I:N/A:L)

Supported, Vulnerable Versions: 11 - 16.  The security team typically does not
test unsupported versions, but this problem is quite old.

Documentation says the `pg_cancel_backend` role cannot signal "a backend owned
by a superuser". On the contrary, it can signal background workers, including
the logical replication launcher. It can signal `autovacuum` workers and the
`autovacuum` launcher. Signaling `autovacuum` workers and those two launchers
provides no meaningful exploit, so exploiting this vulnerability requires a
non-core extension with a less-resilient background worker. For example, a
non-core background worker that does not auto-restart would experience a
denial of service with respect to that particular background worker.

The PostgreSQL project thanks Hemanth Sandrana and Mahendrakar Srinivasarao
for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 55 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 16. Some of these issues may also
affect other supported versions of PostgreSQL.

* Fix issue where GiST indexes had an incorrect behavior during a "page split"
operation that could lead to incorrect results in subsequent index searches.
Please [reindex](https://www.postgresql.org/docs/current/sql-reindex.html) GiST
indexes after installing this update.
* Fix issue where B-tree indexes would incorrectly de-duplicate `interval`
columns. Please [reindex](https://www.postgresql.org/docs/current/sql-reindex.html)
any B-tree index that includes an `interval` column after installing this
update.
* Provide more efficient indexing of `date`, `timestamptz`, and `timestamp`
values in BRIN indexes when using a [`minmax_multi` opsclass](https://www.postgresql.org/docs/current/brin-builtin-opclasses.html).
While not required, we recommend
[reindexing](https://www.postgresql.org/docs/current/sql-reindex.html) BRIN
indexes that include these data types after installing this update.
* Fix for bulk table insertion into partitioned tables.
* Fix for hash-partitioned tables with multiple partition keys during step
generation and runtime pruning that could lead to crashes in some cases.
* Throw the correct error if [`pgrowlocks()`](https://www.postgresql.org/docs/current/pgrowlocks.html) is applied to a partitioned table

* Fix inconsistent rechecking of concurrently-updated rows during
[`MERGE`](https://www.postgresql.org/docs/current/sql-merge.html) when using
[`READ COMMITTED`](https://www.postgresql.org/docs/current/transaction-iso.html#XACT-READ-COMMITTED)
mode.
* Correctly identify the target table in an inherited `UPDATE`/`DELETE`/`MERGE`
even when the parent table is excluded by constraints.
* Fix over-allocation of a constructed [`tsvector`](https://www.postgresql.org/docs/current/datatype-textsearch.html#DATATYPE-TSVECTOR).
* Fix [`ALTER SUBSCRIPTION`](https://www.postgresql.org/docs/current/sql-altersubscription.html)
to apply changes in the `run_as_owner` option.
* Several fixes for [`COPY FROM`](https://www.postgresql.org/docs/current/sql-copy.html),
* Several fixes for handling torn reads with [`pg_control`](https://www.postgresql.org/docs/current/wal-internals.html).
* Fix "could not find pathkey item to sort" errors occurring while planning
aggregate functions with `ORDER BY` or `DISTINCT` options.
* When [`track_io_timing`](https://www.postgresql.org/docs/current/runtime-config-statistics.html#GUC-TRACK-IO-TIMING)
is enabled, include the time taken by relation extension operations as write
time.
* Track the dependencies of cached [`CALL`](https://www.postgresql.org/docs/current/sql-call.html)
statements, and re-plan them when needed.
* Treat out-of-memory failures as `FATAL` while reading WAL.
* Fix [`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html) to
dump the new `run_as_owner` option of subscriptions.
* Fix [`pg_restore`](https://www.postgresql.org/docs/current/app-pgrestore.html)
so that selective restores will include both table-level and column-level ACLs
for selected tables.
* Add logic to [`pg_upgrade`](https://www.postgresql.org/docs/current/pgupgrade.html)
to check for use of obsolete data types `abstime`, `reltime`, and `tinterval`.
* Fix [`vacuumdb`](https://www.postgresql.org/docs/current/app-vacuumdb.html)
to have multiple `-N` switches actually exclude tables in multiple schemas.
* [`amcheck`](https://www.postgresql.org/docs/current/amcheck.html)
will no longer report interrupted page deletion as corruption.
* Fix `btree_gin` indexes on `interval` columns to properly return data when
using the `<` and `<=` operators.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

We recommend [reindexing](https://www.postgresql.org/docs/current/sql-reindex.html)
certain types of indexes after you apply this update, including:

* GiST indexes
* B-tree indexes that use the `interval` data type
* BRIN indexes that use the `date`, `timestamptz`, and `timestamp`
data types and a
[`minmax_multi` opsclass](https://www.postgresql.org/docs/current/brin-builtin-opclasses.html)

On PostgreSQL 12 and above, you can use
[`REINDEX CONCURRENTLY`](https://www.postgresql.org/docs/current/sql-reindex.html)
to avoid blocking writes to the affected index and table, for example:

```
REINDEX INDEX CONCURRENTLY your_index_name;
```

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [PostgreSQL 16 Release Announcement](https://www.postgresql.org/about/press/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
