The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 17.1, 16.5, 15.9, 14.14, 13.17, and 12.21.
This release fixes 4 security vulnerabilities and over 35 bugs reported over the
last several months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 12 EOL Notice
------------------------

**This is the final release of PostgreSQL 12**. PostgreSQL 12 is now end-of-life
and will no longer receive security and bug fixes. If you are
running PostgreSQL 12 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2024-10976](https://www.postgresql.org/support/security/CVE-2024-10976/): PostgreSQL row security below e.g. subqueries disregards user ID changes

CVSS v3.1 Base Score: [4.2](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?version=3.1&vector=AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N)

Supported, Vulnerable Versions: 12 - 17.

Incomplete tracking in PostgreSQL of tables with row security allows a reused
query to view or change different rows from those intended.
[CVE-2023-2455](https://www.postgresql.org/support/security/CVE-2023-2455/) and
[CVE-2016-2193](https://www.postgresql.org/support/security/CVE-2016-2193/)
fixed most interaction between row security and user ID changes. 
They missed cases where a subquery, WITH query, security invoker view, or
SQL-language function references a table with a row-level security policy. 
This has the same consequences as the two earlier CVEs. 
That is to say, it leads to potentially incorrect policies being applied in
cases where role-specific policies are used and a given query is planned under
one role and then executed under other roles. This scenario can happen under
security definer functions or when a common user and query is planned initially
and then re-used across multiple SET ROLEs.

Applying an incorrect policy may permit a user to complete otherwise-forbidden
reads and modifications. This affects only databases that have used
[`CREATE POLICY`](https://www.postgresql.org/docs/current/sql-createpolicy.html)
to define a row security policy. An attacker must tailor an attack to a
particular application's pattern of query plan reuse, user ID changes, and
role-specific row security policies. Versions before
PostgreSQL 17.1, 16.5, 15.9, 14.14, 13.17, and 12.21 are affected.

The PostgreSQL project thanks Wolfgang Walther for reporting this problem.

### [CVE-2024-10977](https://www.postgresql.org/support/security/CVE-2024-10977/): PostgreSQL libpq retains an error message from man-in-the-middle

CVSS v3.1 Base Score: [3.1](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?version=3.1&vector=AV:N/AC:H/PR:N/UI:R/S:U/C:N/I:L/A:N)

Supported, Vulnerable Versions: 12 - 17.

Client use of server error message in PostgreSQL allows a server not trusted
under current SSL or GSS settings to furnish arbitrary non-NUL bytes to the
libpq application. For example, a man-in-the-middle attacker could send a long
error message that a human or screen-scraper user of psql mistakes for valid
query results. This is probably not a concern for clients where the user
interface unambiguously indicates the boundary between one error message and
other text. Versions before PostgreSQL 17.1, 16.5, 15.9, 14.14, 13.17, and 12.21
are affected.

The PostgreSQL project thanks Jacob Champion for reporting this problem.

### [CVE-2024-10978](https://www.postgresql.org/support/security/CVE-2024-10978/): PostgreSQL SET ROLE, SET SESSION AUTHORIZATION reset to wrong user ID

CVSS v3.1 Base Score: [4.2](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?version=3.1&vector=AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N)

Supported, Vulnerable Versions: 12 - 17.

Incorrect privilege assignment in PostgreSQL allows a less-privileged
application user to view or change different rows from those intended. An attack
requires the application to use
[`SET ROLE`](https://www.postgresql.org/docs/current/sql-set-role.html),
[`SET SESSION AUTHORIZATION`](https://www.postgresql.org/docs/current/sql-set-session-authorization.html),
or an equivalent feature. The problem arises when an application query uses
parameters from the attacker or conveys query results to the attacker. If that
query reacts to `current_setting('role')` or the current user ID, it may modify
or return data as though the session had not used `SET ROLE` or
`SET SESSION AUTHORIZATION`.  The attacker does not control which incorrect user
ID applies. Query text from less-privileged sources is not a concern here,
because `SET ROLE` and `SET SESSION AUTHORIZATION` are not sandboxes for unvetted
queries. Versions before PostgreSQL 17.1, 16.5, 15.9, 14.14, 13.17, and 12.21
are affected.

The PostgreSQL project thanks Tom Lane for reporting this problem.

### [CVE-2024-10979](https://www.postgresql.org/support/security/CVE-2024-10979/): PostgreSQL PL/Perl environment variable changes execute arbitrary code

CVSS v3.1 Base Score: [8.8](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?version=3.1&vector=AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H)

Supported, Vulnerable Versions: 12 - 17.

Incorrect control of environment variables in PostgreSQL
[PL/Perl](https://www.postgresql.org/docs/current/plperl.html) allows an
unprivileged database user to change sensitive process environment variables
(e.g. `PATH`). That often suffices to enable arbitrary code execution, even if
the attacker lacks a database server operating system user. Versions before
PostgreSQL 17.1, 16.5, 15.9, 14.14, 13.17, and 12.21 are affected.

The PostgreSQL project thanks Coby Abrams for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 35 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 17. Some of these issues may also
affect other supported versions of PostgreSQL.

* Fix when attaching or detaching table partitions with foreign key constraints.
After upgrade, users impacted by this issue will need to perform manual steps to
finish fixing it. Please see the "Upgrading" section and the release notes for
more information.
* Fix when using libc as the default collation provider when `LC_CTYPE` is `C`
while `LC_COLLATE` is a different locale. This could lead to incorrect query
results. If you have these settings in your database, please reindex any
affected indexes after updating to this release. This issue impacted 17.0 only.
* Several query planner fixes, including disallowing joining partitions
([partitionwise join](https://www.postgresql.org/docs/current/runtime-config-query.html#GUC-ENABLE-PARTITIONWISE-JOIN))
if the collations of the partitions don't match.
* Fix possible wrong answers or `wrong varnullingrels` planner errors for
[`MERGE ... WHEN NOT MATCHED BY SOURCE`](https://www.postgresql.org/docs/current/sql-merge.html)
actions.
* Fix validation of the [`COPY`](https://www.postgresql.org/docs/current/sql-copy.html)
`FORCE_NOT_NULL` and `FORCE_NULL`.
* Fix server crash when a [`json_objectagg()`](https://www.postgresql.org/docs/current/functions-aggregate.html)
call contains a volatile function.
* Ensure there's a registered dependency between a partitioned table and a
non-built-in access method specified in `CREATE TABLE ... USING`. This fix only
prevents problems for partitioned tables created after this update.
* Fix race condition in committing a serializable transaction.
* Fix race condition in [`COMMIT PREPARED`](https://www.postgresql.org/docs/current/sql-commit-prepared.html)
that could require manual file removal after a crash-and-recovery.
* Fix for [`pg_cursors`](https://www.postgresql.org/docs/current/view-pg-cursors.html)
view to prevent errors by excluding cursors that aren't completely set up.
* Reduce logical decoding memory consumption.
* Fix to prevent stable functions from receiving stale row values when they're
called from a [`CALL`](https://www.postgresql.org/docs/current/sql-call.html)
statement's argument list and the `CALL` is within a
[PL/pgSQL `EXCEPTION`](https://www.postgresql.org/docs/current/plpgsql-control-structures.html#PLPGSQL-ERROR-TRAPPING)
block.
* Fix for JIT crashes on ARM (aarch64) systems.
* The `psql` `\watch` now treats values that are less than 1ms to be  0
(no wait between executions).
* Fix failure to use credentials for a replication user in the
[password file](https://www.postgresql.org/docs/current/libpq-pgpass.html)
([`pgpass`](https://www.postgresql.org/docs/current/libpq-pgpass.html))
* [`pg_combinebackup`](https://www.postgresql.org/docs/current/app-pgcombinebackup.html)
now throws an error if an incremental backup file is present in a directory
that should contain a full backup.
* Fix to avoid reindexing temporary tables and indexes in
[`vacuumdb`](https://www.postgresql.org/docs/current/app-vacuumdb.html) and
parallel [`reindexdb`](https://www.postgresql.org/docs/current/app-reindexdb.html).

This release also updates time zone data files to tzdata release 2024b. This
tzdata release changes the old System-V-compatibility zone names to duplicate
the corresponding geographic zones; for example `PST8PDT` is now an alias for
`America/Los_Angeles`. The main visible consequence is that for timestamps
before the introduction of standardized time zones, the zone is considered to
represent local mean solar time for the named location. For example, in
`PST8PDT`, timestamptz input such as 1801-01-01 00:00 would previously have been
rendered as `1801-01-01 00:00:00-08`, but now it is rendered as
`1801-01-01 00:00:00-07:52:58`.

Also, historical corrections for Mexico, Mongolia, and Portugal. Notably,
Asia/Choibalsan is now an alias for Asia/Ulaanbaatar rather than being a
separate zone, mainly because the differences between those zones were found to
be based on untrustworthy data. 

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

If you have a partitioned table with foreign key constraints where you've run
the `ATTACH PARTITION`/`DETACH PARTITION` commands, you will need to take
further steps after upgrading. You can fix this by executing an
[`ALTER TABLE ... DROP CONSTRAINT`](https://www.postgresql.org/docs/current/sql-altertable.html)
on the now stand-alone table for each faulty constraint, and then re-add the
constraint. If re-adding the constraint fails, you will need to manually
re-establish consistency between the referencing and referenced tables, then
re-add the constraint.

This query can be used to identify broken constraints and construct the commands
needed to recreate them:

```
SELECT conrelid::pg_catalog.regclass AS "constrained table",
      conname AS constraint,
      confrelid::pg_catalog.regclass AS "references",
      pg_catalog.format('ALTER TABLE %s DROP CONSTRAINT %I;',
                        conrelid::pg_catalog.regclass, conname) AS "drop",
      pg_catalog.format('ALTER TABLE %s ADD CONSTRAINT %I %s;',
                        conrelid::pg_catalog.regclass, conname,
                        pg_catalog.pg_get_constraintdef(oid)) AS "add"
FROM pg_catalog.pg_constraint c
WHERE contype = 'f' AND conparentid = 0 AND
  (SELECT count(*) FROM pg_catalog.pg_constraint c2
  WHERE c2.conparentid = c.oid) <>
  (SELECT count(*) FROM pg_catalog.pg_inherits i
  WHERE (i.inhparent = c.conrelid OR i.inhparent = c.confrelid) AND
    EXISTS (SELECT 1 FROM pg_catalog.pg_partitioned_table
            WHERE partrelid = i.inhparent));
```

Since it is possible that one or more of the `ADD CONSTRAINT` steps will fail,
you should save the query's output in a file and then attempt to perform each
step.

Additionally, if you are running PostgreSQL 17.0 and using libc as your default
collation provider, and have set `LC_CTYPE` to be `C` while `LC_COLLATE` is a
different locale, you will need to rebuild your text-based indexes. You can do
this with the
[`REINDEX INDEX CONCURRENTLY`](https://www.postgresql.org/docs/current/sql-reindex.html)
command.

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on X/Twitter](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
