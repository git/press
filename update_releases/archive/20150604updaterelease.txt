2015-06-04 Update Release
=========================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.4.3, 9.3.8, 9.2.12, 9.1.17 and 9.0.21.  This release primarily fixes a startup failure problem created by the prior update release, and should be installed by all users who applied that release.

File Permissions Fix
---------------------

The 2015-05-22 update release added an anti-corruption step which will fsync all files in the data directory on restart after a crash. This caused PostgreSQL to fail to start if it encountered any file permissions issues.  The problem is now fixed. Find out more on the PostgreSQL wiki: https://wiki.postgresql.org/wiki/May_2015_Fsync_Permissions_Bug.


Other Fixes and Improvements
----------------------------
In addition to the above, a few other minor issues were patched in this release.  These fixes include:

* Have pg_get_functiondef() show the LEAKPROOF property
* Make pushJsonbValue() function push jbvBinary type
* Allow building with threaded Python on OpenBSD

Update Release Schedule
------------------------

The PostgreSQL developers are currently working hard to fix a few known issues discovered in the past couple of weeks.  As such, we expect to release another update once a solution for those issues is complete and tested. The PostgreSQL project apologizes for any inconvenience from the frequent releases. We are prioritizing getting important fixes to our users as soon as possible.

Updating
--------

As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries. Users who have skipped multiple update releases may need to perform additional post-update steps; see the Release Notes for details.

Links:
  * Download: http://www.postgresql.org/download
  * Release Notes: http://www.postgresql.org/docs/current/static/release.html

