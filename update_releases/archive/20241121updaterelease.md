The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 17.2, 16.6, 15.10, 14.15, and 13.18.
Additionally, due to the nature of one of the issues in the
[previous update release](https://www.postgresql.org/about/news/out-of-cycle-release-scheduled-for-november-21-2024-2958/),
the PostgreSQL Global Development Group is also releasing a 12.22 release for
PostgreSQL 12. PostgreSQL 12 is now EOL and will not receive more fixes.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 12 EOL Notice
------------------------

**This is the final release of PostgreSQL 12**. PostgreSQL 12 is now end-of-life
and will no longer receive security and bug fixes. If you are
running PostgreSQL 12 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Bug Fixes and Improvements
--------------------------
 
The issues listed below affect PostgreSQL 17. Some of these issues may also
affect other supported versions of PostgreSQL.

* Restore functionality of [`ALTER ROLE .. SET ROLE`](https://www.postgresql.org/docs/current/sql-alterrole.html)
and [`ALTER DATABASE .. SET ROLE`](https://www.postgresql.org/docs/current/sql-alterdatabase.html).
The fix for [CVE-2024-10978](https://www.postgresql.org/support/security/CVE-2024-10978/)
accidentally caused settings for role to not be applied if they came from
non-interactive sources, including previous `ALTER {ROLE|DATABASE}` commands and
the [`PGOPTIONS`](https://www.postgresql.org/docs/current/libpq-envars.html)
environment variable.
* Restore compatibility for the `timescaledb` and other PostgreSQL extensions
built using PostgreSQL prior to the 2024-11-14 release
(17.0, 16.4, 15.8, 14.13, 13.16, 12.20, and earlier). This fix restores
`struct ResultRelInfo` to its previous size, so that affected extensions don't
need to be rebuilt.
* Fix cases where a logical replication slot's `restart_lsn` could go backwards.
* Avoid deleting still-needed WAL files during
[`pg_rewind`](https://www.postgresql.org/docs/current/app-pgrewind.html).
* Fix race conditions associated with dropping shared statistics entries, which
could lead to loss of statistics data.
* Fix crash with `ALTER TABLE` when checking to see if an index's opclass
options have changed if the table has an index with a non-default operator
class.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on X/Twitter](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
