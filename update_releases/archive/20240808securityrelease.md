The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 16.4, 15.8, 14.13, 13.16, and 12.20, as well
as the third beta release of PostgreSQL 17. This release fixes 1 security
vulnerability and over 55 bugs reported over the last several months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 12 EOL Notice
------------------------

PostgreSQL 12 will stop receiving fixes on November 14, 2024. If you are
running PostgreSQL 12 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2024-7348](https://www.postgresql.org/support/security/CVE-2024-7348/): PostgreSQL relation replacement during pg_dump executes arbitrary SQL

CVSS v3.1 Base Score: [8.8](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H)

Supported, Vulnerable Versions: 12 - 16.

An attacker able to create and drop non-temporary objects could inject SQL code
that would be executed by a concurrent [`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html)
session with the privileges of the role running `pg_dump` (which is often a
superuser). The attack involves replacing a sequence or similar object with a
view or foreign table that will execute malicious code. To prevent this,
introduce a new server parameter `restrict_nonsystem_relation_kind` that can
disable expansion of non-builtin views as well as access to foreign tables, and
teach `pg_dump` to set it when available. Note that the attack is prevented only
if both `pg_dump` and the server it is dumping from are new enough to have this
fix.

The PostgreSQL project thanks Noah Misch for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 55 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 16. Some of these issues may also
affect other supported versions of PostgreSQL.

* Avoid incorrect results from "Merge Right Anti Join" plans, where if the inner relation is known to have unique join keys, the merge could misbehave when there are duplicated join keys in the outer relation.
* Prevent infinite loop in [`VACUUM`](https://www.postgresql.org/docs/current/sql-vacuum.html).
* Fix partition pruning setup during [`ALTER TABLE DETACH ... PARTITION CONCURRENTLY`](https://www.postgresql.org/docs/current/sql-altertable.html).
* Fix behavior of stable functions that are used as an argument to a [`CALL`](https://www.postgresql.org/docs/current/sql-call.html) statement.
* `pg_sequence_last_value()` now returns `NULL` instead of throwing an error when called on unlogged sequences on standby servers and on temporary sequences of other sessions.
* Fix parsing of ignored operators in `websearch_to_tsquery()`.
* Correctly check updatability of view columns targeted by [`INSERT ... DEFAULT`](https://www.postgresql.org/docs/current/sql-insert.html).
* Lock owned sequences during [`ALTER TABLE ... SET LOGGED|UNLOGGED`](https://www.postgresql.org/docs/current/sql-altertable.html).
* Don't throw an error if a queued `AFTER` trigger no longer exists.
* Fix selection of an arbiter index for [`INSERT ... ON CONFLICT`](https://www.postgresql.org/docs/current/sql-insert.html) when the desired index has expressions or predicates, for example, through an updatable view.
* Refuse to modify a temporary table of another session with [`ALTER TABLE`](https://www.postgresql.org/docs/current/sql-altertable.html).
* Fix handling of extended statistics on expressions in [`CREATE TABLE ... LIKE STATISTICS`](https://www.postgresql.org/docs/current/sql-createtable.html).
* Fix failure to recalculate sub-queries generated from `MIN()` or `MAX()` aggregates.
* Disallow underscores in positional parameters.
* Avoid crashing when a JIT-inlined backend function throws an error.
* Fix handling of subtransactions of prepared transactions when starting a hot standby server.
* Prevent incorrect initialization of logical replication slots.
* Fix memory leak in the logical replication WAL sender when publishing changes to a partitioned table whose partitions have row types that are physically different from the table.
* Disable creation of stateful TLS session tickets by OpenSSL.
* Fix how [PL/pgSQL](https://www.postgresql.org/docs/current/plpgsql.html) handles integer ranges containing underscores (e.g., `FOR i IN 1_001..1_002`).
* Fix incompatibility between [PL/Perl](https://www.postgresql.org/docs/current/plperl.html) and Perl 5.40.
* Several fixes related to recursive [PL/Python](https://www.postgresql.org/docs/current/plpython.html) functions and triggers.
* Ensure that [`pg_restore -l`](https://www.postgresql.org/docs/current/app-pgrestore.html) reports dependent table of contents entries correctly.
* [`pg_stat_statements`](https://www.postgresql.org/docs/current/pgstatstatements.html) now passes a query ID for utility (non-`SELECT`/`INSERT`/`UPDATE`) statements that appears in SQL-language functions.
* Fix for [`postgres_fdw`](https://www.postgresql.org/docs/current/postgres-fdw.html) when mapping a foreign table to a nontrivial remote view.
* [`postgres_fdw`](https://www.postgresql.org/docs/current/postgres-fdw.html) no longer sends a `FETCH FIRST WITH TIES` clause to a remote server.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

A Note on the PostgreSQL 17 Beta
--------------------------------

This release marks the third beta release of PostgreSQL 17 and puts the
community one step closer to general availability tentatively around the end of
the third quarter.

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 17 on your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 17 Beta 3 in production environments, we encourage you to find ways
to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 17
release upholds our standards of delivering a stable, reliable release of the
world's most advanced open source relational database. Please read more about
our [beta testing process](https://www.postgresql.org/developer/beta/) and how
you can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

Upgrading to PostgreSQL 17 Beta 3
---------------------------------

To upgrade to PostgreSQL 17 Beta 3 from an earlier version of PostgreSQL,
you will need to use a strategy similar to upgrading between major versions of
PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`). For more
information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/17/static/upgrading.html).

Changes Since Beta 2
--------------------

Fixes and changes in PostgreSQL 17 Beta 3 include:

* Rename the `standby_slot_names` parameter to to `synchronized_standby_slots`.
* Several SQL/JSON fixes.
* Fix `pg_combinebackup --clone`.
* Fix `pg_createsubscriber` to work for database names that contain a space.
* `pg_createsubscriber` now drops pre-existing subscriptions when run on a
target database.
* Improve efficiency in retrieving subscription information during `pg_upgrade`.
* Fix TLS fallback behavior during `sslmode=prefer` to error when a server sends
an error during the startup process.
* Document an error case with `pg_basebackup` incremental backup on a standby
server when it's executed immediately after the previous backup.
* Fix issue where `pg_upgrade --transaction-size` can cause the backend to use
an order of magnitude more RAM.

Please see the [release notes](https://www.postgresql.org/docs/17/release-17.html)
for a complete list of new and changed features, and
[PostgreSQL 17 open items](https://wiki.postgresql.org/wiki/PostgreSQL_17_Open_Items)
for more details on fixes and changes.

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 17. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_17_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 17 Beta Release Notes](https://www.postgresql.org/docs/17/release-17.html)
* [PostgreSQL 17 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_17_Open_Items)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
* [Follow @postgresql on X/Twitter](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
