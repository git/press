2017-02-09 Cumulative Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.6.2, 9.5.6, 9.4.11, 9.3.16, and 9.2.20. This release includes fixes that prevent data corruption issues in index builds and in certain write-ahead-log replay situations, which are detailed below.  It also patches over 75 other bugs reported over the last three months.

Users should plan to apply this update at the next scheduled downtime.

Build corruption with CREATE INDEX CONCURRENTLY
----------------------------------

There existed a race condition if CREATE INDEX CONCURRENTLY was called on a column that had not been indexed before, then rows that were updated by transactions running at the same time as the CREATE INDEX CONCURRENTLY command could have been indexed incorrectly.

If you suspect this may have happened, the most reliable solution is to rebuild affected indexes after installing this update.

This issue is present in the 9.2, 9.3, 9.4, 9.5, and 9.6 series of PostgreSQL.

Fixes for visibility and write-ahead-log stability
----------------------------------

These release contains several fixes to improve the stability of visible data and WAL logging that we wish to highlight here.

Prior to this release, data could be prematurely pruned by a vacuum operation when a special snapshot used for catalog scans was presently available.  Specifically, the vacuum operation would not be aware of the oldest xmin for this special snapshot. The error would surface with a message such as:

    "cache lookup failed for relation 1255"

This release ensures that vacuum operations will account for the catalog scan snapshots.

Additionally, there are several fixes to improved the stability of write-ahead-logging, including:

    * A fix for BRIN index WAL logging where a replay could make a portion of the BRIN index useless and require recomputing
    * A fix for an unlogged table where a WAL-log would be created under the "wal_level = minimal" setting, and upon replay after crash, the table would not appear to be properly reset
    * A fix in the WAL page header validation when re-reading segments that fixes the error of  “out-of-sequence TLI” that could be reported during recovery

Theses issues are present in the 9.6 series of PostgreSQL releases and may also be present in the 9.2, 9.3, 9.4, and 9.5 series.

Bug Fixes and Improvements
--------------------------

This update also fixes a number of bugs reported in the last few months. Some of these issues affect only the 9.6 series, but many affect all supported versions.  There are more than 75 fixes provided in this release, including:

* Several fixes for operating in hot standby mode
* Disallow setting the num_sync field to zero in synchronous_standby_names
* Don't count background worker processes against a user's connection limit
* Fix check for when an extension member object can be dropped
* Fix tracking of initial privileges for extension member objects so that it works correctly with ALTER EXTENSION ... ADD/DROP
* Several vacuum and autovacuum fixes
* Fix CREATE OR REPLACE VIEW to update the view query before attempting to apply the new view options
* Make sure ALTER TABLE preserves index tablespace assignments when rebuilding indexes
* Several fixes to the query planner, including fixes for foreign tables and CTEs
* Several fixes around the full-text search feature to improve accuracy and performance of the search
* Several fixes and performance improvements in several array functions
* Several fixes around the interaction of foreign-key constraints with trigger functions around specific ALTER TABLE operations
* Remove optimizations for date/time data types that returned incorrect data
* Fix incorrect use of view reloptions as regular table reloptions
* Fix incorrect “target lists can have at most N entries” complaint when using ON CONFLICT with wide tables
* Fix spurious “query provides a value for a dropped column” errors during INSERT or UPDATE on a table with a dropped column
* Prevent multicolumn expansion of foo.* in an UPDATE source expression
* Ensure that column typmods are determined accurately for multi-row VALUES constructs
* Several fixes for the psql command-line tool
* Prevent multiple calls of pg_start_backup() and pg_stop_backup() from running concurrently
* Several fixes for pg_dump, pg_restore, and pg_basebackup, including a possible pg_basebackup failure on standby server when including WAL files
* Several fixes for parallel workers and plans for parallel queries including fixing a crash if the number of workers available to a parallel query decreases during a rescan
* Several fixes to PL/pgSQL, PL/Python, and PL/Tcl
* Several fixes for contrib modules
* Allow DOS-style line endings in ~/.pgpass files, even on Unix

Update time zone data files to tzdata release 2016j for DST law changes in northern Cyprus (adding a new zone Asia/Famagusta), Russia (adding a new zone Europe/Saratov), Tonga, and Antarctica/Casey. Historical corrections for Italy, Kazakhstan, Malta, and Palestine. Switch to preferring numeric zone abbreviations for Tonga.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.

If you believe you have been affected by the aforementioned CREATE INDEX CONCURRENTLY bug, you will have to rebuild the index.  Here is an example of how to rebuild an index in a production system without losing the ability to utilize the index:

    CREATE INDEX CONCURRENTLY new_index_name ON table_name (column_name);
    DROP INDEX CONCURRENTLY old_index_name;
    ALTER INDEX new_index_name RENAME TO old_index_name;

Note that using this method means you will briefly have two copies of the same index, so if disk space is an issue you may need to take other approaches.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Links:
* [Download](http://www.postgresql.org/download)
* [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
* [Security Page](http://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
