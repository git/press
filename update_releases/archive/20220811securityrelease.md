The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 14.5, 13.8, 12.12, 11.17, and 10.22, as well
as the third beta release of PostgreSQL 15. This
release closes one security vulnerability and fixes over 40 bugs reported over
the last three months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 10 EOL Upcoming
--------------------------

PostgreSQL 10 will stop receiving fixes on November 10, 2022. If you are
running PostgreSQL 10 in a production environment, we strongly advise that you
make plans to upgrade to a newer, supported version of PostgreSQL so you can
continue to receive bug and security fixes. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2022-2625](https://www.postgresql.org/support/security/CVE-2022-2625/): Extension scripts replace objects not belonging to the extension.

Versions Affected: 10 - 14. The security team typically does not test
unsupported versions, but this problem is quite old.

Some extensions use `CREATE OR REPLACE` or `CREATE IF NOT EXISTS` commands.
However, some don't adhere to the documented rule to target only objects known
to be extension members already. An attack requires permission to create
non-temporary objects in at least one schema, ability to lure or wait for an
administrator to create or update an affected extension in that schema, and
ability to lure or wait for a victim to use the object targeted in
`CREATE OR REPLACE` or `CREATE IF NOT EXISTS`.

Given all three prerequisites, the attacker can run arbitrary code as the victim
role, which may be a superuser. Known-affected extensions include both
PostgreSQL-bundled and non-bundled extensions. PostgreSQL blocks this attack in
the core server, so there's no need to modify individual extensions.

The PostgreSQL project thanks Sven Klemm for reporting this problem.

A Note on the PostgreSQL 15 Beta
--------------------------------

This release marks the third beta release of PostgreSQL 15 and puts the
community one step closer to general availability tentatively around the end of
the third quarter.

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 15 on your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 15 Beta 3 in production environments, we encourage you to find ways
to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that PostgreSQL 15
upholds our standards of delivering a stable, reliable release of the world's
most advanced open source relational database. Please read more about our
[beta testing process](https://www.postgresql.org/developer/beta/) and how you
can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

You can find information about all of the PostgreSQL 15 features and changes in
the [release notes](https://www.postgresql.org/docs/15/release-15.html):

  [https://www.postgresql.org/docs/15/release-15.html](https://www.postgresql.org/docs/15/release-15.html)

Bug Fixes and Improvements
--------------------------

This update fixes over 40 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 14. Some of these issues may also
affect other supported versions of PostgreSQL.

Included in this release:

* Fix replay of [`CREATE DATABASE`](https://www.postgresql.org/docs/current/sql-createdatabase.html)
write-ahead log (WAL) records on standby servers when encountering a missing
[tablespace](https://www.postgresql.org/docs/current/manage-ag-tablespaces.html)
directory.
* Add support for [tablespaces](https://www.postgresql.org/docs/current/manage-ag-tablespaces.html)
that are plain directories instead of symbolic links to other directories.
* Fix permission checks in [`CREATE INDEX`](https://www.postgresql.org/docs/current/sql-createindex.html)
to use the user's permissions. This fixes broken dump/restore scenarios that
relied on the behavior prior to the fix for
[CVE-2022-1552](https://www.postgresql.org/support/security/CVE-2022-1552/).
* In the extended query protocol, force an immediate commit after
[`CREATE DATABASE`](https://www.postgresql.org/docs/current/sql-createdatabase.html)
and other commands that can't run in a transaction block.
* Fix a race condition around checking transaction visibility that was more
likely to happen when using synchronous replication.
* Fix incorrect permission-checking code for
[extended statistics](https://www.postgresql.org/docs/current/planner-stats.html#PLANNER-STATS-EXTENDED).
* Fix extended statistics machinery to handle
[most common value](https://www.postgresql.org/docs/current/planner-stats.html#id-1.5.13.5.4.13)
([MCV](https://www.postgresql.org/docs/current/planner-stats.html#id-1.5.13.5.4.13))-type
statistics on boolean-valued expressions.
* Avoid planner core dump with constant
[`= ANY(array)`](https://www.postgresql.org/docs/current/functions-subquery.html#FUNCTIONS-SUBQUERY-ANY-SOME)
clauses when there are
[MCV-type extended statistics](https://www.postgresql.org/docs/current/planner-stats.html#id-1.5.13.5.4.13)
on the `array` variable.
* Allow cancellation of [`ANALYZE`](https://www.postgresql.org/docs/current/sql-analyze.html)
while it is computing extended statistics.
* Fix [`ALTER TABLE ... ENABLE/DISABLE TRIGGER`](https://www.postgresql.org/docs/current/sql-altertable.html)
to handle recursion for triggers on partitioned tables.
* Reject `ROW()` expressions and functions in `FROM` that have more than 1600
columns.
* Fix memory leak in logical replication subscribers.
* Fix checks in logical replication of replica identity when the target table is
partitioned.
* Arrange to clean up after commit-time errors within
[`SPI_commit()`](https://www.postgresql.org/docs/current/spi-spi-commit.html),
rather than expecting callers to do that. This includes a fix for the same
scenario in [PL/Python](https://www.postgresql.org/docs/current/plpython.html),
which had reported crashes on Python 3.11 and memory leaks on older versions of
Python 3.
* Improve handling in `libpq` of idle states in
[pipeline mode](https://www.postgresql.org/docs/current/libpq-pipeline-mode.html).
* In the [`psql`](https://www.postgresql.org/docs/current/app-psql.html)
`\watch` command, echo a newline after cancellation with control-C.
* Fix [`pg_upgrade`](https://www.postgresql.org/docs/current/pgupgrade.html) to
detect non-upgradable usages of functions accepting `anyarray` parameters.
* Several [`postgres_fdw`](https://www.postgresql.org/docs/current/postgres-fdw.html)
fixes, including prevention of batch insertions when there are
`WITH CHECK OPTION` constraints present.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Updating to PostgreSQL 15 Beta 3
--------------------------------

To upgrade to PostgreSQL 15 Beta 3 from Beta 2, Beta 1, or an earlier version of
PostgreSQL, you will need to use a strategy similar to upgrading between
major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`).
For more information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/15/static/upgrading.html).

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools to find bugs
and regressions before the general availability of PostgreSQL 15. As
this is a beta release, changes to database behaviors, feature details, and
APIs are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_15_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 15 Beta Release Notes](https://www.postgresql.org/docs/15/release-15.html)
* [PostgreSQL 15 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_15_Open_Items)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
