2019-05-09 Cumulative Security Update
=====================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 11.3, 10.8, 9.6.13, 9.5.17, and 9.4.22. This release fixes two security issues in the PostgreSQL server, a security issue found in two of the PostgreSQL Windows installers, and over 60 bugs reported over the last three months.

Users who installed PostgreSQL using the Windows installers from EnterpriseDB and BigSQL respectively should upgrade as soon as possible. Similarly, users who are running any version of PostgreSQL 9.5, 9.6, 10, and 11 should also plan to upgrade as soon as possible.

All other users should plan to apply this update at the next scheduled downtime.

Security Issues
---------------

Four security vulnerabilities have been closed by this release:

* [CVE-2019-10127](https://access.redhat.com/security/cve/CVE-2019-10127): BigSQL Windows installer does not clear permissive ACL entries
* [CVE-2019-10128](https://access.redhat.com/security/cve/CVE-2019-10128): EnterpriseDB Windows installer does not clear permissive ACL entries

Due to both the EnterpriseDB and BigSQL Windows installers not locking down the permissions of the PostgreSQL binary installation directory and the data directory, an unprivileged Windows user account and an unprivileged PostgreSQL account could cause the PostgreSQL service account to execute arbitrary code.

This vulnerability is present in all supported versions of PostgreSQL for these installers, and possibly exists in older versions. Both sets of installers have fixed the permissions for these directories for both new and existing installations. If you have installed PostgreSQL on Windows using other methods, we advise that you check that your PostgreSQL binary directories are writable only to trusted users and that your data directories are only accessible to trusted users.

The PostgreSQL project thanks Conner Jones for reporting this problem.

* [CVE-2019-10129](https://access.redhat.com/security/cve/CVE-2019-10129):  Memory disclosure in partition routing

Prior to this release, a user running PostgreSQL 11 can read arbitrary bytes of server memory by executing a purpose-crafted INSERT statement to a partitioned table.

* [CVE-2019-10130](https://access.redhat.com/security/cve/CVE-2019-10130): Selectivity estimators bypass row security policies

PostgreSQL maintains statistics for tables by sampling data available in columns; this data is consulted during the query planning process. Prior to this release, a user able to execute SQL queries with permissions to read a given column could craft a leaky operator that could read whatever data had been sampled from that column.  If this happened to include values from rows that the user is forbidden to see by a row security policy, the user could effectively bypass the policy.  This is fixed by only allowing a non-leakproof operator to use this data if there are no relevant row security policies for the table.

This issue is present in PostgreSQL 9.5, 9.6, 10, and 11. The PostgreSQL project thanks Dean Rasheed for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update also fixes over 60 bugs that were reported in the last several months. Some of these issues affect only version 11, but many affect all supported versions.

Some of these fixes include:

* Several catalog corruption fixes, including one related to running `ALTER TABLE` on a partitioned table
* Several fixes for partitioning
* Avoid server crash when an error occurs while trying to persist a cursor query across a transaction commit
* Avoid O(N^2) performance issue when rolling back a transaction that created many tables
* Fix possible “could not access status of transaction” failures in `txid_status()`
* Fix updatable views to handle explicit DEFAULT items in `INSERT .. VALUES` statements where there are multiple VALUES rows
* Fix CREATE VIEW to allow zero-column views
* Add missing support for the `CREATE TABLE IF NOT EXISTS .. AS EXECUTE ..` statement
* Ensure that sub-SELECTs appearing in row-level-security policy expressions are executed with the correct user's permissions
* Accept XML documents as valid values of type xml when xmloption is set to content, as required by SQL:2006 and later
* Fix incompatibility of GIN-index WAL records that were introduced in 11.2, 10.7, 9.6.12, 9.5.16, and 9.4.21 that affected replica servers running these versions reading in changes to GIN indexes from primary servers of older versions
* Several memory leak fixes as well as fixes to management of dynamic shared memory
* Relax panics on fsync and sync_file_range failures for certain cases where a failure indicated "operation not supported"
* Several fixes to the query planner, several of which should lead to planning improvements
* Fix race condition in which a hot-standby postmaster could fail to shut down after receiving a smart-shutdown request
* Several fixes for SCRAM authentication
* Fix handling of `lc_time` settings that imply an encoding different from the database's encoding
* Create the `current_logfiles` file with the same permissions as other files in the server's `data` directory
* Several ecpg fixes
* Make `pg_verify_checksums` verify that the data directory it's pointed at is of the right PostgreSQL version
* Several fixes for `contrib/postgres_fdw`, including one for remote partitions where an UPDATE could lead to incorrect results or a crash
* Several Windows fixes

This update also contains tzdata release 2019a for DST law changes in Palestine and Metlakatla, plus historical corrections for Israel. Etc/UCT is now a backward-compatibility link to Etc/UTC, instead of being a separate zone that generates the abbreviation UCT, which nowadays is typically a typo. PostgreSQL will still accept UCT as an input zone abbreviation, but it won't output it.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use `pg_upgrade` in order to apply this update release; you may simply shutdown PostgreSQL and update its binaries.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

PostgreSQL 9.4 will stop receiving fixes on February 13, 2020. Please see our [versioning policy](https://www.postgresql.org/support/versioning/) for more information.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/11/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
