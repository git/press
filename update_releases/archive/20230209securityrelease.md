The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 15.2, 14.7, 13.10, 12.14, and 11.19.
This release closes one security vulnerability and fixes over 60 bugs reported
over the last several months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Security Issues
---------------

### [CVE-2022-41862](https://www.postgresql.org/support/security/CVE-2022-41862/): Client memory disclosure when connecting, with Kerberos, to modified server.

Versions Affected: 12 - 15.

A modified, unauthenticated server or an unauthenticated man-in-the-middle can
send an unterminated string during the establishment of Kerberos transport
encryption.  When a `libpq` client application has a Kerberos credential cache
and doesn't explicitly disable option
[`gssencmode`](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNECT-GSSENCMODE),
a server can cause `libpq` to over-read and report an error message containing
uninitialized bytes from and following its receive buffer. If `libpq`'s caller
somehow makes that message accessible to the attacker, this achieves a
disclosure of the over-read bytes.  We have not confirmed or ruled out viability
of attacks that arrange for a crash or for presence of notable, confidential
information in disclosed bytes.

The PostgreSQL project thanks Jacob Champion for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 60 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 15. Some of these issues may also
affect other supported versions of PostgreSQL.

Included in this release:

* Fix for partitioned tables to correctly update
[`GENERATED`](https://www.postgresql.org/docs/current/ddl-generated-columns.html)
columns in child tables if the `GENERATED` column does not exist in the parent
table or the child generated column has different dependencies than the parent.
* Several fixes for the [`MERGE`](https://www.postgresql.org/docs/current/sql-merge.html)
command.
* Allow a
[`WITH RECURSIVE ... CYCLE`](https://www.postgresql.org/docs/current/queries-with.html#QUERIES-WITH-CYCLE)
query to access its `SET` output column.
* Fix an issue with bulk insertions on foreign tables that could lead to logical
inconsistencies, for example, a `BEFORE ROW` trigger may not process rows that
should be available.
* Reject uses of undefined variables in
[`jsonpath`](https://www.postgresql.org/docs/current/functions-json.html#FUNCTIONS-SQLJSON-PATH)
existence checks.
* Fix for [`jsonb` subscripting](https://www.postgresql.org/docs/current/datatype-json.html#JSONB-SUBSCRIPTING)
that come directly from a `text` column in a table.
* Honor updated values of `checkpoint_completion_target` on reload.
* Log the correct ending timestamp in `recovery_target_xid` mode.
* Fix issue to allow column lists longer than 100 when using logical
replication.
* Prevent "wrong tuple length" failure at the end of
[`VACUUM`](https://www.postgresql.org/docs/current/sql-vacuum.html).
* Avoid an immediate commit after
[`ANALYZE`](https://www.postgresql.org/docs/current/sql-analyze.html) when using
query pipelining.
* Several fixes to the query planner, including one that provides more
opportunities for using
[memoization with partitionwise joins](https://www.postgresql.org/docs/current/runtime-config-query.html).
* Fix for statistics collection to correctly handle when a relation changes
type (e.g. a table is converted to a view).
* Ensure [full text search](https://www.postgresql.org/docs/current/textsearch.html)
queries can be cancelled while performing phrase matches.
* Fix deadlock between [`DROP DATABASE`](https://www.postgresql.org/docs/current/sql-dropdatabase.html)
and logical replication worker process.
* Fix small session-lifespan memory leak when
[`CREATE SUBSCRIPTION`](https://www.postgresql.org/docs/current/sql-createsubscription.html)
fails its connection attempt.
* Performance improvement for replicas with
[`hot_standby`](https://www.postgresql.org/docs/current/hot-standby.html)
enabled that are processing `SELECT` queries.
* Several fixes for logical decoding that improve its stability and bloat
handling.
* Fix the default logical replication plug-in, `pgoutput`, to not send columns
that are not listed in a table's replication
[column list](https://www.postgresql.org/docs/current/logical-replication-col-lists.html).
* Fix possible corruption of very large tablespace map files in
[`pg_basebackup`](https://www.postgresql.org/docs/current/app-pgbasebackup.html).
* Remove a harmless warning from
[`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html) in
`--if-exists` mode when the
[`public` schema](https://www.postgresql.org/docs/current/ddl-schemas.html#DDL-SCHEMAS-PUBLIC)
has a non-default owner.
* Fix the [`psql`](https://www.postgresql.org/docs/current/app-psql.html)
commands `\sf` and `\ef` to handle SQL-language functions that have
[SQL-standard function bodies](https://www.postgresql.org/docs/currenet/sql-createprocedure.html)
(i.e. `BEGIN ATOMIC`).
* Fix tab completion of `ALTER FUNCTION/PROCEDURE/ROUTINE ... SET SCHEMA`.
* Update the [`pageinspect`](https://www.postgresql.org/docs/current/pageinspect.html)
extension to mark its disk-accessing functions as `PARALLEL RESTRICTED`.
* Fix the [`seg`](https://www.postgresql.org/docs/current/seg.html) extension to
not crash or print garbage if an input number has more than 127 digits.

This release also updates time zone data files to tzdata release 2022g for
DST law changes in Greenland and Mexico, plus historical corrections for
northern Canada, Colombia, and Singapore. Notably, a new timezone,
America/Ciudad_Juarez, has been split off from America/Ojinaga.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [PostgreSQL 15 Release Announcement](https://www.postgresql.org/about/press/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
