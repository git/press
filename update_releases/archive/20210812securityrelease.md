The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 13.4, 12.8, 11.13, 10.18, and 9.6.23,
as well as the third beta release of PostgreSQL 14. This release closes one
security vulnerability and fixes over 75 bugs reported over the last three
months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 9.6 EOL Approaching
------------------------------

PostgreSQL 9.6 will stop receiving fixes on November 11, 2021. If you are
running PostgreSQL 9.6 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2021-3677](https://www.postgresql.org/support/security/CVE-2021-3677/): Memory disclosure in certain queries

Versions Affected: 11 - 13.

A purpose-crafted query can read arbitrary bytes of server memory.  In the
default configuration, any authenticated database user can complete this
attack at will.  The attack does not require the ability to create objects.
If server settings include `max_worker_processes=0`, the known versions of this
attack are infeasible.  However, undiscovered variants of the attack may be
independent of that setting.

A Note on the PostgreSQL 14 Beta
--------------------------------

This release marks the third beta release of PostgreSQL 14 and puts the
community one step closer to general availability tentatively around the end of
the third quarter.

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 14 in your database systems to help us
eliminate any bugs or other issues that may exist. While we do not advise you to
run PostgreSQL 14 Beta 3 in your production environments, we encourage you to
find ways to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 14
release upholds our standards of providing a stable, reliable release of the
world's most advanced open source relational database.

Bug Fixes and Improvements
--------------------------

This update also fixes over 75 bugs that were reported in the last several
months. Some of these issues affect only version 13, but many affect all
supported versions.

Some of these fixes include:

* Completely disable TLS/SSL renegotiation. This was previously disabled, but
the server would still execute a client-initiated renegotiation request.
* Restore the Portal-level snapshot after `COMMIT` or `ROLLBACK` within a
procedure. This change fixes cases where an attempt to fetch a toasted value
immediately after `COMMIT`/`ROLLBACK` would fail with errors like "no known
snapshots" or "missing chunk number 0 for toast value".
* Avoid misbehavior when persisting the output of a cursor that's reading a
volatile query.
* Reject cases where a query in `WITH` rewrites to just `NOTIFY`, which would
cause a crash.
* Several corner-case fixes for numeric types.
* `ALTER EXTENSION` now locks the extension when adding or removing a member
object.
* The "enabled" status is now copied when a partitioned table's triggers are
cloned to a new partition.
* Avoid alias conflicts in queries generated for `REFRESH MATERIALIZED VIEW CONCURRENTLY`.
This command failed on materialized views containing columns with certain names,
notably `mv` and `newdata`.
* Disallow whole-row variables in `GENERATED` expressions.
* Several fixes for `DROP OWNED BY` behavior in relation to row-level security
(RLS) policies.
* Re-allow old-style Windows locale names in `CREATE COLLATION` commands.
* walsenders now show their latest replication command in `pg_stat_activity`,
instead of just showing the latest SQL command.
* `pg_settings.pending_restart` now shows as `true` when a pertinent entry in
`postgresql.conf` is removed.
* On 64-bit Windows, allow the effective value of `work_mem * hash_mem_multiplier`
to exceed 2GB.
* Update minimum recovery point when WAL replay of a transaction abort record
causes file truncation.
* Advance oldest-required-WAL-segment horizon properly after a replication slot
is invalidated. This fixes an issue where the server's WAL storage could run out
of space.
* Improve progress reporting for the sort phase of a parallel B-tree index
build.
* Fix assorted crash cases in logical replication of partitioned-table updates
and when firing AFTER triggers of partitioned tables.
* Prevent infinite loops in SP-GiST index insertion.
* Ensure that SP-GiST index insertion can be terminated by a query cancel
request.
* In `psql` and other client programs, avoid overrunning the ends of strings
when dealing with invalidly-encoded data.
* Fix `pg_dump` to correctly handle triggers on partitioned tables whose enabled
status is different from their parent triggers' status.
* Avoid "invalid creation date in header" warnings when running `pg_restore` on
a file created in a different time zone.
* `pg_upgrade` now carries forward the old installation's `oldestXID` value and
no longer forces an anti-wraparound `VACUUM`."
* Extend `pg_upgrade` to detect and warn about extensions that should be
upgraded.
* Fix `contrib/postgres_fdw` to better work with generated columns, so long as a
generated column in a foreign table represents a generated column in the remote
table.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/current/release.html).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shut down PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/current/release.html).

**NOTE**: PostgreSQL 9.6 will stop receiving fixes on November 11, 2021. Please
see our [versioning policy](https://www.postgresql.org/support/versioning/) for
more information.

PostgreSQL 14 Beta 3 Notes
--------------------------

PostgreSQL 14 Beta 3 introduces the `unnest` function for multirange types as
well as several bug fixes.

For a list of changes that are included in PostgreSQL 14 Beta 3, please review
the [open items](https://wiki.postgresql.org/wiki/PostgreSQL_14_Open_Items#resolved_before_14beta3)
page:

  [https://wiki.postgresql.org/wiki/PostgreSQL_14_Open_Items#resolved_before_14beta3](https://wiki.postgresql.org/wiki/PostgreSQL_14_Open_Items#resolved_before_14beta3)

To upgrade to PostgreSQL 14 Beta 3 from Beta 2, Beta 1, or an earlier version of
PostgreSQL, you will need to use a strategy similar to upgrading between
major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`).
For more information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/14/static/upgrading.html).

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 14. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_14_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

https://www.postgresql.org/account/submitbug/

Beta Schedule
-------------

This is the third beta release of version 14. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2021. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 14 Beta Release Notes](https://www.postgresql.org/docs/14/release-14.html)
* [PostgreSQL 14 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_14_Open_Items)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
