The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 13.1, 12.5, 11.10, 10.15, 9.6.20, and
9.5.24. This release closes three security vulnerabilities and fixes over 65
bugs reported over the last three months.

Due to the nature of CVE-2020-25695, we advise you to update as soon as
possible.

Additionally, this is the second-to-last release of PostgreSQL 9.5. If you are
running PostgreSQL 9.5 in a production environment, we suggest that you make
plans to upgrade.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/current/release.html).

Security Issues
---------------

* CVE-2020-25695: Multiple features escape "security restricted operation" sandbox

Versions Affected: 9.5 - 13. The security team typically does not test
unsupported versions, but this problem is quite old.

An attacker having permission to create non-temporary objects in at least one
schema can execute arbitrary SQL functions under the identity of a superuser.

While promptly updating PostgreSQL is the best remediation for most users, a
user unable to do that can work around the vulnerability by disabling
autovacuum and not manually running `ANALYZE`, `CLUSTER`, `REINDEX`,
`CREATE INDEX`, `VACUUM FULL`, `REFRESH MATERIALIZED VIEW`, or a restore from
output of the `pg_dump` command. Performance may degrade quickly under this
workaround.

`VACUUM` without the `FULL` option is safe, and all commands are fine when a
trusted user owns the target object.

The PostgreSQL project thanks Etienne Stalmans for reporting this problem.

* CVE-2020-25694: Reconnection can downgrade connection security settings

Versions Affected: 9.5 - 13. The security team typically does not test
unsupported versions, but this problem is quite old.

Many PostgreSQL-provided client applications have options that create additional
database connections. Some of those applications reuse only the basic
connection parameters (e.g. `host`, `user`, `port`), dropping others. If this
drops a security-relevant parameter (e.g. `channel_binding`, `sslmode`,
`requirepeer`, `gssencmode`), the attacker has an opportunity to complete a MITM
attack or observe cleartext transmission.

Affected applications are `clusterdb`, `pg_dump`, `pg_restore`, `psql`,
`reindexdb`, and `vacuumdb`. The vulnerability arises only if one invokes an
affected client application with a connection string containing a
security-relevant parameter.

This also fixes how the `\connect` command of `psql` reuses connection
parameters, i.e.   all non-overridden parameters from a previous connection
string now re-used.

The PostgreSQL project thanks Peter Eisentraut for reporting this problem.

* CVE-2020-25696: `psql`'s `\gset` allows overwriting specially treated variables

Versions Affected: 9.5 - 13. The security team typically does not test
unsupported versions, but this problem likely arrived with the feature's debut
in version 9.3.

The `\gset` meta-command, which sets `psql` variables based on query results,
does not distinguish variables that control `psql` behavior.  If an interactive
`psql` session uses `\gset` when querying a compromised server, the attacker can
execute arbitrary code as the operating system account running `psql`.
Using `\gset` with a prefix not found among specially treated variables, e.g.
any lowercase string, precludes the attack in an unpatched `psql`.

The PostgreSQL project thanks Nick Cleaton for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update also fixes over 65 bugs that were reported in the last several
months. Some of these issues only affect version 13, but may also apply to other
supported versions.

Some of these fixes include:

* Fix a breakage in the replication protocol by ensuring that two "command
completion" events are expected for `START_REPLICATION`.
* Ensure `fsync` is called on the SLRU caches that PostgreSQL maintains. This
prevents potential data loss due to an operating system crash.
* Fix `ALTER ROLE` usage for users with the `BYPASSRLS` permission.
* `ALTER TABLE ONLY ... DROP EXPRESSION` is disallowed on partitioned tables
when there are child tables.
* Ensure that `ALTER TABLE ONLY ... ENABLE/DISABLE TRIGGER` does not apply to
child tables.
* Fix for `ALTER TABLE ... SET NOT NULL` on partitioned tables to avoid a
potential deadlock in parallel `pg_restore`.
* Fix handling of expressions in `CREATE TABLE LIKE` with inheritance.
* `DROP INDEX CONCURRENTLY` is disallowed on partitioned tables.
* Allow `LOCK TABLE` to succeed on a self-referential view instead of throwing
an error.
* Several fixes around statistics collection and progress reporting for
`REINDEX CONCURRENTLY`.
* Ensure that `GENERATED` columns are updated when any columns they depend on
are updated via a rule or an updatable view.
* Support hash partitioning with text array columns as partition keys.
* Allow the jsonpath `.datetime()` method to accept ISO 8601-format timestamps.
* During a "smart" shutdown, ensure background processes are not terminated
until all foreground client sessions are completed, fixing an issue that broke
the processing of parallel queries.
* Several fixes for the query planner and optimizer.
* Ensure that data is de-toasted before being inserted into a BRIN index. This
could manifest itself with errors like "missing chunk number 0 for toast value
NNN". If you have seen a similar error in an existing BRIN index, you should be
able to correct it by using `REINDEX` on the index.
* Fix the output of `EXPLAIN` to have the correct XML tag nesting for
incremental sort plans.
* Several fixes for memory leaks, including ones involving RLS policies, using
`CALL` with PL/pgSQL, `SIGHUP` processing a configuration parameter that cannot
be applied without a restart, and an edge-case for index lookup for a partition.
* libpq can now support arbitrary-length lines in the .pgpass file.
* On Windows, `psql` now reads the output of a backtick command in text mode,
not binary mode, so it can now properly handle newlines.
* Fix how `pg_dump`, `pg_restore`, `clusterdb`, `reindexdb`, and `vacuumdb` use
complex connection-string parameters.
* When the `\connect` command of `psql` reuses connection parameters, ensure
that all non-overridden parameters from a previous connection string are also
re-used.
* Ensure that `pg_dump` collects per-column information about extension
configuration tables, avoiding crashes when specifying `--inserts`.
* Ensure that parallel `pg_restore` processes foreign keys referencing
partitioned tables in the correct order.
* Several fixes for `contrib/pgcrypto`, including a memory leak fix.

This update also contains tzdata release 2020d for for DST law changes in Fiji,
Morocco, Palestine, the Canadian Yukon, Macquarie Island, and Casey Station
(Antarctica); plus historical corrections for France, Hungary, Monaco, and
Palestine.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/current/release.html).

PostgreSQL 9.5 EOL Notice
-------------------------

PostgreSQL 9.5 will stop receiving fixes on February 11, 2021. If you are
running PostgreSQL 9.5 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/current/release.html).

**NOTE**: PostgreSQL 9.5 will stop receiving fixes on February 11, 2021. Please
see our [versioning policy](https://www.postgresql.org/support/versioning/) for
more information.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
