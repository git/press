The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 13.2, 12.6, 11.11, 10.16, 9.6.21, and
9.5.25. This release closes two security vulnerabilities and fixes over 80 bugs
reported over the last three months.

Additionally, this is the **final release of PostgreSQL 9.5**. If you are
running PostgreSQL 9.5 in a production environment, we suggest that you make
plans to upgrade.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Security Issues
---------------

### CVE-2021-3393: Partition constraint violation errors leak values of denied columns

Versions Affected: 11 - 13.

A user having an `UPDATE` privilege on a partitioned table but lacking the
`SELECT` privilege on some column may be able to acquire denied-column values
from an error message. This is similar to CVE-2014-8161, but the conditions to
exploit are more rare.

The PostgreSQL project thanks Heikki Linnakangas for reporting this problem.

### CVE-2021-20229: Single-column SELECT privilege enables reading all columns

Versions Affected: 13.

A user having a `SELECT` privilege on an individual column can craft a special
query that returns all columns of the table.

Additionally, a stored view that uses column-level privileges will have
incomplete column-usage bitmaps. In installations that depend on column-level
permissions for security, it is recommended to execute `CREATE OR REPLACE` on
all user-defined views to force them to be re-parsed.

The PostgreSQL project thanks Sven Klemm for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update fixes over 80 bugs that were reported in the last several months.
Some of these issues only affect version 13, but could also apply to other
supported versions.

Some of these fixes include:

* Fix an issue with GiST indexes where concurrent insertions could lead to a
corrupt index with entries placed in the wrong pages. You should `REINDEX` any
affected GiST indexes.
* Fix `CREATE INDEX CONCURRENTLY` to ensure rows from concurrent prepared
transactions are included in the index. Installations that have enabled prepared
transactions should `REINDEX` any concurrently-built indexes.
* Fix for possible incorrect query results when a hash aggregation is spilled to
disk.
* Fix edge case in incremental sort that could lead to sorting results
incorrectly or a "retrieved too many tuples in a bounded sort" error.
* Avoid crash when a `CALL` or `DO` statement that performs a transaction
rollback is executed via extended query protocol, such as from prepared
statements.
* Fix a failure when a PL/pgSQL procedure used `CALL` on another procedure that
has `OUT` parameters that executed a `COMMIT` or `ROLLBACK`.
* Remove errors from `BEFORE UPDATE` triggers on partitioned tables for
restrictions that no longer apply.
* Several fixes for queries with joins that could lead to error messages such as
"no relation entry for relid N" or "failed to build any N-way joins".
* Do not consider parallel-restricted or set-returning functions in an
`ORDER BY` expressions when trying to parallelize sorts.
* Fix `ALTER DEFAULT PRIVILEGES` to handle duplicate arguments safely.
* Several fixes in behavior when `wal_level` is set to `minimal`, including when
tables are rewritten within a transaction.
* Several fixes for `CREATE TABLE LIKE`.
* Ensure that allocated disk space for a dropped relation (e.g. a table) is
released promptly when a transaction is committed.
* Fix progress reporting for `CLUSTER`.
* Fix handling of backslash-escaped multibyte characters in `COPY FROM`.
* Fix recently-introduced race conditions in `LISTEN`/`NOTIFY` queue handling.
* Allow the `jsonb` concatenation operator (`||`) to handle all combinations of
JSON data types.
* Fix WAL-reading logic so that standbys can handle timeline switches correctly.
This issue could have shown itself with errors like "requested WAL segment has
already been removed".
* Several leak fixes for the `walsender` process around logical decoding and
replication.
* Ensure that a nonempty value of `krb_server_keyfile` always overrides any
setting of `KRB5_KTNAME` in the server environment
* Several fixes for GSS encryption support.
* Ensure the `\connect` command allows the use of a password in the
`connection_string` argument.
* Fix assorted bugs with the `\help` command.
* Several fixes for `pg_dump`.
* Ensure that `pg_rewind` accounts for all WAL when rewinding a standby server.
* Fix memory leak in `contrib/auto_explain`.
* Ensure all `postgres_fdw` connections are closed if the a user mapping or
foreign server object those connections depend on are dropped.
* Fix JIT compilation to be compatible with LLVM 11 and LLVM 12.

This update also contains tzdata release 2021a for DST law changes in Russia
(Volgograd zone) and South Sudan, plus historical corrections for Australia,
Bahamas, Belize, Bermuda, Ghana, Israel, Kenya, Nigeria, Palestine, Seychelles,
and Vanuatu.

Notably, the Australia/Currie zone has been corrected to the point where it is
identical to Australia/Hobart.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 9.5 is EOL
---------------------

This is the final release of PostgreSQL 9.5. If you are running PostgreSQL 9.5
in a production environment, we suggest that you make plans to upgrade to a
newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

**NOTE**: PostgreSQL 9.6 will stop receiving fixes on November 11, 2021. Please
see our [versioning policy](https://www.postgresql.org/support/versioning/) for
more information.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
