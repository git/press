The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 15.1, 14.6, 13.9, 12.13, 11.18, and 10.23.
This release fixes 25 bugs reported over the last several months.

This is the **final release of PostgreSQL 10**. PostgreSQL 10 will no longer
receive
[security and bug fixes](https://www.postgresql.org/support/versioning/).
If you are running PostgreSQL 10 in a production environment, we suggest that
you make plans to upgrade.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Bug Fixes and Improvements
--------------------------

This update fixes over 25 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 15. Some of these issues may also
affect other supported versions of PostgreSQL.

Included in this release:

* Fix for updatable views for `INSERT` statements that include multi-row
`VALUES` clauses with a `DEFAULT` set.
* Disallow rules named `_RETURN` that are not `ON SELECT` rules.
* Disallow use of `MERGE` on a partitioned table that has foreign-table
partitions.
* Fix for construction of per-partition foreign key constraints while doing
`ALTER TABLE ... ATTACH PARTITION`, where previously incorrect or duplicate
constraints could be built.
* Fix for a planner failure with extended statistics on partitioned or inherited
tables.
* Fix bugs in logical decoding that could lead to memory leaks when replay
starts from a point between the beginning of a transaction and the beginning of
its subtransaction.
* Fix issues with slow shutdown of replication workers by allowing interrupts
in more places.
* Disallow logical replication into foreign-table partitions.
* Prevent crash in replication works after a SQL or PL/pgSQL function syntax
error.
* `psql -c` now exits with a nonzero status if the query is canceled.
* Allow cross-platform tablespace relocation in `pg_basebackup`.
* Fix pg_dump to include comments attached to some `CHECK` constraints.

This release also updates time zone data files to use tzdata release 2022f. This
includes DST law changes in Chile, Fiji, Iran, Jordan, Mexico, Palestine, and
Syria, plus historical corrections for Chile, Crimea, Iran, and Mexico.

There are several other changes to note in the tzdata 2022f release that may
change the display for pre-1970 timestamps. For a detailed explanation, please
review the [release notes](https://www.postgresql.org/docs/release/).

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 10 is EOL
--------------------

PostgreSQL 10.23 is the final release of PostgreSQL 10. If you are running
PostgreSQL 10 in a production environment, we suggest that you make plans to
upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [PostgreSQL 15 Release Announcement](https://www.postgresql.org/about/press/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
