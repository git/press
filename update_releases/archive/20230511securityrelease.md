The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 15.3, 14.8, 13.11, 12.15, and 11.20.
This release fixes two security vulnerabilities and over 80 bugs reported over
the last several months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 11 EOL Notice
------------------------

PostgreSQL 11 will stop receiving fixes on November 9, 2023. If you are
running PostgreSQL 11 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2023-2454](https://www.postgresql.org/support/security/CVE-2023-2454/): `CREATE SCHEMA ... schema_element` defeats protective search_path changes.

Versions Affected: 11 - 15. The security team typically does not test
unsupported versions, but this problem is quite old.

This enabled an attacker having database-level `CREATE` privilege to execute
arbitrary code as the bootstrap superuser.  Database owners have that right by
default, and explicit grants may extend it to other users.

The PostgreSQL project thanks Alexander Lakhin for reporting this problem.

### [CVE-2023-2455](https://www.postgresql.org/support/security/CVE-2023-2455/): Row security policies disregard user ID changes after inlining.

Versions Affected: 11 - 15. The security team typically does not test
unsupported versions, but this problem is quite old.

While [CVE-2016-2193](https://www.postgresql.org/support/security/CVE-2016-2193/)
fixed most interaction between row security and user ID changes, it missed a
scenario involving function inlining.  This leads to potentially incorrect
policies being applied in cases where role-specific policies are used and a
given query is planned under one role and then executed under other roles.
This scenario can happen under security definer functions or when a common user
and query is planned initially and then re-used across multiple SET ROLEs.
Applying an incorrect policy may permit a user to complete otherwise-forbidden
reads and modifications. This affects only databases that have used
[`CREATE POLICY`](https://www.postgresql.org/docs/current/sql-createpolicy.html)
to define a row security policy.

The PostgreSQL project thanks Wolfgang Walther for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 80 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 15. Some of these issues may also
affect other supported versions of PostgreSQL.

Included in this release:

* Several fixes for [`CREATE DATABASE`](https://www.postgresql.org/docs/current/sql-createdatabase.html)
when using the `STRATEGY = WAL_LOG`, including a potential corruption that could
lose modifications to a template/source database.
* Fix crash with [`CREATE SCHEMA AUTHORIZATION`](https://www.postgresql.org/docs/current/sql-createschema.html).
* Several fixes for [`MERGE`](https://www.postgresql.org/docs/current/sql-merge.html).
* Several fixes for triggers in partitioned tables.
* Disallow altering composite types that are stored in indexes.
* Ensure that [`COPY TO`](https://www.postgresql.org/docs/current/sql-copy.html)
from a parent table with [row-level security](https://www.postgresql.org/docs/current/ddl-rowsecurity.html)
enabled does not copy any rows from child tables.
* Adjust text-search-related character classification logic to correctly detect
whether the prevailing locale is C when the default collation of a database uses
the ICU provider.
* Re-allow exponential notation in ISO-8601 interval fields.
* Improve error reporting for various invalid JSON string literals.
* Fix data corruption due to `vacuum_defer_cleanup_age` being larger than the
current 64-bit xid.
* Several fixes for the query parser and planner, including better detection of
improperly-nested aggregates.
* Fix partition pruning bug with the boolean `IS NOT TRUE` and `IS NOT FALSE`
conditions. Prior to this, `NULL` partitions were accidentally pruned.
* Fix memory leak in [Memoize plan](https://www.postgresql.org/docs/current/runtime-config-query.html#GUC-ENABLE-MEMOIZE)
execution.
* Fix buffer refcount leak on foreign tables using partitions when performing
batched inserts.
* Restore support for sub-millisecond `vacuum_cost_delay` settings.
* Several fixes for
[views and rules](https://www.postgresql.org/docs/current/rules-views.html).
* Avoid unnecessary work while scanning a multi-column
[BRIN index](https://www.postgresql.org/docs/current/brin.html) with multiple
scan keys.
* Ignore dropped columns and generated columns during logical replication of an
`UPDATE` or `DELETE` action.
* Several fixes for naming and availability of wait events.
* Support RSA-PSS certificates with
[SCRAM-SHA-256](https://www.postgresql.org/docs/current/sasl-authentication.html#SASL-SCRAM-SHA-256)
channel binding. This feature requires building with OpenSSL 1.1.1 or newer.
* Avoid race condition with process ID tracking on Windows.
* Fix memory leak within a session for [PL/pgSQL](https://www.postgresql.org/docs/current/plpgsql.html)
[`DO`](https://www.postgresql.org/docs/current/sql-do.html) blocks that use cast
expressions.
* Tighten array dimensionality checks from
[PL/Perl](https://www.postgresql.org/docs/current/plperl.html) and
[PL/Python](https://www.postgresql.org/docs/current/plpython.html) when
converting list structures to multi-dimensional SQL arrays.
* Fix [`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html) so
that partitioned tables that are hash-partitioned on an
[enumerated type](https://www.postgresql.org/docs/current/datatype-enum.html)
column can be restored successfully.
* Fix for [`pg_trgm`](https://www.postgresql.org/docs/current/pgtrgm.html) where
an unsatisfiable regular expression could lead to a crash when using a GiST or
GIN index.
* Limit memory usage of `pg_get_wal_records_info()` in
[`pg_walinspect`](https://www.postgresql.org/docs/current/pgwalinspect.html).

This release also updates time zone data files to tzdata release 2023c for DST
law changes in Egypt, Greenland, Morocco, and Palestine. When observing Moscow
time, Europe/Kirov and Europe/Volgograd now use the abbreviations MSK/MSD
instead of numeric abbreviations, for consistency with other timezones observing
Moscow time. Also, America/Yellowknife is no longer distinct from
America/Edmonton; this affects some pre-1948 timestamps in that area.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [PostgreSQL 15 Release Announcement](https://www.postgresql.org/about/press/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
