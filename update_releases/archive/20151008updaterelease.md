2015-10-08 Security Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.4.5, 9.3.10, 9.2.14, 9.1.19 and 9.0.23. This release fixes two security issues, as well as several bugs found over the last four months.  Users vulnerable to the security issues should update their installations immediately; other users should update at the next scheduled downtime. This is also the final update release for major version 9.0.

Security Fixes
--------------

Two security issues have been fixed in this release which affect users of specific PostgreSQL features:

**CVE-2015-5289**: json or jsonb input values constructed from arbitrary user input can crash the PostgreSQL server and cause a denial of service.

**CVE-2015-5288**: The crypt() function included with the optional pgCrypto extension could be exploited to read a few additional bytes of memory.  No working exploit for this issue has been developed.

The PostgreSQL project thanks Josh Kupershmidt and Oskari Saarenmaa for reporting these issues.

This update will also disable SSL renegotiation by default; previously, it was enabled by default.   SSL renegotiation will be removed entirely in PostgreSQL versions 9.5 and later.

Other Fixes and Improvements
----------------------------

In addition to the above, many other issues were patched in this release based on bugs reported by our users over the last few months.  These fixes include:

* Prevent deeply nested regex, LIKE and SIMILAR matching from crashing the server
* Multiple other fixes with regular expression handling
* Ensure that ALTER TABLE sets all locks for CONSTRAINT modifications
* Fix subtransaction cleanup when a cursor fails, preventing a crash
* Prevent deadlock during WAL insertion when commit_delay is set
* Fix locking during updating of updatable views
* Prevent corruption of relation cache "init file"
* Improve performance of large SPI query results
* Improve LISTEN startup time
* Disable SSL renegotiation by default
* Lower minimum for *_freeze_max_age parameters
* Limit the maximum for wal_buffers to 2GB
* Guard against potential stack overflows in several areas
* Fix handling of DOW and DOY in datetime input
* Allow regular expression queries to be canceled sooner
* Fix assorted planner bugs
* Fix several shutdown issues in the postmaster
* Make anti-wraparound autovacuuming more robust
* Fix minor issues with GIN and SP-GiST indexes.
* Fix several issues with PL/Python, PL/Perl and PL/Tcl
* Improve pg_stat_statements' garbage collection
* Improve collation handling in pgsql_fdw
* Improve libpq's handling of out-of-memory conditions
* Prevent psql crash when there is no current connection
* Multiple fixes to pg_dump, including file and object permissions
* Improve handling of privileges when dumping from old PostgreSQL versions
* Fix issues with support of Alpha, PPC, AIX and Solaris platforms
* Fix startup issue on Windows with Chinese locale
* Fix Windows install.bat script to handle spaces in filenames
* Make the numeric PostgreSQL version number available to extensions

This update also contains tzdata release 2015g, with updates for Cayman Islands, Fiji, Moldova, Morocco, Norfolk Island, North Korea, Turkey, Uruguay, and the new zone America/Fort_Nelson.

Final Update for 9.0
--------------------

9.0.23 is the final update for major version 9.0, which is now End-Of-Life (EOL) as scheduled. Future security updates will not include version 9.0.  As such, users of that version should plan to upgrade to another major version as soon as possible.  For more information about the community's support policy and EOL schedule, see the [Versioning Policy](http://www.postgresql.org/support/versioning/).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries. Users who have skipped multiple update releases may need to perform additional post-update steps; see the Release Notes for details.

Links:
  * [Download](http://www.postgresql.org/download)
  * [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
  * [Security Page](http://www.postgresql.org/support/security/)
