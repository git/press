The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 17.3, 16.7, 15.11, 14.16, and 13.19.
This release fixes 1 security vulnerability and over 70 bugs reported over the
last several months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Security Issues
---------------

### [CVE-2025-1094](https://www.postgresql.org/support/security/CVE-2025-1094/): PostgreSQL quoting APIs miss neutralizing quoting syntax in text that fails encoding validation

CVSS v3.1 Base Score: [8.1](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?version=3.1&vector=AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:H)

Supported, Vulnerable Versions: 13 - 17. 

Improper neutralization of quoting syntax in PostgreSQL [`libpq`](https://www.postgresql.org/docs/current/libpq.html)
functions `PQescapeLiteral()`, `PQescapeIdentifier()`, `PQescapeString()`, and
`PQescapeStringConn()` allows a database input provider to achieve SQL
injection in certain usage patterns. Specifically, SQL injection requires the
application to use the function result to construct input to psql, the
PostgreSQL interactive terminal. Similarly, improper neutralization of quoting
syntax in PostgreSQL command line utility programs allows a source of command
line arguments to achieve SQL injection when [`client_encoding`](https://www.postgresql.org/docs/current/runtime-config-client.html#GUC-CLIENT-ENCODING)
is `BIG5` and
[`server_encoding`](https://www.postgresql.org/docs/current/runtime-config-preset.html#GUC-SERVER-ENCODING)
is one of `EUC_TW` or `MULE_INTERNAL`. Versions before PostgreSQL 17.3, 16.7,
15.11, 14.16, and 13.19 are affected.

The PostgreSQL project thanks Stephen Fewer, Principal Security Researcher,
Rapid7 for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 70 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 17. Some of these issues may also
affect other supported versions of PostgreSQL.

* Restore pre-v17 truncation behavior for >63-byte database names and usernames
in connection requests.
* Don't perform connection privilege checks and limits on parallel workers, and
instead inherit these from the leader process.
* Remove `Lock` suffix from `LWLock` wait event names. 
* Fix possible re-use of stale results in window aggregates, which could lead
to incorrect results.
* Several race condition fixes for [vacuum](https://www.postgresql.org/docs/current/sql-vacuum.html)
that in the worst case could cause corruption to a system catalog.
* Several fixes for [truncating](https://www.postgresql.org/docs/current/sql-truncate.html)
tables and indexes that prevent potential corruption.
* Fix for detaching a partition where its own foreign-key constraint references
a partitioned table.
* Fix for the `FFn` (e.g., `FF1`) format codes for `to_timestamp`, where an integer format code before the `FFn` would consume all available digits.
* Fixes for SQL/JSON and `XMLTABLE()` to double-quote specific entries when
necessary.
* Include the `ldapscheme` option in
[`pg_hba_file_rules()`](https://www.postgresql.org/docs/current/view-pg-hba-file-rules.html).
* Several fixes for [`UNION`](https://www.postgresql.org/docs/current/queries-union.html),
including not merging columns with non-compatible collations.
* Several fixes that could impact availability or speed of starting a connection
to PostgreSQL.
* Fix multiple memory leaks in logical decoding output.
* Fix several memory leaks in
[PL/Python](https://www.postgresql.org/docs/current/plpython.html).
* Add psql tab completion for
[`COPY (MERGE INTO)`](https://www.postgresql.org/docs/current/sql-copy.html).
* Make [`pg_controldata`](https://www.postgresql.org/docs/current/app-pgcontroldata.html)
more resilient when displaying info from corrupted[pg_control](https://www.postgresql.org/docs/current/wal-internals.html)
files.
* Fix for a memory leak in [`pg_restore`](https://www.postgresql.org/docs/current/app-pgrestore.html)
with zstd-compressed data.
* Fix [`pg_basebackup`](https://www.postgresql.org/docs/current/app-pgbasebackup.html)
to correctly handle pg_wal.tar files exceeding 2GB on Windows.
* Modify [earthdistance](https://www.postgresql.org/docs/current/earthdistance.html)
to use SQL-standard function bodies, which fixes possible issues with
major version upgrades to v17 when databases use this extension.
* Fix crash in [pageinspect](https://www.postgresql.org/docs/current/pageinspect.html)
in instances where the `brin_page_items()` function definition is not updated
to the latest version.
* Fix race condition when trying to cancel a
[`postgres_fdw`](https://www.postgresql.org/docs/current/postgres-fdw.html)
remote query.

This release also updates time zone data files to tzdata release 2025a for DST
law changes in Paraguay, plus historical corrections for the Philippines.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
