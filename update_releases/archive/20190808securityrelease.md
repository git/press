2019-08-08 Cumulative Security Update
=====================================

The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 11.5, 10.10, 9.6.15, 9.5.19, and
9.4.24, as well as the third beta of PostgreSQL 12. This release fixes two
security issues in the PostgreSQL server, two security issues found in one of
the PostgreSQL Windows installers, and over 40 bugs reported since the previous
release.

Users should install these updates as soon as possible.

A Note on the PostgreSQL 12 Beta
--------------------------------

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 12 in your database systems to help us
eliminate any bugs or other issues that may exist. While we do not advise you to
run PostgreSQL 12 Beta 3 in your production environments, we encourage you to
find ways to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 12
release upholds our standards of providing a stable, reliable release of the
world's most advanced open source relational database.

Security Issues
---------------

Four security vulnerabilities have been closed by this release:

* [CVE-2019-10208](https://access.redhat.com/security/cve/CVE-2019-10208): `TYPE` in `pg_temp` executes arbitrary SQL during `SECURITY DEFINER` execution

Versions Affected: 9.4 - 11

Given a suitable `SECURITY DEFINER` function, an attacker can execute arbitrary
SQL under the identity of the function owner.  An attack requires `EXECUTE`
permission on the function, which must itself contain a function call having
inexact argument type match.  For example, `length('foo'::varchar)` and
`length('foo')` are inexact, while `length('foo'::text)` is exact.  As part of
exploiting this vulnerability, the attacker uses `CREATE DOMAIN` to create a
type in a `pg_temp` schema. The attack pattern and fix are similar to that for
[CVE-2007-2138](https://nvd.nist.gov/vuln/detail/CVE-2007-2138).

Writing `SECURITY DEFINER` functions continues to require   
following the considerations noted in the documentation:

[https://www.postgresql.org/docs/devel/sql-createfunction.html#SQL-CREATEFUNCTION-SECURITY](https://www.postgresql.org/docs/devel/sql-createfunction.html#SQL-CREATEFUNCTION-SECURITY)

The PostgreSQL project thanks Tom Lane for reporting this problem.

* [CVE-2019-10209](https://access.redhat.com/security/cve/CVE-2019-10209): Memory disclosure in cross-type comparison for hashed subplan

Versions Affected: 11

In a database containing hypothetical, user-defined hash equality operators,
an attacker could read arbitrary bytes of server memory.  For an attack to
become possible, a superuser would need to create unusual operators.  It is
possible for operators not purpose-crafted for attack to have the properties
that enable an attack, but we are not aware of specific examples.

The PostgreSQL project thanks Andreas Seltenreich for reporting this problem.

* [CVE-2019-10210](https://access.redhat.com/security/cve/CVE-2019-10210): EnterpriseDB Windows installer writes PostgreSQL superuser password to unprotected temporary file

Versions Affected: The EnterpriseDB Windows installer for versions 9.4 - 11

The EnterpriseDB Windows installer writes a password to a temporary file in its
installation directory, creates initial databases, and deletes the file.  During
those seconds while the file exists, a local attacker can read the PostgreSQL
superuser password from the file.

The PostgreSQL project thanks Noah Misch for reporting this problem.

* [CVE-2019-10211](https://access.redhat.com/security/cve/CVE-2019-10211): EnterpriseDB Windows installer bundled OpenSSL executes code from unprotected directory

Versions Affected: The EnterpriseDB Windows installer for versions 9.4 - 11

When the database server or libpq client library initializes SSL, libeay32.dll
attempts to read configuration from a hard-coded directory.  Typically, the
directory does not exist, but any local user could create it and inject
configuration.  This configuration can direct OpenSSL to load and execute
arbitrary code as the user running a PostgreSQL server or client.  Most
PostgreSQL client tools and libraries use libpq, and one can encounter this
vulnerability by using any of them.  This vulnerability is much like
[CVE-2019-5443](https://nvd.nist.gov/vuln/detail/CVE-2019-5443), but it originated
independently.  One can work around the vulnerability by setting environment
variable OPENSSL_CONF to "NUL:/openssl.cnf" or any other name that cannot exist
as a file.

The PostgreSQL project thanks Daniel Gustafsson of the curl security team for
reporting this problem.

Bug Fixes and Improvements
--------------------------

This update also fixes over 40 bugs that were reported in the last several
months. Some of these issues affect only version 11, but many affect all
supported versions.

Some of these fixes include:

* Fix for `ALTER TABLE ... ALTER COLUMN TYPE` when multiple column types are
modified in a single-command. This issue was introduced in the previous
cumulative update (11.4, 10.9, 9.6.14, 9.5.18, 9.4.23, and 12 beta 2).
* Ensure that partition key columns will not be dropped as the result of an
"indirect drop," such as from a cascade from dropping the key column's data
type (e.g. a custom data type). This fix is applied only to newly created
partitioned tables: if you believe you have an affected partition table (e.g.
one where the partition key uses a custom data type), you will need to
create a new table and move your data into it or use `pg_upgrade`.
* Prevent dropping a partitioned table's trigger if there are pending trigger
events in child partitions. This particularly affects foreign key constraints,
which are implemented by triggers.
* Several additional fixes for partitioning, including a fix for partition
pruning that could lead to inefficient queries.
* Fix for parallel hash joins that could lead to duplicate result rows in
`EXISTS` queries.
* Several fixes for the query planner.
* Several fixes for issues that would lead to query deadlocks.
* Fix for multi-column foreign keys when rebuilding a foreign key constraint,
* Prevent extended statistics from being built for inherited tables.
* Fix for the canonicalization of date ranges that include `-infinity`/`infinity`
endpoints to ensure the behavior matches the documentation.
* Fix loss of fractional digits when converting very large `money` values to
`numeric`.
* Fix for PL/pgSQL functions that return composite types.
* Make libpq ignore the `\r` carriage return in connection service files, which
was causing connection failures in some edge cases.
* Several fixes for `psql`, which includes avoiding incorrect tab completion
options after `SET variable =`.
* Improve reliability of `contrib/amcheck`'s index verification.
* Set `initdb` to prefer the timezone behavior defined by the C library instead
of what is defined by `localtime` or `posixrules`. This ensures PostgreSQL uses
the "real" timezone name instead of an artificial name.
* Fix `pg_dump` to ensure that custom operator classes are dumped in the correct
order to prevent creating an unrestorable dump.
* Fix possible lockup in `pgbench` when using -R option.
* Fix spinlock assembly code for MIPS CPUs so that it works on MIPS r6.

This update also contains tzdata release 2019b for DST law changes in Brazil,
plus historical corrections for Hong Kong, Italy, and Palestine. This update
also adds support for zic's new -b slim option to reduce the size of the
installed zone files, though it is not currently being used by PostgreSQL.

For more details, you can read the full copy of the release notes here:

[https://www.postgresql.org/docs/release/](https://www.postgresql.org/docs/release/)

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

PostgreSQL 9.4 will stop receiving fixes on February 13, 2020. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 12. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_12_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

https://www.postgresql.org/account/submitbug/

Beta Schedule
-------------

This is the third beta release of version 12. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2019. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 12 Beta Release Notes](https://www.postgresql.org/docs/devel/release-12.html)
* [PostgreSQL 12 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_12_Open_Items)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
