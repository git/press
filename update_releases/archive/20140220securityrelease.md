The PostgreSQL Global Development Group has released an important update to all supported versions of the PostgreSQL database system, which includes minor versions 9.3.3, 9.2.7, 9.1.12, 9.0.16, and 8.4.20. This update contains fixes for multiple security issues, as well as several fixes for replication and data integrity issues.  All users are urged to update their installations at the earliest opportunity, especially those using binary replication or running a high-security application.

Security Fixes
--------------

This update fixes [CVE-2014-0060](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0060), in which PostgreSQL did not properly enforce the WITH ADMIN OPTION permission for ROLE management.  Before this fix, any member of a ROLE was able to grant others access to the same ROLE regardless if the member was given the WITH ADMIN OPTION permission.  It also fixes multiple privilege escalation issues, including: [CVE-2014-0061](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0061), [CVE-2014-0062](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0062), [CVE-2014-0063](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0063), [CVE-2014-0064](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0064), [CVE-2014-0065](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0065), and [CVE-2014-0066](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0066).  More information on these issues can be found on [our security page](http://www.postgresql.org/support/security/) and [the security issue detail wiki page](http://wiki.postgresql.org/wiki/20140220securityrelease).

With this release, we are also alerting users to a known security
hole that allows other users on the same machine to gain access to an
operating system account while it is doing "make check": [CVE-2014-0067](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0067).  "Make check" is normally part of building PostgreSQL from source code.  As it is not possible to fix this issue without causing significant issues to our testing infrastructure, a patch will be released separately and publicly.  Until then, users are strongly advised not to run "make check" on machines where untrusted users have accounts.

Replication and Data Integrity Fixes
------------------------------------

This update also fixes some issues which affect binary replication and row locking, and can cause recoverable data corruption in some cases.  There are several fixes to index page locking issue during replication which can cause indexes on the replica to be corrupted.  There is a fix to a transaction freezing bug in version 9.3 which could cause databases which cycled through transaction ID wraparound several times to have old row versions reappear.  We have also fixed three bugs which could cause new standbys to fail to start up.  Finally, this update fixes an issue which could break foreign keys, although the keys themselves will still need to be fixed manually after applying the update.

In version 9.3, these fixes result in the addition of several new server configuration settings to control multixact freezing.  Importantly, standby servers must be updated to 9.3.3 or later before the replication master is updated, or replication will be broken.

Other Improvements
------------------

In addition to the above, the following issues are fixed in this release:

* Fix WAL logging of visibility map change
* Make sure that GIN indexes log all insertions
* Get pause_at_recovery_target to pause at correct time
* Ensure walreceiver sends hot-standby feedback messages on time
* Prevent timeout interrupts from taking control away from mainline code
* Eliminate several race conditions
* Fix some broken HINTs in error messages
* Prevent server lockup on SSL connection loss
* Fix two Unicode handling issues
* Prevent crash on certain subselect syntax
* Prevent crash on select from zero column table
* Fix two bugs with LATERAL
* Fix issue with UNION ALL, partitioning, and updates
* Ensure that ANALYZE understands domains over ranges
* Eliminate permissions check when using default tablespace
* Fix memory leakage in JSON functions
* Allow extensions with event triggers
* Distinguish numbers correctly in JSON output
* Fix permissions for pg_start_backup() and pg_stop_backup()
* Accept SHIFT_JIS as locale name
* Fix .* expansion for SQL function variables
* Prevent infinite loop on some COPY connection failures
* Several fixes for client issues on Windows
* Enable building PostgreSQL with Visual Studio 2013
* Update time zone files for recent changes

There are also fixes to all of the following optional modules (extensions): ECPG, dblink, ISN, pgbench, pg_stat_statements and postgres_fdw. Additional changes and details of some of the above issues can be found in the Release Notes.

As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.  Users who have skipped multiple update releases may need to perform additional post-update steps; see the Release Notes for details.

Links:
  * [Download](http://postgresql.org/download)
  * [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
  * [Security Page](http://www.postgresql.org/support/security/)
  * [Security Issue Detail Page](http://wiki.postgresql.org/wiki/20140220securityrelease).