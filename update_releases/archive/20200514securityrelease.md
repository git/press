2020-05-14 Cumulative Security Update
====================================

The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 12.3, 11.8, 10.13, 9.6.18, and
9.5.22.  This release fixes one security issue found in the PostgreSQL
server and over 75 bugs reported over the last three months.

Please plan to update at your earliest convenience.

Security Issues
---------------

* CVE-2020-10733: Windows installer runs executables from uncontrolled directories.

Versions Affected: 9.5 - 12. The security team does not test unsupported versions, but this issue existed before PostgreSQL 9.5.

The Windows installer for PostgreSQL invokes system-provided executables that do not have fully-qualified paths. Executables in the directory where the installer loads or the current working directory take precedence over the intended executables. An attacker having permission to add files into one of those directories can use this to execute arbitrary code with the installer's administrative rights.

The PostgreSQL project thanks Hou JingYi (@hjy79425575) for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update also fixes over 75 bugs that were reported in the last several
months. Some of these issues affect only version 12, but may also affect all
supported versions.

Some of these fixes include:

* Several fixes for GENERATED columns, including an issue where it was possible to crash or corrupt data in a table when the output of the generated column was the exact copy of a physical column on the table, e.g. if the expression called a function which could return its own input.
* Several fixes for `ALTER TABLE`, including ensuring the `SET STORAGE` directive is propagated to a table's indexes.
* Fix a potential race condition when using `DROP OWNED BY` while another session is deleting the same objects.
* Allow for a partition to be detached when it has inherited ROW triggers.
* Several fixes for `REINDEX CONCURRENTLY`, particularly with issues when a `REINDEX CONCURRENTLY` operation fails.
* Fix crash when COLLATE is applied to an uncollatable type in a partition bound expression.
* Fix performance regression in floating point overflow/underflow detection.
* Several fixes for full text search, particularly with phrase searching.
* Fix query-lifespan memory leak for a set-returning function used in a query's FROM clause.
* Several reporting fixes for the output of `VACUUM VERBOSE`.
* Allow input of type circle to accept the format `(x,y),r`, which is specified in the documentation.
* Allow for the `get_bit()` and `set_bit()` functions to not fail on `bytea` strings longer than 256MB.
* Avoid premature recycling of WAL segments during crash recovery, which could lead to WAL segments being recycled before being archived.
* Avoid attempting to fetch nonexistent WAL files from archive storage during recovery by skipping irrelevant timelines.
* Several fixes for logical replication and replication slots.
* Fix several race conditions in synchronous standby management, including one that occurred when changing the `synchronous_standby_names` setting.
* Several fixes for GSSAPI support, include a fix for a memory leak that occurred when using GSSAPI encryption.
* Ensure that members of the `pg_read_all_stats` role can read all statistics views.
* Fix performance regression in information_schema.triggers view.
* Fix memory leak in libpq when using `sslmode=verify-full`.
* Fix crash in `psql` when attempting to re-establish a failed connection.
* Allow tab-completion of the filename argument to `\gx` command in `psql`.
* Add `pg_dump` support for `ALTER ... DEPENDS ON EXTENSION`.
* Several other fixes for `pg_dump`, which include dumping comments on RLS policies and postponing restore of event triggers until the end.
* Ensure `pg_basebackup` generates valid tar files.
* `pg_checksums` skips tablespace subdirectories that belong to a different PostgreSQL major version
* Several Windows compatibility fixes


This update also contains tzdata release 2020a for DST law changes in Morocco
and the Canadian Yukon, plus historical corrections for Shanghai. The
America/Godthab zone has been renamed to America/Nuuk to reflect current English usage
; however, the old name remains available as a compatibility link. This
also updates initdb's list of known Windows time zone names to include recent
additions.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/current/release.html).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/current/release.html).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/current/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
