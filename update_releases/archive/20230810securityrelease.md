The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 15.4, 14.9, 13.12, 12.16, and 11.21, as well
as the third beta release of PostgreSQL 16. This release fixes two security
vulnerabilities and over 40 bugs reported over the last several months.

If you use [BRIN]((https://www.postgresql.org/docs/current/brin-intro.html))
indexes to  look up `NULL` values, you will need to [reindex](https://www.postgresql.org/docs/current/sql-reindex.html)
them after upgrading to this release. On PostgreSQL 12 and above, you can use
[`REINDEX CONCURRENTLY`](https://www.postgresql.org/docs/current/sql-reindex.html)
to avoid blocking writes to the affected index and table, for example:

```
REINDEX INDEX CONCURRENTLY your_index_name;
```

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 11 EOL Notice
------------------------

PostgreSQL 11 will stop receiving fixes on November 9, 2023. If you are
running PostgreSQL 11 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2023-39417](https://www.postgresql.org/support/security/CVE-2023-39417/): Extension script `@substitutions@` within quoting allow SQL injection.

Supported, Vulnerable Versions: 11 - 15.  The security team typically does not
test unsupported versions, but this problem is quite old.

An [extension script](https://www.postgresql.org/docs/current/extend-extensions.html)
is vulnerable if it uses `@extowner@`, `@extschema@`, or `@extschema:...@`
inside a quoting construct (dollar quoting, `''`, or `""`). No bundled extension
is vulnerable. Vulnerable uses do appear in a documentation example and in
non-bundled extensions. Hence, the attack prerequisite is an administrator
having installed files of a vulnerable, trusted, non-bundled extension. Subject
to that prerequisite, this enables an attacker having database-level `CREATE`
privilege to execute arbitrary code as the bootstrap superuser. PostgreSQL will
block this attack in the core server, so there's no need to modify individual
extensions.

The PostgreSQL project thanks Micah Gate, Valerie Woolard, Tim Carey-Smith,
and Christoph Berg for reporting this problem.

### [CVE-2023-39418](https://www.postgresql.org/support/security/CVE-2023-39418/): `MERGE` fails to enforce `UPDATE` or `SELECT` row security policies.

Supported, Vulnerable Versions: 15.

PostgreSQL 15 introduced the [`MERGE`](https://www.postgresql.org/docs/current/sql-merge.html)
command, which fails to test new rows against row security policies defined for
[`UPDATE`]((https://www.postgresql.org/docs/current/sql-update.html)) and
[`SELECT`]((https://www.postgresql.org/docs/current/sql-select.html)). If `UPDATE`
and `SELECT` policies forbid some row that `INSERT` policies do not forbid, a
user could store such rows.  Subsequent consequences are application-dependent.
This affects only databases that have used
[`CREATE POLICY`](https://www.postgresql.org/docs/current/sql-createpolicy.html)
to define a row security policy.

The PostgreSQL project thanks Dean Rasheed for reporting this problem.

A Note on the PostgreSQL 16 Beta
--------------------------------

This release marks the third beta release of PostgreSQL 16 and puts the
community one step closer to general availability tentatively around the end of
the third quarter.

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 16 on your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 16 Beta 3 in production environments, we encourage you to find ways
to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that PostgreSQL 16
upholds our standards of delivering a stable, reliable release of the world's
most advanced open source relational database. Please read more about our
[beta testing process](https://www.postgresql.org/developer/beta/) and how you
can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

You can find information about all of the PostgreSQL 16 features and changes in
the [release notes](https://www.postgresql.org/docs/16/release-16.html):

  [https://www.postgresql.org/docs/16/release-16.html](https://www.postgresql.org/docs/16/release-16.html)

Bug Fixes and Improvements
--------------------------
 
This update fixes over 40 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 15. Some of these issues may also
affect other supported versions of PostgreSQL.

* Fix for handling of `NULL` values in [BRIN](https://www.postgresql.org/docs/current/brin-intro.html)
indexes. This fix does not apply to existing BRIN indexes -- you will need to
run [`REINDEX`](https://www.postgresql.org/docs/current/sql-reindex.html) to fix
any BRIN indexes used to search for NULL values.
* Avoid leaving a corrupted database behind when DROP DATABASE is interrupted.
* Several fixes for partitioned indexes.
* Fix [`ALTER EXTENSION ... SET SCHEMA`](https://www.postgresql.org/docs/current/sql-alterextension.html)
to error if the extension contains any objects outside the extension's schema.
* Fix dependency tracking of access methods for tables.
* Don't use partial unique indexes for uniqueness proofs in the planner.
* Correctly handle sub-SELECTs in RLS policy expressions and security-barrier
views when expanding rule actions.
* Fix race conditions in conflict detection for `SERIALIZABLE` transaction isolation mode.
* Fix intermittent failures when trying to update a field of a composite column
that requires [out-of-line TOASTing](https://www.postgresql.org/docs/current/storage-toast.html).
* Fix several memory leaks that occurred during the lifespan of a query.
* Accept fractional seconds in the input to the [jsonpath `datetime()`](https://www.postgresql.org/docs/current/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS)
method.
* Increase token limit in `pg_hba.conf` and `pg_ident.conf` to 10,240 bytes.
* An out-of-memory error from JIT will now cause a PostgreSQL `FATAL` error instead of a C++ exception.
* Allow `VACUUM` to continue after detecting certain types of B-tree index corruption. While this fix allows VACUUM to continue, you still need to `REINDEX` to fix the broken index.
* Avoid double replay of prepared transactions during crash recovery.
* Ensure that checkpoint calls `fsync` on a newly created but empty table.
* Silence "missing contrecord" errors to avoid logging inaccurate messages from
`pg_waldump` and `walsender`.
* Fix the [fuzzystrmatch](https://www.postgresql.org/docs/current/fuzzystrmatch.html
[Soundex](https://www.postgresql.org/docs/current/fuzzystrmatch.html#id-1.11.7.26.6)
`difference()` function to correctly handle empty input.
* Several fixes for [`intarray`](https://www.postgresql.org/docs/current/intarray.html),
including disallowing oversize input arrays in a GiST index.
* Fix [`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html) to
correctly handle SQL-standard function bodies (`BEGIN ATOMIC`) that require
parse-time dependencies on unique indexes.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

Fixes in PostgreSQL 16 Beta 3
-----------------------------

* Add the `\drg` command to `psql` to display information about role grants.
* Add timeline ID to filenames generated with `pg_waldump --save-fullpage`.
* Fix crash after a deadlock occurs in a parallel `VACUUM` worker.

Please see the [release notes](https://www.postgresql.org/docs/16/release-16.html)
for a complete list of new and changed features:

  [https://www.postgresql.org/docs/16/release-16.html](https://www.postgresql.org/docs/16/release-16.html)

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

If you use [BRIN]((https://www.postgresql.org/docs/current/brin-intro.html))
indexes to  look up `NULL` values, you will need to [reindex](https://www.postgresql.org/docs/current/sql-reindex.html)
them after upgrading to this release. On PostgreSQL 12 and above, you can use
[`REINDEX CONCURRENTLY`](https://www.postgresql.org/docs/current/sql-reindex.html)
to avoid blocking writes to the affected index and table, for example:

```
REINDEX INDEX CONCURRENTLY your_index_name;
```

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Updating to PostgreSQL 16 Beta 3
--------------------------------

To upgrade to PostgreSQL 16 Beta 3 from a previous PostgreSQL 16 Beta version,
or a previous major version of PostgreSQL, you will need to use a strategy
similar to upgrading between major versions of PostgreSQL (e.g. `pg_upgrade` or
`pg_dump` / `pg_restore`). For more information, please visit the documentation
section on [upgrading](https://www.postgresql.org/docs/16/static/upgrading.html).

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 16. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_16_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 16 Beta Release Notes](https://www.postgresql.org/docs/16/release-16.html)
* [PostgreSQL 16 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_16_Open_Items)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
