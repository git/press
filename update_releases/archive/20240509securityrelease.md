The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 16.3, 15.7, 14.12, 13.15, and 12.19.
This release fixes one security vulnerability and over 55 bugs reported
over the last several months.

A security vulnerability was found in the system views
[`pg_stats_ext`](https://www.postgresql.org/docs/current/view-pg-stats-ext.html)
and [`pg_stats_ext_exprs`](https://www.postgresql.org/docs/current/view-pg-stats-ext-exprs.html),
potentially allowing authenticated database users to see data they don't have
sufficient privileges to view. The fix for this vulnerability only fixes
fresh PostgreSQL installations, namely those that are created with the
[`initdb`](https://www.postgresql.org/docs/current/app-initdb.html) utility
after this fix is applied. If you have a current PostgreSQL installation and are
concerned about this issue, please follow the instructions in the "Updating"
section for remediation steps.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 12 EOL Notice
------------------------

PostgreSQL 12 will stop receiving fixes on November 14, 2024. If you are
running PostgreSQL 12 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2024-4317](https://www.postgresql.org/support/security/CVE-2024-4317/): Restrict visibility of `pg_stats_ext` and `pg_stats_ext_exprs` entries to the table owner

CVSS v3.1 Base Score: [3.1](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N)

Supported, Vulnerable Versions: 14 - 16.

Missing authorization in PostgreSQL built-in views
[`pg_stats_ext`](https://www.postgresql.org/docs/current/view-pg-stats-ext.html)
and [`pg_stats_ext_exprs`](https://www.postgresql.org/docs/current/view-pg-stats-ext-exprs.html)
allows an unprivileged database user to read most common values and other
statistics from [`CREATE STATISTICS`](https://www.postgresql.org/docs/current/sql-createstatistics.html)
commands of other users. The most common values may reveal column values the
eavesdropper could not otherwise read or results of functions they cannot
execute.

This fix only fixes fresh PostgreSQL installations, namely those that are
created with the [`initdb`](https://www.postgresql.org/docs/current/app-initdb.html)
utility after this fix is applied. If you have a current PostgreSQL installation
and are concerned about this issue, please follow the instructions in the
"Updating" section for remediation steps.

The PostgreSQL project thanks Lukas Fittl for reporting this problem.

Bug Fixes and Improvements
--------------------------
 
This update fixes over 55 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 16. Some of these issues may also
affect other supported versions of PostgreSQL.

* Fix issue with [`INSERT`](https://www.postgresql.org/docs/current/sql-insert.html)
with a multi-row [`VALUES`](https://www.postgresql.org/docs/current/sql-values.html)
clause where a target column is a domain over an array or composite type.
* Require the [SELECT privilege](https://www.postgresql.org/docs/current/sql-grant.html)
on the target table when using [`MERGE`](https://www.postgresql.org/docs/current/sql-merge.html)
when using `MERGE ... DO NOTHING`.
* Per the SQL standard, throw an error if a target row in `MERGE` joins to more
than one source row during a modification.
* Fix incorrect pruning of `NULL` partition when a table is partitioned on
a boolean column and the query has a boolean `IS NOT` clause.
* Make [`ALTER FOREIGN TABLE ... SET SCHEMA`](https://www.postgresql.org/docs/current/sql-alterforeigntable.html)
move any owned sequences into the new schema.
* [`CREATE DATABASE`](https://www.postgresql.org/docs/current/sql-createdatabase.html)
now recognizes `STRATEGY` keywords case-insensitively.
* Fix how [EXPLAIN](https://www.postgresql.org/docs/current/sql-explain.html)
counts heap pages during bitmap heap scan to show all counted pages, not just
ones with visible tuples.
* Avoid deadlock during removal of orphaned temporary tables.
* Several fixes for [`VACUUM`](https://www.postgresql.org/docs/current/sql-vacuum.html),
including one that can reduce unnecessary I/O.
* Several query planner fixes.
* Add optimization for certain operations where an installation has thousands
of roles.
* Fix confusion for SQL-language procedures that return a single composite-type
column.
* Fix incorrect rounding and overflow hazards in [`date_bin()`](https://www.postgresql.org/docs/current/functions-datetime.html#FUNCTIONS-DATETIME-BIN).
* Detect integer overflow when adding or subtracting an
[interval](https://www.postgresql.org/docs/current/datatype-datetime.html#DATATYPE-INTERVAL-INPUT)
to/from a timestamp.
* Fix several race conditions with logical replication, including determining if
a table sync operation is required.
* Disconnect if a new server session's client socket cannot be put into non-blocking mode.
* [`initdb -c`](https://www.postgresql.org/docs/current/app-initdb.html) now
matches parameter names case-insensitively.
* Fix how PL/pgSQL parses of [single-line comments](https://www.postgresql.org/docs/current/plpgsql-structure.html) (`-- style comments`)
following expression.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

For existing installations that are impacted by CVE-2024-4317 that wish to
remediate the issue, you will have to perform the following steps:

1. Find the SQL script `fix-CVE-2024-4317.sql` in the `share` directory of
your PostgreSQL installation (e.g. in `/usr/share/postgresql/`), or download it
from the PostgreSQL git repository from one of the URLs below. You will need to
use the script that matches your major version:

- [PostgreSQL 16](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_16_STABLE): [https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_16_STABLE](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_16_STABLE)
- [PostgreSQL 15](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_15_STABLE): [https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_15_STABLE](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_15_STABLE)
- [PostgreSQL 14](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_14_STABLE): [https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_14_STABLE](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/catalog/fix-CVE-2024-4317.sql;hb=refs/heads/REL_14_STABLE)

From the above URLs, you can click the URL that says "raw" to download a version
that you can copy and paste.

Be sure to use the script appropriate to your PostgreSQL major version. If you
do not see this file, either your version is not vulnerable (only PostgreSQL 14,
15, and 16 are affected) or your minor version is too old to have the fix.

2. In each database of the cluster, run the `fix-CVE-2024-4317.sql` script as
a database superuser. For example, in [`psql`](https://www.postgresql.org/docs/current/app-psql.html),
with the file located in `/usr/share/postgresql/`, this command would look like:

```
\i /usr/share/postgresql/fix-CVE-2024-4317.sql
```

3. You must also execute this script in the `template0` and `template1`
databases, or the vulnerability will still exist in databases you create later.
To fix `template0`, you'll need to temporarily allow it accept connections. You
can do this with the following command:

```
ALTER DATABASE template0 WITH ALLOW_CONNECTIONS true;
```

After executing the `fix-CVE-2024-4317.sql` script in `template0` and
`template1`, you should revoke the ability for `template0` to accept
connections. You can do this with the following command:

```
ALTER DATABASE template0 WITH ALLOW_CONNECTIONS false;
```

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [PostgreSQL 16 Release Announcement](https://www.postgresql.org/about/press/)
* [Follow @postgresql on X/Twitter](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
