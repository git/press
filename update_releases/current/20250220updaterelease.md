The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 17.4, 16.8, 15.12, 14.17, and 13.20.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Bug Fixes and Improvements
--------------------------
 
The issues listed below affect PostgreSQL 17. Some of these issues may also
affect other supported versions of PostgreSQL.

* Improve behavior of quoting functions in [`libpq`](https://www.postgresql.org/docs/current/libpq.html).
The fix for [CVE-2025-1094](https://www.postgresql.org/support/security/CVE-2025-1094/)
caused the quoting functions to not honor their string length parameters and, in
some cases, cause crashes. This problem could be noticeable from a PostgreSQL
client library, based on how it is integrated with `libpq`.
* Fix small memory leak in
[`pg_createsubscriber`](https://www.postgresql.org/docs/current/app-pgcreatesubscriber.html).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional
post-update steps; please see the release notes from earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Donate](https://www.postgresql.org/about/donate/)

If you have corrections or suggestions for this release announcement, please
send them to the _pgsql-www@lists.postgresql.org_ public
[mailing list](https://www.postgresql.org/list/).
