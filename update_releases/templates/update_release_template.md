{{ DATE_HERE }} Cumulative Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including {{ LIST OF MINOR VERSIONS }}. This release fixes {{ 1-3 BIGGEST ISSUES }}. It also patches a number of other bugs reported over the last three months.  
{{ %IF MAJOR_ISSUE AFFECTS ONLY SOME USERS }}
Users who are affected by {{ MAJOR ISSUE }} should update as soon as possible.  Other users should plan to update at the next convenient downtime.
{{ %ELSEIF MAJOR_ISSUE AFFECTS MOST USERS }}
Users of {{ AFFECTED_VERSIONS }} should plan to update {{ immediately | as soon as possible | at the next available opportunity }}
{{ %ELSE }}
Users should plan to apply this update at the next scheduled downtime.
{{ %ENDIF }}
{{ SEE http://www.databasesoup.com/2013/10/how-urgent-is-that-update.html }}

{{ MAJOR_ISSUE }}
---------------

This update fixes {{ MAJOR_ISSUE }}, which {{ DESCRIPTION OF ISSUE }}.  
{{ EXTRA NOTES ON FIX AND COMPATIBILITY ISSUES }}

Bug Fixes and Improvements
--------------------------

This update also fixes a number of bugs reported in the last few months. Some
of these issues affect only version {{ CURRENT_VERSION }}, but many affect
all supported versions:

{{ LIST OF PATCHED ISSUES FROM GITLOG AND/OR RELEASE NOTES }}

{{ EXTRA NOTES ON SPECIAL ISSUE IF REQUIRED }}

This update also contains tzdata release {{ TZDATA_RELEASE }},
with updates for {{ LIST OF TIME ZONES }}.

{{ %IF 2 to 5 MONTHS FROM EOL }}
EOL Warning for Version {{ EOL VERSION }}
-----------------------------------------

PostgreSQL version {{ EOL VERSION }} will be End-of-Life in {{ MONTH YEAR }}.  The project expects to only release one more update for that version.  We urge users to start planning an upgrade to a later version of PostgreSQL as soon as possible. See our Versioning Policy for more information.

{{ %ENDIF }}

{{ %IF 0 to 1 MONTHS FROM EOL }}
EOL Notice for Version {{ EOL VERSION }}
-----------------------------------------

PostgreSQL version {{ EOL VERSION }} is now End-of-Life (EOL).  No additional updates or security patches
will be released by the community for this version.  Users still on {{ EOL VERSION }}
are urged to upgrade as soon as possible.  See our Versioning Policy for more information.

{{ %ENDIF }}

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.
{{ %IF POST-UPDATE STEPS }}
After update, users will need to {{ BRIEF DESC OF UPDATE STEPS }}.  See the Release Notes for more details.
{{ %ELSE }}
Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.
{{ %ENDIF }}

Links:
* [Download](https://www.postgresql.org/download)
* [Release Notes](https://www.postgresql.org/docs/current/static/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
