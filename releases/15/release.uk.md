﻿6 жовтня, 2022 - PostgreSQL Global Development Group сьогодні повідомила про випуск [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html), останньої версії [найсучаснішої бази даних з відкритим вихідним кодом у світі](https://www.postgresql.org/).

PostgreSQL 15 ґрунтується на покращенні продуктивності останніх випусків із помітними перевагами для керування робочими навантаженнями як у локальних, так і в розподілених розгортаннях, включаючи покращене сортування. Випуск покращує роботу розробника завдяки додаванню популярної команди [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) та дає більше можливостей спостереження за станом бази даних.

«Спільнота розробників PostgreSQL продовжує створювати функції, що спрощують роботу з даними у процесах з високим навантаженням, одночасно вдосконалюючи роботу розробника», -  говорить Джонатан Кац, член PostgreSQL Core Team. «PostgreSQL 15 підкреслює, як за допомогою відкритої розробки програмного забезпечення ми можемо надати користувачам базу даних, яка чудово підходить для розробки програм і безпечна для важливих даних».

[PostgreSQL](https://www.postgresql.org) — інноваційна система керування даними, відома своєю надійністю та міцністю, завдяки відкритому коду протягом 25 років розвивається розробниками [глобальної спільноти](https://www.postgresql.org/community/) і стала реляційною базою даних з відкритим кодом, яку обирають організації всіх розмірів.

### Покращена продуктивність сортування та стиснення

В останньому випуску PostgreSQL вдосконалила алгоритми [сортування](https://www.postgresql.org/docs/15/queries-order.html) в пам’яті та на диску, еталонні показники демонструють пришвидшення на 25% - 400% в залежності від типу даних, що сортуються. Використання `row_number()`, `rank()`, `dense_rank()` та `count()` у якості [віконних функцій](https://www.postgresql.org/docs/15/functions-window.html) також має переваги в продуктивності в PostgreSQL 15. Запити з використанням [`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT) тепер можуть [виконуватися паралельно](https://www.postgresql.org/docs/15/parallel-query.html).

Спираючись на напрацювання [попереднього випуску PostgreSQL](https://www.postgresql.org/about/press/presskit14/) для дозволу асинхронних віддалених запитів, [джерело сторонніх данних PostgreSQL (FDW)](https://www.postgresql.org/docs/15/postgres-fdw.html), [`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html), тепер підтримує [асинхронні  коміти](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

Покращення продуктивності в PostgreSQL 15 розповсюджуються на можливості архівування та резервного копіювання. PostgreSQL 15 додає підтримку LZ4 і Zstandard (zstd) [стиснення до файлів журналу попереднього запису (WAL)](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION), які мають переваги як в плані економії місця, так і в плані продуктивності для певних робочих навантажень. На певних операційних системах PostgreSQL 15 додає підтримку [попередньої вибірки сторінок, на які є посилання в WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH), щоб допомогти прискорити час відновлення. Вбудована команда резервного копіювання PostgreSQL [`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html) тепер підтримує стиснення файлів резервних копій на стороні сервера з вибором між gzip, LZ4 і zstd. PostgreSQL 15 дозволяє використання [нестандартних модулів архівації](https://www.postgresql.org/docs/15/archive-modules.html), що усуває накладні витрати, пов'язані з використанням командного інтерпретатора.

### Промовисті можливості розробника

PostgreSQL 15 включає стандартну команду SQL [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html). `MERGE` дозволяє використовувати умовні конструкції, що можуть містити дії `INSERT`, `UPDATE` та `DELETE` в рамках однієї команди.

Останній випуск додає [нові функції для використання регулярних виразів](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP) для перевірки рядків: `regexp_count()`, `regexp_instr()`, `regexp_like()` та `regexp_substr()`. PostgreSQL 15 також розширює функцію `range_agg` для агрегації [типів даних `multirange`](https://www.postgresql.org/docs/15/rangetypes.html), представлених у [попередньому випуску](https://www.postgresql.org/about/press/presskit14/).

PostgreSQL 15 дозволяє користувачам [створювати подання, що запитують дані, використовуючи дозволи того, хто викликає, а не того, хто створив подання](https://www.postgresql.org/docs/15/sql-createview.html). Ця опція під назвою `security_invoker`додає додатковий шар захисту, щоб переконатися, що користувачі, які викликають подання, мають правильні дозволи для роботи з основними даними.

### Більше можливостей з логічною реплікацією

PostgreSQL 15 забезпечує більшу гнучкість для керування [логічною реплікацією](https://www.postgresql.org/docs/15/logical-replication.html). У цьому випуску впроваджено [фільтрування рядків](https://www.postgresql.org/docs/15/logical-replication-row-filter.html) та [списків стовпців](https://www.postgresql.org/docs/15/logical-replication-col-lists.html) для [видавців](https://www.postgresql.org/docs/15/logical-replication-publication.html), що дозволяє користувачам вибирати з таблиці піднабір даних для реплікації. PostgreSQL 15 додає функції для спрощення [управління конфліктами](https://www.postgresql.org/docs/15/logical-replication-conflicts.html), включаючи можливість пропустити відтворення конфліктної транзакції і автоматичне відключення підписки, якщо виявлено помилку. Цей випуск також має підтримку використання двоетапного затвердження (2PC) з логічною реплікацією.

### Покращення журналювання та конфігурації

PostgreSQL 15 впроваджує новий формат журналювання: [`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG). Він виводить журнал даних за допомогою визначеної структури JSON, що дозволяє обробляти журнали PostgreSQL у структурованих системах журналювання.

Цей випуск надає адміністраторам баз даних більше гнучкості в тому, як користувачі можуть управляти конфігурацією PostgreSQL, додавши можливість надавати користувачам дозвіл змінювати параметри конфігурації на рівні сервера. Окрім того, користувачі тепер можуть шукати інформацію про конфігурацію за допомогою команди `\dconfig` з інструменту командного рядка [`psql`](https://www.postgresql.org/docs/15/app-psql.html).

### Інші істотні зміни

PostgreSQL [статистика на рівні сервера](https://www.postgresql.org/docs/15/monitoring-stats.html) тепер збирається в загальній пам’яті, що виключає як процес збору статистики, так і періодичний запис цих даних на диск.

У PostgreSQL 15 з’явилася можливість [сортування ICU](https://www.postgresql.org/docs/15/collation.html) за замовчуванням для кластера або окремої бази даних.

У цьому випуску також з’явилося нове вбудоване розширення, [`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html), що дозволяє користувачам переглядати вміст файлів журналів попереднього запису безпосередньо з інтерфейсу SQL.

PostgreSQL 15 також [відкликає дозвіл `CREATE` усіх користувачів](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS), окрім власника бази даних, зі схеми `public` (або за замовчуванням).

Із PostgreSQL 15 видалено й давно застарілий режим "ексклюзивного резервного копіювання" та підтримку Python 2 з PL/Python.

### Про PostgreSQL

[PostgreSQL](https://www.postgresql.org) — це найдосконаліша в світі база даних з відкритим вихідним кодом та глобальною спільнотою, що налічує тисячі користувачів, контриб'юторів, компаній та організацій. Побудована на основі більш ніж 35-річної інженерної роботи, що започаткована в Каліфорнійському університеті в Берклі. PostgreSQL продовжує розвиватися неперевершеними темпами. Зрілий набір функцій PostgreSQL не тільки відповідає найкращим пропрієтарним системам управління базами даних, але й перевершує їх у функціоналі, розширюваності, безпеці та стабільності.

### Посилання

* [Скачати](https://www.postgresql.org/download/)
* [Примітки до випуску](https://www.postgresql.org/docs/15/release-15.html)
* [Прес-реліз](https://www.postgresql.org/about/press/)
* [Сторінка безпеки](https://www.postgresql.org/support/security/)
* [Політика версіонування](https://www.postgresql.org/support/versioning/)
* [Слідкуйте за @postgresql на Twitter](https://twitter.com/postgresql)

## Більше про функціонал

Із роз'ясненнями щодо вищезазначених та інших функцій можна ознайомитися на таких ресурсах:

* [Примітки до випуску](https://www.postgresql.org/docs/15/release-15.html)
* [Матриця функцій](https://www.postgresql.org/about/featurematrix/)

## Де скачати

Завантажити PostgreSQL 15 можна кількома способами, у тому числі:

* із [офіційної сторінки завантаження](https://www.postgresql.org/download/), що містить інсталятори та інструменти для [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) та багато іншого.
* [вихідний код](https://www.postgresql.org/ftp/source/v15.0)

Інші інструменти та розширення доступні через мережу розширень [PostgreSQL Extension Network](http://pgxn.org/).

## Документація

PostgreSQL 15 постачається як з документацією у форматі HTML, так й у вигляді man-сторінок. Також доступна онлайн-документація у форматах [HTML](https://www.postgresql.org/docs/15/) і [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf).

## Ліцензія

PostgreSQL використовує ліцензію [PostgreSQL License](https://www.postgresql.org/about/licence/), BSD-подібну "дозвільну" ліцензію. Ця [ліцензія сертифікована OSI](http://www.opensource.org/licenses/postgresql/) і вважається широкоприйнятною як гнучка й дружня до бізнесу, тому що не обмежує використання PostgreSQL комерційними й закритими продуктами. Разом з підтримкою від багатьох компаній і публічним володінням коду, наша ліцензія робить PostgreSQL дуже популярною серед компаній, які бажають вбудувати базу даних у свій власний продукт без страху, обмежень, залежностей або змін ліцензійних умов.

## Контакти

Вебсайт

* [https://www.postgresql.org/](https://www.postgresql.org/)

Електронна пошта

* [press@postgresql.org](mailto:press@postgresql.org)

## Зображення та логотипи

Postgres і PostgreSQL, а також логотип зі слоном (Elephant Logo Slonik),є зареєстрованими торговими марками [PostgreSQL Community Association of Canada](https://www.postgres.ca). Якщо ви бажаєте використати ці торгові марки, ви маєте дотримуватися вимог [політики  використання торгових марок](https://www.postgresql.org/about/policies/trademarks/).

## Корпоративна підтримка

PostgreSQL користується підтримкою багатьох компаній, які спонсорують розробників, надають хостингові ресурси та фінансову підтримку. Перегляньте нашу [спонсорську сторінку](https://www.postgresql.org/about/sponsors/) з переліком деяких прихильників проекту.

Існує також велика спільнота [компаній, що пропонують професійну підтримку PostgreSQL](https://www.postgresql.org/support/professional_support/) від індивідуальних консультантів до багатонаціональних компаній.

Якщо ви бажаєте зробити фінансовий внесок для PostgreSQL Global Development Group або для однієї з визнаних неприбуткових організацій, будь ласка, відвідайте сторінку для [пожертвувань](https://www.postgresql.org/about/donate/).