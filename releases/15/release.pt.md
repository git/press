6 de Outubro de 2022 - O Grupo de Desenvolvimento Global do PostgreSQL anunciou
hoje o lançamento do [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html),
a versão mais recente do [banco de dados de código aberto mais avançado do mundo](https://www.postgresql.org/).

PostgreSQL 15 se baseia nas melhorias de performance das versões recentes com
ganhos notáveis no gerenciamento de cargas de trabalho em implantações locais e
distribuídas, incluindo melhoria na ordenação. Esta versão melhora a
experiência do desenvolvedor com a adição do popular comando
[MERGE](https://www.postgresql.org/docs/15/sql-merge.html), e inclui mais
recursos para observar o estado do banco de dados.

"A comunidade de desenvolvedores do PostgreSQL continua desenvolvendo
funcionalidades que simplificam a execução em alta performance de carga de dados
enquanto melhora a experiência do desenvolvedor", disse Jonathan Katz, um
membro do Grupo de Desenvolvimento Global do PostgreSQL. "PostgreSQL 15 destaca
como, através do desenvolvimento de software de código aberto, nós podemos
entregar aos nossos usuários um banco de dados que é ótimo para o
desenvolvimento de aplicações e seguro para os seus dados críticos."

[PostgreSQL](https://www.postgresql.org), um sistema de gerenciamento de dados
inovador conhecido pela sua confiabilidade e robustez, se beneficia de mais de
25 anos de desenvolvimento de código aberto de uma [comunidade global de
desenvolvedores](https://www.postgresql.org/community/) e se tornou o banco de
dados relacional de código aberto preferido pelas organizações de todos os
tamanhos.

### Melhorias na Performance da Ordenação e na Compressão

Nessa última versão, o PostgreSQL melhorou os seus algoritmos de
[ordenação](https://www.postgresql.org/docs/15/queries-order.html) em memória e
em disco, com benchmarks mostrando melhorias de 25% a 400% dependendo dos tipos
de dados que são ordenados. Utilizando `row_number()`, `rank()`, `dense_rank()`
e `count()` como [funções
deslizantes](https://www.postgresql.org/docs/15/functions-window.html) também
tem benefícios de performance no PostgreSQL 15. Consultas utilizando [`SELECT
DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT)
agora podem ser [executadas em
paralelo](https://www.postgresql.org/docs/15/parallel-query.html).

Baseando-se no trabalho da [versão anterior do
PostgreSQL](https://www.postgresql.org/about/press/presskit14/) que permite
consultas remotas assíncronas, o [adaptador de dados externos do
PostgreSQL](https://www.postgresql.org/docs/15/postgres-fdw.html),
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html), agora
suporta [efetivação assíncrona de
transações](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

As melhorias de performance no PostgreSQL 15 se estendem as facilidades de
arquivamento e cópia de segurança. O PostgreSQL 15 adiciona suporte a
[compressão de arquivos de log de transação
(WAL)](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION)
utilizando LZ4 e Zstandard (zstd), que pode ter benefícios de espaço e
desempenho para determinadas cargas de trabalho. Em alguns sistemas
operacionais, o PostgreSQL 15 adiciona suporte a [obtenção prévia de páginas do
WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)
para ajudar a acelerar os tempos de recuperação. O comando de cópia de
segurança integrado do PostgreSQL,
[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html),
agora suporta compressão de arquivos de cópia de segurança do lado do servidor
com a opção de gzip, LZ4 e zstd. O PostgreSQL 15 inclui a habilidade de
utilizar [módulos personalizados para
arquivamento](https://www.postgresql.org/docs/15/archive-modules.html), que
eliminam o tempo adicional ao executar um comando shell.

### Recursos Expressivos para Desenvolvedor

O PostgreSQL 15 inclui o comando
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) do padrão SQL.
`MERGE` permite escrever comandos SQL condicionais que podem incluir comandos
`INSERT`, `UPDATE` e `DELETE` em um único comando.

Esta última versão adiciona [novas funções para utilização de expressões
regulares](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP)
para inspecionar cadeias de caracteres: `regexp_count()`, `regexp_instr()`,
`regexp_like()` e `regexp_substr()`. O PostgreSQL 15 também estende a função
`range_agg` para agregar [tipos de dados
`multirange`](https://www.postgresql.org/docs/15/rangetypes.html), que foram
adicionados na [versão
anterior](https://www.postgresql.org/about/press/presskit14/).

O PostgreSQL 15 permite que usuários [criem visões que consultem dados
utilizando a permissão do usuário que invoca e não do criador da
visão](https://www.postgresql.org/docs/15/sql-createview.html). Esta opção,
chamada `security_invoker`, inclui uma camada adicional de proteção que garante
que usuários que invocam a visão tenham as permissões corretas para trabalhar
com os dados subjacentes.

### Mais Opções para Replicação Lógica

PostgreSQL 15 fornece mais flexibilidade para gerenciamento de [replicação
lógica](https://www.postgresql.org/docs/15/logical-replication.html). Esta
versão introduz [filtro de
registros](https://www.postgresql.org/docs/15/logical-replication-row-filter.html)
e [lista de
colunas](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)
para
[publicador](https://www.postgresql.org/docs/15/logical-replication-publication.html),
permitindo que usuários escolham replicar um subconjunto de dados de uma
tabela. O PostgreSQL 15 adiciona funcionalidades para simplificar o
[gerenciamento de
conflito](https://www.postgresql.org/docs/15/logical-replication-conflicts.html),
incluindo a habilidade de ignorar a aplicação de uma transação conflitante e de
automaticamente desabilitar a subscrição se um erro for detectado. Esta versão
também include suporte a utilização de efetivação em duas fases (2PC) com
replicação lógica.

### Melhorias no Registro e na Configuração

O PostgreSQL 15 introduz um novo formato de registro:
[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG).
Este novo formato produz dados utilizando uma estrutura JSON definida,
permitindo os logs do PostgreSQL serem processados em sistemas de registros
estruturados.

Esta versão fornece a administradores de bancos de dados mais flexibilidade na
forma como usuários podem gerenciar a configuração do PostgreSQL, adicionando a
habilidade de definir a usuários permissão para alterar parâmetros de
configuração do servidor. Adicionalmente, usuários podem obter informação sobre
a configuração utilizando o comando `\dconfig` da ferramenta de linha de
comando [`psql`](https://www.postgresql.org/docs/15/app-psql.html).

### Outras Mudanças Notáveis

As [estatísticas](https://www.postgresql.org/docs/15/monitoring-stats.html) do
PostgreSQL agora são coletadas em memória compartilhada, eliminando o processo
de coleta de estatísticas e a escrita periódica de dados no disco.

O PostgreSQL 15 permite tornar uma [ordenação
ICU](https://www.postgresql.org/docs/15/collation.html) a ordenação padrão para
um agrupamento ou um banco de dados individual.

Esta versão também adiciona uma nova extensão,
[`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html), que
permite usuários inspecionarem o conteúdo dos arquivos de log de transação
diretamente de uma interface SQL.

O PostgreSQL 15 também [revoga a permissão `CREATE` de todos os
usuários](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
exceto do dono do banco de dados do esquema `public` (ou padrão).

O PostgreSQL 15 remove tanto o modo de "cópia de segurança exclusiva" quanto o
suporte a Python 2 da PL/Python há muito tempo declarados obsoletos.


### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) é o banco de dados mais avançado do
mundo, com uma comunidade global de milhares de usuários, colaboradores,
empresas e organizações. O Projeto PostgreSQL baseia-se em mais de 35 anos de
engenharia, iniciando na Universidade da Califórnia, Berkeley, e continua em um
ritmo inigualável de desenvolvimento. Conjunto de funcionalidades maduras do
PostgreSQL não só se igualam aos principais sistemas de bancos de dados
proprietários, mas os supera em funcionalidades avançadas, extensibilidade,
segurança e estabilidade.

### Links

* [Download](https://www.postgresql.org/download/)
* [Notas de Lançamento](https://www.postgresql.org/docs/15/release-15.html)
* [Kit à Imprensa](https://www.postgresql.org/about/press/)
* [Informação sobre Segurança](https://www.postgresql.org/support/security/)
* [Política de Versionamento](https://www.postgresql.org/support/versioning/)
* [Siga @postgresql no Twitter](https://twitter.com/postgresql)

## Mais Sobre as Funcionalidades

Para explicação sobre as funcionalidades acima e outras, consulte os seguintes links:

* [Notas de Lançamento](https://www.postgresql.org/docs/15/release-15.html)
* [Matriz de Funcionalidades](https://www.postgresql.org/about/featurematrix/)

## Onde Baixar

Há várias maneiras de fazer uma cópia do PostgreSQL 15, incluindo:

* A página [oficial de Downloads](https://www.postgresql.org/download/) que contém instaladores e ferramentas para [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) e mais.
* [Código Fonte](https://www.postgresql.org/ftp/source/v15.0)

Outras ferramentas e extensões estão disponíveis na [PostgreSQL Extension
Network](http://pgxn.org/).

## Documentação

O PostgreSQL 15 vem com documentação em HTML bem como páginas man, e você
também pode navegar na documentação online nos formatos
[HTML](https://www.postgresql.org/docs/15/) e
[PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf).

## Licença

O PostgreSQL usa a [PostgreSQL
License](https://www.postgresql.org/about/licence/), uma licença "permissiva"
do tipo BSD. Esta [licença certificada pela
OSI](http://www.opensource.org/licenses/postgresql/) é amplamente apreciada
como flexível e amigável aos negócios, uma vez que não restringe o uso do
PostgreSQL com aplicações comerciais e proprietárias. Juntamente com o suporte
de múltiplas empresas e a propriedade pública do código fonte, nossa licença
torna o PostgreSQL muito popular entre os fornecedores que desejam incorporar
um banco de dados em seus produtos sem o medo de taxas, dependência de
fornecedor ou alterações nos termos de licenciamento.

## Contatos

Página Web

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

## Imagens e Logotipos

Postgres, PostgreSQL e o Logotipo do Elefante (Slonik) são todas marcas
registradas da [PostgreSQL Community Association of
Canada](https://www.postgres.ca). Se você deseja utilizar estas marcas, você
deve estar em conformidade com a [política de marcas
registradas](https://www.postgresql.org/about/policies/trademarks/).


## Suporte Corporativo

O PostgreSQL conta com o apoio de inúmeras empresas, que financiam
desenvolvedores, fornecem recursos de hospedagem e nos dão suporte financeiro.
Veja nossa página de
[patrocinadores](https://www.postgresql.org/about/sponsors/) para alguns desses
apoiadores do projeto.

Há também uma grande comunidade de [empresas que oferecem suporte ao
PostgreSQL](https://www.postgresql.org/support/professional_support/), de
consultores individuais a empresas multinacionais.

Se você deseja fazer uma contribuição financeira para o Grupo de
Desenvolvimento Global do PostgreSQL ou uma das organizações comunitárias sem
fins lucrativos reconhecidas, visite nossa página de
[doações](https://www.postgresql.org/about/donate/).
