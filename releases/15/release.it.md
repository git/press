6 ottobre 2022 - Il PostgreSQL Global Development Group ha annunciato oggi il rilascio
di [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html),
l'ultima versione del [database open source più avanzato al mondo](https://www.postgresql.org/).

PostgreSQL 15 si basa sui miglioramenti delle prestazioni delle versioni recenti con
notevoli vantaggi per la gestione dei carichi di lavoro sia in locale che in remoto
e include migliori funzionalità di ordinamento. Questa versione migliora l'esperienza dello sviluppatore
con l'aggiunta del popolare comando [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) e aggiunge
ulteriori possibilità per osservare lo stato del database.

"La comunità di sviluppatori PostgreSQL continua a creare funzionalità che semplificano
l'esecuzione di carichi di lavoro di dati ad alte prestazioni migliorando allo stesso tempo l'esperienza dello sviluppatore", 
ha affermato Jonathan Katz, un membro del PostgreSQL Core Team. "PostgreSQL 15
evidenzia come, attraverso lo sviluppo di software open source, possiamo fornire ai nostri utenti un
database ottimo per lo sviluppo di applicazioni e sicuro per la gestione dei dati critici."

[PostgreSQL](https://www.postgresql.org), un innovativo sistema di gestione dei dati
noto per la sua affidabilità e robustezza, beneficia di oltre 25 anni di 
sviluppo open source da parte di una [comunità globale di sviluppatori](https://www.postgresql.org/community/)
ed è diventato il database relazionale open source preferito dalle organizzazioni
di qualsiasi dimensione.

### Miglioramento delle prestazioni di ordinamento e di compressione

In questa ultima versione, PostgreSQL migliora la sua gestione in-memory e su disco
degli algoritmi di [ordinamento](https://www.postgresql.org/docs/15/queries-order.html),
con benchmark che mostrano accelerazioni del 25% - 400% in base al tipo di dato
ordinato. Usando `row_number()`, `rank()`, `dense_rank()` e `count()` come
[funzioni finestra](https://www.postgresql.org/docs/15/functions-window.html)
fornisce benefici in termini di prestazioni in PostgreSQL 15. Query che fanno uso di
[`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT)
ora possono essere
[eseguite in parallelo](https://www.postgresql.org/docs/15/parallel-query.html).

Basandosi sul lavoro della [precedente versione di PostgreSQL](https://www.postgresql.org/about/press/presskit14/)
per consentire l'esecuzione di query remote asincrone, il
[Wrapper di dati esterni PostgreSQL](https://www.postgresql.org/docs/15/postgres-fdw.html),
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html),
ora supporta le operazioni di 
[commit asincrono](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

I miglioramenti delle prestazioni in PostgreSQL 15 si estendono all'archiviazione e al backup. 
PostgreSQL 15 aggiunge il supporto per gli algoritmi LZ4 e Zstandard (zstd) nella
[compressione delle attività WAL (compressione del log write-ahead)](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION),
che può avere vantaggi in termini di spazio e prestazioni per determinati carichi di lavoro. 
Su alcuni sistemi operativi, PostgreSQL 15 aggiunge il supporto per la 
[pre-carica di pagine referenziate in WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)
per aiutare ad accelerare i tempi di ripristino. Il comando di backup integrato di PostgreSQL,
[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html), ora
supporta la compressione lato server dei file di backup con una scelta di gzip, LZ4 e
zstd. PostgreSQL 15 include la possibilità di utilizzare
[moduli personalizzati per l'archiviazione](https://www.postgresql.org/docs/15/archive-modules.html),
eliminando la necessità di utilizzare i comandi da shell.

### Funzionalità espressive per sviluppatori

PostgreSQL 15 include il comando standard SQL [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html).
`MERGE` ti consente di scrivere istruzioni SQL condizionali che possono includere `INSERT`,
Azioni `UPDATE` e `DELETE` all'interno di una singola istruzione.

Questa ultima versione aggiunge
[nuove funzioni per l'utilizzo delle espressioni regolari](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP)
per controllare le stringhe: `regexp_count()`, `regexp_instr()`, `regexp_like()` e
`regexp_substr()`. PostgreSQL 15 estende anche la funzione `range_agg` per aggregare
[tipi di dati `multirange`](https://www.postgresql.org/docs/15/rangetypes.html),
che sono stati introdotti nella
[versione precedente](https://www.postgresql.org/about/press/presskit14/).

PostgreSQL 15 consente agli utenti di
[creare viste che richiedono dati utilizzando le autorizzazioni del chiamante, non del creatore della vista](https://www.postgresql.org/docs/15/sql-createview.html).
Questa opzione, chiamata `security_invoker`, aggiunge un ulteriore livello di protezione
per garantire che gli utenti che richiamano la vista dispongano delle autorizzazioni corrette per lavorare con i dati sottostanti.

### Altre opzioni per la replica logica

PostgreSQL 15 offre maggiore flessibilità per la gestione della
[replica logica](https://www.postgresql.org/docs/15/logical-replication.html).
Questa versione introduce i
[filtri di riga](https://www.postgresql.org/docs/15/logical-replication-row-filter.html)
e gli
[elenchi di colonne](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)
per
[i nodi di pubblicazione](https://www.postgresql.org/docs/15/logical-replication-publication.html),
consentendo agli utenti di scegliere di replicare un sottoinsieme di dati da una tabella. PostgreSQL 15
aggiunge funzionalità per semplificare la
[gestione dei conflitti](https://www.postgresql.org/docs/15/logical-replication-conflicts.html),
inclusa la possibilità di saltare la riproduzione di una transazione in conflitto e di farlo
disattivando automaticamente una sottoscrizione se viene rilevato un errore. Anche questa versione
include il supporto per l'utilizzo di commit a due fasi (2PC) con replica logica.

### Miglioramenti ai logs e alla configurazione

PostgreSQL 15 introduce un nuovo formato di log:
[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG).
Questo nuovo formato genera i dati di log utilizzando una struttura JSON definita, che consente ai logs di PostgreSQL di essere elaborati in sistemi di logging strutturati.

Questa versione offre agli amministratori di database una maggiore flessibilità su come gli utenti possono 
gestire la configurazione di PostgreSQL, aggiungendo la possibilità di concedere il permesso agli utenti di
modificare i parametri di configurazione a livello di server. Inoltre, gli utenti possono ora cercare
per informazioni sulla configurazione usando il comando `\dconfig` dallo strumento da riga di comando
[`psql`](https://www.postgresql.org/docs/15/app-psql.html) .

### Altre modifiche degne di nota

PostgreSQL
[statistiche a livello di server](https://www.postgresql.org/docs/15/monitoring-stats.html)
vengono ora raccolti nella memoria condivisa, eliminando entrambi i processi di raccolta delle statistiche e scrivendo periodicamente questi dati su disco.

PostgreSQL 15 consente di creare una
[collation ICU](https://www.postgresql.org/docs/15/collation.html) ovvero l'impostazione predefinita di confronto per un cluster o un singolo database.

Anche questa versione aggiunge una nuova estensione integrata,
[`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html), che
consente agli utenti di ispezionare il contenuto dei file di log write-ahead direttamente da una interfaccia SQL.

PostgreSQL 15 consente inoltre la
[revoca del permesso `CREATE` a tutti gli utenti](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
tranne per il proprietario di database dallo schema `public` (o di quello predefinito).

PostgreSQL 15 rimuove sia la modalità "backup esclusivo" deprecata da tempo che il supporto per Python 2 da PL/Python.

### Informazioni su PostgreSQL

[PostgreSQL](https://www.postgresql.org) è il database open source più avanzato al mondo, con una comunità globale di migliaia di utenti, collaboratori,
aziende e organizzazioni. Costruito su oltre 35 anni di ingegneria, a partire da
l'Università della California, Berkeley, PostgreSQL ha continuato con un
ritmo di sviluppo senza pari. Il set di funzionalità mature di PostgreSQL non solo corrisponde
migliori sistemi di database proprietari, ma li supera in funzionalità di database avanzato, estensibilità, sicurezza e stabilità.

### Collegamenti

* [Download](https://www.postgresql.org/download/)
* [Note sulla versione](https://www.postgresql.org/docs/15/release-15.html)
* [Kit per la stampa](https://www.postgresql.org/about/press/)
* [Pagina sulla sicurezza](https://www.postgresql.org/support/security/)
* [Politica di versione](https://www.postgresql.org/support/versioning/)
* [Segui @postgresql su Twitter](https://twitter.com/postgresql)

## Maggiori informazioni sulle funzionalità

Per le spiegazioni delle funzioni di cui sopra e altre, consultare le seguenti risorse:

* [Note sulla versione](https://www.postgresql.org/docs/15/release-15.html)
* [Matrice delle funzioni](https://www.postgresql.org/about/featurematrix/)

## Dove scaricare

Esistono diversi modi per scaricare PostgreSQL 15, tra cui:

* La pagina di [Download ufficiale](https://www.postgresql.org/download/), con contiene programmi di installazione e strumenti per [Windows](https://www.postgresql.org/download/windows/), [Linux ](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) e altro ancora.
* Il [Codice sorgente](https://www.postgresql.org/ftp/source/v15.0)

Altri strumenti ed estensioni sono disponibili sulla
[Rete di estensioni per PostgreSQL](http://pgxn.org/).

## Documentazione

PostgreSQL 15 viene fornito con documentazione HTML e pagine man e puoi anche sfogliare la documentazione online in [HTML](https://www.postgresql.org/docs/15/) e [PDF](https:// www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf).

## Licenza

PostgreSQL utilizza la [Licenza PostgreSQL](https://www.postgresql.org/about/licence/), una licenza "permissiva" simile a BSD. Questa
[Licenza certificata OSI](http://www.opensource.org/licenses/postgresql/) è ampiamente apprezzata come flessibile e business-friendly, poiché non limita
l'uso di PostgreSQL con applicazioni commerciali e proprietarie. Insieme con il supporto multi-aziendale e la proprietà pubblica del codice, la nostra licenza fa si che
PostgreSQL sia molto popolare tra i fornitori che desiderano incorporare un database nel proprio prodotti senza timore di commissioni, vincoli del fornitore o modifiche ai termini di licenza.

## Contatti

Sito web

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-mail

* [press@postgresql.org](mailto:press@postgresql.org)

## Immagini e loghi

Postgres e PostgreSQL e Elephant Logo (Slonik) sono tutti marchi registrati di [PostgreSQL Community Association of Canada](https://www.postgres.ca).
Se desideri utilizzare questi marchi, devi rispettare la [politica sui marchi](https://www.postgresql.org/about/policies/trademarks/).

## Supporto aziendale

PostgreSQL gode del supporto di numerose aziende che sponsorizzano sviluppatori e forniscono risorse di hosting e supporto finanziario. 
Consulta la nostra pagina [sponsor](https://www.postgresql.org/about/sponsors/) per l'elenco dei sostenitori del progetto.

C'è anche una grande comunità di [aziende che offrono supporto PostgreSQL](https://www.postgresql.org/support/professional_support/), dai singoli consulenti alle multinazionali.

Se desideri dare un contributo finanziario al PostgreSQL Global Development Group o ad una delle organizzazioni non profit riconosciute della comunità,
puoi visitare la nostra pagina delle [donazioni](https://www.postgresql.org/about/donate/).
