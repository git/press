The PostgreSQL Global Development Group announces that the first beta release of
PostgreSQL 15 is now [available for download](https://www.postgresql.org/download/).
This release contains previews of all features that will be available when
PostgreSQL 15 is made generally available, though some details of the release
can change during the beta period.

You can find information about all of the features and changes found in
PostgreSQL 15 in the [release notes](https://www.postgresql.org/docs/15/release-15.html):

  [https://www.postgresql.org/docs/15/release-15.html](https://www.postgresql.org/docs/15/release-15.html)

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 15 on your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 15 Beta 1 in production environments, we encourage you to find ways
to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 15
release upholds our standards of delivering a stable, reliable release of the
world's most advanced open source relational database. Please read more about
our [beta testing process](https://www.postgresql.org/developer/beta/) and how
you can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

PostgreSQL 15 Feature Highlights
--------------------------------

### Developer Experience

PostgreSQL 15 adds new features for simplifying and enhancing the developer
experience.

This release introduces [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html),
a SQL standard command for conditionally performing write operations (`INSERT`,
`UPDATE`, or `DELETE`) on data. Prior to this release, the same behavior could
be accomplished either using stored procedures or, on a limited-basis, with
[`INSERT ... ON CONFLICT`](https://www.postgresql.org/docs/15/sql-insert.html).
With PostgreSQL 15, developers can write simple, expressive queries to choose
the appropriate data modification action to take.

PostgreSQL added support for JSON in 2012 as part of the [9.2 release](https://www.postgresql.org/about/news/postgresql-92-released-1415/). The SQL/JSON standard, published five years
later, specified a variety of interfaces for accessing and manipulating JSON
data stored in relational databases. PostgreSQL 15 builds on its existing
support for the SQL/JSON path language by including more standard
[SQL/JSON functions](https://www.postgresql.org/docs/15/functions-json.html#FUNCTIONS-SQLJSON).
These include [SQL/JSON constructors](https://www.postgresql.org/docs/15/functions-json.html#FUNCTIONS-SQLJSON-PRODUCING),
[query / introspection functions](FUNCTIONS-SQLJSON-QUERYING),
and the ability to [convert JSON data into a table](https://www.postgresql.org/docs/15/functions-json.html#FUNCTIONS-JSONTABLE).

PostgreSQL 15 adds [more regular expression functions](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP),
including `regexp_count` , `regexp_instr`, `regexp_like`, and `regexp_substr`.
the [`range_agg`](https://www.postgresql.org/docs/15/functions-aggregate.html)
function, introduced in PostgreSQL 15 for aggregating
[`range` data types](https://www.postgresql.org/docs/15/rangetypes.html) into
`multirange` types, now supports aggregating `multirange` types too.

### Performance

PostgreSQL 15 continues to build on its performance gains over the past several
releases. This release includes a significant speedup for sorting data when
sorting over larger data sets. In particular, these are data sets that exceed
the `work_mem` parameter.

The performance gains of PostgreSQL 15 extend to a variety of query types.
This includes the introduction of parallelization for
[`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT)
statements and improvements in performance to
[window functions](https://www.postgresql.org/docs/15/functions-window.html)
that use `row_number()`, `rank()`, and `count()`. Applications that use the
[PostgreSQL foreign data wrapper](https://www.postgresql.org/docs/15/postgres-fdw.html)
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html) to manage
data on remote PostgreSQL servers can now enable
[transactions to be committed in parallel](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7). There are also several performance enhancements for queries
involving tables with partitions.

PostgreSQL system and [TOAST](https://www.postgresql.org/docs/15/storage-toast.html)
tables, used for storing data that is larger than a single page (8kB), can now
utilize
[index deduplication](https://www.postgresql.org/docs/15/btree-implementation.html#BTREE-DEDUPLICATION)
and benefit from smaller indexes and faster lookups.

The psql `\copy` command, used to bulk load data, also has performance
improvements in PostgreSQL 15.

### Backups, Archiving, and Compression

Building on the previous release that introduced LZ4 compression, PostgreSQL 15
adds support for Zstandard (zstd) compression to various components.

[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html), a
utility used to take full backups of a PostgreSQL cluster, now supports
server-side compression using Gzip, LZ4, or Zstandard compression. The
`pg_basebackup` client can now also decompress backups that use LZ4 and
Zstandard compression.

Write-ahead log (WAL) files can now be compressed using both LZ4 and Zstandard
compression through the
[`wal_compression`](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION)
configuration parameter. Additionally, PostgreSQL 15 also adds the
[`recovery_prefetch`](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)
option that can help speed up recovery operations by prefetching data blocks.
This release adds a new module called [`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html)
that lets you inspect information about WAL files directly from SQL.

PostgreSQL 15 lets you perform WAL archiving without running a shell command by
specifying an
[`archive_library`](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-ARCHIVE-LIBRARY).
An example WAL archiving library can be found in the
[`basic_archive`](https://www.postgresql.org/docs/15/basic-archive.html) module.
Additionally, extensions can now define their own WAL resource managers that
lets them work with logical replication.

### Logical Replication

PostgreSQL 15 introduces both
[row filtering](https://www.postgresql.org/docs/15/logical-replication-row-filter.html) and
[column filtering](https://www.postgresql.org/docs/15/sql-createpublication.html)
for logical replication. Additionally, users can now conveniently specify to
publish all tables in a schema, where previously it was only possible to do this
for all tables in a database.

This release also adds more support for handling
[conflicts](https://www.postgresql.org/docs/15/logical-replication-conflicts.html).
A subscriber can now specify `disable_on_error` to automatically stop logical
replication on an error.
PostgreSQL 15 makes it possible to skip applying changes using the
[`ALTER SUBSCRIPTION ... SKIP`](https://www.postgresql.org/docs/15/sql-altersubscription.html)
command.

### Administration

PostgreSQL 15 introduces the
[`jsonlog` format for logging](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG). This allows PostgreSQL logs to be consumed by many programs
that perform structured log aggregation and analysis. PostgreSQL 15 now by
default logs checkpoints and autovacuum operations that are slow.

The [`psql`](https://www.postgresql.org/docs/15/app-psql.html) client now has a
command called `\dconfig` for inspecting the values of configuration parameters.
By default, `\dconfig` displays any configuration parameter that is not set to
its default value.

PostgreSQL 15 now stores all server-level statistics in shared memory. The
server no longer runs a statistics collector process.

The [`ALTER TABLE`](https://www.postgresql.org/docs/15/sql-altertable.html)
command can now modify a table's `ACCESS METHOD`, which is used to set the
storage system used by the table.

### Security

PostgreSQL 15 introduces new defaults when databases are created within a
PostgreSQL cluster. When a new database is created, users outside of the
database owner (represented by [`pg_database_owner`](https://www.postgresql.org/docs/15/predefined-roles.html))
and superusers can no longer create objects in the default `public` schema.

PostgreSQL 15 adds support for
"[security invoker views](https://www.postgresql.org/docs/15/sql-createview.html)",
which uses the privileges of the user executing the query instead of the user
who created the view.

Unprivileged users in PostgreSQL 15 can now be
[granted privileges](https://www.postgresql.org/docs/15/sql-grant.html) to
change server variables via `SET` and `ALTER SYSTEM`.

Additional Features
-------------------

Many other new features and improvements have been added to PostgreSQL 15. Many
of these may also be helpful for your use cases. Please see the
[release notes](https://www.postgresql.org/docs/15/release-15.html) for a
complete list of new and changed features:

  [https://www.postgresql.org/docs/15/release-15.html](https://www.postgresql.org/docs/15/release-15.html)

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 15. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_15_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the first beta release of version 15. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2022. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/)
page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 15 Beta Release Notes](https://www.postgresql.org/docs/15/release-15.html)
* [PostgreSQL 15 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_15_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
