6. Oktober 2022 - Die PostgreSQL Global Development Group hat heute die Veröffentlichung von [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html), der aktuellsten Version des weltweit [führenden Open-Source-SQL-Datenbanksystems](https://www.postgresql.org/), bekannt gegeben.

PostgreSQL 15 baut auf den Performance Steigerungen der letzten Versionen auf, durch spürbare Verbesserung der Geschwindigkeit sowohl bei der Verwaltung von Workloads, sowohl lokal, als auch in verteilten Umgebungen, einschließlich verbesserter Sortierung. Diese Version implementiert das bei Entwicklern populäre [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) und erweitert auch die Möglichkeiten zum Monitoring des Zustands der Datenbank.

"Die PostgreSQL-Entwickler-Community entwickelt weiterhin Funktionen, die bei gleichzeitiger Verbesserung der Entwicklerfreundlichkeit die Ausführung von hoch performanten Daten-Workloads vereinfachen," sagt Jonathan Katz, ein Mitglied des PostgreSQL Core Teams. "PostgreSQL 15 zeigt, wie wir durch offene Softwareentwicklung unseren Benutzern sowohl eine Datenbank liefern können, die sich hervorragend für die Anwendungsentwicklung eignet, als auch sicher für kritische Daten ist."

[PostgreSQL](https://www.postgresql.org), ein innovatives Datenverwaltungssystem bekannt für seine Zuverlässigkeit und Robustheit, profitiert von über 25 Jahren Open Source-Entwicklung durch eine [globale Entwickler-Community](https://www.postgresql.org/community/) und hat sich zur bevorzugten relationalen Open-Source-Datenbank für Unternehmen aller Größen entwickelt.

### Verbesserte Sortierleistung und Komprimierung

In dieser neuesten PostgreSQL Version wurden [Sortieralgoryhmen](https://www.postgresql.org/docs/15/queries-order.html) verbessert, die sowowhl im Hauptspeicher, als auch auf Festspeichermedien in Benchmarks zu einer Beschleunigung von 25 % - 400 % führen, je nachdem, welche Datentypen sortiert werden. [`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT) kann jetzt [parallel](https://www.postgresql.org/docs/15/parallel-query.html) ausgeführt werden.

Aufbauend auf der Arbeit an der [vorherigen PostgreSQL-Version](https://www.postgresql.org/about/press/presskit14/), die schon asynchrone remote Abfragen zuliess, unterstützen [PostgreSQL Foreign Data Wrapper](https://www.postgresql.org/docs/15/postgres-fdw.html), [`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html), nun [asynchrone Commits](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

Die Geschwindigkeitsverbesserungen in PostgreSQL 15 erstrecken sich auf auch die Archivierung und Sicherung. PostgreSQL 15 fügt Unterstützung von LZ4 und Z-Standard (zstd)[compression to write-ahead log (WAL) files] für die [Komprimierung von WAL-Dateien (Write-Ahead Log)] (https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION) hinzu, die für bestimmte Workloads sowohl Platz- als auch Leistungsvorteile haben können. Auf bestimmten Betriebssystemen fügt PostgreSQL 15 die Unterstützung von [Block-Prefetching, im WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH) hinzu, um die Wiederherstellungszeiten zu verkürzen. PostgreSQL integrierter Sicherungsbefehl [`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html), unterstützt die serverseitige Komprimierung von Sicherungsdateien mit einer Auswahl von gzip, LZ4 und zstd. PostgreSQL 15 beinhaltet die Möglichkeit zur Verwendung [benutzerdefinierter Module für die Archivierung] (https://www.postgresql.org/docs/15/archive-modules.html), wodurch die Mehraufwand der Verwendung eines Shell-Befehls entfällt.

### Erweiterte Entwicklerfunktionen

PostgreSQL 15 implementiert den SQL-Standard Befehl [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html). Mit `MERGE` können bedingte SQL-Anweisungen geschrieben werden, die sowohl `INSERT`, als auch `UPDATE`- und `DELETE`-Aktionen innerhalb einer einzigen Anweisung enthalten.

In diesem neuen Release sind [neue Funktionen regulärer Ausdrücke](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP) hinzugefügt worden um Zeichenketten zu untersuchen: `regexp_count()`, `regexp_instr()`, `regexp_like()` und`regexp_substr()`. PostgreSQL 15 erweitert auch die Funktion „range_agg“ um [`Multirange`-Datentypen](https://www.postgresql.org/docs/15/rangetypes.html), die in der [vorherigen Version](https://www.postgresql.org/about/press/presskit14/). eingeführt wurden.

PostgreSQL 15 erlaubt es Benutzern [Views zu erstellen, die Daten mit den Berechtigungen des Aufrufenden abfragen und nicht mit denen des Erstellers der View](https://www.postgresql.org/docs/15/sql-createview.html). Diese Option namens „security_invoker“ fügt eine zusätzliche Sicherheitsebene hinzu, die gewährleistet, dass Aufrufende von Views die richtigen Berechtigungen zum Arbeiten mit den zugrundeliegenden Daten haben.

### Mehr Optionen mit logischer Replikation

PostgreSQL 15 bietet mehr Flexibilität für die Verwaltung [logischer Replikationen](https://www.postgresql.org/docs/15/logical-replication.html). Diese Version führt [Filter auf Zeilenebene](https://www.postgresql.org/docs/15/logical-replication-row-filter.html) und [Spaltenebene](https://www.postgresql.org/docs/15/logical-replication-col-lists.html) für [Publications](https://www.postgresql.org/docs/15/logical-replication-publication.html) ein, um Benutzern die Möglichkeit zu geben, eine Teilmenge von Daten aus einer Tabelle zu replizieren. Des weiteren werden in PostgreSQL 15 Funktionen zur Vereinfachung des [Konfliktmanagements](https://www.postgresql.org/docs/15/logical-replication-conflicts.html) eingeführt, einschließlich der Möglichkeit, die Ausführung einer widersprüchlichen Transaktion zu überspringen und eine Subskription automatisch zu deaktivieren, wenn ein Fehler erkannt wird. Dieses Release implementiert auch die Unterstützung für die Verwendung von Zwei-Phasen-Commits (2PC) in der logischen Replikation.

### Log- und Konfigurationsverbesserungen

PostgreSQL 15 führt ein neues Logformat ein: [`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG). Dieses neue Format gibt Logdaten in einer definierten JSON-Struktur aus, die es ermöglicht, PostgreSQL-Logs in strukturierten Logsystemen zu verarbeiten.

Diese Version gibt Datenbankadministratoren mehr Flexibilität, inwieweit Benutzer die PostgreSQL-Konfiguration ändern können. Es wurde die Möglichkeit hinzugefügt, Benutzern die Berechtigung zu erteilen, Konfigurationsparameter auf Serverebene zu ändern. Außerdem können sich Benutzer jetzt Informationen zur Konfiguration mit dem Befehl `\dconfig` im Befehlszeilentool [`psql`](https://www.postgresql.org/docs/15/app-psql.html) anzeigen lassen.

### Andere bemerkenswerte Änderungen

PostgreSQL [Statistiken auf Serverebene](https://www.postgresql.org/docs/15/monitoring-stats.html) werden jetzt im gemeinsam genutzten Speicher (shared memory) gesammelt, wodurch sowohl der Statistik Kollektor Prozess entfällt, als auch das regelmäßige Schreiben dieser Daten auf die Festplatte.

PostgreSQL 15 ermöglicht es, eine [ICU-Sortierung](https://www.postgresql.org/docs/15/collation.html) als Standardeinstellung für die Sortierung für einen Cluster oder eine einzelne Datenbank anzugeben.

Außerdem wird eine neue Erweiterung, [`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html), hinzugefügt, die es Benutzern ermöglicht, den Inhalt von Write-Ahead-Logdateien direkt aus einem SQL Interface heraus zu überprüfen.

PostgreSQL 15 [entzieht allen Benutzern die `CREATE`-Berechtigung](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS), außer einem Datenbank Eigentümer aus dem "public" (oder Standard-)Schema.

PostgreSQL 15 entfernt sowohl den seit langem veralteten Modus „exklusive Sicherung“, als auch die Unterstützung für Python 2 von PL/Python.

### Über PostgreSQL

[PostgreSQL](https://www.postgresql.org) ist das führende Open-Source Datenbanksystem, mit einer weltweiten Community bestehend aus Tausenden von Nutzern und Mitwirkenden sowie Dutzenden von Firmen und Organisationen. Das PostgreSQL Projekt baut auf über 35 Jahre Erfahrung auf, beginnend an der University of California, Berkeley, und hat heute eine nicht zu vergleichende Performance bei der Entwicklung. PostgreSQL's ausgereiftes Feature Set ist nicht nur mit den führenden proprietären Datenbanksystemen vergleichbar, sondern übertrifft diese in erweiterten Datenbankfunktionen, Erweiterbarkeit, Sicherheit und Stabilität.

### Links

* [Download](https://www.postgresql.org/download/)
* [Versionshinweise](https://www.postgresql.org/docs/15/release-15.html)
* [Pressemitteilung](https://www.postgresql.org/about/press/)
* [Sicherheit](https://www.postgresql.org/support/security/)
* [Versionierungsrichtlinie](https://www.postgresql.org/support/versioning/)
* [Folge @postgresql auf Twitter](https://twitter.com/postgresql)

## Mehr über die Funktionen

Erläuterungen zu den oben genannten und anderen Funktionen finden Sie in den folgenden Quellen:

* [Versionshinweise](https://www.postgresql.org/docs/15/release-15.html)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/)

## Wo Herunterladen

Es gibt mehrere Möglichkeiten, PostgreSQL 15 herunterzuladen, darunter:

* Die Seite [Offizielle Downloads](https://www.postgresql.org/download/) enthält Installationsprogramme und Tools für [Windows](https://www.postgresql.org/download/windows/), [Linux ](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) und weitere.
* [Quellcode](https://www.postgresql.org/ftp/source/v15.0)

Weitere Tools und Erweiterungen sind über das [PostgreSQL Extension Network](http://pgxn.org/) verfügbar.

## Dokumentation

PostgreSQL 15 wird mit einer HTML-Dokumentation sowie Manpages geliefert. Sie können die Dokumentation auch online unter [HTML](https://www.postgresql.org/docs/15/) aufrufen und als [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf) Datei herunterladen.

## Lizenz

PostgreSQL verwendet die [PostgreSQL-Lizenz](https://www.postgresql.org/about/licence/), eine BSD-artige "permissive" Lizenz. Diese [OSI-zertifizierte Lizenz](http://www.opensource.org/licenses/postgresql/) ist allgemein als flexibel und geschäftsfreundlich geschätzt, da die Verwendung von PostgreSQL mit kommerziellen und proprietären Anwendungen nicht eingeschränkt wird. Zusammen mit unternehmensübergreifender Unterstützung und öffentlichem Quellcode macht diese Lizenz PostgreSQL sehr beliebt bei Anbietern welche eine Datenbank in ihre eigene Anwendungen einbetten möchten, ohne Angst vor Gebühren, Herstellerbindung oder Änderungen der Lizenzbedingungen.

## Kontakte

Webseite

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-Mail

* [press@postgresql.org](mailto:press@postgresql.org)

## Bilder und Logos

Postgres und PostgreSQL und das Elefanten Logo (Slonik) sind registrierte Marken der [PostgreSQL Community Association of Canada](https://www.postgres.ca). Wenn Sie diese Marken verwenden möchten, müssen Sie die [Markenrichtlinie](https://www.postgresql.org/about/policies/trademarks/) einhalten.

## Professioneller Support

PostgreSQL genießt die Unterstützung zahlreicher Unternehmen, die Entwickler sponsern, Hosting-Ressourcen bereitstellen und finanzielle Unterstützung leisten. Unsere [Sponsorenliste](https://www.postgresql.org/about/sponsors/) listet einige Unterstützer des Projekts auf.

Es gibt eine große Anzahl von [Unternehmen, die PostgreSQL-Support anbieten](https://www.postgresql.org/support/professional_support/), von einzelnen Beratern bis hin zu multinationalen Unternehmen.

Wenn Sie einen finanziellen Beitrag zur PostgreSQL Development Group leisten möchten oder eine der anerkannten gemeinnützigen Organisationen der Community unterstützen möchten, besuchen Sie bitte unsere [Spenden Seite](https://www.postgresql.org/about/donate/).
