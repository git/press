13 octobre 2022 - Le PostgreSQL Global Development Group annonce aujourd'hui la
sortie de [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html),
dernière version de [la base de données open source de référence](https://www.postgresql.org/).

PostgreSQL 15 s'appuie sur les améliorations de performance des dernières
versions. Elle apporte des gains visibles dans la gestion des charges de
travail, autant sur les déploiements locaux que distribués. Cela inclut
l'amélioration des opérations de tri. Cette version améliore aussi l'expérience
développeur avec l'ajout de la très attendue commande
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html), et apporte de
nouvelles possibilités d'observation de l'état de la base de données.

« La communauté des développeurs de PostgreSQL continue de fournir des
fonctionnalités simplifiant l'exécution de charges de travail de données à
hautes performances, tout en améliorant l'expérience développeur » déclare
Jonathan Katz, membre de la PostgreSQL Core Team.
« PostgreSQL 15 démontre comment, grâce au développement de logiciel libre,
nous pouvons fournir à nos utilisateurs une base de données adaptée au
développement d'applications et sûre pour leurs données critiques. »

[PostgreSQL](https://www.postgresql.org), système innovant de gestion des
données, connu pour sa fiabilité et sa robustesse, bénéficie depuis plus de
25 ans d'un développement open source par une [communauté de développeurs mondiale](https://www.postgresql.org/community/).
Il est devenu le système de gestion de bases de données relationnelles de
référence pour des organisations de toute taille.

### Amélioration de la performance de tri et compression

Dans cette dernière version, PostgreSQL améliore ses algorithmes de
[tri](https://www.postgresql.org/docs/15/queries-order.html) en mémoire et sur
disque, avec des tests de performance montrant des gains de 25% à 400% selon
le type des données triées.
L'utilisation de `row_number()`, `rank()`, `dense_rank()` et `count()` comme
[fonctions de fenêtrages](https://www.postgresql.org/docs/15/functions-window.html)
bénéficie aussi d'améliorations de performances dans PostgreSQL 15. Les
requêtes utilisant l'instruction [`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT)
peuvent être [exécutées en parallèle](https://www.postgresql.org/docs/15/parallel-query.html).

S'appuyant sur le travail effectué dans les [versions précédentes de PostgreSQL](https://www.postgresql.org/about/press/presskit14/)
pour autoriser les requêtes asynchrones distantes, le
[wrapper de données distantes de PostgreSQL](https://www.postgresql.org/docs/15/postgres-fdw.html),
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html),
autorise désormais les [commits asynchrones](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

Des améliorations de performance sont apportées à l'archivage et la sauvegarde.
PostgreSQL 15 ajoute le support des compressions LZ4 et Zstandard (zstd) pour
les [fichiers WAL (write-ahead log)](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION).
Ces compressions permettent des gains en taille et en performance pour
certaines charges de travail. Sur certains systèmes d'exploitation, PostgreSQL
15 supporte la [préextraction du contenu des fichiers WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH),
accélérant ainsi les temps de restauration. La commande interne de sauvegarde
de PostgreSQL, [`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html),
supporte désormais la compression  des fichiers de sauvegarde côté serveur avec
le choix de `gzip`, `LZ4` et `zstd`. PostgreSQL 15 offre la possibilité
d'utiliser des [modules complémentaires pour l'archivage](https://www.postgresql.org/docs/15/archive-modules.html),
éliminant la nécessité d'utiliser une commande shell.

### Fonctionnalités pour développeurs

PostgreSQL 15 ajoute la commande [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html)
du standard SQL. `MERGE` permet d'écrire des requêtes SQL conditionnelles 
combinant des actions `INSERT`, `UPDATE` et `DELETE` en une seule requête.

Cette version ajoute de
[nouvelles fonctions de traitement des expressions rationnelles](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP)
afin d'inspecter des chaînes de caractères : `regexp_count()`, `regexp_instr()`,
`regexp_like()` et `regexp_substr()`. PostgreSQL 15 étend aussi la fonction
`range_agg()` pour agréger les [types de données `multirange`](https://www.postgresql.org/docs/15/rangetypes.html),
introduite dans la [version précédente](https://www.postgresql.org/about/press/presskit14/).

PostgreSQL 15 permet aux utilisateurs de
[créer des vues pour requêter des données en utilisant les droits de l'appelant et non ceux du propriétaire de la vue](https://www.postgresql.org/docs/15/sql-createview.html).
Cette option, appelée `security_invoker`, ajoute une couche de sécurité supplémentaire pour
s'assurer que les appelants de vues ont les bons droits pour travailler avec
les données sous-jacentes.

### Plus d'options avec la réplication logique

PostgreSQL 15 offre plus de flexibilité dans la gestion de la
[réplication logique](https://www.postgresql.org/docs/15/logical-replication.html).
Cette version introduit le [filtrage par ligne](https://www.postgresql.org/docs/15/logical-replication-row-filter.html)
et les [listes de colonnes](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)
pour les [fournisseurs](https://www.postgresql.org/docs/15/logical-replication-publication.html),
permettant aux utilisateurs de ne répliquer qu'un sous-ensemble des données
d'une table. PostgreSQL 15 simplifie la [gestion des conflits](https://www.postgresql.org/docs/15/logical-replication-conflicts.html).
Il est ainsi possible de ne pas rejouer une transaction en conflit et de
désactiver automatiquement une souscription si une erreur est détectée. Il est
désormais possible d'utiliser la validation en deux phases (2PC) avec la
réplication logique.

### Amélioration de la journalisation et configuration

PostgreSQL 15 introduit un nouveau format de journalisation :
[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG). Ce nouveau format autorise la sortie des
données de journalisation dans une structure JSON définie. Cela permet aux
journaux de PostgreSQL d'être traités par des systèmes de journalisation
structurés.

Cette version offre plus de flexibilité aux administrateurs de bases de données
dans la gestion de la configuration de PostgreSQL par les utilisateurs. Les
administrateurs peuvent accorder la permission aux utilisateurs de modifier des
paramètres de configuration serveur. De plus, les utilisateurs peuvent
désormais rechercher des informations de configuration en utilisant la commande
`\dconfig` depuis l'outil en ligne de commande [`psql`](https://www.postgresql.org/docs/15/app-psql.html).

### Autres changements importants

Les [statistiques serveur](https://www.postgresql.org/docs/15/monitoring-stats.html)
PostgreSQL sont maintenant collectées en mémoire partagée, éliminant à la fois
le processus de collecte de statistiques et les écritures régulières sur disque
de ces dernières.

PostgreSQL 15 permet de définir une [collation ICU](https://www.postgresql.org/docs/15/collation.html)
comme collation par défaut d'un cluster ou d'une base de données.

Cette version ajoute aussi une nouvelle extension intégrée, [`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html),
qui permet aux utilisateurs d'inspecter les fichiers WAL depuis une interface
SQL.

PostgreSQL 15 [supprime la permission 'CREATE'](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
sur le schéma `public` (ou le schéma par défaut) pour tous les utilisateurs,
sauf le propriétaire de la base de données.

PostgreSQL 15 supprime le mode «&nbsp;exclusive backup&nbsp;» déprécié depuis longtemps,
ainsi que le support de Python 2 pour PL/Python.

### À propos de PostgreSQL

[PostgreSQL](https://www.postgresql.org) est le système de gestion de bases de
données libre de référence. Sa communauté mondiale est composée de plusieurs
milliers d’utilisateurs, contributeurs, entreprises et institutions. Le projet
PostgreSQL, démarré il y a plus de 30 ans à l’université de Californie, à
Berkeley, a atteint aujourd’hui un rythme de développement sans pareil.
L’ensemble des fonctionnalités proposées est mature, et dépasse même celui des
systèmes commerciaux leaders sur les fonctionnalités avancées, les extensions,
la sécurité et la stabilité.

### Liens

* [Téléchargements](https://www.postgresql.org/download/)
* [Notes de version](https://www.postgresql.org/docs/15/release-15.html)
* [Dossier de presse](https://www.postgresql.org/about/press/)
* [Page sécurité](https://www.postgresql.org/support/security/)
* [Police des versions](https://www.postgresql.org/support/versioning/)
* [Suivre @postgresql sur Twitter](https://twitter.com/postgresql)

## En savoir plus sur les fonctionnalités

Pour des explications sur les fonctionnalités ci-dessus et d'autres, merci de
consulter les ressources suivantes :

* [Notes de version](https://www.postgresql.org/docs/15/release-15.html)
* [Matrice de fonctionnalités](https://www.postgresql.org/about/featurematrix/)

## Où télécharger

Il existe plusieurs façons de télécharger PostgreSQL 15, dont :

* la page de [Téléchargements officiels](https://www.postgresql.org/download/),
qui propose des installateurs et outils pour [Windows](https://www.postgresql.org/download/windows/),
[Linux](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/),
et autres OS&nbsp;;
* le [Code Source](https://www.postgresql.org/ftp/source/v15.0).

D'autres outils et extensions sont disponibles sur le [Réseau d'Extension PostgreSQL](http://pgxn.org/).

## Documentation

La documentation au format HTML et les pages de manuel sont installées avec PostgreSQL.
La [documentation en ligne](https://www.postgresql.org/docs/15/), exhaustive et interactive,
peut être parcourue, interrogée et commentée librement.
Une version [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-A4.pdf)
est également disponible.

## Licence

PostgreSQL utilise la [licence PostgreSQL](https://www.postgresql.org/about/licence/),
licence « permissive » de type BSD. Cette [licence certifiée OSI](http://www.opensource.org/licenses/postgresql/)
est largement appréciée pour sa flexibilité et sa compatibilité avec le monde
des affaires, puisqu'elle ne restreint pas l'utilisation de PostgreSQL dans les
applications propriétaires ou commerciales. Associée à un support proposé par
de multiples sociétés et une propriété publique du code, sa licence rend
PostgreSQL très populaire parmi les revendeurs souhaitant embarquer une base de
données dans leurs produits sans avoir à se soucier des prix de licence, des
verrous commerciaux ou modifications des termes de licence.

## Contacts

Site web

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

## Images et logos

Postgres, PostgreSQL et le logo éléphant (Slonik) sont des marques déposées de
l'[Association Canadienne de la Communauté PostgreSQL](https://www.postgres.ca).
Si vous souhaitez utiliser ces marques, vous devez vous conformer à la [politique de la marque](https://www.postgresql.org/about/policies/trademarks/).

## Support professionnel

PostgreSQL bénéficie du support de nombreuses sociétés, qui financent des
développeurs, fournissent l'hébergement ou un support financier.
Les plus fervents supporters sont listés sur la page des [sponsors](https://www.postgresql.org/about/sponsors/).

Il existe également une très grande communauté de​ [sociétés offrant du support PostgreSQL](https://www.postgresql.org/support/professional_support/),
du consultant indépendant aux entreprises multinationales.

Les [dons](https://www.postgresql.org/about/donate/) au PostgreSQL Global
Development Group, ou à l'une des associations à but non lucratif, sont acceptés et encouragés.
