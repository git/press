The PostgreSQL Global Development Group announces that the fourth beta release of
PostgreSQL 15 is now [available for download](https://www.postgresql.org/download/).
This release contains previews of all features that will be available when
PostgreSQL 15 is made generally available, though some details of the release
can change during the beta period.

You can find information about all of the PostgreSQL 15 features and changes in
the [release notes](https://www.postgresql.org/docs/15/release-15.html):

  [https://www.postgresql.org/docs/15/release-15.html](https://www.postgresql.org/docs/15/release-15.html)

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 15 on your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 15 Beta 4 in production environments, we encourage you to find ways
to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that PostgreSQL 15
upholds our standards of delivering a stable, reliable release of the world's
most advanced open source relational database. Please read more about our
[beta testing process](https://www.postgresql.org/developer/beta/) and how you
can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

Upgrading to PostgreSQL 15 Beta 4
---------------------------------

To upgrade to PostgreSQL 15 Beta 4 from an earlier beta or previous version of
PostgreSQL, you will need to use a strategy similar to upgrading between major
versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`). For more
information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/15/static/upgrading.html).

Changes Since Beta 3
--------------------

Fixes and changes in PostgreSQL 15 Beta 4 include:

* The SQL/JSON features proposed for this release have been removed.
* Avoid crash with [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html)
when called inside COPY or a
[common-table expression](https://www.postgresql.org/docs/15/queries-with.html)
(aka `WITH` query).
[`COPY`](https://www.postgresql.org/docs/15/sql-copy.html) statements.
* Enable `table_rewrite` event triggers for `ALTER MATERIALIZED VIEW`.
* Fix crash in `CREATE DATABASE ... STRATEGY WAL_LOG`.
* Fix crash with parallel vacuum.
* Fix issue with
[recovery prefeteching](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)
that could cause a crash on standby promotion.
* Fix LSN returned for error reports of WAL read failures from the
[`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html) extension.

Please see the [release notes](https://www.postgresql.org/docs/15/release-15.html)
for a complete list of new and changed features:

  [https://www.postgresql.org/docs/15/release-15.html](https://www.postgresql.org/docs/15/release-15.html)

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools to find bugs
and regressions before the general availability of PostgreSQL 15. As
this is a beta release, changes to database behaviors, feature details, and
APIs are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_15_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the fourth beta release of version 15. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2022. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 15 Beta Release Notes](https://www.postgresql.org/docs/15/release-15.html)
* [PostgreSQL 15 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_15_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
