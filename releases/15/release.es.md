6 de octubre de 2022 - El Grupo Global de Desarrollo de PostgreSQL ha anunciado hoy el lanzamiento de [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html),
la versión más reciente de la
[base de datos de código abierto más avanzada](https://www.postgresql.org/) del mundo.

PostgreSQL 15 aprovecha las mejoras de rendimiento de las versiones más recientes y ofrece importantes beneficios en la gestión de cargas de trabajo, tanto en implementaciones locales como distribuidas, incluyendo un ordenamiento más eficiente. Esta versión mejora la experiencia del desarrollador introduciendo el popular comando
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html), así como funciones adicionales que permiten monitorear el estado de la base de datos.

"La comunidad de desarrolladores de PostgreSQL sigue creando características que simplifican la ejecución de cargas de trabajo de datos de alto rendimiento y, al mismo tiempo, mejoran la experiencia de los desarrolladores", comentó Jonathan Katz, miembro del Core Team de PostgreSQL. "PostgreSQL 15 demuestra cómo, a través del desarrollo de software libre, podemos ofrecer a nuestros usuarios una base de datos excelente para el desarrollo de aplicaciones y segura para sus datos críticos".

[PostgreSQL](https://www.postgresql.org) es un innovador sistema de gestión de datos conocido por su confiabilidad y robustez. Cuenta con más de 25 años de desarrollo de código abierto por parte de una [comunidad global de desarrolladores](https://www.postgresql.org/community/)
y se ha convertido en la base de datos relacional de código abierto preferida por organizaciones de todos los tamaños.

### Rendimiento del ordenamiento y compresión mejorados

En esta última versión, PostgreSQL mejora sus algoritmos de
[ordenamiento](https://www.postgresql.org/docs/15/queries-order.html) en memoria y en disco, con pruebas comparativas que muestran aumentos de velocidad de entre el 25% y el 400%, según los tipos de datos ordenados. El uso de `row_number()`, `rank()`, `dense_rank()`, y `count()` como
[funciones de ventana deslizante](https://www.postgresql.org/docs/15/functions-window.html)
también ofrece ventajas de rendimiento en PostgreSQL 15. Las consultas que utilizan
[`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT)
ahora pueden
[ejecutarse en paralelo](https://www.postgresql.org/docs/15/parallel-query.html).

Basándose en el trabajo de la [versión anterior de PostgreSQL](https://www.postgresql.org/about/press/presskit14/), con respecto a permitir consultas remotas asíncronas, el
[conector de datos externos de PostgreSQL](https://www.postgresql.org/docs/15/postgres-fdw.html),
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html),
ahora admite
[commits asíncronos](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

Las mejoras en el rendimiento de PostgreSQL 15 se extienden a sus funciones de archivado y respaldo. PostgreSQL 15 añade soporte para la
[compresión de archivos de WAL (write-ahead log)](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION),
con LZ4 y Zstandard (zstd), lo cual puede aportar beneficios en términos de espacio y rendimiento para determinadas cargas de trabajo. En ciertos sistemas operativos, PostgreSQL 15 añade soporte para la
[carga previa (prefetch) de páginas referenciadas en el WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)
con el fin de disminuir los tiempos de recuperación.
[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html), el comando integrado en PostgreSQL para realizar respaldos, ahora admite la compresión de archivos de respaldo en el lado del servidor, con los formatos gzip, LZ4 y zstd. PostgreSQL 15 incluye la posibilidad de utilizar
[módulos personalizados para el archivado](https://www.postgresql.org/docs/15/archive-modules.html),
lo cual elimina la sobrecarga derivada del uso de un comando de shell.

### Características expresivas para el desarrollador

PostgreSQL 15 incluye el comando
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) del estándar SQL. Con `MERGE` es posible escribir sentencias SQL condicionales que pueden incluir acciones `INSERT`,
`UPDATE`, y `DELETE` dentro de la misma sentencia.

Esta última versión añade
[nuevas funciones que permiten utilizar expresiones regulares](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP)
para inspeccionar cadenas: `regexp_count()`, `regexp_instr()`, `regexp_like()`, y
`regexp_substr()`. PostgreSQL 15 también amplía la función `range_agg` para agregar los
[tipos de datos `multirange`](https://www.postgresql.org/docs/15/rangetypes.html),
introducidos en la
[versión anterior](https://www.postgresql.org/about/press/presskit14/).

PostgreSQL 15 permite a los usuarios
[crear vistas para consultar datos usando los permisos de quien invoca la vista, no del creador de la misma](https://www.postgresql.org/docs/15/sql-createview.html).
Esta opción, llamada `security_invoker`, añade una capa adicional de protección para asegurar que los que invocan la vista cuenten con los permisos correctos para trabajar con los datos subyacentes.

### Más opciones para la replicación lógica

PostgreSQL 15 ofrece más flexibilidad para la gestión de la [replicación lógica](https://www.postgresql.org/docs/15/logical-replication.html).
En esta versión se introducen el
[filtrado de filas](https://www.postgresql.org/docs/15/logical-replication-row-filter.html)
y las
[listas de columnas](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)
para los
[publicadores](https://www.postgresql.org/docs/15/logical-replication-publication.html),
lo cual permite a los usuarios elegir replicar un subconjunto de datos procedentes de una tabla. PostgreSQL 15 añade características que simplifican la
[gestión de conflictos](https://www.postgresql.org/docs/15/logical-replication-conflicts.html),
incluyendo la posibilidad de omitir la reproducción de una transacción conflictiva y de desactivar de forma automática una suscripción al detectar un error. Esta versión también incluye soporte para el uso en la replicación lógica de la confirmación en dos fases (2PC).

### Mejoras en el registro y la configuración

PostgreSQL 15 introduce el nuevo formato de registro
[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG).
Este nuevo formato genera datos de registro utilizando una estructura JSON definida, permitiendo que los registros de PostgreSQL sean procesados en sistemas de registro estructurados.

Esta versión ofrece a los administradores de bases de datos más flexibilidad en la forma en que los usuarios pueden gestionar la configuración de PostgreSQL, añadiendo la posibilidad de otorgar a los usuarios permisos para alterar los parámetros de configuración a nivel de servidor. Además, ahora los usuarios pueden buscar información sobre la configuración utilizando el comando `\dconfig` desde la herramienta de línea de comandos de
[`psql`](https://www.postgresql.org/docs/15/app-psql.html).

### Otros cambios destacados

Las
[estadísticas a nivel de servidor](https://www.postgresql.org/docs/15/monitoring-stats.html)
de PostgreSQL se recogen ahora en la memoria compartida, eliminando tanto el proceso de recopilación de estadísticas como la escritura periódica de estos datos en el disco.

PostgreSQL 15 permite hacer una
[intercalación de ICU](https://www.postgresql.org/docs/15/collation.html), sea la intercalación por defecto para un clúster o para una base de datos individual.

Esta versión también añade una nueva extensión integrada,
[`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html), que permite a los usuarios inspeccionar el contenido de los archivos WAL directamente desde una interfaz SQL.

Además, PostgreSQL 15
[revoca el permiso `CREATE` a todos los usuarios](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
(excepto al propietario de la base de datos) en el esquema `public` o en el predefinido.

PostgreSQL 15 elimina tanto la obsoleta modalidad de "respaldo exclusivo", como el soporte para Python 2 de PL/Python.

### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) es la base de datos de código abierto más avanzada del mundo, que cuenta con una comunidad global de miles de usuarios, contribuidores, empresas y organizaciones. Basada en más de 35 años de ingeniería, que comenzaron en la Universidad de Berkeley en California, PostgreSQL ha continuado con un ritmo de desarrollo inigualable. El maduro conjunto de características de PostgreSQL no sólo iguala a los principales sistemas de bases de datos propietarios, sino que los supera en términos de características avanzadas, extensibilidad, seguridad y estabilidad.

### Enlaces

* [Descargas](https://www.postgresql.org/download/)
* [Notas de la versión](https://www.postgresql.org/docs/15/release-15.html)
* [Kit de prensa](https://www.postgresql.org/about/press/)
* [Información de seguridad](https://www.postgresql.org/support/security/)
* [Política de versiones](https://www.postgresql.org/support/versioning/)
* [Sigan @postgresql en Twitter](https://twitter.com/postgresql)

## Más información sobre las características

Para más información sobre las características antes mencionadas y otras más, consulten los siguientes recursos:

* [Notas de la versión](https://www.postgresql.org/docs/15/release-15.html)
* [Tabla de características](https://www.postgresql.org/about/featurematrix/)

## Dónde descargarlo

Hay varias maneras de descargar PostgreSQL 15, que incluyen:

* La [página oficial de descargas](https://www.postgresql.org/download/) que contiene instaladores y herramientas para [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/), etc.
* [Código fuente](https://www.postgresql.org/ftp/source/v15.0)

Otras herramientas y extensiones están disponibles en el
[PostgreSQL Extension Network](http://pgxn.org/).

## Documentación

PostgreSQL 15 incluye documentos HTML y páginas de manual. Es posible también consultar la documentación en línea en formato [HTML](https://www.postgresql.org/docs/15/) y [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf).

## Licencia

PostgreSQL utiliza la [PostgreSQL License](https://www.postgresql.org/about/licence/),
una licencia "permisiva" de tipo BSD. Esta
[licencia certificada por la OSI](http://www.opensource.org/licenses/postgresql/) es ampliamente apreciada por ser flexible y adecuada para las empresas, ya que no limita el uso de PostgreSQL con aplicaciones comerciales y propietarias. Junto con el soporte para múltiples empresas y la propiedad pública del código, nuestra licencia hace que PostgreSQL sea muy popular entre los proveedores que desean integrar una base de datos en sus propios productos sin tener que preocuparse por tarifas, dependencia de un único proveedor o cambios en los términos de la licencia.

## Contactos

Sitio web

* [https://www.postgresql.org/](https://www.postgresql.org/)

Correo electrónico

* [press@postgresql.org](mailto:press@postgresql.org)

## Imágenes y logotipos

Postgres, PostgreSQL y el logo del elefante (Slonik) son todas marcas registradas de la [PostgreSQL Community Association of Canada](https://www.postgres.ca).
Quien desee utilizar estas marcas, deberá cumplir con la [política de marca](https://www.postgresql.org/about/policies/trademarks/).

## Soporte corporativo

PostgreSQL cuenta con el soporte de numerosas empresas, que patrocinan a los desarrolladores, ofrecen recursos de hosting y nos dan apoyo financiero. Consulten nuestra página de
[patrocinadores](https://www.postgresql.org/about/sponsors/) para conocer algunos de los que dan soporte al proyecto.

Existe también una gran comunidad de
[empresas que ofrecen soporte para PostgreSQL](https://www.postgresql.org/support/professional_support/),
desde consultores individuales hasta empresas multinacionales.

Si desean hacer una contribución financiera al Grupo Global de Desarrollo de PostgreSQL o a una de las organizaciones sin fines de lucro reconocidas por la comunidad, visiten nuestra página de [donaciones](https://www.postgresql.org/about/donate/).
