2022년 10월 13일, PostgreSQL 글로벌 개발 그룹은 세상에서
[가장 진보적인 공개 소스 데이터베이스](https://www.postgresql.org/)의 
가장 최신 버전인 [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html)가
출시되었음을 알립니다.

PostgreSQL 15는 향상된 정렬 성능을 포함해서, 로컬 환경과 분산 배포 환경
모두를 고려한 성능 관리를 위한 눈에 띄는 성능 개선을 했습니다.  이번 배포판에는
대중적인 [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) 명령과
데이터베이스 상태를 좀 더 자세히 살펴볼 수 있는 기능들을 추가해서 
개발자 편의성을 좀 더 높혔습니다.

"PostgreSQL 개발자 커뮤니티는 개발자 경험을 향상시킴과 함께 자료 처리 성능도 
보다 좋아지도록 계속 개선해 왔습니다. PostgreSQL 15는 이 데이터베이스를
사용해서 훌륭한 응용 프로그램을 개발하고 그들의 중요한 자료를 안전하게 
지킬 수 있도록 하겠다는 목표 아래 공개 소프트웨어 개발 방식으로 만들어지고
있다는 것이 주목할 점입니다." - PostgreSQL 코어팀 구성원, 조나단 캐츠

신뢰성과 견고성으로 유명한 혁신적인 데이터 관리 시스템인
[PostgreSQL](https://www.postgresql.org)은 
[글로벌 개발자 커뮤니티](https://www.postgresql.org/community/)에서
25년 이상 공개 소스로 개발하고 있습니다.  이렇게 해서 모든 규모의 조직에서
사용하는 공개 소스 관계형 데이터베이스가 되었습니다.

### 정렬 속도 향상과 압축

이번 배포판에서는 메모리 기반, 디스크 기반 모두 
[정렬](https://www.postgresql.org/docs/15/queries-order.html) 알고리즘을
개선해서, 자료형에 따라 25% - 400% 가량 빠르게 처리할 수 있습니다.
`row_number()`, `rank()`, `dense_rank()`, `count()` 같은
[윈도우 함수](https://www.postgresql.org/docs/15/functions-window.html)에서도
PostgreSQL 15에서 성능이 나아졌습니다. 
[`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT)
쿼리가 이제 
[병렬 쿼리](https://www.postgresql.org/docs/15/parallel-query.html)로 실행될 수 있습니다.

[PostgreSQL 외부 자료 싸개](https://www.postgresql.org/docs/15/postgres-fdw.html)인,
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html)는
[이전 PostgreSQL 배포판](https://www.postgresql.org/about/press/presskit14/)에서
비동기식 원격 쿼리를 처음 지원했고, 이제
[비동기식 커밋](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7)을 지원합니다.

PostgreSQL 15에서는 아카이빙과 백업 기능도 개선했습니다.
PostgreSQL 15는 이제 LZ4, Zstandard (zstd) 알고리즘을 이용한
[미리쓰기 로그 파일 압축](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION)을
지원합니다. 이렇게 함으로 기능 향상과 공간 절약을 할 수 있습니다.
또한 대부분 OS에서는 
[prefetch pages referenced in WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)
환경 설정을 이용해 복구 시간을 보다 빠르게 할 수 있습니다.
PostgreSQL 기본 제공 백업 도구인
[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html)은
압축 기법으로 gzip, LZ4, zstd 방식을 사용할 수 있습니다. 또한
아카이빙 작업에서 쉘 명령을 이용함으로 발생하는 부가 비용을 줄일 수 있는
[아카이빙용 사용자 정의 모듈](https://www.postgresql.org/docs/15/archive-modules.html)을 지원합니다.

### 눈에 띄는 개발자 기능

PostgreSQL 15는 표준 SQL 명령어인
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html)를 지원합니다.
`MERGE` 명령은 `INSERT`, `UPDATE`, `DELETE` 명령을 상황에 맞게 하나의 명령으로
처리할 수 있습니다.

이번 최근 배포판은 `regexp_count()`, `regexp_instr()`, `regexp_like()`,
`regexp_substr()` 같은 
[정규식을 다루는 새 함수들](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP)을
추가했습니다.
또한 [이전 배포판](https://www.postgresql.org/about/press/presskit14/ko/)에 처음 소개한
[`multirange` 자료형](https://www.postgresql.org/docs/15/rangetypes.html)을 대상으로
`range_agg` 함수도 쓸 수 있습니다.

PostgreSQL 15에서는 함수의 `security_invoker` 옵션을 뷰에서도 사용할 수 있습니다.
이 옵션은 기존 함수처럼 뷰 소유주만 자료를 다루지 않고 뷰를 사용하는 롤도 다룰 수 있게합니다.
(많은 이야기가 있는데, 여기서는 여기까지만. - 옮긴이)

### 논리 복제 기능에 더 추가된 옵션

PostgreSQL 15는
[논리 복제](https://www.postgresql.org/docs/15/logical-replication.html) 관리를
보다 쉽게 할 수 있습니다.
[배포](https://www.postgresql.org/docs/15/logical-replication-publication.html)에서
[로우 거르기](https://www.postgresql.org/docs/15/logical-replication-row-filter.html),
[칼럼 목록지정](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)을
할 수 있습니다.
이렇게 함으로 테이블의 원하는 부분만 복제할 수 있습니다.
PostgreSQL 15는
충돌하는 트랜잭션 재실행을 건너뛰는 것과 오류 발생시 구독을 자동으로 중지함으로
[자료 충돌 관리](https://www.postgresql.org/docs/15/logical-replication-conflicts.html)가
보다 간단해졌습니다.
아울러, 이제 논리 복제에서는 2단계 커밋(2PC)을 사용할 수 있습니다.

### 로깅과 환경 설정 확장

PostgreSQL 15에서는 새로운 로그 출력 양식을 제공합니다:
[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG).
이 설정은 로그를 JSON 양식으로 출력할 수 있습니다.

이번 배포판은 데이터베이스 관리자가 
일반 사용자에게 서버 차원의 환경 설정 매개변수를 직접 수정할 수 
있도록 권한을 부여할 있어 PostgreSQL 환경 설정을 보다
유연하게 할 수 있도록 제공합니다. 

아울러, [`psql`](https://www.postgresql.org/docs/15/app-psql.html)에서
`\dconfig` 내장 명령어가 추가 되었습니다. 직접 살펴보세요.

### 기타 소개할 것들

[인스턴스 통계 정보](https://www.postgresql.org/docs/15/monitoring-stats.html)
는 통계 수집기 프로세스와 주기적인 디스크 기록 없이 이제 공유 메모리에서 수집됩니다. 

[ICU 정렬규칙](https://www.postgresql.org/docs/15/collation.html)을
클러스터 단위나 개별 데이터베이스 단위로 이제 기본값으로 지정할 수 있습니다.

[`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html) 확장 모듈이 
새롭게 추가 되었습니다.

또한, PostgreSQL 15에서는
일반 사용자가 `public` (또는 기본) 스키마에서 더 이상 
[`CREATE` 권한이 없습니다](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS). 
이게 기본 설정입니다.

PostgreSQL 15에서는 "exclusive backup" 모드가 사라졌으며, 
PL/Python은 더 이상 Python 2버전을 지원하지 않습니다.

### PostgreSQL이란?

[PostgreSQL](https://www.postgresql.org)은 수천 명의 사용자, 공헌자, 회사
및 조직의 범세계적 커뮤니티가 사용, 개발하는 세계에서 가장 진보적인 공개 소스
데이터베이스입니다. PostgreSQL 프로젝트는 캘리포니아 버클리 대학에서 시작하여
35년이 넘는 공학을 기반으로 빠른 속도로 계속 개발되고 있습니다. PostgreSQL의
완성도 높은 기능들은 상용 데이터베이스 시스템과 거의 같으며, 확장성, 보안 및
안정성 측면의 한 발 앞선 기능들은 더 뛰어납니다.

### 링크들

* [다운로드](https://www.postgresql.org/download/)
* [출시 소식](https://www.postgresql.org/docs/15/release-15.html)
* [홍보글](https://www.postgresql.org/about/press/)
* [보안 정보](https://www.postgresql.org/support/security/)
* [버전 정책](https://www.postgresql.org/support/versioning/)
* [트위트 팔로우 @postgresql](https://twitter.com/postgresql)

## 기능에 대해서 더 살펴볼 것들

기능이나 기타 사항에 대해 더 자세히 알고 싶다면, 다음 자료를 살펴보세요:

* [출시 소식](https://www.postgresql.org/docs/15/release-15.html)
* [기능표](https://www.postgresql.org/about/featurematrix/)

## 다운로드 받는 곳

PostgreSQL 15 버전을 다운로드 할 수 있는 방법은 다음과 같습니다:

* [공식 다운로드](https://www.postgresql.org/download/) 페이지에서 
[Windows](https://www.postgresql.org/download/windows/), 
[Linux](https://www.postgresql.org/download/), 
[macOS](https://www.postgresql.org/download/macosx/) 외 기타 운영체제용 설치파일과
도구들을 다운로드 할 수 있습니다.
* [소스 코드](https://www.postgresql.org/ftp/source/v15.0)

그 외 도구들과 확장모듈은 
[PostgreSQL Extension Network](http://pgxn.org/)에서 구할 수 있습니다.

## 문서

PostgreSQL 15는 맨페이지와 HTML 문서를 제공합니다. 
또한 온라인으로 [HTML](https://www.postgresql.org/docs/15/) 양식과
[PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf) 양식도
제공합니다.

## 사용허가권

PostgreSQL은 BSD와 같은 "허용" 라이선스인 
[PostgreSQL 라이선스](http://www.opensource.org/licenses/postgresql/)를 사용합니다. 
이 OSI 인증 라이선스는 상용 또는 독점 응용 프로그램에서
PostgreSQL 사용을 제한하지 않기 때문에 유연하고 비즈니스
친화적이라는 평가를 받고 있습니다. 여러 회사들의 지원과 코드 공개로
이 라이선스는 PostgreSQL을 사용료, 공급업체 종속성, 라이선스 조건 변경에
대한 두려움 없이 자체 제품에 데이터베이스를 내장하려는 공급업체에게 인기가 많습니다.

## 연락처

홈페이지

* [https://www.postgresql.org/](https://www.postgresql.org/)

이메일

* [press@postgresql.org](mailto:press@postgresql.org)

## 이미지와 로고

Postgres, PostgreSQL, 코끼리 로고(슬로닉)는 
[캐나다 PostgreSQL 커뮤니티 협회](https://www.postgres.ca)의 고유 상표권으로
등록되어 있습니다.
이 이미지와 로고를 사용하려면, 
[상표 정책](https://www.postgresql.org/about/policies/trademarks/)을 준수해야합니다.

## 협력 지원

PostgreSQL은 개발자 후원, IT 자원 제공, 재정 지원 등 다양한 캠페인 형태의
지원을 환영합니다.
현재 지원하고 있는 [후원](https://www.postgresql.org/about/sponsors/) 목록을
살펴보세요.

또한 개별 컨설턴트에서 다국적 기업에 이르기까지 
[PostgreSQL 지원](https://www.postgresql.org/support/professional_support/)을
제공하는 대규모 커뮤니티가 있습니다.

PostgreSQL 글로벌 개발 그룹이나 
기타 PostgreSQL 관련 분명한 비영리 커뮤니티에 재정적 기부를 하려면
[기부](https://www.postgresql.org/about/donate/) 페이지를 살펴보십시오.
