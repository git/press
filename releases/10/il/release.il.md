# PostgreSQL 10 שוחררה לאויר

5 באוקטובר 2017 - קבוצת הפיתוח הגלובלית של PostgreSQL הודיעה היום על שחרורה של PostgreSQL 10, הגרסה העדכנית ביותר של מסד הנתונים המתקדם בעולם עם קוד פתוח.

תכונה קריטית של עומסי העבודה המודרניים היא היכולת להפיץ נתונים על פני צמתים רבים עבור גישה מהירה יותר, ניהול וניתוח אשר ידועה גם בשם אסטרטגיה "הפרד ומשול". שחרורה של 10 PostgreSQL כוללת שיפורים משמעותיים ליישם ביעילות את אסטרטגיה "הפרד ומשול", כולל שכפול לוגי מובנה, חלוקה טבלאות הצהרתית, שאילתות מקבילות משופרות.

"קהילת המפתחים שלנו התמקדה בבניית תכונות שינצלו את תצורות של התשתית המודרניות להפצת עומסי עבודה", אמר מגנוס האגנדר, חבר [צוות הליבה] (קישור https://www.postgresql.org/developer/core/) ב
[קבוצת הפיתוח הגלובלית של PostgreSQL] (קישור https://www.postgresql.org/) "תכונות כגון שכפול לוגי והקבלת שאילתות משופרת מייצגות שנים של עבודה ומדגימות את המשך מסירות הקהילה להבטחת עליונותה של Postgres כאשר דרישות של טכנולוגיה מתפתחות".

גירסה זו מסמנת גם את שינוי ערכת הגרסאות של PostgreSQL לפורמט "x.y". משמעות הדבר היא כי גרסה מינורית הבאה של PostgreSQL תהיה 10.1 וגרסה מז'ורית תהיה 11.

## שכפול לוגי - תשתית publish/subscribe להפצת נתונים

שכפול לוגי מרחיב את תכונות השכפול הנוכחיות של PostgreSQL עם היכולת לשלוח שינויים ברמת מסד נתונים או ברמה של הטבלה למסדי נתונים שונים של PostgreSQL. משתמשים יכולים עכשיו לכוונן את הנתונים משוכפלים לאשכולות מסד נתונים שונות ויהיו יכולים לבצע שדרוגים עם אפס זמן השבתה לגרסאות PostgreSQL עתידיים גדולים.


"אנו משתמשים כבדים של PostgreSQL מאז 9.3 ומתלהבים מאוד מהגרסה 10 מכיוון שהיא מביאה את הבסיס להפרדת הטבלאות ולשכפול לוגי מובני. והיא תאפשר לנו להשתמש ב- PostgreSQL בשירותים אחרים גם", אמר וולדימיר בורודין, ראש צוות DBA  ב [Yandex] (קישור https://www.yandex.com/).

##  חלוקה טבלאות הצהרתית - נוחות חלוקת הנתונים שלך

חלוקת טבלת קיימת במשך שנים ב- PostgreSQL, אך נדרש nהמשתמש לשמור סט לא טריוויאלי של כללים טריגרים על מנת לגרום לחלוקה לעבוד. PostgreSQL 10 מציגה תחביר חלוקת הטבלה המאפשר למשתמשים בקלות ליצור ולשמור טווח ולרשום טבלאות מחולקות. הוספת תחביר חלוקה היא הצעד הראשון בסדרה של תכונות מתוכננות כדי לספק תשתית חלוקה חזקה בתוך PostgreSQL.

## שאילתות מקבילות משופרות - במהירות לכבוש את הניתוחים שלך

PostgreSQL 10 מספקת תמיכה טובה יותר לשאילתות מקבילות בכך שהיא מאפשרת לחלקים נוספים של תהליך ביצוע השאילתה לרוץ במקביל. השיפורים כוללים סוגים נוספים של סריקות נתונים מקביליות, כמו גם אופטימיזציה כאשר הנתונים מסודרים מחדש, כגון מיון מראש. שיפורים אלה מאפשרים להחזיר תוצאות במהירות רבה יותר.

## מניין להתחייבות ביצוע בשכפול נתונים סינכרוני - להפיץ נתונים בביטחון

PostgreSQL 10 מציגה מניין להתחייבות ביצוע עבור שכפול סינכרוני, אשר מאפשר גמישות כיצד מסד נתונים ראשי מקבל הודאה כי השינויים נכתבו בהצלחה בעותקים משוכפלים מרוחקים. מנהל מערכת יכול כעת לציין כי אם מספר כלשהו של עותקים משוכפלים הודיעו כי נעשה שינוי למסד הנתונים, אז הנתונים יכולים להיחשב כנכתב בבטחה.

"מניין להתחייבות ביצוע בשכפול נתונים סינכרוני ב- PostgreSQL 10 מעניק יותר אפשרויות להרחיב את יכולתנו לקדם את תשתיות מסד הנתונים עם זמן השבתה כמעט אפסי מנקודת מבט של היישום. זה מאפשר לנו ברציפות לפרוס ולעדכן את תשתית מסד הנתונים שלנו מבלי לגרום לחלונות תחזוקה ארוכים", אמר קורט מיקול , מהנדס צוות תשתיות ב- [Simple Finance] (קישור https://www.simple.com/).

## אימות SCRAM-SHA-256 - אבטחת גישה לנתונים שלך

The Salted Challenge Response Authentication Mechanism (ר"ת SCRAM) המוגדר ב [RFC5802] (קישור https://tools.ietf.org/html/rfc5802) מגדיר פרוטוקול לשפר את האחסון המאובטח והעברת הסיסמאות על-ידי מתן תשתית למשא ומתן חזק עם סיסמאות . PostgreSQL 10 מציג אתה שיטת האימות SCRAM-SHA-256, המוגדרת ב [RFC7677] (קישור https://tools.ietf.org/html/rfc7677
) כדי לספק אבטחה טובה יותר מאשר שיטת אימות הסיסמאות הקיימת המבוססת על MD5.

קישורים
-----

* [הורדות] (https://www.postgresql.org/downloads)
* [ערכת העיתונות] (https://www.postgresql.org/about/press/presskit10)
* [הערות הגרסה] (https://www.postgresql.org/docs/current/static/release-10.html)
* [מה חדש ב -10] (https://wiki.postgresql.org/wiki/New_in_postgres_10)

אודות PostgreSQL
----------------

הפרויקט PostgreSQL נבנה על מעל 30 שנות הנדסה, החל באוניברסיטת קליפורניה, ברקלי, והמשיך להתפתח עם קצב ללא תחרות. ערכת התכונות הבשלות של PostgreSQL לא רק תואמת מערכות נתונים קנייניות מובילות, אלא עולה עליהן במאפיינים של מסדי נתונים מתקדמים ,הרחבה, אבטחה ויציבות. למידע נוסף על PostgreSQL והשתתפות בקהילה שלנו ניתן ב [PostgreSQL.org] (קישור https://www.postgresql.org).
