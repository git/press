# PostgreSQL 10 erschienen

5. OKTOBER 2017 - Die  PostgreSQL Global Development Group hat heute die Veröffentlichung von PostgreSQL 10, der aktuellsten Version des weltweit führenden Open-Source-SQL-Datenbanksystems, bekannt gegeben.

Ein kritisches Merkmal moderner Workloads ist die Fähigkeit, Daten über viele Knoten hinweg zu verteilen, um schnelleren Zugriff, Management und die Analyse zu ermöglichen, was auch als "teile und herrsche" - Strategie bekannt ist. PostgreSQL Version 10 beinhaltet signifikante Verbesserungen, um die "Teile und Herrsche Strategie" effektiv umzusetzen, einschließlich der nativen logischen Replikation, der deklarativen Tabellenpartitionierung und der verbesserten Abfrage Parallelität.

"Unsere Entwickler-Community konzentrierte sich auf den Aufbau von Features, die moderne Infrastruktur-Setups für die Verteilung von Workloads nutzen würden", sagte Magnus Hagander, ein [Kernteam] (https://www.postgresql.org/developer/core/) Mitglied der [PostgreSQL Global Development Group] (https://www.postgresql.org/). "Features wie logische Replikation und verbesserte Abfrage Parallelität sind das Ergebnis jahrelanger Arbeit und demonstrieren das kontinuierliche Engagement der Community, um Postgres die Führung in einem sich technologisch wandelnden Umfeld zu ermöglichen."

Diese Version markiert ebenfalls die Änderung des Versionsschemas für PostgreSQL in ein "x.y" Format. Dies bedeutet, dass die nächste Minor Version von PostgreSQL 10.1 sein wird, und das nächste Major Release 11 ist.

## Logische Replikation - Ein publish/subscribe Framework für verteilte Daten

Die logische Replikation erweitert die aktuell bereits vorhandenen Merkmale der PostgreSQL Replikation um die Möglichkeit, Modifikationen auf der Ebene der Datenbank und pro Tabellenebene an verschiedene PostgreSQL Datenbanken zu senden. Benutzer können nun die Daten, die auf verschiedene Datenbankcluster repliziert werden, fein abstimmen, und haben so die Möglichkeit, Zero-Downtime-Upgrades für zukünftige PostgreSQL-Versionen durchzuführen.

"Wir nutzen PostgreSQL sehr umfangreich seit 9.3 und freuen uns sehr über Version 10, da es eine Basis für die lang erwartete Partitionierung und eine integrierte logische Replikation bietet. Das wird uns erlauben, PostgreSQL in noch mehr Services zu nutzen", sagte Vladimir Borodin, DBA Team Lead bei [Yandex] (https://www.yandex.com/).

## Deklarative Tabellenpartitionierung - Bequemlichkeit bei der Aufteilung Ihrer Daten

Die Tabellenpartitionierung existiert seit Jahren in PostgreSQL, erfordert aber einen Nutzer um einen nicht-trivialen Satz von Regeln und Triggern für die Partitionierung zu pflegen. PostgreSQL 10 führt eine Syntax für die Tabellenpartitionierung ein, mit der Benutzer problemlos Range und List Partitionen erstellen und pflegen können. Das Hinzufügen der Partitionierungs-Syntax ist der erste Schritt in einer Reihe von geplanten Features, um ein robustes Framework für die Partitionierung innerhalb von PostgreSQL bereitzustellen.

## Verbesserte Abfrage Parallelität - schnell erobern Sie Ihre Analyse

PostgreSQL 10 bietet eine bessere Unterstützung für parallele Abfragen, indem mehr Teile des Prozesses für die Ausführung von Abfragen parallelisiert werden. Verbesserungen beinhalten zusätzliche Arten von Daten-Scans, die parallelisiert werden, sowie Optimierungen, wenn die Daten rekombiniert werden, wie z. B. Vorsortierung. Mit diesen Erweiterungen können die Ergebnisse schneller zurück geliefert werden.

## Quorum Commit für synchrone Replikation - Daten mit Zuversicht verteilen

PostgreSQL 10 führt Quorum-Commit für die synchrone Replikation ein, was eine Flexibilität bei der Bestätigung für die primäre Datenbank ermöglicht. Dabei wird bestätigt dass Änderungen erfolgreich in Remote-Repliken geschrieben wurden. Ein Administrator kann nun festlegen, dass wenn eine Anzahl von Replikaten bestätigt hat, dass eine Änderung der Datenbank vorgenommen wurde, die Daten als sicher geschrieben angesehen werden können.

"Quorum Commit für die synchrone Replikation in PostgreSQL 10 bietet mehr Möglichkeiten um die Infrastruktur der Datenbank mit nahezu null Ausfallzeiten aus der Anwendungsperspektive zu erweitern. Damit können wir unsere Datenbank Infrastruktur kontinuierlich einsetzen und aktualisieren, ohne dass lange Wartungsfenster entstehen", so Curt Micol , Personal Infrastructure Engineer bei [Simple Finance] (https://www.simple.com/).

## SCRAM-SHA-256 Authentifizierung - Sichern Sie Ihren Datenzugriff

Der in [RFC5802] (https://tools.ietf.org/html/rfc5802) definierte Salted Challenge Response Authentication Mechanism (SCRAM) definiert ein Protokoll zur Verbesserung der sicheren Speicherung und Übertragung von Passwörtern durch die Bereitstellung eines Frameworks für eine starke Passwortverhandlung . PostgreSQL 10 implementiert die SCRAM-SHA-256-Authentifizierungsmethode, die in [RFC7677] (https://tools.ietf.org/html/rfc7677) definiert ist, um eine verbesserte Sicherheit gegenüber der vorhandenen MD5-basierten Authentifizierungsmethode mittels Passwörtern zu ermöglichen.

Links
-----

* [Downloads](https://www.postgresql.org/downloads)
* [Pressemappe](https://www.postgresql.org/about/press/presskit10)
* [Release Notes](https://www.postgresql.org/docs/current/static/release-10.html)
* [Was ist neu in 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)

Über PostgreSQL
----------------

PostgreSQL ist das führende Open-Source Datenbanksystem, mit einer weltweiten Community bestehend aus Tausenden von Nutzern und Mitwirkenden sowie Dutzenden von Firmen und Organisationen. Das PostgreSQL Projekt baut auf über 30 Jahre Erfahrung auf, beginnend an der University of California, Berkeley, und hat heute eine nicht zu vergleichende Performance bei der Entwicklung. PostgreSQL's ausgereiftes Feature Set ist nicht nur mit den führenden proprietären Datenbanksystemen vergleichbar, sondern übertrifft diese in erweiterten Datenbankfunktionen, Erweiterbarkeit, Sicherheit und Stabilität. Lerne mehr über PostgreSQL und nimm an unserer Community teil, unter: http://www.postgresql.org.
