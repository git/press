# PostgreSQL 10 版本发布！

2017-10-05 -  PostgreSQL全球开发组今天宣布，PostgreSQL 10 正式发布，PostgreSQL作为世界上最先进的开源关系型数据库， 10 是迄今为止的最新版本。

要作为现代工作负载(modern workloads)， 其关键特性之一便是具备横跨多节点处理数据，从而达到更快访问、管理与分析数据的能力，即数据可被"分而治之(divide and conquer)"。PostgreSQL 10.0版本所具备的重要改进，如逻辑复制、显示声明的表分区功能与改进后的并行查询功能等，高效地实现了"分而治之(divide and conquer)"。

"PG开发者社区一直以来，着重构建那些可充分适用于现代基础网络架构(modern infrastructure setups)的新特性"。 作为[PostgreSQL全球开发组](https://www.postgresql.org/) 核心成员(https://www.postgresql.org/developer/core/) 之一，Magnus Hagander 还表示，对逻辑复制和改进后的并行查询等功能的研发，代表着社区多年的努力成果，也表明随着技术需求的发展, 社区会继续努力以确保 Postgres 的领导地位。

PostgreSQL 10也标志着PostgreSQL的版本号更新实现了"x.y"的转变。这意味着下一次小版本的 PostgreSQL 将是 10.1，而下个大版本将是11。

## 逻辑复制-用于分发数据的发布/订阅(publish/subscribe)框架

PostgreSQL 10 的逻辑复制对现有的PostgreSQL复制特性进行了扩展，新版本的逻辑复制可以将单个数据库级别或者表级别的改动(modifications)发送至不同的PostgreSQL 数据库。这意味着用户可以细粒度地将数据变化发送到不同的数据库集群(database clusters)，甚至在大版本升级期间，实现零停机 (zero-downtime)。

"自9.3版本发布后，Yandex 公司便广泛使用PostgreSQL数据库产品，现在我们惊讶于PostgreSQL10，因为10版本终于给我们带来了翘首以盼的分区功能和内置的逻辑复制。这将会使得PostgreSQL 应用到Yandex公司更多的服务上，"Vladimir Borodin 表示。Vladimir Borodin，[Yandex](https://www.yandex.com/)Yandex公司的DBA团队负责人。

## 显示声明的表分区功能 - 更加方便地进行数据分区

PostgreSQL中的表分区功能早已存在多年，不过表分区功能的正常使用却要求用户维护一套具有一定复杂度的分区规则和触发器，用以保证分区机制正常运行。PostgreSQL 10版本引入了新的表分区的语法，这允许用户更加方便地进行创建与维护范围类型的分区和列表类型的分区。增加表分区语法，只是PostgreSQL内部用以提供稳健的分区机制的一系列特性的第一步而已。

## 改进后的并行查询 - 更快满足你的数据分析需求

PostgreSQL 10 版本可以通过对查询过程中更多的步骤进行并行化执行，从而对并行查询提供了更好地支持。改进内容包括当数据重组时(recombined)，PostgreSQL10支持更多的数据扫描类型被并行执行和优化，例如预排序操作。这些增强的特性可以更快的返回结果值。

## 同步复制的优选提交(Quorum Commit)  - 更加自信地分发数据

PostgreSQL10 引入了优化提交机制(Quorum Commit)，这使得主数据库在接收到远程备库更新成功的确认信息时具备灵活性。数据库管理员现在可以指定当固定数量的备库已确认对更新进行写入后，数据才被认为是安全写入。

"PostgreSQL 10.0版本中的同步复制的优选提交（Quorum Commit，即通过读写副本数的优化来平衡读写性能）功能，从应用程序的角度给了我们更多的选择，使得我们具备以几乎零停机的代价来扩展我们基础设施的能力。这允许我们可以连续发布或是更新数据库基础设施而不必忍受长时间停机维护的代价,"Curt Micol表示。Curt Micol，[Simplem财务公司](https://www.simple.com/)的高级基础设施工程师。

## 安全认证机制 - 确保数据访问安全性。

SCRAM安全认证算法在 [RFC5802](https://tools.ietf.org/html/rfc5802)进行了详细的定义，它是通过提供一个强加密的框架来改进密码的安全存储和传输的协议。PostgreSQL 10版本引入的SCRAM-SHA-256 安全认证方法， 在[RFC7677](https://tools.ietf.org/html/rfc7677)中有详细定义，它可以提供比现在基于MD5加密算法更好的安全性。

相关链接
-----

* [下载](https://www.postgresql.org/downloads)
* [新闻发布稿](https://www.postgresql.org/about/press/presskit10)
* [详细发布说明](https://www.postgresql.org/docs/current/static/release-10.html)
* [10.0版本中的新增功能](https://wiki.postgresql.org/wiki/New_in_postgres_10)


关于PostgreSQL数据库
----------------

PostgreSQL是最先进的开源关系型数据库，它的全球社区是一个由数千名用户、开发人员、公司或其他组织而组成。
PostgreSQL项目有30年以上的历史，起源于加利福利亚的伯克利大学，经历了无数次开发升级。
PostgreSQL的专业特性不仅包含顶级商业数据库系统的功能特性，更是在高级数据库功能、数据库扩展性、数据库安全性和稳定性方面超过了它们。
若想获取到更多关于PostgreSQL的信息或者加入到PostgreSQL社区，请访问 [PostgreSQL.org](https://www.postgresql.org)
