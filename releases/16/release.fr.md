Le 14 septembre 2023 - Le PostgreSQL Global Development Group annonce
aujourd’hui la publication de PostgreSQL 16, dernière version de la base de
données open source la plus avancée du monde.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) augmente ses
 performances avec des améliorations notables apportées dans la parallélisation
des requêtes, le chargement en masse et la réplication logique. Cette version
inclut de nombreuses fonctionnalités pour les développeurs et développeuses et
pour les administrateurs et administratrices en embarquant plus d'éléments de
syntaxes SQL/JSON, de nouvelles statistiques de suivi de la charge de travail et
plus de flexibilité dans la définition des règles de contrôle d'accès pour la
gestion des politiques applicables à de grandes flottes de serveurs.

« Les utilisations des bases de données évoluant, PostgreSQL continue
d’améliorer les performances en recherche et gestion des données à mesure que
les volumes augmentent, » déclare Dave Page, membre de la Core-Team PostgreSQL.
« PostgreSQL 16 offre aux utilisateurs et utilisatrices de nouvelles méthodes
pour accroître et répartir la charge, tout en offrant de nouveaux moyens
d’observer et optimiser la gestion de leurs données. »

Connu pour sa fiabilité et sa robustesse, PostgreSQL est le résultat d'un
développement open source de plus de 25 ans par une communauté mondiale de
développement. PostgreSQL est devenu le système de gestion de base de données
relationnelles open source préféré d'organismes de toutes tailles.

PostgreSQL, système innovant de gestion des données, connu pour sa fiabilité et
sa robustesse, bénéficie depuis plus de 35 ans d'un développement open source
par une communauté de développeurs et développeuses mondiale. Il est devenu le
système de gestion de bases de données relationnelles de référence pour des
organisations de toute taille.


### Améliorations des performances

PostgreSQL 16 améliore la performance de fonctionnalités existantes via de
nouvelles optimisations du planificateur de requêtes. Dans cette nouvelle
version, le planificateur de requête peut
[paralléliser](https://www.postgresql.org/docs/16/parallel-query.html) les
[jointures](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN)
`FULL` et `RIGHT`, générer de meilleurs plans pour les requêtes qui utilisent
des [fonction
d’agrégation]((https://www.postgresql.org/docs/16/functions-aggregate.html) avec
des clauses `DISTINCT` ou `ORDER BY`, bénéficier de tris incrémentaux pour les
requêtes [`SELECT
DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT)
et d’exécuter des [requêtes
fenêtrées](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS)
plus efficacement. Il introduit également les « anti-jointures » `RIGHT` et
`OUTER` permettant ainsi d'identifier les lignes qui ne sont pas présentes dans
une table jointe.

Cette version inclut des améliorations sur le chargement en masse utilisant
[`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) en opérations
unitaires ou concurrentes. Les tests ont pu montrer un gain de performance
allant jusqu'à 300% dans certaines conditions. PostgreSQL ajoute également le
support de la [répartition de
charge](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS)pour
les clients utilisant `libpq` et des améliorations dans la stratégie de vacuum
réduisant la nécessité de verrouiller l'ensemble d'une table. De plus,
PostgreSQL 16 introduit une accélération processeur en utilisant `SIMD` pour les
architectures x86 et ARM. Cette accélération permet des gains de performance
lors du traitement des chaînes ASCII et des types JSON ainsi que lors de
recherches dans les tableaux ou les sous-transactions.


### Réplication logique

La [réplication
logique](https://www.postgresql.org/docs/16/logical-replication.html) permet aux
utilisateurs et utilisatrices de PostgreSQL d’envoyer des données au fil de
l’eau à d’autres instances PostgreSQL ou à des destinataires capables
d'interpréter le protocole de réplication logique de PostgreSQL. À partir de
PostgreSQL 16, les utilisateurs et utilisatrices peuvent utiliser la réplication
logique depuis une instance de réplication physique secondaire. Cela signifie
qu’une instance secondaire peut publier des modifications logiques à destination
d’autres serveurs. Les développeurs et développeuses ont ainsi accès à de
nouvelles options de répartition de la charge – par exemple, en utilisant un
serveur secondaire en lieu et place du primaire déjà fortement chargé pour
répliquer des modifications logiques vers des serveurs en aval.

PostgreSQL 16 apporte également de nombreuses améliorations des performances de
la réplication logique. Les souscripteurs peuvent maintenant appliquer de larges
transactions en utilisant le parallélisme. Pour les tables sans [clé
primaire](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS),
les souscripteurs peuvent utiliser un index B-tree en lieu et place des parcours
séquentiels pour trouver des lignes. Sous certaines conditions, les utilisateurs
et utilisatrices peuvent aussi accélérer la synchronisation des tables en
utilisant un format binaire.

PostgreSQL 16 inclut plusieurs améliorations de gestion des accès comme le
nouveau [rôle
prédéfini](https://www.postgresql.org/docs/16/predefined-roles.html)
`pg_create_subscription` qui permet d’accorder le droit de créer de nouveaux
abonnements logiques. Enfin, cette version apporte les prémices d’une
réplication logique bidirectionnelle en permettant de répliquer des données
entre deux tables provenant de sources différentes.


### Expérience développeur/développeuse

PostgreSQL 16 ajoute des syntaxes du standard
[SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html) comme les
constructeurs et prédicats `JSON_ARRAY()`, `JSON_ARRAYAGG()`, et `IS JSON`.
Cette version propose également la possibilité d’utiliser des underscores comme
séparateur de milliers (par exemple `5_432_000`) pour les valeurs numériques
entières comme `0x1538`, `0o12470`, and `0b1010100111000`.

Les développeurs et développeuses utilisant PostgreSQL 16 bénéficient également
de l’ajout de plusieurs commandes au client `psql`, comme la commande
[`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND),
qui permet aux utilisateurs et utilisatrices d’exécuter une requête paramétrée
(par exemple `SELECT $1 + $2`) puis d’utiliser `\bind` pour valoriser les
variables.

PostgreSQL 16 améliore de manière le support général des différentes
[collations](https://www.postgresql.org/docs/16/collation.html), qui définissent
les règles de tri des champs texte. PostgreSQL 16,  compilé par défaut avec
prise en charge de ICU (International Components for Unicode), détermine la
locale ICU par défaut à partir de l'environnement et permet aux utilisateurs et
utilisatrices de définir des comportements personnalisés liés à une collation
ICU.


### Suivi d'exploitation

Un des points clés du réglage de la performance d’un trafic de base de données
est de comprendre l’impact des opérations d’entrées/sorties sur son système.
PostgreSQL 16 simplifie l’analyse de ces données avec la nouvelle vue
[`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW)
qui enregistre les statistiques clés des entrées/sorties comme le taux de succès
dans le `shared_buffer` et la latence en entrée/sortie.

Cette version ajoute également un nouveau champ à la vue
[`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW)
qui indique le dernier instant où une table ou un index a été parcouru.
PostgreSQL 16 rend aussi
[`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) plus
lisible en traçant les valeurs passées dans les requêtes paramétrées et améliore
la précision de l’algorithme de suivi des requêtes utilisé par
[`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html)
et
[`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW).


### Contrôle d'accès et sécurité

PostgreSQL 16 fournit des options plus fines pour la gestion du contrôle d’accès
et améliore les fonctionnalités de sécurité. Cette version améliore la gestion
des fichiers
[`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) et
[`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html) en
incluant la possibilité de définir des expressions rationnelles applicables sur
les noms des utilisateurs et utilisatrices et des bases de données ainsi que la
directive `include` pour les fichiers de configuration externes.

Cette version ajoute plusieurs paramètres de connexion orientés sécurité au
niveau des clients. Ainsi `require_auth` permet à un client de spécifier les
paramètres d’authentification qu’il accepte de la part du serveur. Le
paramètre
[`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT)
permet d’indiquer à PostgreSQL qu’il doit utiliser le magasin d’autorité de
certification de confiance (CA) fourni par le système d’exploitation du client.
De plus, cette version ajoute le support de la délégation d’identification de
Kerberos. Cela permet aux extensions comme
[`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) et
[`dblink`](https://www.postgresql.org/docs/16/dblink.html) d’utiliser ce
mécanisme d'identification pour se connecter à des services de confiance.


### À propos de PostgreSQL

[PostgreSQL](https://www.postgresql.org) est le système de gestion de bases de
données libre de référence. Sa communauté mondiale est composée de plusieurs
milliers d’utilisateurs, utilisatrices, contributeurs, contributrices,
entreprises et institutions. Le projet PostgreSQL, démarré il y a plus de 30 ans
à l’université de Californie, à Berkeley, a atteint aujourd’hui un rythme de
développement sans pareil. L’ensemble des fonctionnalités proposées est mature,
et dépasse même celui des systèmes commerciaux leaders sur les fonctionnalités
avancées, les extensions, la sécurité et la stabilité.


### Liens

* [Téléchargements](https://www.postgresql.org/download/)
* [Notes de version](https://www.postgresql.org/docs/16/release-16.html)
* [Dossier de presse](https://www.postgresql.org/about/press/)
* [Page sécurité](https://www.postgresql.org/support/security/)
* [Politique des versions](https://www.postgresql.org/support/versioning/)
* [Suivre @postgresql sur Twitter](https://twitter.com/postgresql)
* [Dons](https://www.postgresql.org/about/donate/)


##  En savoir plus sur les fonctionnalités

Pour de plus amples informations sur les fonctionnalités ci-dessus et toutes les
autres, vous pouvez consulter les liens suivants :

* [Notes de version](https://www.postgresql.org/docs/16/release-16.html)
* [Matrice de fonctionnalités](https://www.postgresql.org/about/featurematrix/)


## Où télécharger

Il existe plusieurs façons de télécharger PostgreSQL 16, dont :

* la [page de téléchargement](https://www.postgresql.org/download/) , qui
  contient les installateurs et les outils pour
  [Windows](https://www.postgresql.org/download/windows/),
  [Linux](https://www.postgresql.org/download/),
  [macOS](https://www.postgresql.org/download/macosx/), et bien plus ;
* le [code source](https://www.postgresql.org/ftp/source/v16.0).

D'autres outils et extensions sont disponibles sur le [PostgreSQL Extension
Network](http://pgxn.org/).


## Documentation

La documentation au format HTML et les pages de manuel sont installées avec
PostgreSQL. La documentation peut également être [consultée en
ligne](https://www.postgresql.org/docs/16/) ou récupérée au format
[PDF](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-A4.pdf).


## Licence

PostgreSQL utilise la [licence
PostgreSQL](https://www.postgresql.org/about/licence/), licence « permissive »
de type BSD. Cette [licence certifiée
OSI](http://www.opensource.org/licenses/postgresql/) est largement appréciée
pour sa flexibilité et sa compatibilité avec le monde des affaires, puisqu'elle
ne restreint pas l'utilisation de PostgreSQL dans les applications propriétaires
ou commerciales. Associée à un support proposé par de multiples sociétés et une
propriété publique du code, sa licence rend PostgreSQL très populaire parmi les
revendeurs souhaitant embarquer une base de données dans leurs produits sans
avoir à se soucier des prix de licence, des verrous commerciaux ou modifications
des termes de licence.


## Contacts

Site internet

* [https://www.postgresql.org/](https://www.postgresql.org/)

Courriel

* [press@postgresql.org](mailto:press@postgresql.org) ou
* [fr@postgreql.org](mailto:fr@postgresql.org) pour un contact francophone


## Images et logos

Postgres, PostgreSQL et le logo éléphant (Slonik) sont des marques déposées de
l'[Association de la Communauté PostgreSQL](https://www.postgres.ca). Si vous
souhaitez utiliser ces marques, vous devez vous conformer à la [politique de la
marque](https://www.postgresql.org/about/policies/trademarks/).


## Support professionnel

PostgreSQL bénéficie du support de nombreuses sociétés, qui financent des
développeurs et développeuses, fournissent l'hébergement ou un support
financier. Les plus fervents supporters sont listés sur la page des
[sponsors](https://www.postgresql.org/about/sponsors/).

Il existe également une très grande communauté de​ [sociétés offrant du support
PostgreSQL](https://www.postgresql.org/support/professional_support/), du
consultant indépendant aux entreprises multinationales.

Les [dons](https://www.postgresql.org/about/donate/) au PostgreSQL Global
Development Group, ou à l'une des associations à but non lucratif, sont acceptés
et encouragés.
