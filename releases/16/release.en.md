September 14, 2023 - The PostgreSQL Global Development Group today announced the
release of PostgreSQL 16, the latest version of the world's most advanced open
source database.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) raises its
performance, with notable improvements to query parallelism, bulk data loading,
and logical replication. There are many features in this release for developers
and administrators alike, including more SQL/JSON syntax, new monitoring stats
for your workloads, and greater flexibility in defining access control rules for
management of policies across large fleets.

"As relational database patterns evolve, PostgreSQL continues to make
performance gains in searching and managing data at scale," said Dave Page, a
PostgreSQL Core Team member. "PostgreSQL 16 gives users more methods to scale-up
and scale-out their workloads, while giving them new ways to gain insights and
optimize how they manage their data."

PostgreSQL, an innovative data management system known for its reliability and
robustness, benefits from over 25 years of open source development from a global
developer community and has become the preferred open source relational database
for organizations of all sizes.

### Performance Improvements

PostgreSQL 16 improves the performance of existing PostgreSQL functionality
through new query planner optimizations. In this latest release, the
[query planner can parallelize](https://www.postgresql.org/docs/16/parallel-query.html)
`FULL` and `RIGHT`
[joins](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN),
generate better optimized plans for queries that use
[aggregate functions](https://www.postgresql.org/docs/16/functions-aggregate.html)
with a `DISTINCT` or `ORDER BY` clause, utilize incremental sorts for
[`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT)
queries, and optimize
[window functions](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS)
so they execute more efficiently. It also improves `RIGHT` and `OUTER`
"anti-joins", which enables users to identify rows not present in a joined
table.

This release includes improvements for bulk loading using
[`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) in both single
and concurrent operations, with tests showing up to a 300% performance
improvement in some cases. PostgreSQL 16 adds support for
[load balancing](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS)
in clients that use `libpq`, and improvements to vacuum strategy that reduce the
necessity of full-table freezes. Additionally, PostgreSQL 16 introduces CPU
acceleration using `SIMD` in both x86 and ARM architectures, resulting in
performance gains when processing ASCII and JSON strings, and performing array
and subtransaction searches.

### Logical replication 

[Logical replication](https://www.postgresql.org/docs/16/logical-replication.html)
lets users stream data to other PostgreSQL instances or subscribers that can
interpret the PostgreSQL logical replication protocol. In PostgreSQL 16, users
can perform logical replication from a standby instance, meaning a standby can
publish logical changes to other servers. This provides developers with new
workload distribution options, for example, using a standby rather than the
busier primary to logically replicate changes to downstream systems.

Additionally, there are several performance improvements in PostgreSQL 16 to
logical replication. Subscribers can now apply large transactions using parallel
workers. For tables that do not have a [primary key](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS), subscribers can use B-tree
indexes instead of sequential scans to find rows. Under certain conditions,
users can also speed up initial table synchronization using the binary format.

There are several access control improvements to logical replication in
PostgreSQL 16, including the new
[predefined role](https://www.postgresql.org/docs/16/predefined-roles.html)
`pg_create_subscription`, which grants users the ability to create new logical
subscriptions. Finally, this release begins adding support for bidirectional
logical replication, introducing functionality to replicate data between two
tables from different publishers.

### Developer Experience

PostgreSQL 16 adds more syntax from the
[SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html) standard,
including constructors and predicates such as `JSON_ARRAY()`, `JSON_ARRAYAGG()`,
and `IS JSON`. This release also introduces the ability to use underscores for
thousands separators (e.g. `5_432_000`) and non-decimal integer literals, such
as `0x1538`, `0o12470`, and `0b1010100111000`.

Developers using PostgreSQL 16 also benefit from new commands in `psql`. This
includes
[`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND),
which allows users to prepare parameterized queries and use `\bind` to
substitute the variables (e.g `SELECT $1::int + $2::int \bind 1 2 \g `). 

PostgreSQL 16 improves general support for
[text collations](https://www.postgresql.org/docs/16/collation.html), which
provide rules for how text is sorted. PostgreSQL 16 builds with ICU support by
default, determines the default ICU locale from the environment, and allows
users to define custom ICU collation rules.

### Monitoring

A key aspect of tuning the performance of database workloads is understanding
the impact of your I/O operations on your system. PostgreSQL 16 introduces
[`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW),
a new source of key I/O metrics for granular analysis of I/O access patterns.

Additionally, this release adds a new field to the
[`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW)
view  that records a timestamp representing when a table or index was last
scanned. PostgreSQL 16 also makes
[`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) more
readable by logging values passed into parameterized statements, and improves
the accuracy of the query tracking algorithm used by
[`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html)
and [`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW).

### Access Control & Security

PostgreSQL 16 provides finer-grained options for access control and enhances
other security features. The release improves management of
[`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) and
[`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html)
files, including allowing regular expression matching for user and database
names and `include` directives for external configuration files.

This release adds several security-oriented client connection parameters,
including `require_auth`, which allows clients to specify which authentication
parameters they are willing to accept from a server, and
[`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT),
which indicates that PostgreSQL should use the trusted certificate authority
(CA) store provided by the client's operating system. Additionally, the release
adds support for Kerberos credential delegation, allowing extensions such as
[`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) and
[`dblink`](https://www.postgresql.org/docs/16/dblink.html) to use authenticated
credentials to connect to trusted services.

### About PostgreSQL

[PostgreSQL](https://www.postgresql.org) is the world's most advanced open
source database, with a global community of thousands of users, contributors,
companies and organizations. Built on over 35 years of engineering, starting at
the University of California, Berkeley, PostgreSQL has continued with an
unmatched pace of development. PostgreSQL's mature feature set not only matches
top proprietary database systems, but exceeds them in advanced database
features, extensibility, security, and stability.

### Links

* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/16/release-16.html)
* [Press Kit](https://www.postgresql.org/about/press/)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql](https://twitter.com/postgresql)
* [Donate](https://www.postgresql.org/about/donate/)

## More About the Features

For explanations of the above features and others, please see the following
resources:

* [Release Notes](https://www.postgresql.org/docs/16/release-16.html)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/)

## Where to Download

There are several ways you can download PostgreSQL 16, including:

* The [Official Downloads](https://www.postgresql.org/download/) page, with contains installers and tools for [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/), and more.
* [Source Code](https://www.postgresql.org/ftp/source/v16.0)

Other tools and extensions are available on the
[PostgreSQL Extension Network](http://pgxn.org/).

## Documentation

PostgreSQL 16 comes with HTML documentation as well as man pages, and you can also browse the documentation online in both [HTML](https://www.postgresql.org/docs/16/) and [PDF](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf) formats.

## Licence

PostgreSQL uses the [PostgreSQL License](https://www.postgresql.org/about/licence/),
a BSD-like "permissive" license. This
[OSI-certified license](http://www.opensource.org/licenses/postgresql/) is
widely appreciated as flexible and business-friendly, since it does not restrict
the use of PostgreSQL with commercial and proprietary applications. Together
with multi-company support and public ownership of the code, our license makes
PostgreSQL very popular with vendors wanting to embed a database in their own
products without fear of fees, vendor lock-in, or changes in licensing terms.

## Contacts

Website

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

## Images and Logos

Postgres, PostgreSQL, and the Elephant Logo (Slonik) are all registered
trademarks of the [PostgreSQL Community Association](https://www.postgres.ca).
If you wish to use these marks, you must comply with the [trademark policy](https://www.postgresql.org/about/policies/trademarks/).

## Corporate Support

PostgreSQL enjoys the support of numerous companies, who sponsor developers,
provide hosting resources, and give us financial support. See our
[sponsors](https://www.postgresql.org/about/sponsors/) page for some of these
project supporters.

There is also a large community of
[companies offering PostgreSQL Support](https://www.postgresql.org/support/professional_support/),
from individual consultants to multinational companies.

If you wish to make a financial contribution to the PostgreSQL Global
Development Group or one of the recognized community non-profit organizations,
please visit our [donations](https://www.postgresql.org/about/donate/) page.
