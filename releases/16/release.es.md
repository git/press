14 de septiembre de 2023 - El Grupo Global de Desarrollo de PostgreSQL ha anunciado hoy el lanzamiento de PostgreSQL 16, la versión más reciente de la base de datos de código abierto más avanzada del mundo.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) eleva su rendimiento, con notables mejoras en el paralelismo de consultas, la carga masiva de datos y la replicación lógica. Esta versión contiene muchas características para desarrolladores y administradores, como más sintaxis SQL/JSON, nuevas estadísticas de monitoreo para cargas de trabajo y mayor flexibilidad en la definición de reglas de control de acceso para la gestión de políticas en flotas de gran tamaño.

"A medida que evolucionan los patrones de las bases de datos relacionales, PostgreSQL continúa mejorando el rendimiento en la búsqueda y gestión de datos a gran escala", dijo Dave Page, miembro del Core Team de PostgreSQL. "PostgreSQL 16 ofrece a los usuarios nuevos métodos para escalar vertical y horizontalmente sus cargas de trabajo, ofreciéndoles al mismo tiempo nuevas formas de adquirir información del funcionamiento interno y optimizar la gestión de sus datos."

PostgreSQL es un innovador sistema de gestión de datos conocido por su confiabilidad y robustez. Cuenta con más de 35 años de desarrollo de código abierto por parte de una comunidad global de desarrolladores y se ha convertido en la base de datos relacional de código abierto preferida por organizaciones de todos los tamaños.

### Mejoras de rendimiento

PostgreSQL 16 mejora el rendimiento de las actuales funcionalidades de PostgreSQL a través de nuevas optimizaciones del planificador de consultas. En esta nueva versión, el
[planificador de consultas puede paralelizar](https://www.postgresql.org/docs/16/parallel-query.html) los
[joins](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN) `FULL` y `RIGHT`,
generar planes mejor optimizados para consultas que usan
[funciones de agregado](https://www.postgresql.org/docs/16/functions-aggregate.html)
con cláusulas `DISTINCT` u `ORDER BY`, utilizar ordenamientos incrementales para consultas
[`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT)
, y optimizar
[funciones de ventana deslizante](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS)
para que se ejecuten de forma más eficiente. También mejora los "anti-joins" `RIGHT` y `OUTER`, lo cual permite a los usuarios identificar registros no presentes en una tabla unida.

Esta versión incluye mejoras para la carga masiva de datos utilizando
[`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) tanto en operaciones individuales como simultáneas, con pruebas que muestran, en algunos casos, una mejora del rendimiento de hasta el 300%. PostgreSQL 16 añade soporte para el
[balanceo de carga](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS)
en clientes que utilizan `libpq`, y mejoras en la estrategia de vacuum que reducen la necesidad de congelar tablas completas. Además, PostgreSQL 16 introduce la aceleración de CPU mediante `SIMD` en arquitecturas x86 y ARM, lo cual resulta en mejoras de rendimiento al procesar cadenas ASCII y JSON, y al realizar búsquedas en matrices y sub transacciones.

### Replicación lógica 

La [replicación lógica](https://www.postgresql.org/docs/16/logical-replication.html)
permite a los usuarios transmitir datos a otras instancias PostgreSQL o a suscriptores que puedan interpretar el protocolo de replicación lógica de PostgreSQL. En PostgreSQL 16, los usuarios pueden realizar la replicación lógica a partir de una instancia standby, lo cual significa que un standby puede publicar cambios lógicos a otros servidores. Esto proporciona a los desarrolladores nuevas opciones de distribución de cargas de trabajo - por ejemplo, utilizar una instancia standby en lugar de la instancia primaria más concurrida para replicar de forma lógica los cambios a los sistemas downstream.

Además, se han realizado varias mejoras de rendimiento en PostgreSQL 16 con respecto a la replicación lógica. Los suscriptores ahora pueden aplicar transacciones de gran tamaño utilizando workers en paralelo. En tablas que no disponen de una [clave primaria](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS), para encontrar registros los suscriptores pueden usar índices B-tree en lugar de lecturas secuenciales. Bajo ciertas condiciones, los usuarios también pueden acelerar la sincronización inicial de tablas utilizando el formato binario.

En PostgreSQL 16 se han introducido varias mejoras en el control de acceso a la replicación lógica, que incluyen el nuevo
[rol predefinido](https://www.postgresql.org/docs/16/predefined-roles.html)
`pg_create_subscription`, el cual otorga a los usuarios la posibilidad de crear nuevas suscripciones lógicas. Por último, con esta versión se empieza a añadir soporte para la replicación lógica bidireccional, introduciendo funcionalidad para replicar datos entre dos tablas de diferentes editores.

### Experiencia para los desarrolladores

PostgreSQL 16 añade más sintaxis del estándar
[SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html),
lo que incluye constructores y predicados como `JSON_ARRAY()`, `JSON_ARRAYAGG()`,
e `IS JSON`. En esta versión también se introduce la posibilidad de utilizar guiones bajos como separadores de miles (por ejemplo, `5_432_000`) y literales enteros no decimales, como `0x1538`, `0o12470`, y `0b1010100111000`.

Los desarrolladores que utilizan PostgreSQL 16 también se benefician de los nuevos comandos en `psql`. Estos incluyen
[`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND),
que permite a los usuarios preparar consultas parametrizadas y utilizar `\bind` para sustituir las variables (por ejemplo `SELECT $1::int + $2::int \bind 1 2 \g `). 

PostgreSQL 16 mejora el soporte general para
[intercalaciones de texto](https://www.postgresql.org/docs/16/collation.html), las cuales proporcionan reglas sobre cómo ordenar el texto. PostgreSQL 16 se compila con soporte ICU por defecto, determina la configuración regional ICU por defecto a partir del entorno y permite a los usuarios definir reglas de intercalación ICU personalizadas.

### Monitoreo

Un elemento clave a la hora de optimizar el rendimiento de las cargas de trabajo de las bases de datos es comprender el impacto que tienen las operaciones de E/S en el sistema. PostgreSQL 16 introduce
[`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW),
una nueva fuente de métricas clave de E/S para el análisis granular de los patrones de acceso de E/S.

Además, esta versión añade un nuevo campo a la vista 
[`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW)
el cual registra un timestamp para indicar cuándo se escaneó por última vez una tabla o índice. PostgreSQL 16 también mejora la legibilidad de
[`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) mediante el registro de valores transferidos a sentencias parametrizadas, así como la precisión del algoritmo de seguimiento de consultas utilizado por
[`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html)
y [`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW).

### Control de acceso y seguridad

PostgreSQL 16 proporciona opciones más detalladas para el control de acceso y mejora otras características de seguridad. Esta versión mejora la gestión de los archivos
[`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) y 
[`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html)
permitiendo la coincidencia de expresiones regulares para nombres de usuarios y bases de datos, y directivas `include` para la inclusión de archivos de configuración externos.

Se han añadido varios parámetros de conexión cliente orientados a la seguridad, incluido `require_auth`, que permite a los clientes especificar qué parámetros de autenticación están dispuestos a aceptar de un servidor, y
[`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT),
que indica que PostgreSQL debe utilizar el almacén de autoridades de certificación (CA) de confianza proporcionado por el sistema operativo del cliente. Además, la versión añade soporte para la delegación de credenciales Kerberos, lo cual permite a extensiones como
[`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) y
[`dblink`](https://www.postgresql.org/docs/16/dblink.html) utilizar credenciales autenticadas para conectarse a servicios de confianza.

### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) es la base de datos de código abierto más avanzada del mundo, que cuenta con una comunidad global de miles de usuarios, contribuidores, empresas y organizaciones. Basada en más de 35 años de ingeniería, que comenzaron en la Universidad de Berkeley en California, PostgreSQL ha continuado con un ritmo de desarrollo inigualable. El maduro conjunto de características de PostgreSQL no sólo iguala a los principales sistemas de bases de datos propietarios, sino que los supera en términos de características avanzadas, extensibilidad, seguridad y estabilidad.

### Enlaces

* [Descargas](https://www.postgresql.org/download/)
* [Notas de la versión](https://www.postgresql.org/docs/16/release-16.html)
* [Kit de prensa](https://www.postgresql.org/about/press/)
* [Información de seguridad](https://www.postgresql.org/support/security/)
* [Política de versiones](https://www.postgresql.org/support/versioning/)
* [Sigan @postgresql en Twitter](https://twitter.com/postgresql)
* [Donaciones](https://www.postgresql.org/about/donate/)

## Más información sobre las características

Para más información sobre las características antes mencionadas y otras más, consulten los siguientes recursos:

* [Notas de la versión](https://www.postgresql.org/docs/16/release-16.html)
* [Tabla de características](https://www.postgresql.org/about/featurematrix/)

## Dónde descargarlo

Hay varias maneras de descargar PostgreSQL 16, que incluyen:

* La [página oficial de descargas](https://www.postgresql.org/download/) que contiene instaladores y herramientas para [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/), etc.
* [Código fuente](https://www.postgresql.org/ftp/source/v16.0)

Otras herramientas y extensiones están disponibles en el
[PostgreSQL Extension Network](http://pgxn.org/).

## Documentación

PostgreSQL 16 incluye documentos HTML y páginas de manual. Es posible también consultar la documentación en línea en formato [HTML](https://www.postgresql.org/docs/16/) y [PDF](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf).

## Licencia

PostgreSQL utiliza la [PostgreSQL License](https://www.postgresql.org/about/licence/),
una licencia "permisiva" de tipo BSD. Esta
[licencia certificada por la OSI](http://www.opensource.org/licenses/postgresql/) es ampliamente apreciada por ser flexible y adecuada para las empresas, ya que no limita el uso de PostgreSQL con aplicaciones comerciales y propietarias. Junto con el soporte para múltiples empresas y la propiedad pública del código, nuestra licencia hace que PostgreSQL sea muy popular entre los proveedores que desean integrar una base de datos en sus propios productos sin tener que preocuparse por tarifas, dependencia de un único proveedor o cambios en los términos de la licencia.

## Contactos

Sitio web

* [https://www.postgresql.org/](https://www.postgresql.org/)

Correo electrónico

* [press@postgresql.org](mailto:press@postgresql.org)

## Imágenes y logotipos

Postgres, PostgreSQL y el logo del elefante (Slonik) son todas marcas registradas de la [PostgreSQL Community Association](https://www.postgres.ca).
Quien desee utilizar estas marcas, deberá cumplir con la [política de marca](https://www.postgresql.org/about/policies/trademarks/).

## Soporte corporativo

PostgreSQL cuenta con el soporte de numerosas empresas, que patrocinan a los desarrolladores, ofrecen recursos de hosting y nos dan apoyo financiero. Consulten nuestra página de
[patrocinadores](https://www.postgresql.org/about/sponsors/) para conocer algunos de los que dan soporte al proyecto.

Existe también una gran comunidad de
[empresas que ofrecen soporte para PostgreSQL](https://www.postgresql.org/support/professional_support/),
desde consultores individuales hasta empresas multinacionales.

Si desean hacer una contribución financiera al Grupo Global de Desarrollo de PostgreSQL o a una de las organizaciones sin fines de lucro reconocidas por la comunidad, visiten nuestra página de [donaciones](https://www.postgresql.org/about/donate/).
