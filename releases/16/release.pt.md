14 de Setembro de 2023 - O Grupo de Desenvolvimento Global do PostgreSQL
anunciou hoje o lançamento do PostgreSQL 16, a versão mais recente do banco de
dados de código aberto mais avançado do mundo.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) aumentou a
sua performance, com melhorias notáveis no paralelismo de consultas, carga de
dados e replicação lógica. Há muitas funcionalidades nesta versão para
desenvolvedores e administradores, incluindo mais sintaxe SQL/JSON, novas
estatísticas para monitoramento de suas cargas de dados e maior flexibilidade
na definição de regras de controle de acesso para gerenciamento de políticas em
grandes conjuntos de servidores.

"À medida que os padrões de bancos de dados evoluem, PostgreSQL continua a
obter ganhos de performance na busca e gerenciamento de dados em escala", disse
Dave Page, um membro do Grupo de Desenvolvimento Global do PostgreSQL.
"PostgreSQL 16 oferece aos usuários mais métodos para aumentar e expandir suas
cargas de trabalho, ao mesmo tempo que oferece novas maneiras de obter
conhecimento e otimizar a forma como gerenciam os seus dados".

PostgreSQL, um sistema de gerenciamento de dados inovador conhecido pela sua
confiabilidade e robustez, se beneficia de mais de 35 anos de desenvolvimento
de código aberto de uma comunidade global de desenvolvedores e se tornou o
banco de dados relacional de código aberto preferido pelas organizações de
todos os tamanhos.

### Melhorias de Performance

PostgreSQL 16 melhora a performance da funcionalidade existente do PostgreSQL
por meio de novas otimizações do planejador de consultas. Nesta última
versão, o [planejador de consultas pode
paralelizar](https://www.postgresql.org/docs/16/parallel-query.html)
[junções](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN)
`FULL` e `RIGHT`, gerar planos mais otimizados para consultas que usam
[funções de
agregação](https://www.postgresql.org/docs/16/functions-aggregate.html) com a
cláusula `DISTINCT` ou `ORDER BY`, utilizar ordenação incremental para consultas
[`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT)
e otimizar [funções
deslizantes](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS)
para que elas executem com mais eficiência. Ele também melhora "anti-joins"
`RIGHT`e `OUTER`, que permite aos usuários identificar registros que não estão
presentes em uma tabela unida.

Esta versão inclui melhorias na carga de dados utilizando
[`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) em operações únicas
ou simultâneas, com testes mostrando até 300% de melhoria de performance em
alguns casos. PostgreSQL 16 adiciona suporte a [balanceamento de
carga](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS)
em clientes que usam `libpq` e melhorias na estratégia de limpeza que reduz a
necessidade de execução do FREEZE em toda tabela. Além disso, o PostgreSQL 16
introduz a aceleração de CPU utilizando `SIMD` nas arquiteturas x86 e ARM,
resultando em ganhos de performance ao processar cadeias de caracteres ASCII e
JSON e ao realizar pesquisas em matrizes e subtransações.

### Replicação Lógica

[Replicação lógica](https://www.postgresql.org/docs/16/logical-replication.html)
permite que os usuários transmitam dados para outras instâncias PostgreSQL ou
assinantes que podem interpretar o protocolo de replicação lógica do
PostgreSQL. No PostgreSQL 16, usuários podem realizar replicação lógica de uma
instância em espera, o que significa que uma instância em espera pode publicar
alterações lógicas para outros servidores. Isso fornece aos desenvolvedores
novas opções de distribuição de carga de trabalho - por exemplo, utilizar um
servidor em espera ao invés de um servidor primário mais ocupado para replicar
logicamente as mudanças para sistemas posteriores.

Além disso, há diversas melhorias de performance no PostgreSQL 16 para
replicação lógica. Assinantes agora podem aplicar transações grandes utilizando
processos paralelos. Para tabelas que não tem uma [chave
primária](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS),
assinantes podem utilizar índices B-tree ao invés de buscas sequenciais para
encontrar registros. Sob certas condições, usuários também podem acelerar a
sincronização inicial de tabelas utilizando o formato binário.

Existem várias melhorias de controle de acesso para replicação lógica no
PostgreSQL 16, incluindo a nova [role
predefinida](https://www.postgresql.org/docs/16/predefined-roles.html)
`pg_create_subscription`, que concede aos usuários a capacidade de criar novas
assinaturas. Finalmente, esta versão começa a adicionar suporte a replicação
lógica bidirecional, introduzindo funcionalidade para replicar dados entre duas
tabelas de publicadores diferentes.

### Experiência do Desenvolvedor

PostgreSQL 16 adiciona mais sintaxe do padrão
[SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html), incluindo
construtores e predicados tais como `JSON_ARRAY()`, `JSON_ARRAYAGG()` e `IS
JSON`. Esta versão também introduz a capacidade de utilizar sublinhados para
separadores de milhar (por exemplo, `5_432_000`) e inteiros não decimais, tais
como `0x1538`, `0o12470`, and `0b1010100111000`.

Desenvolvedores utilizando PostgreSQL 16 também se beneficiam de novos comandos
do `psql`. Isso inclui
[`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND),
que permite a usuários preparar consultas parametrizadas e utilizar `\bind`
para substituir as variáveis (por exemplo, `SELECT $1::int + $2::int \bind 1 2
\g `).

PostgreSQL 16 melhora o suporte geral para
[ordenação](https://www.postgresql.org/docs/16/collation.html), que fornece
regras sobre como o texto é ordenado. PostgreSQL 16 constrói com suporte ICU
por padrão, determina a configuração regional ICU padrão a partir do ambiente e
permite que os usuários definam regras de ordenação ICU personalizadas.

### Monitoramento

Um aspecto chave do ajuste de performance de cargas de trabalho de banco de
dados é entender o impacto de operações de I/O no seu sistema. PostgreSQL
16 introduz
[`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW),
uma nova fonte de métricas-chave de I/O para análise granular de padrões de
acesso de I/O.

Além disso, esta versão adiciona um novo campo a visão
[`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW)
que registra a data e hora da última vez que a tabela ou o índice foi
utilizado. PostgreSQL 16 também torna
[`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) mais
legível ao registrar valores passados em comandos parametrizados e melhora a
precisão do algoritmo de rastreamento de consultas utilizado pelo
[`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html)
e
[`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW).

### Controle de Acesso e Segurança

PostgreSQL 16 fornece opções mais refinadas para controle de acesso e aprimora
outros recursos de segurança. Esta versão melhora gerenciamento dos arquivos
[`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) e
[`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html),
incluindo permitir correspondência de expressões regulares para nomes de
usuário e banco de dados e diretivas `include` para arquivos de configuração
externos.

Esta versão adiciona vários parâmetros de conexão orientados à segurança,
incluindo `require_auth`, que permite aos clientes especificar quais parâmetros
de autenticação eles desejam aceitar de um servidor, e
[`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT),
que indica que o PostgreSQL deve utilizar uma autoridade certificadora (CA)
confiável fornecida pelo sistema operacional do cliente. Além disso, esta
versão adiciona suporte a delegação de credenciais Kerberos, permitindo
extensões tais como
[`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) e
[`dblink`](https://www.postgresql.org/docs/16/dblink.html) utilizarem
credenciais autenticadas para conectar-se a serviços confiáveis.

### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) é o banco de dados mais avançado do
mundo, com uma comunidade global de milhares de usuários, colaboradores,
empresas e organizações. O Projeto PostgreSQL baseia-se em mais de 35 anos de
engenharia, iniciando na Universidade da Califórnia, Berkeley, e continua em um
ritmo inigualável de desenvolvimento. Conjunto de funcionalidades maduras do
PostgreSQL não só se igualam aos principais sistemas de bancos de dados
proprietários, mas os supera em funcionalidades avançadas, extensibilidade,
segurança e estabilidade.

### Links

* [Download](https://www.postgresql.org/download/)
* [Notas de Lançamento](https://www.postgresql.org/docs/16/release-16.html)
* [Kit à Imprensa](https://www.postgresql.org/about/press/)
* [Informação sobre Segurança](https://www.postgresql.org/support/security/)
* [Política de Versionamento](https://www.postgresql.org/support/versioning/)
* [Siga @postgresql](https://twitter.com/postgresql)
* [Doação](https://www.postgresql.org/about/donate/)

## Mais Sobre as Funcionalidades

Para explicação sobre as funcionalidades acima e outras, consulte os seguintes links:

* [Notas de Lançamento](https://www.postgresql.org/docs/16/release-16.html)
* [Matriz de Funcionalidades](https://www.postgresql.org/about/featurematrix/)

## Onde Baixar

Há várias maneiras de fazer uma cópia do PostgreSQL 16, incluindo:

* A página [oficial de Downloads](https://www.postgresql.org/download/) que contém instaladores e ferramentas para [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/) e mais.
* [Código Fonte](https://www.postgresql.org/ftp/source/v16.0)

Outras ferramentas e extensões estão disponíveis na [PostgreSQL Extension
Network](http://pgxn.org/).

## Documentação

O PostgreSQL 16 vem com documentação em HTML bem como páginas man, e você
também pode navegar na documentação online nos formatos
[HTML](https://www.postgresql.org/docs/16/) e
[PDF](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf).

## Licença

O PostgreSQL usa a [PostgreSQL
License](https://www.postgresql.org/about/licence/), uma licença "permissiva"
do tipo BSD. Esta [licença certificada pela
OSI](http://www.opensource.org/licenses/postgresql/) é amplamente apreciada
como flexível e amigável aos negócios, uma vez que não restringe o uso do
PostgreSQL com aplicações comerciais e proprietárias. Juntamente com o suporte
de múltiplas empresas e a propriedade pública do código fonte, nossa licença
torna o PostgreSQL muito popular entre os fornecedores que desejam incorporar
um banco de dados em seus produtos sem o medo de taxas, dependência de
fornecedor ou alterações nos termos de licenciamento.

## Contatos

Página Web

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

## Imagens e Logotipos

Postgres, PostgreSQL e o Logotipo do Elefante (Slonik) são todas marcas
registradas da [PostgreSQL Community Association](https://www.postgres.ca). Se
você deseja utilizar estas marcas, você deve estar em conformidade com a
[política de marcas
registradas](https://www.postgresql.org/about/policies/trademarks/).


## Suporte Corporativo

O PostgreSQL conta com o apoio de inúmeras empresas, que financiam
desenvolvedores, fornecem recursos de hospedagem e nos dão suporte financeiro.
Veja nossa página de
[patrocinadores](https://www.postgresql.org/about/sponsors/) para alguns desses
apoiadores do projeto.

Há também uma grande comunidade de [empresas que oferecem suporte ao
PostgreSQL](https://www.postgresql.org/support/professional_support/), de
consultores individuais a empresas multinacionais.

Se você deseja fazer uma contribuição financeira para o Grupo de
Desenvolvimento Global do PostgreSQL ou uma das organizações comunitárias sem
fins lucrativos reconhecidas, visite nossa página de
[doações](https://www.postgresql.org/about/donate/).
