﻿14 вересня 2023 - PostgreSQL Global Development Group сьогодні повідомила про випуск PostgreSQL 16, останньої версії найсучаснішої бази даних з відкритим вихідним кодом у світі.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) підвищує свою продуктивність з помітними покращеннями в паралельності запитів, у масовому завантаженні даних та в логічній реплікації. У цьому випуску є багато можливостей як для розробників, так і для адміністраторів, зокрема розширений синтаксис SQL/JSON, нова статистика моніторингу робочих навантажень, а також більша гнучкість у визначенні правил контролю доступу для управління політиками великих флотилій.

"В умовах розвитку реляційних баз даних PostgreSQL продовжує підвищувати продуктивність пошуку та управління даними у великих масштабах", - говорить Дейв Пейдж (Dave Page), член PostgreSQL Core Team. "PostgreSQL 16 надає користувачам більше методів для масштабування й розгортання своїх робочих навантажень, одночасно надаючи їм нові способи отримання інсайтів та оптимізації управління даними".

PostgreSQL — інноваційна система керування даними, відома своєю надійністю та міцністю, завдяки відкритому коду протягом 35 років розвивається розробниками глобальної спільноти й стала реляційною базою даних з відкритим кодом, яку обирають організації всіх розмірів.

### Покращення продуктивності

PostgreSQL 16 покращує продуктивність наяної функціональності PostgreSQL за рахунок нових оптимізацій планувальника запитів. У цьому останньому випуску [планувальник запитів може розпаралелювати](https://www.postgresql.org/docs/16/parallel-query.html) `FULL` і `RIGHT` [об'єднання](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN), генерувати краще оптимізовані плани для запитів, які використовують [агрегатні функції](https://www.postgresql.org/docs/16/functions-aggregate.html) з операторами `DISTINCT` або `ORDER BY`, використовувати інкрементне сортування для запитів [`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT), а також оптимізувати [віконні функції](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS), щоб вони виконувалися ефективніше. Також покращені `RIGHT` та `OUTER` "анти-об'єднання", що дозволяє користувачам визначати рядки, яких немає в об'єднаній таблиці.

Цей випуск включає покращення для масового завантаження за допомогою [`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) як в одиночних, так і в паралельних операціях, причому тести показують покращення продуктивності до 300% у деяких випадках. У PostgreSQL 16 додано підтримку [балансування навантаження](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS) у клієнтах, що використовують `libpq`, а також покращено стратегію вакуумування, яка зменшує необхідність повного заморожування таблиці. Крім того, у PostgreSQL 16 реалізовано прискорення роботи процесора за допомогою `SIMD` на архітектурах x86 і ARM, що призводить до збільшення продуктивності при обробці ASCII і JSON рядків, а також при виконанні пошуку в масивах і субтранзакціях.

### Логічна реплікація 

[Логічна реплікація](https://www.postgresql.org/docs/16/logical-replication.html) дозволяє користувачам передавати дані до інших екземплярів PostgreSQL або абонентів, які можуть інтерпретувати протокол логічної реплікації PostgreSQL. У PostgreSQL 16 користувачі можуть виконувати логічну реплікацію з резервного сервера, тобто резервний сервер може публікувати логічні зміни на інших серверах. Це надає розробникам нові можливості розподілу робочого навантаження. Наприклад, використання резервного, а не більш завантаженого основного сервера для логічної реплікації змін до подальших систем.

Крім того, в PostgreSQL 16 є кілька поліпшень продуктивності логічної реплікації. Абоненти тепер можуть застосовувати великі транзакції з використанням паралельних виконавців. Для таблиць, які не мають [первинного ключа](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS), абоненти можуть використовувати індекси B-tree замість послідовного сканування для пошуку рядків. За певних умов користувачі також можуть прискорити початкову синхронізацію таблиць, використовуючи двійковий формат.

У PostgreSQL 16 є кілька поліпшень контролю доступу до логічної реплікації, включаючи нову [попередньо визначену роль](https://www.postgresql.org/docs/16/predefined-roles.html) `pg_create_subscription`, яка надає користувачам можливість створювати нові логічні підписки. Окрім того, у цьому випуску додано підтримку двонаправленої логічної реплікації, що дозволяє реплікувати дані між двома таблицями від різних видавців.

### Досвід розробників

PostgreSQL 16 додає більше синтаксису зі стандарту [SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html), включаючи конструктори й предикати, такі як `JSON_ARRAY()`, `JSON_ARRAYAGG()` і `IS JSON`. У цьому випуску також додано можливість використовувати підкреслення для відокремлення тисяч (наприклад, `5_432_000`), а також підтримку недесяткових цілих чисел, таких як `0x1538`, `0o12470` і `0b1010100111000`.

Розробники, які використовують PostgreSQL 16, також отримують переваги від нових команд у `psql`. Сюди входить [`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND), яка дозволяє користувачам створювати параметризовані запити й використовувати `\bind` для підстановки змінних (наприклад, `SELECT $1::int + $2::int \bind 1 2 \g`). 

У PostgreSQL 16 покращено загальну підтримку [текстових зіставлень](https://www.postgresql.org/docs/16/collation.html), які надають правила для сортування тексту. PostgreSQL 16 збирається з підтримкою ICU за замовчуванням, визначає локаль ICU за замовчуванням з оточення й дозволяє користувачам визначати власні правила зіставлення ICU.

### Моніторинг

Ключовим аспектом налаштування продуктивності робочих навантажень баз даних є розуміння впливу операцій вводу/виводу на вашу систему. PostgreSQL 16 представляє [`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW) — нове джерело ключових метрик вводу/виводу для детального аналізу шаблонів доступу до вводу/виводу.

Крім того, у цьому випуску додано нове поле до подання [`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW), яке записує мітку часу, що показує, коли таблицю або індекс було востаннє проскановано. PostgreSQL 16 також робить [`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) більш читабельним, записуючи значення, передані в параметризовані запити, і покращує точність алгоритму відстеження запитів, що використовується в [`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html) і [`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW).

### Контроль доступу та безпека

PostgreSQL 16 надає більш досконалі можливості контролю доступу та покращує інші функції безпеки. У випуску покращено керування файлами [`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) та [`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html). Зокрема, дозволено співставлення регулярних виразів для імен користувачів та баз даних, а також використання директиви `include` для зовнішніх конфігураційних файлів.

У цьому випуску додано кілька параметрів клієнтського з'єднання, орієнтованих на безпеку, зокрема `require_auth`, який дозволяє клієнтам вказувати, які параметри автентифікації вони бажають приймати від сервера, та [`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT), який вказує, що PostgreSQL має використовувати сховище довірених центрів сертифікації ("CA"), що надається операційною системою клієнта. Крім того, у випуску додано підтримку делегування облікових даних Kerberos, що дозволяє таким розширенням як [`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) і [`dblink`](https://www.postgresql.org/docs/16/dblink.html) використовувати автентифіковані облікові дані для підключення до довірених сервісів.

### Про PostgreSQL

[PostgreSQL](https://www.postgresql.org) — це найдосконаліша в світі база даних з відкритим вихідним кодом та глобальною спільнотою, що налічує тисячі користувачів, контриб'юторів, компаній та організацій. Побудована на основі більш ніж 35-річної інженерної роботи, що започаткована в Каліфорнійському університеті в Берклі. PostgreSQL продовжує розвиватися неперевершеними темпами. Зрілий набір функцій PostgreSQL не тільки відповідає найкращим пропрієтарним системам управління базами даних, але й перевершує їх у функціоналі, розширюваності, безпеці та стабільності.

### Посилання

* [Скачати](https://www.postgresql.org/download/)
* [Примітки до випуску](https://www.postgresql.org/docs/16/release-16.html)
* [Прес-реліз](https://www.postgresql.org/about/press/)
* [Сторінка безпеки](https://www.postgresql.org/support/security/)
* [Політика версіонування](https://www.postgresql.org/support/versioning/)
* [Слідкуйте за @postgresql на Twitter](https://twitter.com/postgresql)
* [Пожертвувати](https://www.postgresql.org/about/donate/)

## Більше про функціонал

Із роз'ясненнями щодо вищезазначених та інших функцій можна ознайомитися на таких ресурсах:

* [Примітки до випуску](https://www.postgresql.org/docs/16/release-16.html)
* [Матриця функцій](https://www.postgresql.org/about/featurematrix/)

## Де скачати

Завантажити PostgreSQL 16 можна кількома способами:

* із [офіційної сторінки завантаження](https://www.postgresql.org/download/), що містить інсталятори та інструменти для [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/) та багато іншого;
* у вигляді [вихідного коду](https://www.postgresql.org/ftp/source/v16.0)

Інші інструменти та розширення доступні через мережу розширень [PostgreSQL Extension Network](http://pgxn.org/).

## Документація

PostgreSQL 16 постачається як з документацією у форматі HTML, так й у вигляді man-сторінок. Також доступна онлайн-документація у форматах [HTML](https://www.postgresql.org/docs/16/) і [PDF](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf).

## Ліцензія

PostgreSQL використовує ліцензію [PostgreSQL License](https://www.postgresql.org/about/licence/), BSD-подібну "дозвільну" ліцензію. Ця [ліцензія сертифікована OSI](http://www.opensource.org/licenses/postgresql/) і вважається широкоприйнятною як гнучка й дружня до бізнесу, тому що не обмежує використання PostgreSQL комерційними й закритими продуктами. Разом з підтримкою від багатьох компаній і публічним володінням коду, наша ліцензія робить PostgreSQL дуже популярною серед компаній, які бажають вбудувати базу даних у свій власний продукт без страху, обмежень, залежностей або змін ліцензійних умов.

## Контакти

Вебсайт

* [https://www.postgresql.org/](https://www.postgresql.org/)

Електронна пошта

* [press@postgresql.org](mailto:press@postgresql.org)

## Зображення та логотипи

Postgres і PostgreSQL, а також логотип зі слоном (Elephant Logo Slonik) є зареєстрованими торговими марками [PostgreSQL Community Association](https://www.postgres.ca). Якщо ви бажаєте використати ці торгові марки, ви маєте дотримуватися вимог [політики використання торгових марок](https://www.postgresql.org/about/policies/trademarks/).

## Корпоративна підтримка

PostgreSQL користується підтримкою багатьох компаній, які спонсорують розробників, надають хостингові ресурси та фінансову підтримку. Перегляньте нашу [спонсорську сторінку](https://www.postgresql.org/about/sponsors/) з переліком деяких прихильників проекту.

Існує також велика спільнота [компаній, що пропонують професійну підтримку PostgreSQL](https://www.postgresql.org/support/professional_support/) від індивідуальних консультантів до багатонаціональних компаній.

Якщо ви бажаєте зробити фінансовий внесок для PostgreSQL Global Development Group або для однієї з визнаних неприбуткових організацій, будь ласка, відвідайте сторінку для [пожертвувань](https://www.postgresql.org/about/donate/).