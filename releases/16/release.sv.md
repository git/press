14 September 2023 - PostgreSQL Global Development Group presenterade idag
PostgreSQL 16, den senaste versionen av världens mest avancerade databas
byggd med öppen källkod.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) höjer
prestandan, med tydliga förbättringar för parallella databasfrågor, inläsning
av data i stora volymer och logisk replikering. Den nya versionen innehåller mycket ny
funktionalitet för både utvecklare och databasadministratörer, bland annat
utökat stöd för SQL/JSON syntax, bättre övervakning av arbetsbelastning samt
utökad flexibilitet i hur åtkomstregler kan hanteras över många databaser i
större installationer.

"Medan användningsmönster för relationsdatabaser utvecklas
och ändras så fortsätter PostgreSQL att förbättra prestandan för att hantera
stora datamängder", säger Dave Page, medlem i PostgreSQL core team. "PostgreSQL
16 ger bättre möjligheter för att både skala up och skala ut installationer,
och ger samtidigt nya och bättre insikter i hur datahantering kan optimeras."

PostgreSQL är känt för pålitlighet, stabilitet och tillförlitlighet. Med mer än
35 års utveckling som öppen källkod av en global grupp av utvecklare har
PostgreSQL blivit den mest populära relationsdatabasen byggd på öppen källkod
för organisationer av alla storlekar.

### Prestandaförbättringar

PostgreSQL 16 förbättrar prestandan av redan befintlig funktionalitet genom
nya optimeringar vid frågeplanering. `FULL` och `RIGHT` 
[joins](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN),
kan i denna versionen generera en mer optimerad frågeplan för frågor som 
använder
[aggregatfunktioner](https://www.postgresql.org/docs/16/functions-aggregate.html)
med en `DISTINCT` eller `ORDER BY` klausul. Vidare kan inkrementell sortering
också användas för
[`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT)
frågor, och
[fönsterfunktioner](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS)
kan optimeras för att exekvera snabbare.  `RIGHT` och `OUTER` "anti-joins",
vilka möjliggör att identifiera rader som inte finns i en tabell, är också
förbättrade.


Denna version innehåller förbättringar för inläsning av stora volymer med 
[`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) för både
enskilda och parallella operationer, med upp till 300% prestandaförbättring
under vissa omständigheter.  PostgreSQL 16 adderar också stöd för 
[lastbalansering](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS)
för klienter som använder `libpq`, samt förbättringar kring vacuum-strategier
som minskar behovet av frysning av en hel tabell. Utöver det introduceras
även CPU-acceleration med hjälp av `SIMD` för både x86 och ARM arkitekturer vilket
leder till förbättrad prestanda för hantering av ASCII och JSON strängar samt
vid sökning i arrayer och subtransaktioner.

### Logisk replikering

[Logisk replikering](https://www.postgresql.org/docs/16/logical-replication.html)
ger användare möjlighet att strömma data till andra PostgreSQL instanser eller
prenumeranter som kan tolka PostgreSQL-protokollet för logisk replikering. Med
PostgreSQL 16 kan användare utföra logisk replikering från en standby, vilket
betyder att en standby kan publicera logiska förändringar till andra servrar.
Detta ger utvecklare nya möjligheter för distribution av last, till exempel
genom att avlasta den primära noden genom att använda en standby för att 
replikera förändringar till konsumenter av datan.

Utöver det innehåller PostgreSQL 16 många förbättringar för logisk replikering.
Prenumeranter kan nu applicera stora transaktioner med hjälp av parallella
processer. För tabeller som inte har en 
[primärnyckel](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS)
kan prenumeranter nu använda ett B-tree index istället för sekventiell sökning
för att hitta matchande rader. I speciella fall kan användare också öka
hastigheten av initial tabellsynkronisering med hjälp av binärformatet.

Det finns många förbättringar kring rättighetshantering för logisk replikering
i PostgreSQL 16, bland annat den nya 
[fördefinierade rollen](https://www.postgresql.org/docs/16/predefined-roles.html)
`pg_create_subscription`, vilket ger användare möjlighet att skapa en nya
prenumerationer av logiska förändringar. Slutligen så har arbetet med att
stödja dubbelriktad logisk replikering påbörjats i och med denna versionen,
där funktionalitet för att replikera data mellan två tabeller från olika
publiceringar medges.

### Utvecklarfunktioner

PostgreSQL 16 har adderat stöd för mer syntax från
[SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html) standarden,
bland annat konstruktörer och predikat som `JSON_ARRAY()`, `JSON_ARRAYAGG()`
och `IS JSON`. Denna versionen introducerar också möjligheten att använda
understreck som tusenseparator (t.ex `5_432_000`) och heltal i andra baser,
som `0x1538`, `0o12470` och `0b1010100111000`.

Användare av PostgreSQL 16 kan också dra nytta från nya kommandon i `psql`.
Dessa inkluderar bland annat 
[`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND),
vilket medger att förbereda parameteriserade frågor och med `\bind` ersätta
variablerna (t.ex `SELECT $1::int + $2::int \bind 1 2 \g `).

Stödet för
[text jämförelser](https://www.postgresql.org/docs/16/collation.html) är
förbättrat i PostgreSQL 16, med nya regler för hur text sorteras. PostgreSQL
16 byggs ny som standard med stöd för ICU, använder standard locale från
exekveringsmiljön och ger möjlighet för att skapa egna regler för jämförelser
med ICU.

### Övervakning

För att kunna optimera prestandan av databasen är det mycket viktigt att
förstå vilken påverkan I/O-operationer har på systemet. PostgreSQL 16
introducerar
[`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW),
en ny källa till mätpunkter för noggrann analys av I/O-accessmönster.

Utöver har en ny kolumn lagts till i
[`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW)
vilken innehåller en tidstämpel för när en tabell eller ett index senast
lästes. PostgreSQL 16 förbättrar också 
[`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html)
genom att inkludera värden som skickats till parameteriserade frågor, samt
utvecklar spårningen av frågor till
[`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html)
och [`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW).

### Rättighetshantering och säkerhet

PostgreSQL 16 tillåter detaljerad konfiguration av rättighetshantering och
förbättrar ett flertal säkerhetsfunktioner.
Förbättringar i denna versionen är hanteringen av 
[`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) och
[`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html)
med bland annat stöd för reguljära uttryck för matchning av användarnamn och
databasnamn and `include` direktiv för externa konfigurationsfiler.

Nytt i denna versionen är ett flertal säkerhetsorienterade anslutningsparametrar
för klienter, bland dem `require_auth` vilken ger klienter möjlighet att
specificera vilka autentiseringsparametrar de är villiga att acceptera från
en server, och 
[`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT),
vilken indikerar att PostgreSQL ska använda operativsystemets källa för
certifikatsutfärdare (CA). Utöver det stöds identitetsdelegering för
Kerberos, vilket ger tilläggsmoduler såsom 
[`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) och
[`dblink`](https://www.postgresql.org/docs/16/dblink.html) möjlighet att
använda autentiserade identiteter för att anslutningar till tjänster.

### Om PostgreSQL

[PostgreSQL](https://www.postgresql.org) är världens mest avancerade databas
byggd på öppen källkod, med tusentals användare, utvecklare, företag och 
organisationer världen över. Med över 35 års utveckling, med start på
University of California, Berkeley, har PostgreSQL fortsatt utvecklas med
en enastående fart. PostgreSQL:s väl etablerade funktioner är inte bara
jämförbara med proprietära databassystem, utan överträffar dem när det gäller
avancerade databas funktioner, utbyggbarhet, säkerhet och stabilitet.

### Länkar

* [Nerladdning](https://www.postgresql.org/download/)
* [Nyheter i version 16](https://www.postgresql.org/docs/16/release-16.html)
* [Press Kit](https://www.postgresql.org/about/press/)
* [Säkerhet](https://www.postgresql.org/support/security/)
* [Versionspolicy](https://www.postgresql.org/support/versioning/)
* [Följ @postgresql](https://twitter.com/postgresql)
* [Donera](https://www.postgresql.org/about/donate/)

## Mer om funktionerna

För förklaringar och dokumentation av de ovan nämnda funktionerna, och mer,
se följande resurser:

* [Nyheter i version 16](https://www.postgresql.org/docs/16/release-16.html)
* [Funktionsmatris](https://www.postgresql.org/about/featurematrix/)

## Nerladdning

PostgreSQL 16 kan laddas ner på ett flertal olika sätt, bland annat:

* Sidan med [officiella nerladdningar](https://www.postgresql.org/download/), vilken innehåller installationspaket för [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/) med flera.
* [Källkod](https://www.postgresql.org/ftp/source/v16.0)

Andra verktyg och tilläggsmoduler finns tillgängliga på
[PostgreSQL Extension Network](http://pgxn.org/).

## Dokumentation

PostgreSQL 16 levereras med dokumentation i HTML-format samt man-sidor, och
dokumentationen kan också läsas online i både [HTML-format](https://www.postgresql.org/docs/16/) och
som [PDF](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf).


## Licens

PostgreSQL använder [PostgreSQL
Licensen](https://www.postgresql.org/about/licence/), en BSD-liknande
"tillåtande" licens. Denna
[OSI-certifierade licens](http://www.opensource.org/licenses/postgresql/) anses
flexibel och företagsvänlig eftersom den inte begränsar användningen av
PostgreSQL i kommersiella eller proprietära applikationer. Licensen, tillsammans
med brett stöd från många företag och ett publikt ägande av koden, gör att
PostgreSQL är väldigt populär bland tillverkare som vill bygga in en databas
i sin produkt utan att riskera avgifter, inlåsning eller förändrade licensvillkor.

## Kontakter

Hemsida

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-post

* [press@postgresql.org](mailto:press@postgresql.org)

## Bilder och logotyper

Postgres, PostgreSQL, och elefantlogotypen (Slonik) är av
[PostgreSQL Community Association](https://www.postgres.ca) registrerade
varumärken. Användning av dess varumärken måste följa dess
[varumärkespolicy](https://www.postgresql.org/about/policies/trademarks/).

## Kommersiell support

PostgreSQL projektet stöttas av ett stort antal företag som bland annat
sponsrar utvecklare, erbjuder infrastruktur och ger finansiellt stöd. Se listan
över PostgreSQL:s [sponsors](https://www.postgresql.org/about/sponsors/) för
mer information om vem de är.

Det finns också en stor grupp
[företag som säljer PostgreSQL Support](https://www.postgresql.org/support/professional_support/),
allt från små konsultbolag till multinationella företag.

För att ge ett ekonomiskt bidrag till PostgreSQL Global Development Group eller
en av de officiella ideella organisationerna, se sidan för
[donations](https://www.postgresql.org/about/donate/) för mer information.
