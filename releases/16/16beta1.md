The PostgreSQL Global Development Group announces that the first beta release of
PostgreSQL 16 is now [available for download](https://www.postgresql.org/download/).
This release contains previews of all features that will be available when
PostgreSQL 16 is made generally available, though some details of the release
can change during the beta period.

You can find information about all of the features and changes found in
PostgreSQL 16 in the [release notes](https://www.postgresql.org/docs/16/release-16.html):

  [https://www.postgresql.org/docs/16/release-16.html](https://www.postgresql.org/docs/16/release-16.html)

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 16 on your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 16 Beta 1 in production environments, we encourage you to find ways
to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 16
release upholds our standards of delivering a stable, reliable release of the
world's most advanced open source relational database. Please read more about
our [beta testing process](https://www.postgresql.org/developer/beta/) and how
you can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

PostgreSQL 16 Feature Highlights
--------------------------------

### Performance

PostgreSQL 16 includes performance improvements in query execution. This release
adds more query parallelism, including allowing `FULL` and `RIGHT` joins to
execute in parallel, and parallel execution of the `string_agg` and `array_agg`
aggregate functions. Additionally, PostgreSQL 16 can use incremental sorts in
`SELECT DISTINCT` queries. There are also several optimizations for
[window queries](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS),
improvements in lookups for `RANGE` and `LIST` partitions, and support for
"anti-joins" in `RIGHT` and `OUTER` queries.

PostgreSQL 16 can also improve the performance of concurrent bulk loading of
data using [`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) up to
300%.

This release also introduces support for CPU acceleration using SIMD for both
x86 and ARM architectures, including optimizations for processing ASCII and JSON
strings, and array and subtransaction searches. Additionally, PostgreSQL 16
introduces [load balancing](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS)
to libpq, the client library for PostgreSQL.

### Logical Replication Enhancements

Logical replication lets PostgreSQL users stream data in real-time to other
PostgreSQL or other external systems that implement the logical protocol. Until
PostgreSQL 16, users could only create logical replication publishers on primary
instances. PostgreSQL 16 adds the ability to perform logical decoding on a
standby instance, giving users more options to distribute their workload, for
example, use a standby that's less busy than a primary to logically replicate
changes.

PostgreSQL 16 also includes several performance improvements to logical
replication. This includes allowing the subscriber to apply large transactions
in parallel, use indexes other than the `PRIMARY KEY` to perform lookups during
`UPDATE` or `DELETE` operations, and allow for tables to be copied using binary
format during initialization.

### Developer Experience

PostgreSQL 16 continues to implement the [SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html)
standard for manipulating [JSON](https://www.postgresql.org/docs/16/datatype-json.html)
data, including support for SQL/JSON constructors (e.g. `JSON_ARRAY()`,
`JSON_ARRAYAGG()` et al), and identity functions (`IS JSON`). This release also
adds the SQL standard [`ANY_VALUE`](https://www.postgresql.org/docs/16/functions-aggregate.html#id-1.5.8.27.5.2.4.1.1.1.1)
aggregate function, which returns any arbitrary value from the aggregate set.
For convenience, PostgreSQL 16 now lets you specify non-decimal integer
literals, such as `0xff`, `0o777`, and `0b101010`, and use underscores as
thousands separators, such as `5_432`.

This release adds support for the extended query protocol to the [`psql`](https://www.postgresql.org/docs/16/app-psql.html)
client. Users can execute a query, e.g. `SELECT $1 + $2`, and use the
[`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND)
command to substitute the variables.

### Security Features

PostgreSQL 16 continues to give users the ability to grant privileged access to
features without requiring superuser with new
[predefined roles](https://www.postgresql.org/docs/16/predefined-roles.html).
These include `pg_maintain`, which enables execution of operations such as
`VACUUM`, `ANALYZE`, `REINDEX`, and others, and `pg_create_subscription`, which
allows users to create a logical replication subscription. Additionally,
starting with this release, logical replication subscribers execute transactions
on a table as the table owner, not the superuser.

PostgreSQL 16 now lets you use regular expressions in the [`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html)
and [`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html)
files for matching user and databases names. Additionally, PostgreSQL 16 adds
the ability to include other files in both `pg_hba.conf` and `pg_ident.conf`.
PostgreSQL 16 also adds support for the SQL standard [`SYSTEM_USER`](https://www.postgresql.org/docs/16/functions-info.html#id-1.5.8.32.3.4.2.2.24.1.1.1)
keyword, which returns the username and authentication method used to establish
a session. 

PostgreSQL 16 also adds support for Kerberos credential delegation, which allows
extensions such as `postgres_fdw` and `dblink` to use the authenticated
credentials to connect to other services. This release also adds several new
security-oriented connection parameters for clients. This includes [`require_auth`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-REQUIRE-AUTH),
where a client can specify which authentication methods it is willing to accept
from the server. You can now set `sslrootcert` to `system` to instruct
PostgreSQL to use the trusted certificate authority (CA) store provided by the
client's operating system.

### Monitoring and Management

PostgreSQL 16 adds several new monitoring features, including the new
[`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW)
view that provides information on I/O statistics. This release also provides a
timestamp for the last time that a [table or index was scanned](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW).
There are also improvements to the normalization algorithm used for
`pg_stat_activity`.

This release includes improvements to the page freezing strategy, which helps
the performance of vacuuming and other maintenance operations. PostgreSQL 16
also improves general support for text collations, which provide rules for how
text is sorted. PostgreSQL 16 sets ICU to be the default collation provider, and
also adds support for the predefined `unicode` and `ucs_basic` collations.

PostgreSQL 16 adds additional compression options to `pg_dump`, including
support for both `lz4` and `zstd` compression.

### Other Notable Changes

PostgreSQL 16 removes the `promote_trigger_file` option to enable the promotion
of a standby. Users should use the `pg_ctl promote` command or `pg_promote()`
function to promote a standby.

PostgreSQL 16 introduced the Meson build system, which will ultimately replace
Autoconf. This release also adds foundational support for developmental features
that will be improved upon in future releases. This includes a developer flag to
enable DirectIO and the ability to use logical replication to bidirectionally
replicate between two tables when `origin=none` is specified in the subscriber.

For Windows installations, PostgreSQL 16 now supports a minimum version of
Windows 10.

Additional Features
-------------------

Many other new features and improvements have been added to PostgreSQL 16. Many
of these may also be helpful for your use cases. Please see the
[release notes](https://www.postgresql.org/docs/16/release-16.html) for a
complete list of new and changed features:

  [https://www.postgresql.org/docs/16/release-16.html](https://www.postgresql.org/docs/16/release-16.html)

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 16. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_16_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the first beta release of version 16. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2023. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/)
page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 16 Beta Release Notes](https://www.postgresql.org/docs/16/release-16.html)
* [PostgreSQL 16 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_16_Open_Items)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/#configuration-management)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
