14. September 2023 - Die PostgreSQL Global Development Group gab heute die Veröffentlichung von PostgreSQL 16 bekannt, der neuesten Version der weltweit fortschrittlichsten Open-Source-Datenbank.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) verbessert seine Performance mit merklichen Verbesserungen bei der parallelen Verarbeitung, Massenbeladung und logischer Replikation. In der neuen Version finden sich viele neuer Funktionen für Entwickler als auch Administratoren, wie z.B. erweiterte SQL/JSON-Syntax, neue Statistiken zur Überwachung und mehr Flexibilität bei der Vergabe von Zugangsrechten, was die Verwaltung großer Bestände an Servern vereinfacht.

“So wie sich Relationale Datenbanken insgesamt weiterentwickeln, entwickelt sich auch PostgreSQL weiter und macht große Schritte nach vorne, wenn es um das Durchsuchen und Verwalten von großen Datenbeständen geht.”, so Dave Page, PostgreSQL Core Team Mitglied. “PostgreSQL 16 ermöglicht Benutzern mehr Möglichkeiten zur vertikalen und horizontalen Skalierung, während es den Anwendern gleichzeitig neue Möglichkeiten bietet, Daten zu betrachten, und optimierte Methoden zur Datenverwaltung an Bord hat.”

PostgreSQL, ein innovatives Datenverwaltungssystem, welches für seine Zuverlässigkeit und Robustheit bekannt ist, profitiert von über 35 Jahren Open-Source-Entwicklung einer globalen Entwicklergemeinschaft und hat sich zur bevorzugten relationalen Open-Source-Datenbank für Organisationen jeder Größe entwickelt.

### Performance Verbesserungen

PostgreSQL 16 verbessert die Geschwindigkeit existierender Funktionalitäten durch  erweiterte Funktionalitäten des Query-Planers. In dieser Version kann der Query-Planer ‘FULL’ und ‘RIGHT’ [joins](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN) parallelisieren. Optimierte Pläne für Abfragen, die [Aggregat Funktionen](https://www.postgresql.org/docs/16/functions-aggregate.html) mit einer ‘DISTINCT’ oder ‘ORDER BY’ Klausel benutzen, verwenden inkrementelle Sortierung für [`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT) Abfrage und optimieren [window functions](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS), so dass sie effizienter ausgeführt werden. Dadurch werden auch ‘RIGHT OUTER’ “anti-joins” optimiert, mit denen Benutzer Zeilen identifizieren können, die in einer verbundenen Tabelle nicht vorhanden sind.

Diese Version beinhaltet Verbesserungen für das Massen-Laden von Daten  mit [`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) in Einzel- und nebenäufigen Operationen. Tests zeigten in einigen Fällen bis zu 300 % Steigerung der Geschwindigkeit. In PostgreSQL 16 wurde die Unterstützung für [load balancing](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS) für Clients, die die Bibliothek`libpq` benutzten, hinzugefügt, und Verbesserungen der Vacuum Strategie reduzieren die Notwendigkeit für Full Table Freezes. Zusätzlich führt PostgreSQL 16 CPU Beschleunigung mittels `SIMD` sowohl in x86, als auch ARM Chip Architekturen ein. Im Ergebnis werden Geschwindigkeitssteigerungen bei der Verarbeitung von ASCII und JSON Zeilen, als auch bei der Ausführung von Array und Untertertransaktios-Suchen erreicht.

### Logische Replikation

[“Logische” Replikation](https://www.postgresql.org/docs/16/logical-replication.html) erlaubt es, Daten an andere PostgreSQL-Instanzen oder Clients, die das “PostgreSQL logical replication protocol” implementieren, zu senden. Mit PostgreSQL 16 ist dies nun auch von Replika-Servern, sog. “standby”-Instanzen, möglich. Dies eröffnet neue Möglichkeiten der Lastverteilung, da nicht mehr nur die häufig bereits ausgelastete primäre Instanz als Versender logischer Replikationsdaten in Frage kommt.

Zusätzlich erhielt PostgreSQL 16 einige Performance-Verbesserungen der logischen Replikation. So können Empfänger (“Subscriber”) beim Einpflegen großer Transaktionen die Arbeit auf mehrere parallele Prozesse verteilen, um den Vorgang zu beschleunigen. Tabellen ohne [Primärschlüssel](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS) können jetzt existierende B-Tree-Indexe nutzen, um passende Zeilen zu identifizieren, was zuvor eine sequentielle Suche erforderte. Unter bestimmten Voraussetzungen kann die initiale Synchronisation von Tabellen auch mit dem effektiveren binären Format durchgeführt werden.

Die Zugriffskontrolle im Bereich der Logischen Replikation erfuhr in PostgreSQL 16 ebenfalls einige Verbesserungen, u.a. die [vordefinierte Rolle](https://www.postgresql.org/docs/16/predefined-roles.html) `pg_create_subscription`, die es Nutzern erlaubt, eigenständig neue Subskriptionen hinzuzufügen. Zu guter Letzt wurden mit der Funktionalität, Tabellen in zwei oder mehrere Richtungen zu replizieren, die Grundsteine für Bidirektionale Logische Replikation gelegt.

### Entwickler Experience

PostgreSQL 16 fügt mehr [SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html) Kompatibilität mit dem SQL Standard hinzu, einschließlich Konstruktoren und Prädikaten wie `JSON_ARRAY()`, `JSON_ARRAYAGG()`, sowie `IS JSON`. In dieser Version wird auch die Möglichkeit eingeführt, Unterstriche für Tausendertrennzeichen (z. B. „5_432_000“) und nichtdezimale Ganzzahlliterale, wie „0x1538“, „0o12470“ und „0b1010100111000“ zu verwenden.

Entwickler, die PostgreSQL 16 verwenden, profitieren auch von neuen Befehlen in „psql“. Das beinhaltet [`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND), das es Benutzern ermöglicht parametrisierte Abfragen vorzubereiten und `\bind` zu verwenden um Variablen zu befüllen (z. B. `SELECT $1::int + $2::int \bind 1 2 \g `).

PostgreSQL 16 verbessert die allgemeine Unterstützung für [text collations](https://www.postgresql.org/docs/16/collation.html), die angeben, welche Regeln für die Sortierung von Text verwendet werden. PostgreSQL 16 wird standardmäßig mit ICU-Unterstützung kompiliert, die das Standard-ICU-Gebietsschema aus dem Betriebssystem erkennt und es Anwendern ermöglichen kann,  benutzerdefinierte ICU-Sortierungsregeln zu definieren.

### Monitoring

Ein wichtiger Aspekt bei der Optimierung der Leistung von Datenbank-Workloads ist das Verständnis für die Auswirkungen der I/O-Vorgänge im System. PostgreSQL 16 stellt mit [`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW), eine neue Quelle zu wichtigen I/O-Metriken für die detaillierte Analyse von I/O-Zugriffsmustern bereit.

Darüber hinaus fügt diese Version ein neues Feld zur View [`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW) hinzu, welche einen Zeitstempel mitschreibt, der angibt, wann eine Tabelle oder ein Index zuletzt gelesen wurde. In PostgreSQL 16 ist auch [`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) lesbarer, da Werte protokolliert werden, die an parametrisierte Anweisungen übergeben werden. Außerdem wurde die Genauigkeit des verwendeten Abfrageverfolgungsalgorithmus in den Views [`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html) und [`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW) verbessert.

### Access Control & Security

PostgreSQL 16 bietet detailliertere Optionen für die Zugriffskontrolle und Verbesserungen für weitere Sicherheitsfunktionen. Diese Version verbessert die Verwaltung von [`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) und [`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html) Dateien, einschließlich der Möglichkeit, reguläre Ausdrücke für Benutzer und Datenbank Namen abzugleichen und „include“-Direktiven für externe Konfigurationsdateien.

Diese Version fügt mehrere sicherheitsorientierte Client-Verbindungsparameter hinzu, einschließlich `require_auth`, welches es Clients ermöglicht anzugeben, welche Authentifizierungsparameter von einem Server akzeptiert werden, und [`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT), das angibt, dass PostgreSQL eine Trusted Certificate Authority (CA) verwenden sollte, die vom Betriebssystem des Clients bereitgestellt wird. Zusätzlich wird in dieser Version die Unterstützung für Kerberos Berechtigungs Delegation hinzugefügt und ermöglicht Erweiterungen wie [`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) und [`dblink`](https://www.postgresql.org/docs/16/dblink.html) die Verwendung von authentifiziertem Anmeldeinformationen für die Verbindung zu vertrauenswürdigen Diensten.

### Über PostgreSQL

[PostgreSQL](https://www.postgresql.org) ist das führende Open-Source Datenbanksystem, mit einer weltweiten Community bestehend aus Tausenden von Nutzern und Mitwirkenden sowie Dutzenden von Firmen und Organisationen. Das PostgreSQL Projekt baut auf über 35 Jahre Erfahrung auf, beginnend an der University of California, Berkeley, und hat heute eine nicht zu vergleichende Performance bei der Entwicklung. PostgreSQL's ausgereiftes Feature Set ist nicht nur mit den führenden proprietären Datenbanksystemen vergleichbar, sondern übertrifft diese in erweiterten Datenbankfunktionen, Erweiterbarkeit, Sicherheit und Stabilität.

### Links

* [Download](https://www.postgresql.org/download/)
* [Versionshinweise](https://www.postgresql.org/docs/16/release-16.html)
* [Pressemitteilung](https://www.postgresql.org/about/press/)
* [Sicherheit](https://www.postgresql.org/support/security/)
* [Versionierungsrichtlinie](https://www.postgresql.org/support/versioning/)
* [Folge @postgresql auf Twitter](https://twitter.com/postgresql)
* [Spende](https://www.postgresql.org/about/donate/)

## Mehr über die Funktionen

Erläuterungen zu den oben genannten und anderen Funktionen finden Sie in den folgenden Quellen:

* [Versionshinweise](https://www.postgresql.org/docs/16/release-16.html)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/)

## Wo Herunterladen

Es gibt mehrere Möglichkeiten, PostgreSQL 16 herunterzuladen, darunter:

* Die Seite [Offizielle Downloads](https://www.postgresql.org/download/) enthält Installationsprogramme und Tools für [Windows](https://www.postgresql.org/download/windows/), [Linux ](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) und weitere.
* [Quellcode](https://www.postgresql.org/ftp/source/v16.0)

Weitere Tools und Erweiterungen sind über das [PostgreSQL Extension Network](http://pgxn.org/) verfügbar.

## Dokumentation

PostgreSQL 16 wird mit einer HTML-Dokumentation sowie Manpages geliefert. Sie können die Dokumentation auch online unter [HTML](https://www.postgresql.org/docs/16/) aufrufen und als [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-16-US.pdf) Datei herunterladen.

## Lizenz

PostgreSQL verwendet die [PostgreSQL-Lizenz](https://www.postgresql.org/about/licence/), eine BSD-artige "permissive" Lizenz. Diese [OSI-zertifizierte Lizenz](http://www.opensource.org/licenses/postgresql/) wird  allgemein als flexibel und geschäftsfreundlich geschätzt, da die Verwendung von PostgreSQL mit kommerziellen und proprietären Anwendungen nicht eingeschränkt wird. Zusammen mit unternehmensübergreifender Unterstützung und öffentlichem Quellcode macht diese Lizenz PostgreSQL sehr beliebt bei Anbietern die eine Datenbank in ihre eigene Anwendungen einbetten möchten, ohne Einschränkugen bei Gebühren, Herstellerbindung oder Änderungen der Lizenzbedingungen.
## Kontakte

Website

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

Webseite

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-Mail

* [press@postgresql.org](mailto:press@postgresql.org)

## Bilder und Logos

Postgres und PostgreSQL und das Elefanten Logo (Slonik) sind registrierte Marken der [PostgreSQL Community Association](https://www.postgres.ca). Wenn Sie diese Marken verwenden möchten, müssen Sie die [Markenrichtlinie](https://www.postgresql.org/about/policies/trademarks/) einhalten.

## Professioneller Support

PostgreSQL genießt die Unterstützung zahlreicher Unternehmen, die Entwickler sponsern, Hosting-Ressourcen bereitstellen und finanzielle Unterstützung leisten. Unsere [Sponsorenliste](https://www.postgresql.org/about/sponsors/) listet einige Unterstützer des Projekts auf.

Es gibt eine große Anzahl von [Unternehmen, die PostgreSQL-Support anbieten](https://www.postgresql.org/support/professional_support/), von einzelnen Beratern bis hin zu multinationalen Unternehmen.

Wenn Sie einen finanziellen Beitrag zur PostgreSQL Development Group leisten möchten oder eine der anerkannten gemeinnützigen Organisationen der Community unterstützen möchten, besuchen Sie bitte unsere [Spenden Seite](https://www.postgresql.org/about/donate/).
