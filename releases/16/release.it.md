14 settembre 2023 - Il PostgreSQL Global Development Group annuncia oggi il rilascio
della release 16 di PostgreSQL, l'ultima versione del più avanzato database open source al mondo.

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) eleva le sue prestazioni,
con notevoli miglioramenti nell'esecuzione parallela delle query, nel caricamento di dati in blocco e nella replica logica. 
Ci sono molte funzionalità in questa versione sia per gli sviluppatori che per gli amministratori, incluse nuove sintassi SQL/JSON,
nuove statistiche di monitoraggio per i carichi di lavoro e maggiore flessibilità nella definizione delle regole di controllo 
degli accessi per la gestione su grandi bacini di utenze. 

"Mentre i modelli di database relazionali si evolvono, PostgreSQL continua a creare miglioramenti delle prestazioni nella ricerca e nella gestione dei dati su larga scala", 
ha affermato Dave Page, Membro del team principale di PostgreSQL. 
"PostgreSQL 16 offre agli utenti più metodi per crescere e ampliare i carichi di lavoro, offrendo loro nuovi modi per acquisire informazioni e
ottimizzare il modo in cui gestiscono i propri dati."

PostgreSQL, un innovativo sistema di gestione dei dati noto per la sua affidabilità e robustezza, 
beneficia di oltre 35 anni di sviluppo open source da parte di una comunità globale di sviluppatori 
ed è diventato il database relazionale open source preferito per organizzazioni di ogni dimensione.

### Miglioramenti delle performance

PostgreSQL 16 migliora le performance delle funzionalità esistenti tramite nuove ottimizazioni relative al pianificatore delle query 
In questo rilascio il [pianificatore delle query può parallelizzare](https://www.postgresql.org/docs/16/parallel-query.html)
le [joins](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN) di tipo `FULL` and `RIGHT` generando
piani otttimizzati in modo migliore per le quey usano [funzioni aggregate](https://www.postgresql.org/docs/16/functions-aggregate.html)
con clausole `DISTINCT` o `ORDER BY`, utilizza ordinamenti incrmentali per le query di tipo
[`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT), e ottimizza le
[funzioni window](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS) in modo che venga eseguita in maniera più efficiente.
Migliora le "anti-joins" di tipo `RIGHT` e `OUTER` che consentono agli utenti di identificare righe non presenti nelle tabelle collegate.

Questo rilascio include miglioramenti per il caricamento di grosse quantità di dati usando il comando  [`COPY`](https://www.postgresql.org/docs/16/sql-copy.html)
sia per le operazioni singole che per quelle concorrenti, con alcuni test che dimostrano un miglioraremento del 300% in alcuni casi specifici.
PostgreSQL 16 aggiunge il supporto [caricamento bilanciato](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS)
per i clients che usano `libpq` e migliora le strategie di vacuum riducendo la necessità di un congelamento totale della tabella.
Inoltre, PostgreSQL 16 introduce l'accelerazione della CPU utilizzando "SIMD" sia nelle architetture x86 che ARM, con conseguente
miglioramenti delle prestazioni durante l'elaborazione di stringhe ASCII e JSON e l'esecuzione di ricerche di array e sottotransazioni.

### Replica logica  

La [Replica logica](https://www.postgresql.org/docs/16/logical-replication.html)
consente agli utenti di trasmettere dati ad altre istanze PostgreSQL o subscribers che possono interpretare il protocollo di replica logica PostgreSQL. 
In PostgreSQL 16, gli utenti possono eseguire la replica logica da un'istanza in standby, il che significa che uno standby può pubblicare modifiche logiche su altri server. 
Ciò fornisce agli sviluppatori nuove opzioni di distribuzione del carico di lavoro, ad esempio utilizzando uno standby anziché il più impegnato primario per replicare logicamente le modifiche ai sistemi a valle.

Inoltre, ci sono diversi miglioramenti delle prestazioni in PostgreSQL 16 che riguardano la replica logica. 
I subscribers possono ora applicare transazioni di grandi dimensioni utilizzando i workers paralleli. 
Per le tabelle che non hanno una [chiave primaria](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS), i subscribers possono utilizzare indici B-tree invece di scansioni sequenziali per trovare righe. 
In determinate condizioni, gli utenti possono anche accelerare la sincronizzazione iniziale della tabella utilizzando il formato binario.

Sono stati apportati diversi miglioramenti al controllo degli accessi alla replica logica in PostgreSQL 16, incluso il nuovo
[ruolo predefinito](https://www.postgresql.org/docs/16/prefined-roles.html) `pg_create_subscription`, che garantisce agli utenti la possibilità di creare una nuova sottoscrizione logica.
Infine, questa versione inizia ad aggiungere il supporto per la replica logica bidirezionale, introducendo funzionalità per replicare i dati tra due tabelle di diversi publishers.

### Esperienza dello sviluppatore

PostgreSQL 16 aggiunge più sintassi di [SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html) standard, inclusi costruttori e predicati come `JSON_ARRAY()`, `JSON_ARRAYAGG()`,e "IS JSON". 
Questa versione introduce anche la possibilità di utilizzare i caratteri di sottolineatura per
separatori delle migliaia (ad esempio `5_432_000`) e valori letterali interi non decimali, come come "0x1538", "0o12470" e "0b1010100111000".

Gli sviluppatori che utilizzano PostgreSQL 16 beneficiano anche dei nuovi comandi in `psql`. Questo include
[`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND),
che consente agli utenti di preparare query parametrizzate e utilizzare `\bind` per
sostituire le variabili (ad esempio `SELECT $1::int + $2::int \bind 1 2 \g `).

PostgreSQL 16 migliora il supporto generale per le [collazioni di testo](https://www.postgresql.org/docs/16/collation.html), che forniscono regole su come viene ordinato il testo. 
PostgreSQL 16 viene compilato con il supporto ICU di default, determinando la ICU locale predefinita dall'ambiente e consente agli utenti di definire regole di confronto personalizzate per l'ICU.

### Monitoraggio

Un aspetto chiave nell'ottimizzazione delle prestazioni dei carichi di lavoro del database è la comprensione l'impatto delle operazioni di I/O sul sistema. 
PostgreSQL 16 introduce [`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW), una nuova fonte di metriche I/O chiave per l'analisi granulare dei modelli di accesso I/O.

Inoltre, questa versione aggiunge un nuovo campo alla vista [`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW)
che registra un timestamp che rappresenta l'ultima volta in cui una tabella o un indice è stato utilizzato. 
Infine PostgreSQL 16 rende [`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) più leggibile registrando i valori passati in istruzioni parametrizzate e migliora
l'accuratezza dell'algoritmo di tracciamento delle query utilizzato da [`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html) e [`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW).

### Controllo accessi e sicurezza

PostgreSQL 16 fornisce opzioni più dettagliate per il controllo degli accessi e miglioramenti ad altre funzionalità di sicurezza. 
Il rilascio migliora la gestione dei files [`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) e [`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html), 
inclusa la corrispondenza delle espressioni regolari per i nomi degli utente e dei database e direttive "include" per file di configurazione esterni.

Questa versione aggiunge diversi parametri di connessione client orientati alla sicurezza, incluso `require_auth`, che consente ai clients di specificare quali parametri di autenticazione
sono disposti ad accettare da un server e [`sslrootcert="system"`](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT),
che indica che PostgreSQL dovrebbe utilizzare l'autorità di certificazione attendibile (CA) archivio fornito dal sistema operativo del client. 
Inoltre, il rilascio aggiunge il supporto per la delega delle credenziali Kerberos, consentendo ad estensioni come [`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) e
[`dblink`](https://www.postgresql.org/docs/16/dblink.html) di utilizzare le credenziali autenticate per connettersi a servizi attendibili.

### Informazioni su PostgreSQL

[PostgreSQL](https://www.postgresql.org) è il database open source più avanzato al mondo, con una comunità globale di migliaia di utenti, collaboratori,
aziende e organizzazioni. Costruito su oltre 35 anni di ingegneria, a partire da
l'Università della California, Berkeley, PostgreSQL ha continuato con un
ritmo di sviluppo senza pari. Il set di funzionalità mature di PostgreSQL non solo corrisponde
migliori sistemi di database proprietari, ma li supera in funzionalità di database avanzato, estensibilità, sicurezza e stabilità.

### Collegamenti

* [Download](https://www.postgresql.org/download/)
* [Note sulla versione](https://www.postgresql.org/docs/16/release-16.html)
* [Kit per la stampa](https://www.postgresql.org/about/press/)
* [Pagina sulla sicurezza](https://www.postgresql.org/support/security/)
* [Politica di versione](https://www.postgresql.org/support/versioning/)
* [Segui @postgresql su Twitter](https://twitter.com/postgresql)
* [Donazioni](https://www.postgresql.org/about/donate/)

## Maggiori informazioni sulle funzionalità

Per le spiegazioni delle funzioni di cui sopra e altre, consultare le seguenti risorse:

* [Note sulla versione](https://www.postgresql.org/docs/16/release-16.html)
* [Matrice delle funzioni](https://www.postgresql.org/about/featurematrix/)

## Dove scaricare

Esistono diversi modi per scaricare PostgreSQL 16, tra cui:

* La pagina di [Download ufficiale](https://www.postgresql.org/download/), con contiene programmi di installazione e strumenti per [Windows](https://www.postgresql.org/download/windows/), [Linux ](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) e altro ancora.
* Il [Codice sorgente](https://www.postgresql.org/ftp/source/v16.0)

Altri strumenti ed estensioni sono disponibili sulla
[Rete di estensioni per PostgreSQL](http://pgxn.org/).

## Documentazione

PostgreSQL 16 viene fornito con documentazione HTML e pagine man e puoi anche sfogliare la documentazione online in [HTML](https://www.postgresql.org/docs/16/) e [PDF](https:// www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf).

## Licenza

PostgreSQL utilizza la [Licenza PostgreSQL](https://www.postgresql.org/about/licence/), una licenza "permissiva" simile a BSD. Questa
[Licenza certificata OSI](http://www.opensource.org/licenses/postgresql/) è ampiamente apprezzata come flessibile e business-friendly, poiché non limita
l'uso di PostgreSQL con applicazioni commerciali e proprietarie. Insieme con il supporto multi-aziendale e la proprietà pubblica del codice, la nostra licenza fa si che
PostgreSQL sia molto popolare tra i fornitori che desiderano incorporare un database nel proprio prodotti senza timore di commissioni, vincoli del fornitore o modifiche ai termini di licenza.

## Contatti

Sito web

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-mail

* [press@postgresql.org](mailto:press@postgresql.org)

## Immagini e loghi

Postgres e PostgreSQL e Elephant Logo (Slonik) sono tutti marchi registrati di [PostgreSQL Community Association](https://www.postgres.ca).
Se desideri utilizzare questi marchi, devi rispettare la [politica sui marchi](https://www.postgresql.org/about/policies/trademarks/).

## Supporto aziendale

PostgreSQL gode del supporto di numerose aziende che sponsorizzano sviluppatori e forniscono risorse di hosting e supporto finanziario. 
Consulta la nostra pagina [sponsor](https://www.postgresql.org/about/sponsors/) per l'elenco dei sostenitori del progetto.

C'è anche una grande comunità di [aziende che offrono supporto PostgreSQL](https://www.postgresql.org/support/professional_support/), dai singoli consulenti alle multinazionali.

Se desideri dare un contributo finanziario al PostgreSQL Global Development Group o ad una delle organizzazioni non profit riconosciute della comunità,
puoi visitare la nostra pagina delle [donazioni](https://www.postgresql.org/about/donate/).
