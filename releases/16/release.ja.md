2023 年 9 月 14 日 - PostgreSQLグローバル開発グループは本日、世界で最も先進的なオープンソースデータベースの最新バージョンである PostgreSQL 16 のリリースを発表しました。

[PostgreSQL 16](https://www.postgresql.org/docs/16/release-16.html) は、クエリの並列処理、バルクデータローディング、論理レプリケーションが顕著に改善され、性能が向上しました。このリリースには、SQL/JSON 構文の追加、ワークロードの新しい監視統計、大規模な要素群にわたるポリシーの管理のためのアクセス制御ルールの定義の柔軟性の向上など、開発者や管理者向けの多くの機能があります。

PostgreSQL コアチームのメンバーである Dave Page は次のように述べています。
「リレーショナルデータベースのパターンが進化する中で、PostgreSQL は大規模なデータの検索と管理における性能向上を続けています。
PostgreSQL 16 は、ユーザにワークロードをスケールアップおよびスケールアウトするためにより多くの方法を提供し、同時に、ユーザが洞察を得てデータ管理方法を最適化するための新しい方法を提供します。」

PostgreSQL は、信頼性と堅牢性で知られる革新的なデータ管理システムであり、グローバルな開発者コミュニティによる 35 年以上にわたるオープンソース開発の恩恵を受けて、あらゆる規模の組織に好まれるオープンソースリレーショナルデータベースとなっています。

### パフォーマンスの向上

PostgreSQL 16 では、新しい問い合わせプランナが最適化され、既存の PostgreSQL 機能の性能が向上しました。この最新リリースでは、`FULL` と `RIGHT` の [JOIN](https://www.postgresql.org/docs/16/queries-table-expressions.html#QUERIES-JOIN) を [問い合わせプランナが並列化](https://www.postgresql.org/docs/16/parallel-query.html) できるようになります。
`DISTINCT` 節または `ORDER BY` 節を使用する [集約関数](https://www.postgresql.org/docs/16/functions-aggregate.html) を使ったクエリに対してはより最適化された計画を生成し、 [`SELECT DISTINCT`](https://www.postgresql.org/docs/16/queries-select-lists.html#QUERIES-DISTINCT) クエリに対してインクリメンタルソートを利用し、[WINDOW 関数](https://www.postgresql.org/docs/16/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS) が最適化されより効率的に実行できるようになります。
また、`RIGHT` と` OUTER` の "アンチジョイン" を改良し、JOIN されたテーブルに存在しない行を識別できるようにした。

このリリースでは、単一操作と同時操作の両方で [`COPY`](https://www.postgresql.org/docs/16/sql-copy.html) を使用した一括ロードが改善され、いくつかのテストでは最大 300 % の性能向上が確認されました。
PostgreSQL 16 では、`libpq` を使用するクライアントの [負荷分散](https://www.postgresql.org/docs/16/libpq-connect.html#LIBPQ-CONNECT-LOAD-BALANCE-HOSTS) がサポートされ、VACUUM 戦略の改善によりテーブル全体を FREEZE する必要性が減少しました。
さらに、PostgreSQL 16 では、x86 と ARM の両方のアーキテクチャで `SIMD` を使用した CPU 高速化が導入され、ASCII と JSON 文字列の処理や、配列検索とサブトランザクション検索の実行時の性能が向上しました。

### 論理レプリケーション

[論理レプリケーション](https://www.postgresql.org/docs/16/logical-replication.html) を使用すると、PostgreSQL の論理レプリケーションプロトコルを解釈できる他の PostgreSQL インスタンスやサブスクライバに、データをストリームすることができます。
PostgreSQL 16 では、ユーザはスタンバイインスタンスから論理レプリケーションを実行することができ、開発者に新しい負荷分散の選択肢が提供されます。例えば、下流のシステムに変更を論理的に複製するために、忙しいプライマリではなく、スタンバイを使用することができます。

さらに、PostgreSQL 16 では論理レプリケーションの性能がいくつか向上しています。
サブスクライバが、並列ワーカを使用して大きなトランザクションを適用できるようになりました。
[主キー](https://www.postgresql.org/docs/16/ddl-constraints.html#DDL-CONSTRAINTS-PRIMARY-KEYS) を持たないテーブルでは、サブスクライバは逐次走査の代わりに B-tree インデックスを使用して、行を見つけることができます。
特定の条件下では、バイナリ形式を使用して、テーブルの初期同期を高速化することもできます。

PostgreSQL 16 では、論理レプリケーションに対するいくつかのアクセス制御が改善されました。新しい [定義済みロール](https://www.postgresql.org/docs/16/predefined-roles.html) `pg_create_subscription` が追加されて、ユーザに新しい論理サブスクリプションを作成する権限を与えます。
最後に、このリリースでは双方向での論理レプリケーションのサポートが追加され、異なる発行元にある 2つのテーブル間でデータを複製する機能が導入されました。

### 開発者体験

PostgreSQL 16 では、`JSON_ARRAY()`、`JSON_ARRAYAGG()`、`IS JSON` といった、コンストラクタや述語を含む [SQL/JSON](https://www.postgresql.org/docs/16/functions-json.html) 標準の構文が追加されました。
このリリースでは、アンダースコアを数千の区切り文字（例えば `5_432_000`）に使用できるようになりました。
また、`0x1538`, `0o12470`, `0b1010100111000` などの 10 進数以外の整数リテラルも使用できるようになりました。

PostgreSQL 16 を使用している開発者は、`psql` の新しいコマンドの恩恵を受けることもできます。
これには [`\bind`](https://www.postgresql.org/docs/16/app-psql.html#APP-PSQL-META-COMMAND-BIND) が含まれ、パラメータ化されたクエリを準備して、`bind` を使用して変数を置換することができます
（例： `SELECT $1::int + $2::int ￤bind 1 2 ￤g `）。

PostgreSQL 16 では、文字列のソート方法の規則を提供する [文字列の照合順序](https://www.postgresql.org/docs/16/collation.html) の一般的なサポートが改善されました。
PostgreSQL 16 はデフォルトで ICU をサポートして構築され、環境からデフォルトの ICU ロケールを決定し、ユーザが独自の ICU 照合規則を定義することができます。

### モニタリング

データベースワークロードの性能をチューニングする上で重要なことは、I/O 操作がシステムに与える影響を理解することです。PostgreSQL 16 は [`pg_stat_io`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-IO-VIEW) を導入しました。
これは、I/O アクセスパターンを詳細に分析するための主要な I/O 指標の新しい情報源です。

さらに、このリリースでは、[`pg_stat_all_tables`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ALL-TABLES-VIEW) ビューに、テーブルやインデックスが最後にスキャンされた日時を示すタイムスタンプを記録する新しいフィールドが追加されました。
また、PostgreSQL 16 では、パラメータ化された文に渡された値を記録することで、[`auto_explain`](https://www.postgresql.org/docs/16/auto-explain.html) がより読みやすくなり、[`pg_stat_statements`](https://www.postgresql.org/docs/16/pgstatstatements.html) と [`pg_stat_activity`](https://www.postgresql.org/docs/16/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW) で使用される問い合わせ追跡アルゴリズムの精度が向上しました。

### アクセスコントロールとセキュリティ

PostgreSQL 16 はアクセス制御のより細かいオプションを提供し、他のセキュリティ機能を強化します。
このリリースでは、[`pg_hba.conf`](https://www.postgresql.org/docs/16/auth-pg-hba-conf.html) と [`pg_ident.conf`](https://www.postgresql.org/docs/16/auth-username-maps.html) ファイルの管理が改善され、ユーザ名とデータベース名の正規表現マッチングや外部設定ファイルの `include` ディレクティブが使用できるようになりました。
このリリースでは、サーバからどの認証パラメータを受け付けるかをクライアントに指定できるようにする `require_auth` や、PostgreSQL がクライアントのオペレーティングシステムから提供された信頼された認証局（CA）ストアを使用することを示す [`sslrootcert="system"`](https://www.postgresql.org/docs/15/libpq-connect.html#LIBPQ-CONNECT-SSLROOTCERT) など、セキュリティ指向のクライアント接続パラメータがいくつか追加されました。
さらに、このリリースでは Kerberos 認証情報の委譲のサポートが追加され、[`postgres_fdw`](https://www.postgresql.org/docs/16/postgres-fdw.html) や [`dblink`](https://www.postgresql.org/docs/16/dblink.html) などの拡張が、認証された認証情報を使用して信頼されたサービスに接続できるようになりました。

### PostgreSQL について

[PostgreSQL](https://www.postgresql.org) は、世界で最も先進的なオープンソースデータベースであり、何千ものユーザ、貢献者、企業、組織からなるグローバルコミュニティを有しています。
PostgreSQL は、カリフォルニア大学バークレー校で始まった 35 年以上のエンジニアリングを基盤として、他に類を見ないペースで開発を続けてきました。
PostgreSQL の成熟した機能セットは、トップクラスのプロプライエタリなデータベースシステムに匹敵するだけでなく、高度なデータベース機能、拡張性、セキュリティ、安定性においてそれらを凌駕しています。

### リンク
​
* [ダウンロード](https://www.postgresql.org/download/)
* [リリースノート](https://www.postgresql.org/docs/16/release-16.html)
* [プレスキット](https://www.postgresql.org/about/press/)
* [セキュリティ](https://www.postgresql.org/support/security/)
* [バージョンポリシー](https://www.postgresql.org/support/versioning/)
* [@postgresql をフォロー](https://twitter.com/postgresql)
* [寄付](https://www.postgresql.org/about/donate/)

## 特徴についての詳細

上記の機能やその他の機能については、以下を参照してください。

* [リリースノート](https://www.postgresql.org/docs/16/release-16.html)
* [機能マトリックス](https://www.postgresql.org/about/featurematrix/)

## ダウンロード

PostgreSQL 16 をダウンロードする方法はいくつかあります。

* [Official Downloads](https://www.postgresql.org/download/) に [Windows](https://www.postgresql.org/download/windows/)、[Linux](https://www.postgresql.org/download/linux/)、[macOS](https://www.postgresql.org/download/macosx/) などのインストーラやツールがあります。
* [ソースコード](https://www.postgresql.org/ftp/source/v16.0)

その他のツールや拡張機能は [PostgreSQL Extension Network](http://pgxn.org/) にあります。

## ドキュメント

PostgreSQL 16 には HTML ドキュメントと man ページが付属しており、[HTML](https://www.postgresql.org/docs/16/) 形式と [PDF](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf) 形式の両方のドキュメントをオンラインで閲覧することができます。

### ライセンス

PostgreSQL は、BSD に似た「寛容な」ライセンスである PostgreSQL ライセンスを使用しています。この OSI 認証ライセンスは、PostgreSQL を商用およびプロプライエタリなアプリケーションで使用することを制限しないため、柔軟でビジネスフレンドリーであると広く評価されています。複数企業のサポートやコードの公的所有権とともに、このライセンスは、料金やベンダロックイン、ライセンス条項の変更を心配せずにデータベースを自社製品に組み込みたいと考えるベンダにとって PostgreSQL を非常に人気のあるものにしています。

## お問い合わせ先

Web サイト

* [https://www.postgresql.org/](https://www.postgresql.org/)

メールアドレス

* [press@postgresql.org](mailto:press@postgresql.org)

### 画像とロゴ

Postgres、PostgreSQL、象のロゴ (Slonik)は、すべて [PostgreSQL Community Association](https://www.postgres.ca) の登録商標です。これらのマークの使用を希望する場合は、[trademark policy](https://www.postgresql.org/about/policies/trademarks/) に従わなければなりません。

### コーポレートサポート

PostgreSQL は、開発者のスポンサーとなり、ホスティングリソースを提供し、財政的なサポートを提供してくれる多くの企業の支援を受けています。これらのプロジェクト支援者の一部は [スポンサー](https://www.postgresql.org/about/sponsors/) のページを参照してください。

また、個人のコンサルタントから多国籍企業まで、[PostgreSQL サポートを提供する企業](https://www.postgresql.org/support/professional_support/) の大きなコミュニティがあります。

PostgreSQL グローバル開発グループ、または認定されたコミュニティの非営利団体に金銭的な寄付をしたい場合は、[寄付](https://www.postgresql.org/about/donate/) のページを参照してください。