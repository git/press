The PostgreSQL Global Development Group announces that the second beta release of
PostgreSQL 16 is now [available for download](https://www.postgresql.org/download/).
This release contains previews of all features that will be available when
PostgreSQL 16 is made generally available, though some details of the release
can change during the beta period.

You can find information about all of the features and changes found in
PostgreSQL 16 in the [release notes](https://www.postgresql.org/docs/16/release-16.html):

  [https://www.postgresql.org/docs/16/release-16.html](https://www.postgresql.org/docs/16/release-16.html)

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 16 on your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 16 Beta 2 in production environments, we encourage you to find ways
to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 16
release upholds our standards of delivering a stable, reliable release of the
world's most advanced open source relational database. Please read more about
our [beta testing process](https://www.postgresql.org/developer/beta/) and how
you can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

Upgrading to PostgreSQL 16 Beta 2
---------------------------------

To upgrade to PostgreSQL 16 Beta 2 from an earlier version of PostgreSQL,
you will need to use a strategy similar to upgrading between major versions of
PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`). For more
information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/15/static/upgrading.html).

Changes Since Beta 1
--------------------

Fixes and changes in PostgreSQL 16 Beta 2 include:

* The default collation provider selected by `initdb` is changed back to `libc`.
* The behavior of selecting the `C` locale with `libicu` is deferred back to
`libicu`. On ICU 64 and higher, the `C` locale is obsolete, and ICU provides its
own mechanism for selecting a locale or throwing an error.
* Several fixes related to join optimizations.
* Fix for B-tree code related to the changes introduced by logical decoding
from standbys.
* Fix cache lookup hazards when looking up `MAINTAIN` privileges on partition
ancestors.

Please see the [release notes](https://www.postgresql.org/docs/16/release-16.html)
for a complete list of new and changed features:

  [https://www.postgresql.org/docs/16/release-16.html](https://www.postgresql.org/docs/16/release-16.html)

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 16. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_16_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the second beta release of version 16. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2023. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/)
page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 16 Beta Release Notes](https://www.postgresql.org/docs/16/release-16.html)
* [PostgreSQL 16 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_16_Open_Items)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/#configuration-management)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
