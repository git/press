PostgreSQL 13 RC 1 Released
===========================

The PostgreSQL Global Development Group announces that the first release
candidate of PostgreSQL 13 is now available for download. As a release
candidate, PostgreSQL 13 RC 1 will be mostly identical to the initial release of
PostgreSQL 13, though some more fixes may be applied prior to the general
availability of PostgreSQL 13.

The planned date for the general availability of PostgreSQL 13 is September 24,
2020. Please see the "Release Schedule" section for more details.

Upgrading to PostgreSQL 13 RC 1
-------------------------------

To upgrade to PostgreSQL 13 RC 1 from earlier versions of PostgreSQL, you will
need to use a major version upgrade strategy, e.g. `pg_upgrade` or
`pg_dump` / `pg_restore`. For more information, please visit the documentation
section on [upgrading](https://www.postgresql.org/docs/13/static/upgrading.html):

[https://www.postgresql.org/docs/13/static/upgrading.html](https://www.postgresql.org/docs/13/static/upgrading.html)

Changes Since 13 Beta 3
-----------------------

Several bug fixes were applied for PostgreSQL 13 during the Beta 3 period. These
include:

* Adjustments to the costing model for hash aggregates that spill to disk.
* Adjustments to the output of `EXPLAIN (BUFFERS)`.
* Display the stats target of extended statistics in the output of `\d`.

For a detailed list of fixes, please visit the
[open items](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items#resolved_before_13rc1)
page.

Release Schedule
----------------

This is the first release candidate for PostgreSQL 13. Unless an issue is
discovered that warrants a delay or to produce an additional release candidate,
PostgreSQL 13 should be made generally available on September 24, 2020.

For further information please see the
[Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 13 RC 1 Release Notes](https://www.postgresql.org/docs/13/static/release-13.html)
* [PostgreSQL 13 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
* [Follow @postgresql on Twitter](https://www.twitter.com/postgresql)
