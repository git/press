Le PostgreSQL Global Development Group annonce aujourd'hui la sortie de
[PostgreSQL 13](https://www.postgresql.org/docs/13/release-13.html), la dernière
version du [SGBD open source de référence](https://www.postgresql.org/).

PostgreSQL 13 améliore considérablement le système d'indexation et de recherche,
au bénéfice des bases de données volumineuses. Ces améliorations intègrent la
réduction de l'espace disque occupé et des gains de performance pour les index,
de meilleurs temps de réponse pour les requêtes utilisant des agrégats ou des
partitions, une meilleure planification lors de l'utilisation des statistiques
améliorées, et bien d'autres.

À côté de fonctionnalités très demandées comme le
[vacuum parallèle](https://www.postgresql.org/docs/13/sql-vacuum.html) et le
[tri incrémental](https://www.postgresql.org/docs/13/runtime-config-query.html#GUC-ENABLE-INCREMENTAL-SORT),
PostgreSQL 13 améliore l'expérience de gestion des données pour tous les types
de trafic, en offrant des optimisations pour l'administration au quotidien, plus
de facilités pour les développeurs d'applications et des améliorations de
sécurité.

Selon Peter Eisentraut, membre de la Core Team de PostgreSQL, «&nbsp;PostgreSQL
13 démontre la collaboration et l'implication de l'ensemble de la communauté
dans la poursuite du développement des fonctionnalités de la base de données
relationnelle open source la plus avancée au monde. Les innovations incluses
dans chaque version, et la réputation de fiabilité et de stabilité expliquent
pourquoi de plus en plus de gens choisissent PostgreSQL pour leurs
applications.&nbsp;»

[PostgreSQL](https://www.postgresql.org), système innovant de gestion des
données, connu pour sa fiabilité et sa robustesse, bénéficie depuis plus de
25 ans d'un développement open source par une
[communauté de développeurs mondiale](https://www.postgresql.org/community/).
Il est devenu le système de gestion de bases de données relationnelles de
référence pour des organisations de toute taille.

### Poursuite des gains de performance

Continuant sur les travaux de la version précédente, PostgreSQL 13 gère
efficacement
[la duplication de données dans un index B-tree](https://www.postgresql.org/docs/13/btree-implementation.html#BTREE-DEDUPLICATION),
type standard d'index. Cela réduit l'espace utilisé par les index B-tree tout en
améliorant la performance des requêtes.

PostgreSQL 13 introduit le tri incrémental, dans lequel les données triées lors
d'une étape précédente du plan de requêtage peuvent accélérer le tri dans une
étape ultérieure de ce même plan. De plus, PostgreSQL peut maintenant utiliser
le système de
[statistiques étendues](https://www.postgresql.org/docs/13/planner-stats.html#PLANNER-STATS-EXTENDED)
(accessible au travers de la commande
[`CREATE STATISTICS`](https://www.postgresql.org/docs/13/sql-createstatistics.html)) pour
créer de meilleurs plans pour les requêtes contenant des clauses `OR` et des
recherches `IN`/`ANY` sur des listes.

Avec PostgreSQL 13, la fonctionnalité d'agrégation par hachage peut améliorer
différents types requêtes utilisant les
[agrégats](https://www.postgresql.org/docs/13/functions-aggregate.html) et les
[grouping sets](https://www.postgresql.org/docs/13/queries-table-expressions.html#QUERIES-GROUPING-SETS),
en évitant aux agrégats volumineux de devoir tenir entièrement en mémoire. Les
requêtes sur des
[tables partitionnées](https://www.postgresql.org/docs/13/ddl-partitioning.html) sont
plus performantes&nbsp;; il y a désormais davantage de possibilités d'écarter des
partitions, ou de les joindre directement.


### Optimisations concernant l'administration

[VACUUM](https://www.postgresql.org/docs/13/routine-vacuuming.html) est une
opération essentielle de l'administration de PostgreSQL. Elle permet de
récupérer l'espace de stockage après mise à jour ou suppression de lignes. Ce
processus n'est pas dénué de difficultés, en dépit du travail mené sur les
versions précédentes de PostgreSQL afin d'en réduire l'empreinte.

PostgreSQL 13 poursuit l'amélioration du vacuum en introduisant la
[parallélisation du vacuum pour les index](https://www.postgresql.org/docs/13/sql-vacuum.html).
En plus des gains de performances rendus possibles par cette avancée, la charge induite par cette
fonctionnalité peut être finement ajustée par l'administrateur qui peut définir
le nombre de processus parallèles à lancer. Pour compléter ces améliorations de
performances, l'insertion de données peut maintenant déclencher le processus
d'autovacuum.

[Les slots de réplication](https://www.postgresql.org/docs/13/warm-standby.html#STREAMING-REPLICATION-SLOTS)
qui sont utilisés pour éviter la suppression des journaux de transactions (WAL)
avant leur réception par un réplica, peuvent être finement configurés dans
PostgreSQL 13. Cela permet de définir
[le nombre maximum de fichiers WAL à conserver](https://www.postgresql.org/docs/13/runtime-config-replication.html#GUC-MAX-SLOT-WAL-KEEP-SIZE)
et ainsi aider à la prévention des erreurs de saturation de l'espace disque.

PostgreSQL 13 ajoute également plusieurs éléments de suivi de l'activité de la
base de données par l'administrateur. Cela inclut notamment les statistiques
d'utilisation des WAL par `EXPLAIN`, la progression de la sauvegarde en continu,
et la progression des commandes `ANALYZE`. L'intégrité de la sortie de la
commande [`pg_basebackup`](https://www.postgresql.org/docs/13/app-pgbasebackup.html)
peut, de plus, être vérifiée avec la nouvelle commande
[`pg_verifybackup`](https://www.postgresql.org/docs/13/app-pgverifybackup.html).


### Commodités pour le développement d'applications

PostgreSQL 13 simplifie le travail avec les types de données provenant de
différentes sources de données. Cette version ajoute notamment la fonction
[`datetime()`](https://www.postgresql.org/docs/13/functions-json.html#FUNCTIONS-SQLJSON-OP-TABLE)
au support de SQL/JSON path. Celle-ci convertit les dates d'un format valide
(des chaînes ISO 8601, par exemple) en types natifs PostgreSQL. De plus, la
fonction de génération UUID v4,
[`gen_random_uuid()`](https://www.postgresql.org/docs/13/functions-uuid.html),
est maintenant disponible sans extension.

Le système de partitionnement de PostgreSQL est plus souple maintenant que les
tables partitionnées supportent la réplication logique et les déclencheurs
(triggers) `BEFORE` de niveau ligne.

La syntaxe
[`FETCH FIRST`](https://www.postgresql.org/docs/13/sql-select.html#SQL-LIMIT) a été
étendue pour y inclure la clause `WITH TIES`. `WITH TIES` permet d'inclure
toutes les lignes liées à la dernière ligne du résultat fourni par `ORDER BY`.


### Amélioration de la sécurité

Le système d'extension de PostgreSQL est un élément clé de sa robustesse,
puisqu'il permet aux développeurs d'en étendre les fonctionnalités. Dans les
versions précédentes, les nouvelles extensions ne pouvaient être installées que
par un superutilisateur de la base de données. Pour simplifier cette gestion,
PostgreSQL 13 ajoute le concept
«&nbsp;[d'extension de confiance](https://www.postgresql.org/docs/13/sql-createextension.html)&nbsp;»,
qui permet aux utilisateurs d'installer les extensions identifiées comme de
«&nbsp;confiance&nbsp;» par un superutilisateur. Certaines extensions incluses dans
PostgreSQL sont définies comme de confiance par défaut&nbsp;:
[`pgcrypto`](https://www.postgresql.org/docs/13/pgcrypto.html),
[`tablefunc`](https://www.postgresql.org/docs/13/tablefunc.html),
[`hstore`](https://www.postgresql.org/docs/13/hstore.html), parmi d'autres.

Pour les applications qui nécessitent des méthodes d'authentification
sécurisées, PostgreSQL 13 permet aux clients
d'[exiger l'agrégation de canaux](https://www.postgresql.org/docs/13/libpq-connect.html#LIBPQ-CONNECT-CHANNEL-BINDING)
lors de l'utilisation de
[l'authentification SCRAM](https://www.postgresql.org/docs/13/sasl-authentication.html#SASL-SCRAM-SHA-256).
Le _foreign data wrapper_ de
PostgreSQL ([`postgres_fdw`](https://www.postgresql.org/docs/13/postgres-fdw.html))
peut maintenant utiliser une authentification à base de certificats.

### À propos de PostgreSQL

[PostgreSQL](https://www.postgresql.org) est le système de gestion de bases de
données libre de référence. Sa communauté mondiale est composée de plusieurs
milliers d’utilisateurs, contributeurs, entreprises et institutions. Le projet
PostgreSQL, démarré il y a plus de 30 ans à l’université de Californie, à
Berkeley, a atteint aujourd’hui un rythme de développement sans pareil.
L’ensemble des fonctionnalités proposées est mature, et dépasse même celui des
systèmes commerciaux leaders sur les fonctionnalités avancées, les extensions,
la sécurité et la stabilité.


### Traduction du dossier de presse

* TBD

### Liens

* [Téléchargements](https://www.postgresql.org/download/)
* [Notes de version](https://www.postgresql.org/docs/13/release-13.html)
* [Dossier de presse](https://www.postgresql.org/about/press/)
* [Page de sécurité](https://www.postgresql.org/support/security/)
* [Politique de versions](https://www.postgresql.org/support/versioning/)
* [Suivre @postgresql sur Twitter](https://twitter.com/postgresql)
