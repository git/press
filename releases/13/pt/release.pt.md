O Grupo de Desenvolvimento Global do PostgreSQL anunciou hoje o lançamento do
[PostgreSQL 13](https://www.postgresql.org/docs/13/release-13.html), a versão
mais recente do [banco de dados de código aberto mais avançado do
mundo](https://www.postgresql.org/).

PostgreSQL 13 inclui melhorias significativas no seu sistema de indexação e
busca que beneficia grandes bancos de dados, incluindo economia de espaço e
ganho de desempenho para índices, tempos de resposta mais rápidos para
consultas que usam agregações ou partições, melhor planejamento de consultas ao
utilizar estatísticas aprimoradas e muito mais.

Junto com recursos altamente solicitados como [VACUUM
paralelizado](https://www.postgresql.org/docs/13/sql-vacuum.html) e [ordenação
incremental](https://www.postgresql.org/docs/13/runtime-config-query.html#GUC-ENABLE-INCREMENTAL-SORT),
o PostgreSQL 13 fornece uma melhor experiência de gerenciamento de dados para
cargas de trabalho grandes e pequenas, com otimizações para administração
diária, mais conveniências para desenvolvedores de aplicações e melhorias de
segurança.

"PostgreSQL 13 mostra a colaboração e dedicação de nossa comunidade global em
promover as habilidades do banco de dados relacional de código aberto mais
avançado do mundo", disse Peter Eisentraut, um membro do Grupo de
Desenvolvimento Global do PostgreSQL. "As inovações que cada versão traz junto
com sua reputação de confiabilidade e estabilidade é o motivo pelo qual mais
pessoas optam pelo uso do PostgreSQL em suas aplicações."

[PostgreSQL](https://www.postgresql.org), um sistema de gerenciamento de dados
inovador conhecido pela sua confiabilidade e robustez, se beneficia de mais de
25 anos de desenvolvimento de código aberto de uma [comunidade global de
desenvolvedores](https://www.postgresql.org/community/) e se tornou o banco de
dados relacional de código aberto preferido pelas organizações de todos os
tamanhos.

### Ganhos de Desempenho Contínuo

Baseado no trabalho da versão anterior do PostgreSQL, PostgreSQL 13 pode
eficientemente lidar com [dados duplicados em índices
B-tree](https://www.postgresql.org/docs/13/btree-implementation.html#BTREE-DEDUPLICATION),
o índice padrão do banco de dados. Isso diminui o uso geral de espaço que os
índices B-tree exigem, melhorando o desempenho geral da consulta.

PostgreSQL 13 introduz a ordenação incremental, em que os dados ordenados em
uma etapa anterior de uma consulta pode acelerar a ordenação em uma etapa
posterior. Além disso, o PostgreSQL pode utilizar o sistema de [estatísticas
estendidas](https://www.postgresql.org/docs/13/planner-stats.html#PLANNER-STATS-EXTENDED)
(criadas com [`CREATE
STATISTICS`](https://www.postgresql.org/docs/13/sql-createstatistics.html))
para criar planos aprimorados para consultas com cláusulas `OR` e buscas
`IN`/`ANY` em listas.

No PostgreSQL 13, mais tipos de consultas de
[agregação](https://www.postgresql.org/docs/13/functions-aggregate.html) e
[conjunto de
agrupamento](https://www.postgresql.org/docs/13/queries-table-expressions.html#QUERIES-GROUPING-SETS)
podem aproveitar a funcionalidade de agregação hash eficiente do PostgreSQL, já
que as consultas com agregações grandes não cabem inteiramente na memória.
Consultas com tabelas particionadas receberam um aumento de desempenho, agora
há mais casos onde partições podem ser removidas e onde as partições podem ser
juntadas diretamente.

### Otimizações na Administração

[VACUUM](https://www.postgresql.org/docs/13/routine-vacuuming.html) é uma parte
essencial da administração do PostgreSQL, permitindo que o banco de dados
recupere espaço de armazenamento após registros serem atualizados ou removidos.
Este processo também pode impor desafios administrativos, embora as versões
anteriores do PostgreSQL tenham feito um trabalho para minimizar a sobrecarga
do VACUUM.

PostgreSQL 13 continua a melhorar o sistema de limpeza de dados com a
introdução do [VACUUM paralelizado para
índices](https://www.postgresql.org/docs/13/sql-vacuum.html).  Além dos
benefícios de desempenho do VACUUM que ele oferece, o uso desta nova
funcionalidade pode ser ajustado para cargas de trabalho específicas já que os
administradores podem escolher o número de processos paralelos a serem
utilizados. Além desses benefícios de performance, as inserções de dados podem
disparar o processo do autovacuum.

[Slots de replicação](https://www.postgresql.org/docs/13/warm-standby.html#STREAMING-REPLICATION-SLOTS),
que são utilizados para evitar que os logs de transação (WAL) 
sejam removidos antes que eles sejam recebidos pela réplica, podem ser 
ajustados no PostgreSQL 13 para especificar o [número máximo de 
arquivos do WAL retidos](https://www.postgresql.org/docs/13/runtime-config-replication.html#GUC-MAX-SLOT-WAL-KEEP-SIZE)
e ajudar a evitar erros de falta de espaço em disco.

PostgreSQL 13 também adiciona algumas maneiras que o administrador pode
monitorar a atividade do banco de dados, incluindo estatísticas de uso do WAL
no `EXPLAIN`, o progresso de cópia de segurança base e o progresso de comandos
`ANALYZE`. Além disso, a integridade da saída do comando
[`pg_basebackup`](https://www.postgresql.org/docs/13/app-pgbasebackup.html)
pode ser verificado utilizando o novo comando
[`pg_verifybackup`](https://www.postgresql.org/docs/13/app-pgverifybackup.html).

### Conveniências para Desenvolvimento de Aplicações

O PostgreSQL 13 torna ainda mais fácil trabalhar com tipos de dados do
PostgreSQL provenientes de diferentes fontes de dados. Esta versão adiciona a
função
[`datetime()`](https://www.postgresql.org/docs/13/functions-json.html#FUNCTIONS-SQLJSON-OP-TABLE)
para seu suporte SQL/JSON path, que converte formatos de tempo válidos (e.g.
cadeia de caracteres ISO 8601) para tipos nativos do PostgreSQL.  Além disso, a
função UUID v4,
[`gen_random_uuid()`](https://www.postgresql.org/docs/13/functions-uuid.html),
também está disponível sem precisar instalar nenhuma extensão.

Sistema de particionamento do PostgreSQL está mais flexível, já que tabelas
particionadas suportam totalmente replicação lógica e gatilhos BEFORE a nível
de registro.

A sintaxe [`FETCH
FIRST`](https://www.postgresql.org/docs/13/sql-select.html#SQL-LIMIT) no
PostgreSQL 13 foi expandida para incluir a cláusula `WITH TIES`.  Quando
especificada, `WITH TIES` inclui qualquer registro que, baseado na cláusula
`ORDER BY`, corresponde ao último lugar no conjunto de resultados.

### Melhorias de Segurança

O sistema de extensões do PostgreSQL é um componente-chave de sua robustez pois
permite que os desenvolvedores expandam sua funcionalidade. Em versões
anteriores, novas extensões só podiam ser instaladas por um super-usuário do
banco de dados. Para maximizar o aproveitamento da extensibilidade do
PostgreSQL, PostgreSQL 13 adiciona o conceito de uma "[extensão
confiável](https://www.postgresql.org/docs/13/sql-createextension.html)", que
permite usuários de banco de dados instalar extensões que um super-usuário
marcou como "confiável". Algumas extensões embutidas são marcadas como
confiáveis por padrão, incluindo
[`pgcrypto`](https://www.postgresql.org/docs/13/pgcrypto.html),
[`tablefunc`](https://www.postgresql.org/docs/13/tablefunc.html),
[`hstore`](https://www.postgresql.org/docs/13/hstore.html) e mais.

Para aplicações que exigem métodos de autenticação seguros, o PostgreSQL 13
permite que clientes possam [exigir ligação de
canal](https://www.postgresql.org/docs/13/libpq-connect.html#LIBPQ-CONNECT-CHANNEL-BINDING)
ao utilizar [autenticação
SCRAM](https://www.postgresql.org/docs/13/sasl-authentication.html#SASL-SCRAM-SHA-256)
e o adaptador de dados externos do PostgreSQL
([`postgres_fdw`](https://www.postgresql.org/docs/13/postgres-fdw.html)) pode
utilizar autenticação baseada em certificados.

### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) é o banco de dados mais avançado do
mundo, com uma comunidade global de milhares de usuários, colaboradores,
empresas e organizações. O Projeto PostgreSQL baseia-se em mais de 30 anos de
engenharia, iniciando na Universidade da Califórnia, Berkeley, e continua em um
ritmo inigualável de desenvolvimento. Conjunto de funcionalidades maduras do
PostgreSQL não só se igualam aos principais sistemas de bancos de dados
proprietários, mas os supera em funcionalidades avançadas, extensibilidade,
segurança e estabilidade.

### Traduções do Comunicado à Imprensa

* TBD

### Links

* [Download](https://www.postgresql.org/download/)
* [Notas de Lançamento](https://www.postgresql.org/docs/13/release-13.html)
* [Kit à Imprensa](https://www.postgresql.org/about/press/)
* [Informação de Segurança](https://www.postgresql.org/support/security/)
* [Política de Versionamento](https://www.postgresql.org/support/versioning/)
* [Siga @postgresql no Twitter](https://twitter.com/postgresql)
