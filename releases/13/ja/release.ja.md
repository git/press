﻿PostgreSQL Global Development Groupは本日、世界で[最も高度なオープンソースデータベース](https://www.postgresql.org/)の最新バージョンである[PostgreSQL13](https://www.postgresql.org/docs/13/release-13.html)のリリースを発表しました。

PostgreSQL13では、インデックス作成およびルックアップシステムが大幅に改善されています。これにより、インデックスのスペース節約とパフォーマンス向上、集約またはパーティションを使用するクエリの応答時間の高速化、拡張された統計情報を使用する場合のクエリ計画の改善など、大規模なデータベースにメリットがあります。

[並列バキューム処理](https://www.postgresql.org/docs/13/sql-vacuum.html)や[インクリメンタルソート](https://www.postgresql.org/docs/13/runtime-config-query.html#GUC-ENABLE-INCREMENTAL-SORT)などの要求の高い機能に加えて、PostgreSQL13は、日々の管理のための最適化、アプリケーション開発者の利便性の向上、セキュリティの強化により、大小のワークロードに対してより良いデータ管理エクスペリエンスを提供します。

「PostgreSQL13は、世界で最も先進的なオープンソースのリレーショナルデータベースの能力をさらに高めるために、我々のグローバルコミュニティが協力し、献身していることを示しています。」PostgreSQL Core Teamのメンバーであるピーター・アイゼントラウト氏はこのように述べています。「各リリースがもたらす革新と、その信頼性と安定性に対する評判が、より多くの人々がアプリケーションにPostgreSQLを使うことを選択する理由です。」

信頼性と堅牢性で知られる革新的なデータ管理システムである[PostgreSQL](https://www.postgresql.org)は、[グローバルな開発者コミュニティ](https://www.postgresql.org/community/)による25年以上にわたるオープンソース開発の恩恵を受けており、あらゆる規模の組織にとって好ましいオープンソースリレーショナルデータベースとなっています。


### 継続的なパフォーマンス向上

PostgreSQL13では、以前のPostgreSQLリリースからの作業を基に、標準データベースインデックスである[B-treeインデックスの重複データ](https://www.postgresql.org/docs/13/btree-implementation.html#BTREE-DEDUPLICATION)を効率的に処理することができます。これにより、B-treeインデックスが必要とする全体的な領域使用量が削減されると同時に、全体的なクエリパフォーマンスが向上します。

PostgreSQL13ではインクリメンタルソートが導入されており、問い合わせの前のステップでソートされたデータを後のステップで高速にソートすることができます。さらに、PostgreSQLは[拡張された統計](https://www.postgresql.org/docs/13/planner-stats.html#PLANNER-STATS-EXTENDED)システム([`CREATE STATISTICS`](https://www.postgresql.org/docs/13/sql-createstatistics.html)経由でアクセス)を使用して、`OR` 句を持つ問い合わせやリストに対する`IN`/`ANY` ルックアップのための改善された計画を作成できるようになりました。

PostgreSQL13では、より多くのタイプの[集約クエリ](https://www.postgresql.org/docs/13/functions-aggregate.html)と[グループ化セットクエリ](https://www.postgresql.org/docs/13/queries-table-expressions.html#QUERIES-GROUPING-SETS)が、PostgreSQLの効率的なハッシュ集約機能を利用できます。これは、大きな集約を持つクエリが完全にメモリに収まる必要がないためです。[パーティション化](https://www.postgresql.org/docs/13/ddl-partitioning.html)されたテーブルを持つクエリのパフォーマンスが向上しました。これは、パーティションをプルーニングしたり、パーティションを直接結合したりできるケースが増えたためです。


### 管理の最適化

[vacuum](https://www.postgresql.org/docs/13/routine-vacuuming.html)はPostgreSQLの管理に不可欠な部分であり、行が更新されたり削除されたりした後にデータベースが記憶領域を再利用できるようにします。以前のPostgreSQLリリースではバキューム処理のオーバーヘッドを軽減するための作業が行われていましたが、このプロセスには管理上の問題もあります。

PostgreSQL13では、インデックスに[並列化されたバキューム](https://www.postgresql.org/docs/13/sql-vacuum.html)を導入することで、バキュームシステムの改善を続けています。バキューム処理のパフォーマンスのメリットに加え、管理者は実行する並列ワーカーの数を選択できるため、この新機能の使用を特定のワークロードに合わせて調整できます。これらのパフォーマンス上のメリットに加えて、データ挿入によって自動バキューム・プロセスを起動できるようになりました。

レプリカによって受信される前に先行書き込みログ(WAL)が削除されないようにするために使用される[レプリケーションスロット](https://www.postgresql.org/docs/13/warm-standby.html#STREAMING-REPLICATION-SLOTS)は、PostgreSQL13において、[保持するWALファイルの最大数](https://www.postgresql.org/docs/13/runtime-config-replication.html#GUC-MAX-SLOT-WAL-KEEP-SIZE)を指定するように調整することができ、ディスク外エラーの回避に役立ちます。

また、PostgreSQL13では、`EXPLAIN` からのWAL使用統計の参照、ストリーミングベースバックアップの進行状況、`ANALYZE` コマンドの進行状況など、管理者がデータベースの動作を監視する方法が追加されています。さらに、[`pg_basebackup`](https://www.postgresql.org/docs/13/app-pgbasebackup.html)コマンドの出力の完全性は、新たな[`pg_verifybackup`](https://www.postgresql.org/docs/13/app-pgverifybackup.html)コマンドを使用して検査できます。


### アプリケーション開発の利便性


PostgreSQL13では、異なるデータソースからのPostgreSQLデータ型をより簡単に扱うことができます。このリリースでは、[`datetime()`](https://www.postgresql.org/docs/13/functions-json.html#FUNCTIONS-SQLJSON-OP-TABLE) 関数がSQL/JSONパスサポートに追加され、有効な時刻フォーマット(例えばISO8601文字列)をPostgreSQLのネイティブ型に変換します。さらに、UUID v4生成関数である [`gen_random_uuid()`](https://www.postgresql.org/docs/13/functions-uuid.html)が拡張機能をインストールしなくても使用できるようになりました。

PostgreSQLのパーティショニングシステムは、パーティショニングされたテーブルが論理的な複製とBEFORE行レベルのトリガを完全にサポートするため、より柔軟です。

PostgreSQL13の [`FETCH FIRST`](https://www.postgresql.org/docs/13/sql-select.html#SQL-LIMIT) 構文がWITH TIES句を含むように拡張されました。`WITH TIES` を指定すると、`ORDER BY` 句に基づいて、結果セットの最後の行と同順「タイ」の行が含まれます。


### セキュリティの強化

PostgreSQLの拡張システムは、開発者がその機能を拡張できるようにするため、その堅牢性の重要な要素です。以前のリリースでは、新しい拡張機能をインストールできるのはデータベーススーパーユーザのみでした。PostgreSQLの拡張性を利用しやすくするために、PostgreSQL13には「[信頼できる拡張機能](https://www.postgresql.org/docs/13/sql-createextension.html)」という概念が追加されています。これにより、データベースユーザは、スーパーユーザが「信頼できる」とマークした拡張機能をインストールできます。[`pgcrypto`](https://www.postgresql.org/docs/13/pgcrypto.html)、[`tablefunc`](https://www.postgresql.org/docs/13/tablefunc.html)、[`hstore`](https://www.postgresql.org/docs/13/hstore.html)など、特定の組み込み拡張はデフォルトでtrustedとマークされます。

セキュアな認証方式を必要とするアプリケーションの場合、PostgreSQL13では、クライアントが[SCRAM認証](https://www.postgresql.org/docs/13/sasl-authentication.html#SASL-SCRAM-SHA-256)を使用する際に[チャネルバインディングを要求](https://www.postgresql.org/docs/13/libpq-connect.html#LIBPQ-CONNECT-CHANNEL-BINDING)できるようになっており、PostgreSQLの外部データラッパー([`postgres_fdw`](https://www.postgresql.org/docs/13/postgres-fdw.html))は証明書ベースの認証を使用できるようになりました。


### PostgreSQLについて

[PostgreSQL](https://www.postgresql.org)は世界で最も高度なオープンソースデータベースであり、数1000人のユーザ、貢献者、企業、組織からなるグローバルコミュニティを持っています。カリフォルニア大学バークレー校、カリフォルニア大学バークレー校を皮切りに30年以上に構築されたPostgreSQLは、比類のない速さで開発を続けてきました。PostgreSQLの成熟した機能セットは、トッププロプライエタリなデータベースシステムに匹敵するだけでなく、高度なデータベース機能、拡張性、セキュリティ、安定性においても優れています。


### プレスリリースの翻訳

* TBD


### リンク


* [ダウンロード](https://www.postgresql.org/download/)
* [リリースノート](https://www.postgresql.org/docs/13/release-13.html)
* [プレスキット](https://www.postgresql.org/about/press/)
* [Securityページ](https://www.postgresql.org/support/security/)
* [バージョン管理ポリシー](https://www.postgresql.org/support/versioning/)
* [Twitterで @postgresql をフォローする](https://twitter.com/postgresql)

