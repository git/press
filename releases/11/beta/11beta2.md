PostgreSQL 11 Beta 2 Released
=============================

The PostgreSQL Global Development Group announces that the second beta release of PostgreSQL 11 is now available for download. This release contains previews of all features that will be available in the final release of PostgreSQL 11 (though some details of the release could change before then) as well as bug fixes that were reported during the first beta.

In the spirit of the open source PostgreSQL community, we strongly encourage you to test the new features of PostgreSQL 11 in your database systems to help us eliminate any bugs or other issues that may exist. While we do not advise for you to run PostgreSQL 11 Beta 2 in your production environments, we encourage you to find ways to run your typical application workloads against this beta release.

Upgrading to PostgreSQL 11 Beta 2
---------------------------------

To upgrade to PostgreSQL 11 Beta 2 from Beta 1 or an earlier version of PostgreSQL, you will to use a strategy similar to upgrading between major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`). For more information, please visit the documentation section on [upgrading](https://www.postgresql.org/docs/11/static/upgrading.html).

Changes to Major Features of PostgreSQL 11
------------------------------------------

The PostgreSQL 11 Beta 1 release announced "Channel Binding for SCRAM Authentication" as a major feature as it could prevent man-in-the-middle attacks when using SCRAM authentication. During the course of testing, it was determined that libpq, the client connection library for PostgreSQL, [is unable force the use of channel binding](https://www.postgresql.org/message-id/20180623030153.GB21575%40momjian.us).

While this functionality can be fixed in a future version of PostgreSQL, for now channel binding for SCRAM authentication will not be considered a major feature for PostgreSQL 11.

Changes Since Beta 2
--------------------

There have been many bug fixes for PostgreSQL 11 reported during the Beta 1 period and applied to the Beta 2 release. Several bug fixes reported for version 10 or earlier that also affected version 11 are included in the Beta 2 release. These fixes include:

* Several fixes for XML support, including using the document node as the context for XPath queries as defined in the SQL standard, which affects the `xpath` and `xpath_exists` functions, as well as XMLTABLE
* Several fixes related to VACUUM, including potential data corruption issues
* Fixes for partitioning, including ensuring partitioning works with temporary tables, eliminating a needless additional partition constraint checks on `INSERT`s and generating incorrect paths for partitionwise aggregates
* Fix for potential replica server crashes where a replica would attempt to read a recycled WAL segment
* Fixes for `pg_replication_slot_advance` including returning `NULL` if slot is not advanced and changes for how the slot is updated depending on if it is a physical or logical replication slot
* Ensure `pg_resetwal` works with relative paths to data directory
* Fixes for query parallelism, including preventing a crash by ignoring "parallel append" for parallel unsafe paths in a query plan
* Fix returning accurate results with `variance` and similar aggregate functions when executed using parallel query
* Fix issue where `COPY FROM .. WITH HEADER` would drop a line after every 4,294,967,296 lines processed
* Ensure the "B" (bytes) parameter is accepted by all memory-related configuration parameters
* Several fixes for the JSONB transform in PL/Python and PL/Perl
* Fix for plpgsql checking statements where it needs to check original write statement before rewrite, could cause crash
* Fix for `SHOW ALL` to display superuser configuration settings to roles that are allowed to read all settings
* Fix for `pg_upgrade` that ensures defaults are written when using the "fast ALTER TABLE .. ADD COLUMN" feature with a non-NULL default
* Several fixes for memory leaks
* Several fixes specific to the Windows platform

This update also contains tzdata release 2018e, with updates for North Korea. The 2018e also reintroduces the negative-DST changes that were originally introduced in 2018a, which affects historical and present timestamps for Ireland (1971-), as well as historical timestamps for Namibia (1994-2017) and the former Czechoslovakia (1946-1947). If your application is storing timestamps with those timezones in the affected date ranges, we ask that you please test to ensure your applications behave as expected.

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community, to test the upcoming version with your workloads and testing tools in order to find bugs and regressions before the release of PostgreSQL 11. As this is a Beta, minor changes to database behaviors, feature details, and APIs are still possible. Your feedback and testing will help determine the final tweaks on the new features, so please test in the near future. The quality of user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items) is publicly available in the PostgreSQL wiki.  You can [report bugs](https://www.postgresql.org/account/submitbug/) using this form on the PostgreSQL website:

[https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the second beta release of version 11. The PostgreSQL Project will release additional betas as required for testing, followed by one or more release candidates, until the final release in late 2018. For further information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 11 Beta Release Notes](https://www.postgresql.org/docs/11/static/release-11.html)
* [PostgreSQL 11 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
