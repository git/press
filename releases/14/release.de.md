Die PostgreSQL Global Development Group hat heute die Veröffentlichung von [PostgreSQL 14](https://www.postgresql.org/docs/14/release-14.html), der aktuellsten Version des weltweit [führenden Open-Source-SQL-Datenbanksystems](https://www.postgresql.org/), bekannt gegeben.

PostgreSQL 14 bringt eine Reihe von Verbesserungen, die es Entwicklern und Administratoren vereinfachen, ihre datengetriebenen Applikation einzusetzen. Im Bereich der komplexen Datentypen gibt auch in dieser Version wieder erhebliche Innovationen, z.B. bequemeren Zugriff für JSON und Unterstützung für nicht zusammenhängende Range-Datentypen. Die Trends der letzten Versionen, Geschwindigkeits-Verbesserungen und bessere verteilte Arbeitsabläufe, werden auch in dieser Version fortgesetzt: PostgreSQL 14 bietet Verbesserungen bei vielen gleichzeitigen Verbindungen, für Anwendungsfälle mit hoher Schreiblast, paralleler Abfrageverarbeitung und logischer Replikation.

“Diese Version von PostgreSQL erweitert die Fähigkeit unserer Benutzer, Daten auch dann verwalten zu können, wenn die Bestände riesig werden. Die Überwachungsmöglichkeiten von PostgreSQL wurden erweitert und PostgreSQL enthält nun neue Optionen, die Applikations-Entwicklern helfen”, sagt Magnus Hagander, ein Mitglied des PostgreSQL Core Teams. "PostgreSQL 14 beweist einmal wieder, dass die globale PostgreSQL Community es durch Engagement schafft, Rückmeldungen aufzunehmen und zu adressieren, und dabei auch noch innovative Datenbanksoftware erstellt, die sowohl in kleinen, wie auch in großen Unternehmen zum Einsatz kommt."

[PostgreSQL](https://www.postgresql.org), ein innovatives Management System für Daten, bekannt für seine Robustheit und Zuverlässigkeit, profitiert von über 25 Jahren Open Source Entwicklung und einer [globalen Entwicklergemeinschaft](https://www.postgresql.org/community/), und hat sich zur bevorzugten Open Source Datenbank für Unternehmen jeder Größe entwickelt.

### Komfort beim Umgang mit JSON-Daten und Multi-Ranges

PostgreSQL unterstützt schon seit Version 9.2 die Verarbeitung von Daten im [JSON](https://www.postgresql.org/docs/14/datatype-json.html)-Format, die Syntax zur Extraktion von Daten war aber sehr spezifisch. PostgreSQL 14 erlaubt den Zugriff nun per [Subscripts](https://www.postgresql.org/docs/14/datatype-json.html#JSONB-SUBSCRIPTING), was Abfragen wie z.B. `SELECT ('{ "postgres": { "release": 14 }}'::jsonb)['postgres']['release'];` ermöglicht. Diese Syntax entspricht der üblichen Methode zum Zugriff auf JSON-Daten. Die zugrunde liegende Technik (“subscripting”), die in PostgreSQL 14 hierfür hinzugefügt wurde, kann auch auf andere verschachtelte Datenstrukturen angewendet werden. So erhielt mit diesem Release auch [`hstore`](https://www.postgresql.org/docs/14/hstore.html) die entsprechende Funktionalität.

[Range-Typen](https://www.postgresql.org/docs/14/rangetypes.html), die ebenfalls seit 9.2 erstmals verfügbar waren, unterstützen nun mit "[Multi-Range](https://www.postgresql.org/docs/14/rangetypes.html#RANGETYPES-BUILTIN)" Typen nicht zusammenhängende Bereiche, also nicht überlappende Reihen von Werten (z.B. 08:00-12:00 Uhr, 15:00-18:30 Uhr). Die eingebauten Range-Typen (Datum, Zeit und Zahlen) sind bereits um Multi-Range-Fähigkeiten erweitert worden, es kann aber generell jeder Range-fähige Datentyp auch für Multi-Range fähig gemacht werden.

### Performance-Verbesserungen für ressourcenintensive Lasten

Der Durchsatz von Systemen mit vielen gleichzeitigen Verbindungen wird mit PostgreSQL 14 signifikant erhöht. Einige Benchmarks zeigen eine Beschleunigung um den Faktor 2. Zudem wurde die Entstehung von Bloat auf [häufig aktualisierten Indexen reduziert](https://www.postgresql.org/docs/14/btree-implementation.html#BTREE-DELETION).

PostgreSQL 14 erlaubt es, [mehrere asynchrone Anfragen](https://www.postgresql.org/docs/14/libpq-pipeline-mode.html) an die Datenbank zu senden, was vor allem bei höheren Latenzen zwischen Applikations- und Datenbankserver oder Arbeitslasten mit sehr vielen kleinen schreibenden Operationen (`INSERT`/`UPDATE`/`DELETE`) den Durchsatz deutlich verbessern kann. Diese Technik wird auf Client-Seite implementiert und ist bei Einsatz eines PostgreSQL 14 Clients oder der entsprechenden Version 14 der [libpq](https://wiki.postgresql.org/wiki/List_of_drivers) mit jeder aktuellen PostgreSQL-Server-Version nutzbar.

### Verbesserungen für verteilte Datenbanken

Verteilte PostgreSQL-Datenbanken profitieren besonders von Version 14. Beim Einsatz von [logischer Replikation](https://www.postgresql.org/docs/current/logical-replication.html) kann PostgreSQL jetzt Daten von noch laufenden Transaktionen an Subscriber senden, was das Nachspielen dieser Transaktionen auf den Subscribern deutlich beschleunigt. PostgreSQL 14 enthält weitere Optimierungen des “logical decoding”-Systems, auf dem die logische Replikation basiert.

[Foreign Data Wrappers](https://www.postgresql.org/docs/14/sql-createforeigndatawrapper.html), die für die Anbindung von externen Datenquellen, seien es relationale Datenbanken (wie PostgreSQL) oder sonstige Fremdsysteme, verwendet werden, können nun Parallelisierung nutzen. PostgreSQL 14 implementiert diese Fähigkeit für [`postgres_fdw`](https://www.postgresql.org/docs/14/postgres-fdw.html), den Foreign Data Wrapper, der mit anderen PostgreSQL-Datenbanken interagiert.

Zusätzlich zur Parallelverarbeitung hat `postgres_fdw` die Fähigkeit hinzugewonnen, foreign-Tabellen mit großen Datenmengen auf einmal zu befüllen und kann mittels [`IMPORT FOREIGN SCHEMA`](https://www.postgresql.org/docs/14/sql-importforeignschema.html) auch partitionierte Tabellen importieren.

### Administration und Fortschrittsanalyse

Die Performance-Verbesserungen von PostgreSQL 14 erstrecken sich auch auf das [VACUUM-System](https://www.postgresql.org/docs/14/routine-vacuuming.html), indem z.B. der Verwaltungsaufwand für B-Trees reduziert wurde. Ebenso wurde ein VACUUM "Notfallmodus" implementiert, der effektiv Problemen beim “Transaction ID Wraparound” vorgreift. Der Befehl [`ANALYZE`](https://www.postgresql.org/docs/14/sql-analyze.html), welcher Statistiken über die Datenbank sammelt, wurde signifikant beschleunigt.

Die transparente Kompression des PostgreSQL [TOAST-Systems](https://www.postgresql.org/docs/14/storage-toast.html), mit dem größere Daten wie Text oder auch geometrische Daten gespeichert werden, [ist jetzt konfigurierbar](https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-DEFAULT-TOAST-COMPRESSION). PostgreSQL 14 führt LZ4-Kompression als Option für TOAST-Spalten ein, während `pglz`-Kompression weiterhin unterstützt wird.

PostgreSQL 14 hat einige neue Features, die bei der Überwachung und bei der Fortschrittsanalyse helfen, um zum Beispiel [den Fortschritt von `COPY`-Kommandos](https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING), [write-ahead-log (WAL) Aktivität](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW) und [Statistiken von Replikations-Slots](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW) zu verfolgen. Wird [`compute_query_id`](https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID) aktiviert, lassen sich Datenbankabfragen individuell im System verfolgen, zum Beispiel in [`pg_stat_activity`](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
[`EXPLAIN VERBOSE`](https://www.postgresql.org/docs/14/sql-explain.html) und einigen anderen Bereichen.

### SQL Performance, Konformität, und Komfort

Das Planen und Ausführen von Abfragen erfährt ebenfalls Verbesserungen mit PostgreSQL 14. Diese Version verbessert die parallelisierte Abfrageverarbeitung, indem beispielsweise parallele sequenzielle Scans beschleunigt werden. [`PL/pgSQL`](https://www.postgresql.org/docs/14/plpgsql.html) erlaubt nun die Nutzung paralleler Abfragen wenn der `RETURN QUERY` Befehl genutzt wird. Außerdem kann nun [`REFRESH MATERIALIZED VIEW`](https://www.postgresql.org/docs/14/sql-refreshmaterializedview.html) parallele Abfragen durchführen. Nested-Loops können in PostgreSQL 14 von einem Caching der Tupel des inneren Knotens profitieren.

[Erweiterte Statistiken](https://www.postgresql.org/docs/14/planner-stats.html#PLANNER-STATS-EXTENDED) können jetzt in PostgreSQL 14 für Ausdrücke verwendet werden. [Window Functions](https://www.postgresql.org/docs/14/functions-window.html) können nun von inkrementellen Sortierungen, einer in [PostgreSQL 13](https://www.postgresql.org/about/news/postgresql-13-released-2077/) neu implementierten Funktionalität, profitieren.

[Stored procedures](https://www.postgresql.org/docs/14/sql-createprocedure.html), die eine Transaktionssteuerung in einem Codeblock ermöglichen, können jetzt Daten mit `OUT`-Parametern zurückgeben.

PostgreSQL 14 führt die Möglichkeit ein, Zeitstempel mittels der [`date_bin`](https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-BIN) Funktion an einem bestimmten Intervall auszurichten. Diese Version fügt auch die SQL-konforme [`SEARCH`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-SEARCH) und [`CYCLE`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-CYCLE) Klauseln zur Unterstützung bei der Reihenfolge- und Zykluserkennung für rekursive [Common Table Expressions](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-RECURSIVE) ein.

### Verbesserungen bei der Sicherheit

PostgreSQL 14 ermöglicht es, komfortabel lesende und schreibende Berechtigungen an Benutzer von Tabellen, Views und Schemas mit `pg_read_all_data` und `pg_write_all_data` [vordefinierte Rollen](https://www.postgresql.org/docs/14/predefined-roles.html) zuzuweisen.

Darüber hinaus setzt diese Version jetzt den Passwortstandard [`SCRAM-SHA-256`](https://www.postgresql.org/docs/14/auth-password.html) für das Verwaltungs- und Authentifizierungssystem als Voreinstellung für neue PostgreSQL-Instanzen.

## Über PostgreSQL

[PostgreSQL](https://www.postgresql.org) ist das führende Open-Source Datenbanksystem, mit einer weltweiten Community bestehend aus Tausenden von Nutzern und Mitwirkenden sowie Dutzenden von Firmen und Organisationen. Das PostgreSQL Projekt baut auf über 30 Jahre Erfahrung auf, beginnend an der University of California, Berkeley, und hat heute eine nicht zu vergleichende Performance bei der Entwicklung. PostgreSQL's ausgereiftes Feature Set ist nicht nur mit den führenden proprietären Datenbanksystemen vergleichbar, sondern übertrifft diese in erweiterten Datenbankfunktionen, Erweiterbarkeit, Sicherheit und Stabilität.

### Links

* [Download](https://www.postgresql.org/download/)
* [Versionshinweise](https://www.postgresql.org/docs/14/release-14.html)
* [Pressemitteilung](https://www.postgresql.org/about/press/)
* [Sicherheit](https://www.postgresql.org/support/security/)
* [Versionierungsrichtlinie](https://www.postgresql.org/support/versioning/)
* [Folge @postgresql auf Twitter](https://twitter.com/postgresql)
