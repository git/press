The PostgreSQL Global Development Group today announced the release of
[PostgreSQL 14](https://www.postgresql.org/docs/14/release-14.html), the latest
version of the world’s [most advanced open source database](https://www.postgresql.org/).

PostgreSQL 14 brings a variety of features that help developers and
administrators deploy their data-backed applications. PostgreSQL continues to
add innovations on complex data types, including more convenient access for
JSON and support for noncontiguous ranges of data. This latest release adds
to PostgreSQL's trend on improving high performance and distributed
data workloads, with advances in connection concurrency, high-write
workloads, query parallelism and logical replication.

"This latest release of PostgreSQL advances our users' ability to manage data
workloads at scale, enhances observability, and contains new features that help
application developers," said Magnus Hagander, a PostgreSQL Core Team member.
"PostgreSQL 14 is a testament to the dedication of the global PostgreSQL
community in addressing feedback and continuing to deliver innovative database
software that is deployed by organizations large and small."

[PostgreSQL](https://www.postgresql.org), an innovative data management system
known for its reliability and robustness, benefits from over 25 years of open
source development from a [global developer community](https://www.postgresql.org/community/)
and has become the preferred open source relational database for organizations
of all sizes.

### JSON Conveniences and Multiranges

PostgreSQL has supported manipulating [JSON](https://www.postgresql.org/docs/14/datatype-json.html)
data since the release of PostgreSQL 9.2, though retrieval of values used a
unique syntax. PostgreSQL 14 now lets you [access JSON data using subscripts](https://www.postgresql.org/docs/14/datatype-json.html#JSONB-SUBSCRIPTING), e.g. a query like `SELECT ('{ "postgres": { "release": 14 }}'::jsonb)['postgres']['release'];`
now works. This aligns PostgreSQL with syntax that is commonly recognized for
retrieving information from JSON data. The subscripting framework added to
PostgreSQL 14 can be generally extended to other nested data structures, and is
also applied to the [`hstore`](https://www.postgresql.org/docs/14/hstore.html)
data type in this release.

[Range types](https://www.postgresql.org/docs/14/rangetypes.html), also first
released in PostgreSQL 9.2, now have support for noncontiguous ranges through
the introduction of the "[multirange](https://www.postgresql.org/docs/14/rangetypes.html#RANGETYPES-BUILTIN)"
data type. A multirange is an ordered list of ranges that are nonoverlapping,
which lets developers write simpler queries for dealing with complex sequences
of ranges. The range types native to PostgreSQL (dates, times, numbers) support
multiranges, and other data types can be extended to use multirange support.

### Performance Improvements for Heavy Workloads

PostgreSQL 14 provides a significant throughput boost on workloads that use many
connections, with some benchmarks showing a 2x speedup. This release continues
on the recent improvements to the management of B-tree indexes by reducing index
bloat on tables with [frequently updated indexes](https://www.postgresql.org/docs/14/btree-implementation.html#BTREE-DELETION).

PostgreSQL 14 introduces the ability to [pipeline queries](https://www.postgresql.org/docs/14/libpq-pipeline-mode.html)
to a database, which can significantly improve performance over high latency
connections or for workloads with many small write (`INSERT`/`UPDATE`/`DELETE`)
operations. As this is a client-side feature, you can use pipeline mode with any
modern PostgreSQL database with the version 14 client
or [a client driver built with version 14 of libpq](https://wiki.postgresql.org/wiki/List_of_drivers).

### Enhancements for Distributed Workloads

Distributed PostgreSQL databases stand to benefit from PostgreSQL 14. When using
[logical replication](https://www.postgresql.org/docs/current/logical-replication.html),
PostgreSQL can now stream in-progress transactions to subscribers, with
significant performance benefits for applying large transactions on subscribers.
PostgreSQL 14 also adds several other performance enhancements to the logical
decoding system that powers logical replication.

[Foreign data wrappers](https://www.postgresql.org/docs/14/sql-createforeigndatawrapper.html),
which are used for working with federated workloads across PostgreSQL and other
databases, can now leverage query parallelism in PostgreSQL 14. This release
implements this ability in the [`postgres_fdw`](https://www.postgresql.org/docs/14/postgres-fdw.html),
the foreign data wrapper that interfaces with other PostgreSQL databases.

In addition to supporting query parallelism, `postgres_fdw` can now bulk insert
data on foreign tables and import table partitions with the
[`IMPORT FOREIGN SCHEMA`](https://www.postgresql.org/docs/14/sql-importforeignschema.html)
directive.

### Administration and Observability

PostgreSQL 14 extends its performance gains to the [vacuuming](https://www.postgresql.org/docs/14/routine-vacuuming.html)
system, including optimizations for reducing overhead from B-Trees. This release
also adds a vacuum "emergency mode" that is designed to prevent transaction ID
wraparound. [`ANALYZE`](https://www.postgresql.org/docs/14/sql-analyze.html),
used to collect database statistics, now runs significantly faster in
PostgreSQL 14 due to its own performance improvements.

Compression for PostgreSQL's [TOAST](https://www.postgresql.org/docs/14/storage-toast.html)
system, which is used to store larger data like blocks of text or geometries,
can [now be configured](https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-DEFAULT-TOAST-COMPRESSION).
PostgreSQL 14 adds LZ4 compression for TOAST columns while retaining support for
`pglz` compression.

PostgreSQL 14 adds several new features to help with monitoring and
observability, including the ability to [track the progress of `COPY` commands](https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING),
[write-ahead-log (WAL) activity](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW),
and [statistics on replication slots](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW).
Enabling [`compute_query_id`](https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID)
lets you uniquely track a query through several PostgreSQL features, including
[`pg_stat_activity`](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
[`EXPLAIN VERBOSE`](https://www.postgresql.org/docs/14/sql-explain.html), and
more.

### SQL Performance, Conformance, and Convenience

Query planning and execution benefit from enhancements in PostgreSQL 14. This
release includes several improvements to PostgreSQL's query parallelism support,
including better performance of parallel sequential scans, the ability for
[`PL/pgSQL`](https://www.postgresql.org/docs/14/plpgsql.html) to execute
parallel queries when using the `RETURN QUERY` command, and enabling
[`REFRESH MATERIALIZED VIEW`](https://www.postgresql.org/docs/14/sql-refreshmaterializedview.html)
to execute parallel queries. Additionally, queries that use nested loop joins
may see performance benefits through additional caching that is added in
PostgreSQL 14.

[Extended statistics](https://www.postgresql.org/docs/14/planner-stats.html#PLANNER-STATS-EXTENDED)
can now be used in PostgreSQL 14 for expressions. Additionally,
[window functions](https://www.postgresql.org/docs/14/functions-window.html) can
now benefit from incremental sorts, a feature introduced in
[PostgreSQL 13](https://www.postgresql.org/about/news/postgresql-13-released-2077/).

[Stored procedures](https://www.postgresql.org/docs/14/sql-createprocedure.html),
which allow for transaction control in a block of code, can now return data by
using `OUT` parameters.

PostgreSQL 14 introduces the ability to "bin", or align, timestamps to a
particular interval using the [`date_bin`](https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-BIN)
function. This release also adds the SQL conforming
[`SEARCH`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-SEARCH)
and [`CYCLE`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-CYCLE)
clauses to help with ordering and cycle detection for recursive
[common table expressions](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-RECURSIVE).

### Security Enhancements

PostgreSQL 14 makes it convenient to assign read-only and write-only privileges
to users on tables, views, and schemas using the `pg_read_all_data` and
`pg_write_all_data` [predefined roles](https://www.postgresql.org/docs/14/predefined-roles.html).

Additionally, this release now makes the standard compliant
[`SCRAM-SHA-256`](https://www.postgresql.org/docs/14/auth-password.html) password
management and authentication system the default on new PostgreSQL instances.

### About PostgreSQL

[PostgreSQL](https://www.postgresql.org) is the world's most advanced open
source database, with a global community of thousands of users, contributors,
companies and organizations. Built on over 30 years of engineering, starting at
the University of California, Berkeley, PostgreSQL has continued with an
unmatched pace of development. PostgreSQL's mature feature set not only matches
top proprietary database systems, but exceeds them in advanced database
features, extensibility, security, and stability.

### Links

* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/14/release-14.html)
* [Press Kit](https://www.postgresql.org/about/press/)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
