The PostgreSQL Global Development Group announces that the first beta release of
PostgreSQL 14 is now [available for download](https://www.postgresql.org/download/).
This release contains previews of all features that will be available in the
final release of PostgreSQL 14, though some details of the release could change
before then.

You can find information about all of the features and changes found in
PostgreSQL 14 in the [release notes](https://www.postgresql.org/docs/14/release-14.html):

  [https://www.postgresql.org/docs/14/release-14.html](https://www.postgresql.org/docs/14/release-14.html)

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 14 in your systems to help us eliminate
bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 14 Beta 1 in your production environments, we encourage you to find
ways to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 14
release upholds our standards of delivering a stable, reliable release of the
world's most advanced open source relational database. Please read more about
our [beta testing process](https://www.postgresql.org/developer/beta/) and how
you can contribute:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

PostgreSQL 14 Feature Highlights
--------------------------------

### Performance

PostgreSQL 14 continues the trend of recent releases in providing performance
benefits to workloads of all sizes.

This release has significant improvements in transaction throughput for
PostgreSQL systems that have large numbers of connections to the database,
regardless if they are in an active or idle state.

PostgreSQL 14 also contains additional gains in reducing B-tree indexing
overhead, including [reducing the bloat on tables with frequently updated indexes](https://www.postgresql.org/docs/14/btree-implementation.html#BTREE-DELETION).
[GiST indexes](https://www.postgresql.org/docs/14/indexes-types.html#INDEXES-TYPE-GIST)
can now presort data during their build process, allowing both for faster
index creation and smaller indexes.
[SP-GiST indexes](https://www.postgresql.org/docs/14/indexes-types.html#INDEXES-TYPE-SPGIST)
now support [covering indexes](https://www.postgresql.org/docs/14/indexes-index-only-scans.html)
that allow users to add additional nonsearchable columns to the index through
the `INCLUDE` clause.

There are many improvements to query parallelism in PostgreSQL 14. In addition
to overall performance improvements for [parallel sequential scans](https://www.postgresql.org/docs/14/parallel-plans.html#PARALLEL-SCANS),
the `RETURN QUERY` directive in [PL/pgSQL](https://www.postgresql.org/docs/14/plpgsql.html)
can now execute queries with parallelism.
[`REFRESH MATERIALIZED VIEW`](https://www.postgresql.org/docs/14/sql-refreshmaterializedview.html)
can now use query parallelism as well.

PostgreSQL 14 also introduces the ability to leverage query parallelism when
querying remote databases using [foreign data wrappers](https://www.postgresql.org/docs/14/sql-createforeigndatawrapper.html).
The PostgreSQL foreign data wrapper, [`postgres_fdw`](https://www.postgresql.org/docs/14/postgres-fdw.html),
added support for this in PostgreSQL 14 when the `async_capable` flag is set.
`postgres_fdw` also supports bulk inserts and can import table partitions using
[`IMPORT FOREIGN SCHEMA`](https://www.postgresql.org/docs/14/sql-importforeignschema.html)
and can now execute [`TRUNCATE`](https://www.postgresql.org/docs/14/sql-truncate.html)
on foreign tables.

This release also has several improvements to the partitioning system, including
performance gains when updating or deleting rows on tables where only a few
partitions are affected. In PostgreSQL 14, partitions can now be detached in a
non-blocking manner using the [`ALTER TABLE ... DETACH PARTITION ... CONCURRENTLY`](https://www.postgresql.org/docs/14/sql-altertable.html#SQL-ALTERTABLE-DETACH-PARTITION)
command.

Incremental sorting, introduced in the previous release, can now be used by
[window functions](https://www.postgresql.org/docs/14/functions-window.html) in
PostgreSQL 14. This new release adds more capabilities to [extended statistics](https://www.postgresql.org/docs/14/planner-stats.html),
which can now be applied to [expressions](https://www.postgresql.org/docs/14/view-pg-stats-ext-exprs.html).

PostgreSQL has supported compression on its "oversized data" columns (i.e. the
[TOAST](https://www.postgresql.org/docs/14/storage-toast.html) system) for
decades, but this release adds the ability to now choose to use
[LZ4 compression](https://www.postgresql.org/docs/14/sql-createtable.html) for
column compression.

### Data Types + SQL

Building upon its existing support for [range types](https://www.postgresql.org/docs/14/rangetypes.html),
PostgreSQL 14 adds the new [multirange type](https://www.postgresql.org/docs/14/rangetypes.html#RANGETYPES-BUILTIN)
that lets you specify an ordered list of noncontiguous ranges, e.g.:

```
SELECT datemultirange(
  daterange('2021-07-01', '2021-07-31'),
  daterange('2021-09-01', '2021-09-30'),
  daterange('2021-11-01', '2021-11-30'),
  daterange('2022-01-01', '2022-01-31'),
  daterange('2022-03-01', '2022-04-07')
);
```

The new multirange type supports the existing operations available to range
types.

PostgreSQL 14 now adds a general subscripting framework for retrieving
information in nested objects. For example, you can now retrieve nested info
in the `JSONB` data type using subscript syntax, e.g.:

```
SELECT ('{ "this": { "now": { "works": "in postgres 14!" }}}'::jsonb)['this']['now']['works'];
```

PostgreSQL 14 also adds support for `OUT` parameters in [stored procedures](https://www.postgresql.org/docs/14/sql-createprocedure.html),
and allows for the `GROUP BY` clause to use the `DISTINCT` keyword to remove duplicate
`GROUPING SET` combinations.

For recursive [common table expressions](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-RECURSIVE)
(`WITH` queries), PostgreSQL 14 adds the syntactic conveniences of [`SEARCH`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-SEARCH)
and [`CYCLE`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-CYCLE)
to help with ordering and cycle detection respectively.

There is also the new [`date_bin`](https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-BIN)
function in PostgreSQL 14 that can align timestamps with specified intervals, a
technique known as "binning."

### Administration

PostgreSQL 14 makes numerous improvements to [`VACUUM`](https://www.postgresql.org/docs/14/routine-vacuuming.html),
with optimizations geared towards indexes. Autovacuum now analyzes partitioned
tables and can propagate information about row counts to parent tables. There
are also performance gains in [`ANALYZE`](https://www.postgresql.org/docs/14/sql-analyze.html)
that can be controlled with [`maintenance_io_concurrency`](https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-MAINTENANCE-IO-CONCURRENCY)
parameter.

PostgreSQL 14 includes numerous improvements on what information can be
monitored, including the ability to track progress on `COPY` using the
[`pg_stat_progress_copy`](https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING)
view. This release lets you track WAL activity from the [`pg_stat_wal`](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW)
view, and inspect replication slot statistics from the [`pg_stat_replication_slots`](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW)
view.

There are several new parameters in PostgreSQL 14 to help manage connections.
These include [`idle_session_timeout`](https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-IDLE-SESSION-TIMEOUT),
which can close idle connections after the specified period, and
[`client_connection_check_interval`](https://www.postgresql.org/docs/14/runtime-config-connection.html#GUC-CLIENT-CONNECTION-CHECK-INTERVAL)
parameter that lets PostgreSQL cancel long running queries if a client
disconnects.

The [`REINDEX`](https://www.postgresql.org/docs/14/sql-reindex.html) command can
now process all of the child indexes of a partitioned table, and PostgreSQL 14
adds the [`pg_amcheck`](https://www.postgresql.org/docs/14/app-pgamcheck.html)
utility to help check for data corruption.

### Replication & Recovery

PostgreSQL 14 adds numerous performance benefits for logical replication,
including the ability to stream in-progress transactions to subscribers instead
of waiting for them to be completed. The [`ALTER SUBSCRIPTION`](https://www.postgresql.org/docs/14/sql-altersubscription.html)
makes it easier to add/remove publications using the new `ADD/DROP PUBLICATION`]
syntax.

There are performance improvements in PostgreSQL 14 to how PostgreSQL starts up
when in crash recovery, and you can now use
[`pg_rewind`](https://www.postgresql.org/docs/14/app-pgrewind.html) on a
PostgreSQL instance that is in standby mode.

### Security

PostgreSQL 14 adds the ability to give users universal "read only" and "write only"
privileges on tables/views/sequences through the use of the `pg_read_all_data`
and `pg_write_all_data` [predefined roles](https://www.postgresql.org/docs/14/predefined-roles.html),
respectively.

This release also defaults to using [SCRAM-SHA-256](https://www.postgresql.org/docs/14/auth-password.html)
for password management on new PostgreSQL instances. Additionally, the
`clientcert` parameter in the `pg_hba.conf` must now use either the values of
`verify-ca` or `verify-full` instead of the legacy values.

PostgreSQL 14 can use a certificate's "distinguished name" (DN) for
[certificated-based authentication](https://www.postgresql.org/docs/14/auth-cert.html)
with `clientname=DN` parameter in the `pg_hba.conf` file.

Additional Features
-------------------

Many other new features and improvements have been added to PostgreSQL 14, some
of which may be as or more important to your use case than what is mentioned
above. Please see the [release notes](https://www.postgresql.org/docs/14/release-14.html)
for a complete list of new and changed features:

  [https://www.postgresql.org/docs/14/release-14.html](https://www.postgresql.org/docs/14/release-14.html)

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 14. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_14_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the first beta release of version 14. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2021. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 14 Beta Release Notes](https://www.postgresql.org/docs/14/release-14.html)
* [PostgreSQL 14 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_14_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
