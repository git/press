El Grupo Global de Desarrollo de PostgreSQL ha anunciado hoy el lanzamiento de
[PostgreSQL 14](https://www.postgresql.org/docs/14/release-14.html), la última versión de la [base de datos de código abierto más avanzada del mundo](https://www.postgresql.org/).

PostgreSQL 14 introduce una variedad de características que ayudarán a desarrolladores y administradores a implementar sus aplicaciones para el manejo de datos. PostgreSQL sigue añadiendo innovaciones para tipos de datos complejos, que incluyen mayor facilidad de acceso a datos JSON y soporte para rangos de datos no contiguos. Esta última versión refuerza la tendencia de PostgreSQL hacia las mejoras para el alto rendimiento y las cargas de trabajo de datos distribuidos, presentando avances en la concurrencia de conexiones, cargas de trabajo con elevado nivel de escritura, paralelismo de consultas y replicación lógica.

"Esta última versión de PostgreSQL incrementa la capacidad de nuestros usuarios de administrar cargas de trabajo de datos a gran escala, mejora la observabilidad e incluye nuevas características que facilitan el trabajo de los desarrolladores de aplicaciones", dijo Magnus Hagander, miembro del Core Team de PostgreSQL. "PostgreSQL 14 constituye la prueba del compromiso de la comunidad global de PostgreSQL para analizar sugerencias y opiniones recibidas y continuar ofreciendo un software de base de datos innovador utilizado por organizaciones grandes y pequeñas."

[PostgreSQL](https://www.postgresql.org) es un innovador sistema de gestión de datos conocido por su fiabilidad y solidez. Gracias a los más de 25 años de desarrollo de código abierto realizado por una [comunidad mundial de desarrolladores](https://www.postgresql.org/community/), se ha convertido en la base de datos relacional de código abierto preferida por organizaciones de todos los tamaños.

### Utilidades para JSON y rangos múltiples

PostgreSQL ha ofrecido soporte para la manipulación de datos [JSON](https://www.postgresql.org/docs/14/datatype-json.html)
desde el lanzamiento de su versión 9.2, aunque hasta ahora para obtener los valores se utilizaba una sintaxis única. Con PostgreSQL 14, en cambio, es posible [acceder a datos JSON usando subíndices](https://www.postgresql.org/docs/14/datatype-json.html#JSONB-SUBSCRIPTING). Por ejemplo, ahora es posible realizar una consulta como `SELECT ('{ "postgres": { "release": 14 }}'::jsonb)['postgres']['release'];`
. Esto alinea a PostgreSQL con la sintaxis comúnmente reconocida para obtener información de datos JSON. El sistema de subíndices añadido a PostgreSQL 14 es generalmente extensible a otras estructuras de datos anidadas, y se aplica también al tipo de dato [`hstore`](https://www.postgresql.org/docs/14/hstore.html)
presente en esta versión.

Los [tipos de rangos](https://www.postgresql.org/docs/14/rangetypes.html) (que también fueron introducidos en PostgreSQL 9.2) ahora cuentan con el soporte para rangos no contiguos a través de la introducción del tipo de datos "[multirango](https://www.postgresql.org/docs/14/rangetypes.html#RANGETYPES-BUILTIN)".
Un rango múltiple consiste en una lista ordenada de rangos que no se superponen, lo cual permite a los desarrolladores escribir consultas más sencillas para manejar secuencias complejas de rangos. Los tipos de rango nativos de PostgreSQL (fechas, horas, números) soportan los rangos múltiples. Asimismo, es posible extender el soporte de rangos múltiples a otros tipos de datos.

### Mejoras de rendimiento para cargas de trabajo intensivas

PostgreSQL 14 ofrece un importante incremento de rendimiento en las cargas de trabajo con un alto número de conexiones. Algunas pruebas de rendimiento indican un aumento de velocidad equivalente al doble. Esta versión continúa con las recientes mejoras en la gestión de los índices B-tree, al reducir el sobredimensionamiento de los índices en aquellas tablas cuyos [índices se actualizan con frecuencia](https://www.postgresql.org/docs/14/btree-implementation.html#BTREE-DELETION).

PostgreSQL 14 introduce la posibilidad de [canalizar consultas](https://www.postgresql.org/docs/14/libpq-pipeline-mode.html)
hacia una base de datos, lo cual puede mejorar significativamente el rendimiento en las conexiones de alta latencia o para cargas de trabajo con un gran número de pequeñas operaciones de escritura (`INSERT`/`UPDATE`/`DELETE`). Al tratarse de una característica del lado cliente, es posible utilizar el modo pipeline con cualquier base de datos PostgreSQL actual que cuente con la versión 14 del cliente o con un [controlador de cliente creado con la versión 14 de libpq](https://wiki.postgresql.org/wiki/List_of_drivers).

### Mejoras para cargas de trabajo distribuidas

También las bases de datos PostgreSQL distribuidas se benefician de PostgreSQL 14. Al utilizar la [replicación lógica](https://www.postgresql.org/docs/current/logical-replication.html),
PostgreSQL puede ahora enviar las transacciones en curso a los suscriptores, con importantes ventajas de rendimiento cuando se aplican a los mismos transacciones de gran volumen. Además, PostgreSQL 14 añade otras mejoras de rendimiento al sistema de decodificación lógica, en el que se basa la replicación lógica.

Los [conectores de datos externos](https://www.postgresql.org/docs/14/sql-createforeigndatawrapper.html), empleados para manejar cargas de trabajo federadas entre PostgreSQL y otras bases de datos, ahora pueden aprovechar el paralelismo de consultas que ofrece PostgreSQL 14. En esta versión se implementa dicha funcionalidad a través de [`postgres_fdw`](https://www.postgresql.org/docs/14/postgres-fdw.html),
el conector de datos externos que interactúa con otras bases de datos PostgreSQL.

Además de ofrecer soporte para el paralelismo de consultas, `postgres_fdw` permite ahora la inserción masiva de datos en tablas foráneas y la importación de particiones de tablas a través de la directiva [`IMPORT FOREIGN SCHEMA`](https://www.postgresql.org/docs/14/sql-importforeignschema.html).

### Administración y observabilidad

PostgreSQL 14 extiende sus incrementos de rendimiento al sistema de [vacuum](https://www.postgresql.org/docs/14/routine-vacuuming.html), incluyendo optimizaciones que reducen la sobrecarga de los B-Trees. Esta versión introduce también en vacuum una "modalidad de emergencia" diseñada para prevenir el wraparound del ID de transacción. Gracias a las mejoras de rendimiento realizadas en PostgreSQL 14,  [`ANALYZE`](https://www.postgresql.org/docs/14/sql-analyze.html) (utilizado para recopilar estadísticas de la base de datos) ahora se ejecuta con mucha más rapidez.

Ahora es [posible configurar](https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-DEFAULT-TOAST-COMPRESSION)
la compresión para el sistema [TOAST](https://www.postgresql.org/docs/14/storage-toast.html) de PostgreSQL, utilizado para almacenar datos de mayor tamaño como bloques de texto o geometrías. PostgreSQL 14 añade la compresión LZ4 para las columnas TOAST, conservando al mismo tiempo el soporte para la compresión `pglz`.

PostgreSQL 14 incorpora varias características nuevas que facilitan el monitoreo y la observabilidad, incluyendo la posibilidad de [dar seguimiento al progreso de los comandos `COPY`](https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING),
[la actividad del WAL (write-ahead-log)](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW),
y [las estadísticas de los slots de replicación](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW).
Al habilitar [`compute_query_id`](https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID)
es posible realizar el seguimiento único de una consulta a través de varias características de PostgreSQL, que incluyen
[`pg_stat_activity`](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
[`EXPLAIN VERBOSE`](https://www.postgresql.org/docs/14/sql-explain.html), y más.

### Rendimiento, conformidad y utilidad para SQL

La planificación y ejecución de consultas se benefician de las mejoras introducidas en PostgreSQL 14. Esta versión incluye varias mejoras en el soporte al paralelismo de consultas de PostgreSQL. Entre ellas, un mejor desempeño de los escaneos secuenciales paralelos, la capacidad de
[`PL/pgSQL`](https://www.postgresql.org/docs/14/plpgsql.html) de realizar consultas paralelas utilizando el comando `RETURN QUERY`, y la posibilidad de permitirle a
[`REFRESH MATERIALIZED VIEW`](https://www.postgresql.org/docs/14/sql-refreshmaterializedview.html)
ejecutar consultas paralelas. Adicionalmente, las consultas que usan *nested loop joins* pueden obtener beneficios de rendimiento a través del caché adicional que ha sido añadido en PostgreSQL 14.

Las [estadísticas extendidas](https://www.postgresql.org/docs/14/planner-stats.html#PLANNER-STATS-EXTENDED)
pueden ahora ser utilizadas en PostgreSQL 14 para las expresiones. Asimismo, las [funciones de ventana deslizante](https://www.postgresql.org/docs/14/functions-window.html) pueden beneficiarse del ordenamiento incremental, característica introducida en
[PostgreSQL 13](https://www.postgresql.org/about/news/postgresql-13-released-2077/).

Los [procedimientos almacenados](https://www.postgresql.org/docs/14/sql-createprocedure.html),
que permiten controlar las transacciones en un bloque de código, pueden ahora devolver datos utilizando parámetros `OUT`.

PostgreSQL 14 introduce la posibilidad de realizar un "bin", o alinear, los timestamps a un intervalo determinado utilizando la función [`date_bin`](https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-BIN)
. En esta versión se añaden también las cláusulas
[`SEARCH`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-SEARCH)
y [`CYCLE`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-CYCLE)
 (conformes al estándar SQL) que ayudan a ordenar y detectar la existencia de ciclos en las
[expresiones recursivas de tablas comunes](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-RECURSIVE).

### Mejoras en la seguridad

Gracias a los [roles predefinidos](https://www.postgresql.org/docs/14/predefined-roles.html) `pg_read_all_data` y
`pg_write_all_data`, en PostgreSQL 14 es más fácil asignar a los usuarios privilegios de sólo lectura y sólo escritura para tablas, vistas y esquemas.

Además, a partir de esta versión, el sistema de gestión de contraseñas y autenticación, conforme con el estándar [`SCRAM-SHA-256`](https://www.postgresql.org/docs/14/auth-password.html), será el predeterminado en todas las nuevas instancias de PostgreSQL.

### Información sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) es la base de datos de código abierto más avanzada del mundo, que cuenta con una comunidad global de miles de usuarios, colaboradores, empresas y organizaciones. Basada en más de 30 años de ingeniería, que comenzaron en la Universidad de Berkeley en California, PostgreSQL ha continuado con un ritmo de desarrollo inigualable. El maduro conjunto de características de PostgreSQL no sólo iguala a los principales sistemas de bases de datos propietarios, sino que los supera en términos de características avanzadas, extensibilidad, seguridad y estabilidad.


### Enlaces

* [Descargas](https://www.postgresql.org/download/)
* [Notas de la versión](https://www.postgresql.org/docs/14/release-14.html)
* [Kit de prensa](https://www.postgresql.org/about/press/)
* [Información de seguridad](https://www.postgresql.org/support/security/)
* [Directiva de versiones](https://www.postgresql.org/support/versioning/)
* [Sígannos en Twitter @postgresql](https://twitter.com/postgresql)
