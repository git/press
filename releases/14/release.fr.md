Le PostgreSQL Global Development Group a annoncé aujourd'hui la sortie
de [PostgreSQL 14](https://www.postgresql.org/docs/14/release-14.html),
la toute dernière version
de [la base de données open source de référence](https://www.postgresql.org/).

PostgreSQL 14 apporte de nouvelles fonctionnalités aux développeurs et
administrateurs pour déployer leurs applications adossées aux bases de
données. Des innovations sont apportées aux types de données
complexes, facilitant l'accès aux données JSON et le support des
plages de données non contiguës. Poursuivant la tendance, cette
dernière version améliore les trafics de charges à haute-performance
et distribuées, avec des avancées significatives dans le support des
connexions concurrentes, des trafics intensifs en écriture, le
parallélisme des requêtes et la réplication logique.

«&nbsp;Cette dernière version de PostgreSQL apporte à nos utilisateurs la
capacité de gérer des trafics de données à grande échelle, améliore
l'observabilité, et contient de nouvelles fonctionnalités pour les
développeurs d'applications », déclare Magnus Hagander, membre de la
Core Team de PostgreSQL. « PostgreSQL 14 témoigne de l'engagement de
la communauté globale de PostgreSQL à prendre en compte le retours des
utilisateurs tout en délivrant un logiciel de base de données
innovant, déployé dans tout type d'organisations, grandes ou petites.&nbsp;»

[PostgreSQL](https://www.postgresql.org), reconnu pour la fiabilité et
la robustesse de son système de gestion de données, bénéficiant d'un
développement open source par une
[communauté globale de développeurs](https://www.postgresql.org/community/)
depuis plus de 25 ans, est devenu le moteur de gestion de base de données relationnelles
préféré des entreprises de toutes tailles.

### JSON plus convivial et plages multiples

PostgreSQL supporte la manipulation de données
[JSON](https://www.postgresql.org/docs/14/datatype-json.html) depuis
la version PostgreSQL 9.2 ; il s'agissait toutefois d'une syntaxe
propre au moteur. PostgreSQL 14 permet désormais
[d'accéder aux données JSON en utilisant la notation subscript](https://www.postgresql.org/docs/14/datatype-json.html#JSONB-SUBSCRIPTING).
Ainsi, une requête de type
`SELECT ('{ "postgres": { "release": 14}}'::jsonb)['postgres']['release'];`
est maintenant parfaitement fonctionnelle. Cela permet à PostgreSQL
d'être aligné avec la syntaxe couramment utilisée pour récupérer des
données JSON. L'infrastructure de subscripting ajoutée à PostgreSQL 14
peut être généralisée à toutes les autres formes de données
structurées et est également appliquée au type de données
[`hstore`](https://www.postgresql.org/docs/14/hstore.html) à partir de
cette version.

Les [types « plage de données »](https://www.postgresql.org/docs/14/rangetypes.html), également
introduits dans la version PostgreSQL 9.2, supportent maintenant les
plages de données non continues par le biais de l'introduction du type
«[multirange](https://www.postgresql.org/docs/14/rangetypes.html#RANGETYPES-BUILTIN)».
Un multirange est une liste ordonnée de plages disjointes. Cela permet
aux développeurs d'écrire des requêtes plus simples pour traiter des
séquences complexes de plages. Les types natifs à PostgreSQL
supportant les plages (dates, heures, nombres) supportent maintenant
les plages multiples. D'autres types de données peuvent être étendus
pour utiliser ce support de plages multiples.

### Améliorations des performances sur les trafics intensifs

PostgreSQL 14 accélère considérablement le débit des trafics reposant
sur de nombreuses connexions concurrentes ; les tests de performance
montrent un doublement de la vitesse de traitement. Cette version
poursuit également l'amélioration de la gestion des index B-tree en
réduisant la perte d'espace des
[index fréquemment mis à jour](https://www.postgresql.org/docs/14/btree-implementation.html#BTREE-DELETION).

PostgreSQL 14 introduit la possibilité d'effectuer des
[requêtes en rafale](https://www.postgresql.org/docs/14/libpq-pipeline-mode.html)
(pipeline mode) vers la base de données. Cette nouvelle fonctionnalité
permet d'améliorer la performance des connexions ayant une latence
élevée ou pour les trafics effectuant de nombreuses opérations
d'écriture de petite taille (`INSERT`/`UPDATE`/`DELETE`). Comme il
s'agit d'une amélioration côté client, le mode en rafale peut être
utilisé sur des versions plus anciennes de PostgreSQL, dès lors que le
client est en version 14.

### Amélioration sur les trafics distribués

PostgreSQL 14 apporte son lot d'améliorations aux bases de données
distribuées. Lors de l'utilisation de la
[réplication logique](https://www.postgresql.org/docs/current/logical-replication.html),
PostgreSQL peut maintenant transmettre les transactions en cours au
travers du flux de réplication aux souscripteurs. Cela permet une
amélioration sensible de la performance lors de l'application de
grosses transactions sur les souscripteurs. PostgreSQL 14 ajoute
également plusieurs autres améliorations de performance au système de
décodage logique, base de la réplication logique.

Les [Foreign data wrappers](https://www.postgresql.org/docs/14/sql-createforeigndatawrapper.html),
qui sont utilisés pour permettre les trafics fédérés entre PostgreSQL
et d'autres bases de données, peuvent maintenant utiliser le
parallélisme des requêtes avec PostgreSQL 14. Cette version implante
cette fonctionnalité pour le foreign data wrapper
[`postgres_fdw`](https://www.postgresql.org/docs/14/postgres-fdw.html)
dont le rôle est de se connecter à d'autres bases PostgreSQL.


En plus de supporter le parallélisme des requêtes, `postgres_fdw` peut
maintenant faire des insertions en masse dans une table étrangère et
importer des partitions de table avec la directive
[`IMPORT FOREIGN SCHEMA`](https://www.postgresql.org/docs/14/sql-importforeignschema.html).


### Administration et observabilité

PostgreSQL 14 ajoute un gain de performance au système de
[vacuuming](https://www.postgresql.org/docs/14/routine-vacuuming.html)
par l'introduction d'optimisations permettant de réduire la surcharge
liée au B-trees. Cette version inclut également un vacuum « d'urgence
» qui est conçu afin de prévenir le rebouclage des identifiants de
transaction. La commande
[`ANALYZE`](https://www.postgresql.org/docs/14/sql-analyze.html),
utilisée pour collecter des statistiques sur la base de données,
fonctionne plus rapidement sur PostgreSQL 14.

Le mode de compression du système de
[TOAST](https://www.postgresql.org/docs/14/storage-toast.html) de
PostgreSQL, permettant de stocker les données volumineuses comme des
blocs de texte ou des géométries,
[peut maintenant être configuré](https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-DEFAULT-TOAST-COMPRESSION).
PostgreSQL 14 introduit la compression LZ4 pour les colonnes TOAST
tout en maintenant le support de la compression `pglz`.


PostgreSQL 14 apporte de nombreuses fonctionnalités de surveillance et
d'observabilité dont la possibilité de
[suivre la progression des commandes `COPY`](https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING),
[l'activité du write-ahead-log (WAL)](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW),
et
[les statistiques des slots de réplication](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW).
L'activation de
[`compute_query_id`](https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID)
permet de suivre de façon unique une requête au sein de différentes
fonctionnalités de PostgreSQL comme
[`pg_stat_activity`](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
[`EXPLAIN VERBOSE`](https://www.postgresql.org/docs/14/sql-explain.html),
et différentes fonctions de journalisation.

### Performance du SQL, conformité et simplicité

La planification et l'exécution des requêtes bénéficient des
optimisations de PostgreSQL 14. Cette version inclut plusieurs
améliorations du parallélisme des requêtes de PostgreSQL. On peut
citer de meilleures performances des lectures séquentielles
parallèles, la possibilité pour
[`PL/pgSQL`](https://www.postgresql.org/docs/14/plpgsql.html)
d'exécuter des requêtes parallélisées lors de l'utilisation de la
commande `RETURN QUERY` et la possibilité pour
[`REFRESH MATERIALIZED VIEW`](https://www.postgresql.org/docs/14/sql-refreshmaterializedview.html)
d'exécuter des requêtes parallèlisées. De plus, les requêtes utilisant
des jointures à boucles imbriquées bénéficieront de meilleures
performances gràce au système de cache additionnel intégré à
PostgreSQL 14.

[Les statistiques
étendues](https://www.postgresql.org/docs/14/planner-stats.html#PLANNER-STATS-EXTENDED)
qde PostgreSQL 14 peuvent maintenant être utilisées pour les
expressions. En outre, les
[fonctions de fenêtrage](https://www.postgresql.org/docs/14/functions-window.html)
peuvent maintenant bénéficier de tris incrémentaux, une fonctionnalité
introduite par
[PostgreSQL 13](https://www.postgresql.org/about/news/postgresql-13-released-2077/).

[Les procédures stockées](https://www.postgresql.org/docs/14/sql-createprocedure.html),
qui permettent le contrôle des transactions dans un bloc de code,
peuvent maintenant retourner des données en utilisant des paramètres
`OUT`.

PostgreSQL 14 introduit la possibilité de regrouper, ou d'aligner, des
estampilles temporelles dans un intervalle particulier en utilisant la
fonction
[`date_bin`](https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-BIN).
Cette version apporte également le support des clauses conformes à la
norme SQL
[`SEARCH`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-SEARCH)
et
[`CYCLE`](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-CYCLE)
qui aident au tri et à la détection de cycles dans les
[expressions communes de tables (CTE)](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-RECURSIVE)
récursives.


### Amélioration de la sécurité

PostgreSQL 14 simplifie l'assignation des privilèges de lecture seule
ou écriture seule aux utilisateurs sur les tables, vues et schéma à
l'aide
[des rôles prédéfinis](https://www.postgresql.org/docs/14/predefined-roles.html)
`pg_read_all_data` et `pg_write_all_data`.

De plus, cette version définit par défaut le gestionnaire de mot de
passe et d'authentification
[`SCRAM-SHA-256`](https://www.postgresql.org/docs/14/auth-password.html)
sur les nouvelles instances PostgreSQL.

### À propos de PostgreSQL

[PostgreSQL](https://www.postgresql.org) est le système de gestion de bases de
données libre de référence. Sa communauté mondiale est composée de plusieurs
milliers d’utilisateurs, contributeurs, entreprises et institutions. Le projet
PostgreSQL, démarré il y a plus de 30 ans à l’université de Californie, à
Berkeley, a atteint aujourd’hui un rythme de développement sans pareil.
L’ensemble des fonctionnalités proposées est mature, et dépasse même celui des
systèmes commerciaux leaders sur les fonctionnalités avancées, les extensions,
la sécurité et la stabilité.

### Liens

* [Téléchargements](https://www.postgresql.org/download/)
* [Notes de version](https://www.postgresql.org/docs/14/release-14.html)
* [Dossier de presse](https://www.postgresql.org/about/press/)
* [Page sécurité](https://www.postgresql.org/support/security/)
* [Politique des versions](https://www.postgresql.org/support/versioning/)
* [Suivre @postgresql sur Twitter](https://twitter.com/postgresql)