26 septembre 2024 - Le [PostgreSQL Global Development
Group](https://www.postgresql.org) annonce aujourd'hui la publication de
[PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html), dernière
version de la base de données open source de référence.

PostgreSQL 17 repose sur plusieurs décennies de développement, améliorant ses
performances et sa mise à l'échelle tout en s'adaptant aux modèles émergents
d'accès aux données et à leur stockage. Cette version de
[PostgreSQL](https://www.postgresql.org) amène des gains de performance
généralisés, dont une révision complète de l'implantation de la gestion de la
mémoire des opérations de vacuum, des optimisations de l'accès au stockage, des
améliorations pour les charges de travail fortement concurrentielles,
l'accélération des chargements et exports en masse et des améliorations de
l'exécution des requêtes <!--pour--> utilisant les index. PostgreSQL 17 possède
des fonctionnalités qui profitent aussi bien aux nouvelles charges de travail
qu'aux systèmes critiques. On peut citer les ajouts à l'expérience développeur
avec la commande SQL/JSON `JSON_TABLE` et les améliorations de réplication
logique qui simplifient la gestion de la haute disponibilité et des mises à jour
de version majeures.

«&nbsp;PostgreSQL 17 souligne la manière dont la communauté open source
mondiale, qui pilote le développement de PostgreSQL, construit les améliorations
qui aident les utilisateurs à tous les niveaux de leur expérience avec la base
de données&nbsp;» dit Jonathan Katz, un membre de la «&nbsp;core team&nbsp;» de
PostgreSQL. «&nbsp;Qu'il s'agisse d'améliorations pour opérer les bases de
données à l'échelle ou de nouvelles fonctionnalités qui contribuent à une
expérience développeur agréable, PostgreSQL va parfaire votre expérience de la
gestion de données.&nbsp;»

PostgreSQL, système innovant de gestion des données, reconnu pour sa fiabilité
et sa robustesse, bénéficie depuis plus de 25 ans d'un développement open source
par une communauté mondiale de développeurs et développeuses. Il est devenu le
système de gestion de bases de données relationnelles de référence pour les
organisations de toute taille.

### Des gains de performance sur l'ensemble du moteur

Le processus de
[vacuum](https://www.postgresql.org/docs/17/routine-vacuuming.html) de
PostgreSQL, critique pour le bon déroulement des opérations, nécessite des
ressources du serveur de l'instance pour s'exécuter. PostgreSQL 17 introduit une
nouvelle structure interne de la mémoire pour vacuum qui divise par 20
l'utilisation mémoire. PostgreSQL améliore ainsi la vitesse des opérations de
vacuum tout en réduisant l'usage des ressources partagées, les rendant
disponibles à votre charge de travail.

PostgreSQL 17 poursuit l'amélioration des performances de sa couche
d'entrées/sorties. Les charges de travail hautement concurrentes pourront voir
leurs performances en écriture doubler grâce à une amélioration sur le
traitement du [write-ahead
log](https://www.postgresql.org/docs/17/wal-intro.html)
([WAL](https://www.postgresql.org/docs/17/wal-intro.html)). De plus, la nouvelle
interface d'entrées/sorties par flux accélère les lectures séquentielles
(lecture de toutes les données d'une table). Cette même fonctionnalité bénéficie
aussi à [`ANALYZE`](https://www.postgresql.org/docs/17/sql-analyze.html) qui
peut ainsi mettre à jour les statistiques du planificateur de requêtes bien plus
rapidement.

PostgreSQL 17 étend ses gains de performance à l'exécution de requêtes. Il
améliore la performance des requêtes avec des clauses `IN` utilisant des index
de type
[B-tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE),
la méthode d'indexation par défaut de PostgreSQL. De plus, il est maintenant
possible de paralléliser la construction des index
[BRIN](https://www.postgresql.org/docs/17/brin.html). PostgreSQL 17 comporte
plusieurs améliorations dans la planification des requêtes, dont des
optimisations sur les contraintes `NOT NULL` et des améliorations dans le
traitement des [common table
expressions](https://www.postgresql.org/docs/17/queries-with.html) ([les
requêtes`WITH`](https://www.postgresql.org/docs/17/queries-with.html)). Cette
version prend en charge plus d'instructions SIMD (Single Instruction/Multiple
Data) pour accélérer les calculs, incluant l'usage d'AVX-512 pour la fonction
[`bit_count`](https://www.postgresql.org/docs/17/functions-bitstring.html).

### Amélioration pour les développeurs

PostgreSQL a été la [première base de données relationnelle à ajouter le support
de JSON
(2012)](https://www.postgresql.org/about/news/postgresql-92-released-1415/), et
PostgreSQL 17 complète son implantation du standard SQL/JSON.
[`JSON_TABLE`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE)
est maintenant disponible dans PostgreSQL 17, permettant aux développeurs de
convertir des données JSON dans une table standard PostgreSQL. PostgreSQL 17
supporte maintenant les [constructeurs
SQL/JSON](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE)
(`JSON`, `JSON_SCALAR`, `JSON_SERIALIZE`) et [les fonctions de
requêtage](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS)
(`JSON_EXISTS`, `JSON_QUERY`, `JSON_VALUE`), offrant de nouvelles possibilités
aux développeurs d'interagir avec leurs données JSON. Cette version ajoute plus
d'[expressions
`jsonpath`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS),
avec un accent sur la conversion de données JSON vers des types de données
natifs de PostgreSQL comme les types numériques, booléens, chaînes de caractères
et date/heure.

PostgreSQL 17 rajoute des fonctionnalités à la commande
[`MERGE`](https://www.postgresql.org/docs/17/sql-merge.html), utilisée pour les
mises à jour conditionnelles, en incluant une clause `RETURNING` et la capacité
de mettre à jour les
[vues](https://www.postgresql.org/docs/17/sql-createview.html). En prime,
PostgreSQL 17 dispose de nouvelles capacités de chargement et d'export de
données en masse pouvant aller jusqu'à doubler la performance lors de l'export
de grandes lignes en utilisant la commande
[`COPY`](https://www.postgresql.org/docs/17/sql-copy.html). `COPY` bénéficie
d'améliorations de performance, lorsque les encodages de la source et de la
destination correspondent et inclut une nouvelle option, `ON_ERROR`, qui permet
la poursuite d'un import même en cas d'erreur d'insertion.

Cette version étend les fonctionnalités de gestion des données à la fois dans
les partitions et dans les données distribuées sur des instances PostgreSQL
distantes. PostgreSQL 17 supporte l'utilisation de colonnes identité et des
contraintes d'exclusions sur des [tables
partitionnées](https://www.postgresql.org/docs/17/ddl-partitioning.html). Les
[foreign data wrapper
PostgreSQL](https://www.postgresql.org/docs/17/postgres-fdw.html)
([`postgres_fdw`](https://www.postgresql.org/docs/17/postgres-fdw.html)), qui
sont utilisés pour exécuter des requêtes sur des instances PostgreSQL distantes,
peuvent maintenant transmettre les sous-requêtes `EXISTS` et `IN` vers un
serveur distant pour un traitement plus efficace.

PostgreSQL 17 inclut un fournisseur de collation interne, indépendant de la
plateforme et immutable permettant de garantir l'immutabilité des résultats et
fournit une sémantique de tri similaire à la collation `C`  mais avec l'encodage
`UTF-8` au lieu de `SQL_ASCII`. L'utilisation de ce nouveau fournisseur de
collation garantit que les résultats triés des requêtes basées sur du texte
seront identiques, indépendamment de l'environnement.

### Améliorations de la réplication logique pour la haute disponibilité et les mises à jour majeures

La [réplication
logique](https://www.postgresql.org/docs/17/logical-replication.html) est
utilisée pour transmettre des données en temps réel dans de nombreux cas
d'usage. Toutefois, avant cette version, une mise à jour majeure nécessitait de
supprimer les [slots de réplication
logique](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT),
ce qui obligeait à resynchroniser les données vers les souscripteurs après la
mise à jour. À partir de PostgreSQL 17, les mises à jour utilisant la
réplication logique seront simplifiées : elles ne nécessiteront plus de supprimer
les slots de réplication logique.

PostgreSQL 17 inclut désormais un contrôle des bascules sur incident pour la
réplication logique, ce qui la rend plus résiliente dans les environnements
hautement disponibles.

Enfin, PostgreSQL 17 introduit l'outil en ligne de commande
[`pg_createsubscriber`](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html)
pour convertir un réplica physique en réplica logique.

### Autres options de gestion de la sécurité et des opérations de maintenance

PostgreSQL 17 étend les possibilités de gestion du cycle de vie des systèmes de
bases de données. Une nouvelle option TLS, `sslnegotiation`, est ajoutée, qui
permet aux utilisateurs d'effectuer une <!-- poignée de main--> négociation TLS
directe lors de l'utilisation
d'[ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation)
(enregistrée comme `postgresql` dans le répertoire ALPN). PostgreSQL 17 ajoute
le [rôle prédéfini](https://www.postgresql.org/docs/17/predefined-roles.html)
`pg_maintain`, qui donne les privilèges d'effectuer des opérations de
maintenance aux utilisateurs.

[`pg_basebackup`](https://www.postgresql.org/docs/17/app-pgbasebackup.html),
l'utilitaire de sauvegarde intégré à PostgreSQL, supporte désormais les
sauvegardes incrémentales et ajoute l'utilitaire
[`pg_combinebackup`](https://www.postgresql.org/docs/17/app-pgcombinebackup.html)
pour reconstruire une sauvegarde complète.
En complément, [`pg_dump`](https://www.postgresql.org/docs/17/app-pgdump.html)
intègre une nouvelle option, appelée `--filter`, qui permet de préciser un
fichier contenant la liste des objets à intégrer lors de la génération d'un
export.

PostgreSQL 17 ajoute des améliorations aux fonctionnalités de supervision et
d'analyse.
[`EXPLAIN`](https://www.postgresql.org/docs/17/sql-explain.html) présente
maintenant le temps passé sur les lectures et écritures de blocs et intègre deux
nouvelles options : `SERIALIZE` et `MEMORY`, utiles pour voir le temps passé
dans la conversion de données lors des transmissions réseau, et la quantité de
mémoire utilisée.
PostgreSQL 17 indique désormais la [progression du vacuum des
index](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING). 
Cette version ajoute la vue système
[`pg_wait_events`](https://www.postgresql.org/docs/17/view-pg-wait-events.html),
qui combinée avec
[`pg_stat_activity`](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
donne plus d'informations sur les raisons pour lesquelles une session active est
en attente.

### Fonctionnalités additionnelles

De nombreuses autres fonctionnalités ont été ajoutées à PostgreSQL 17. Elles
peuvent aussi être utiles dans vos cas d'usage. Vous pouvez vous référer aux
[notes de version](https://www.postgresql.org/docs/17/release-17.html) (en
anglais) pour consulter la liste complète des fonctionnalités modifiées ou
ajoutées.

### À propos de PostgreSQL

[PostgreSQL](https://www.postgresql.org) est le système de gestion de bases de
données libre de référence. Sa communauté mondiale est composée de plusieurs
milliers d’utilisateurs, utilisatrices, contributeurs, contributrices,
entreprises et institutions. Le projet PostgreSQL, démarré il y a plus de 30 ans
à l’université de Californie, à Berkeley, a atteint aujourd’hui un rythme de
développement sans pareil. L’ensemble des fonctionnalités proposées est mature,
et dépasse même celui des systèmes commerciaux leaders sur les fonctionnalités
avancées, les extensions, la sécurité et la stabilité.

### Liens

* [Téléchargements](https://www.postgresql.org/download/)
* [Notes de version](https://www.postgresql.org/docs/17/release-17.html)
* [Dossier de presse](https://www.postgresql.org/about/press/)
* [Page sécurité](https://www.postgresql.org/support/security/)
* [Politique des versions](https://www.postgresql.org/support/versioning/)
* [Suivre @postgresql sur Twitter](https://twitter.com/postgresql)
* [Dons](https://www.postgresql.org/about/donate/)

##  En savoir plus sur les fonctionnalités

Pour de plus amples informations sur les fonctionnalités ci-dessus et toutes
les autres, vous pouvez consulter les liens suivants :

* [Notes de version](https://www.postgresql.org/docs/17/release-17.html)
* [Matrice de fonctionnalités](https://www.postgresql.org/about/featurematrix/)

## Où télécharger

Il existe plusieurs façons de télécharger PostgreSQL 17, dont :

* la [page de téléchargement](https://www.postgresql.org/download/), qui
  contient les installeurs et les outils pour
  [Windows](https://www.postgresql.org/download/windows/),
  [Linux](https://www.postgresql.org/download/),
  [macOS](https://www.postgresql.org/download/macosx/), et bien plus ;
* le [code source](https://www.postgresql.org/ftp/source/v17.0).

D'autres outils et extensions sont disponibles sur le [PostgreSQL Extension
Network](http://pgxn.org/).

## Documentation

La documentation au format HTML et les pages de manuel sont installées avec
PostgreSQL. La documentation peut également être [consultée en
ligne](https://www.postgresql.org/docs/17/) ou récupérée au format
[PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-A4.pdf).


## Licence

PostgreSQL utilise la [licence
PostgreSQL](https://www.postgresql.org/about/licence/), licence
«&nbsp;permissive&nbsp;» de type BSD. Cette [licence certifiée
OSI](http://www.opensource.org/licenses/postgresql/) est largement appréciée
pour sa flexibilité et sa compatibilité avec le monde des affaires, puisqu'elle
ne restreint pas l'utilisation de PostgreSQL dans les applications propriétaires
ou commerciales. Associée à un support proposé par de multiples sociétés et une
propriété publique du code, sa licence rend PostgreSQL très populaire parmi les
revendeurs souhaitant embarquer une base de données dans leurs produits sans
avoir à se soucier des prix de licence, des verrous commerciaux ou modifications
des termes de licence.


## Contacts

Site internet

* [https://www.postgresql.org/](https://www.postgresql.org/)

Courriel

* [press@postgresql.org](mailto:press@postgresql.org) ou
* [fr@postgreql.org](mailto:fr@postgresql.org) pour un contact francophone

## Images et logos

Postgres, PostgreSQL et le logo éléphant (Slonik) sont des marques déposées de
l'[Association de la Communauté PostgreSQL](https://www.postgres.ca). Si vous
souhaitez utiliser ces marques, vous devez vous conformer à la [politique de la
marque](https://www.postgresql.org/about/policies/trademarks/).

## Support professionnel et dons

PostgreSQL bénéficie du support de nombreuses sociétés, qui financent des
développeurs et développeuses, fournissent l'hébergement ou un support
financier. Les plus fervents supporters sont listés sur la page des
[sponsors](https://www.postgresql.org/about/sponsors/).

Il existe également une très grande communauté de [sociétés offrant du support
PostgreSQL](https://www.postgresql.org/support/professional_support/), du
consultant indépendant aux entreprises multinationales.

Les [dons](https://www.postgresql.org/about/donate/) au PostgreSQL Global
Development Group, ou à l'une des associations à but non lucratif, sont acceptés
et encouragés.
