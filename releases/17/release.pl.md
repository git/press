26 września 2024 - [PostgreSQL Global Development Group](https://www.postgresql.org) ogłosiła dziś wydanie [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html), najnowszej wersji najbardziej zaawansowanej na świecie bazy danych open source.

PostgreSQL 17 opiera się na dziesięcioleciach rozwoju open source, poprawiając wydajność i skalowalność, jednocześnie dostosowując się do nowych wzorców dostępu do danych i ich przechowywania. Ta wersja [PostgreSQL](https://www.postgresql.org) wprowadza znaczące ogólne wzrosty wydajności, w tym gruntowną przebudowę zarządzania pamięcią dla procesu vacuum, optymalizacje dostępu do pamięci masowej oraz usprawnienia dla pracy z wysoką współbieżnością, przyspieszenia w ładowaniu i eksportowaniu dużych ilości danych oraz poprawy w wykonywaniu zapytań dla indeksów. PostgreSQL 17 posiada funkcje, które są korzystne zarówno dla nowych obciążeń, jak i dla krytycznych systemów, takie jak dodatki dla programistów z komendą SQL/JSON `JSON_TABLE` oraz ulepszenia replikacji logicznej, które upraszczają zarządzanie obciążeniami związanymi z wysoką dostępnością i aktualizacjami do nowych wersji.

"PostgreSQL 17 podkreśla, jak globalna społeczność open source, która napędza rozwój PostgreSQL, tworzy ulepszenia, które pomagają użytkownikom na wszystkich etapach ich pracy z bazami danych," powiedział Jonathan Katz, członek zespołu głównego PostgreSQL. "Niezależnie od tego, czy chodzi o usprawnienia w zarządzaniu bazami danych na dużą skalę, czy o nowe funkcje, które wzbogacają doświadczenie programistów, PostgreSQL 17 poprawi Twoje doświadczenie w zarządzaniu danymi."

PostgreSQL, innowacyjny system zarządzania danymi znany ze swojej niezawodności, solidności i rozszerzalności, czerpie korzyści z ponad 25 lat rozwoju open source, tworzonego przez globalną społeczność programistów, i stał się preferowaną relacyjną bazą danych open source dla organizacji każdej wielkości.

### Ogólnosystemowe wzrosty wydajności

Proces [vacuum](https://www.postgresql.org/docs/17/routine-vacuuming.html) w PostgreSQL jest kluczowy dla zdrowego działania systemu, wymagając zasobów serwera do działania. PostgreSQL 17 wprowadza nową wewnętrzną strukturę pamięci dla vacuum, która zużywa do 20 razy mniej pamięci. Zwiększa to szybkość vacuum i zmniejsza wykorzystanie zasobów współdzielonych, pozostawiając więcej zasobów dla innych obciążeń.

PostgreSQL 17 kontynuuje ulepszanie wydajności swojej warstwy I/O. Obciążenia o wysokiej współbieżności mogą zobaczyć do 2x lepszą przepustowość zapisu dzięki ulepszeniom w przetwarzaniu [write-ahead log](https://www.postgresql.org/docs/17/wal-intro.html) ([WAL](https://www.postgresql.org/docs/17/wal-intro.html)). Dodatkowo, nowe interfejsy I/O streamingowego przyspieszają skanowanie sekwencyjne (czytanie wszystkich danych z tabeli) oraz proces aktualizacji statystyk planera za pomocą komendy [`ANALYZE`](https://www.postgresql.org/docs/17/sql-analyze.html).

PostgreSQL 17 rozszerza również wzrosty wydajności w zakresie wykonywania zapytań. Ulepsza wydajność zapytań z klauzulą `IN`, które korzystają z indeksów [B-tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE), domyślnej metody indeksowania w PostgreSQL. Ponadto, indeksy [BRIN](https://www.postgresql.org/docs/17/brin.html) teraz wspierają budowanie równoległe. PostgreSQL 17 zawiera kilka ulepszeń planowania zapytań, w tym optymalizacje dla ograniczeń `NOT NULL` oraz usprawnienia w przetwarzaniu [wyrażeń tabelarycznych](https://www.postgresql.org/docs/17/queries-with.html) ([`WITH`](https://www.postgresql.org/docs/17/queries-with.html)). Ta wersja dodaje także więcej wsparcia dla SIMD (Single Instruction/Multiple Data) w celu przyspieszenia obliczeń, w tym korzystanie z AVX-512 dla funkcji [`bit_count`](https://www.postgresql.org/docs/17/functions-bitstring.html).

### Dalsze rozszerzanie solidnego doświadczenia programistów

PostgreSQL był [pierwszą relacyjną bazą danych, która dodała wsparcie dla JSON w 2012 roku](https://www.postgresql.org/about/news/postgresql-92-released-1415/), a PostgreSQL 17 rozszerza swoją implementację standardu SQL/JSON. Funkcja [`JSON_TABLE`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE) jest teraz dostępna w PostgreSQL 17, umożliwiając programistom konwersję danych JSON do standardowej tabeli PostgreSQL. PostgreSQL 17 wspiera teraz [konstrukcje SQL/JSON](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE) (`JSON`, `JSON_SCALAR`, `JSON_SERIALIZE`) oraz [funkcje zapytań](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS) (`JSON_EXISTS`, `JSON_QUERY`, `JSON_VALUE`), dając programistom inne sposoby interakcji z danymi JSON. Ta wersja dodaje więcej [wyrażeń `jsonpath`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS), z naciskiem na konwersję danych JSON do natywnego typu danych PostgreSQL, w tym typów numerycznych, logicznych, tekstowych oraz typów daty i czasu.

PostgreSQL 17 dodaje więcej funkcji do [`MERGE`](https://www.postgresql.org/docs/17/sql-merge.html), które jest używane do warunkowych aktualizacji, w tym klauzulę `RETURNING` oraz możliwość aktualizowania [widoków](https://www.postgresql.org/docs/17/sql-createview.html). Dodatkowo PostgreSQL 17 oferuje nowe możliwości ładowania hurtowego i eksportowania danych, w tym do 2x poprawę wydajności przy eksportowaniu dużych wierszy za pomocą komendy [`COPY`](https://www.postgresql.org/docs/17/sql-copy.html). Wydajność `COPY` również wzrosła, gdy kodowanie źródła i miejsca docelowego jest takie samo, a komenda ta zawiera nową opcję `ON_ERROR`, która pozwala na kontynuowanie importu, nawet jeśli wystąpi błąd podczas wstawiania.

Ta wersja rozszerza funkcjonalność zarówno w zarządzaniu danymi w partycjach, jak i danymi rozproszonymi pomiędzy zdalnymi instancjami PostgreSQL. PostgreSQL 17 obsługuje teraz używanie kolumn z tożsamością oraz ograniczeń wykluczających w [tabelach partycjonowanych](https://www.postgresql.org/docs/17/ddl-partitioning.html). [Wrapper dla zdalnych danych PostgreSQL](https://www.postgresql.org/docs/17/postgres-fdw.html) ([`postgres_fdw`](https://www.postgresql.org/docs/17/postgres-fdw.html)), używany do wykonywania zapytań na zdalnych instancjach PostgreSQL, może teraz przekazywać zapytania `EXISTS` i `IN` do zdalnego serwera w celu bardziej wydajnego przetwarzania.

PostgreSQL 17 zawiera również wbudowanego, niezależnego od platformy dostawcę porządku sortowania, który jest gwarantowany jako niezmienny i zapewnia podobne semantyki sortowania jak `C`, ale z kodowaniem `UTF-8`, zamiast `SQL_ASCII`. Korzystanie z tego nowego dostawcy gwarantuje, że zapytania oparte na tekście będą zwracać te same posortowane wyniki, niezależnie od tego, gdzie działa PostgreSQL.

### Ulepszenia replikacji logicznej dla wysokiej dostępności i aktualizacji do nowych wersji

[Replikacja logiczna](https://www.postgresql.org/docs/17/logical-replication.html) służy do strumieniowego przesyłania danych w czasie rzeczywistym w różnych przypadkach użycia. Jednak przed tym wydaniem, użytkownicy, którzy chcieli dokonać aktualizacji do nowej wersji, musieli usunąć [gniazda replikacji logicznej](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT), co wymagało ponownej synchronizacji danych z subskrybentami po aktualizacji. Począwszy od aktualizacji z PostgreSQL 17, użytkownicy nie muszą już usuwać gniazd replikacji logicznej, co upraszcza proces aktualizacji przy korzystaniu z replikacji logicznej.

PostgreSQL 17 zawiera teraz kontrolę przełączenia w przypadku awarii dla replikacji logicznej, co czyni ją bardziej odporną w środowiskach o wysokiej dostępności. Dodatkowo, PostgreSQL 17 wprowadza narzędzie wiersza poleceń [`pg_createsubscriber`](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html), które pozwala na konwersję fizycznej repliki w nową replikę logiczną.

### Więcej opcji zarządzania bezpieczeństwem i operacjami

PostgreSQL 17 rozszerza sposób zarządzania cyklem życia systemów baz danych. PostgreSQL ma nową opcję TLS, `sslnegotiation`, która pozwala użytkownikom na bezpośrednie zestawienie TLS przy użyciu [ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation) (zarejestrowane jako `postgresql` w katalogu ALPN). PostgreSQL 17 dodaje także rolę zdefiniowaną `pg_maintain` [predefined role](https://www.postgresql.org/docs/17/predefined-roles.html), która daje użytkownikom uprawnienia do wykonywania operacji konserwacyjnych.

[`pg_basebackup`](https://www.postgresql.org/docs/17/app-pgbasebackup.html), narzędzie do tworzenia kopii zapasowych zawarte w PostgreSQL, teraz obsługuje kopie zapasowe przyrostowe i dodaje narzędzie [`pg_combinebackup`](https://www.postgresql.org/docs/17/app-pgcombinebackup.html) do rekonstrukcji pełnej kopii zapasowej. Dodatkowo, [`pg_dump`](https://www.postgresql.org/docs/17/app-pgdump.html) zawiera nową opcję `--filter`, która pozwala wybrać, które obiekty uwzględnić przy generowaniu pliku zrzutu.

PostgreSQL 17 zawiera również ulepszenia funkcji monitorowania i analizy. [`EXPLAIN`](https://www.postgresql.org/docs/17/sql-explain.html) teraz pokazuje czas poświęcony na lokalne odczyty i zapisy bloków I/O oraz zawiera dwie nowe opcje: `SERIALIZE` i `MEMORY`, które są przydatne do zobaczenia czasu poświęconego na konwersję danych dla transmisji sieciowej oraz ilości użytej pamięci. PostgreSQL 17 teraz raportuje [postęp odkurzania indeksów](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING) i dodaje widok systemowy [`pg_wait_events`](https://www.postgresql.org/docs/17/view-pg-wait-events.html), który w połączeniu z [`pg_stat_activity`](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW) daje więcej wglądu w to, dlaczego aktywna sesja oczekuje.

### Dodatkowe funkcje

Wiele innych nowych funkcji i usprawnień zostało dodanych do PostgreSQL 17, które mogą być również pomocne w Twoich przypadkach użycia. Proszę zobaczyć [informacje o wydaniu](https://www.postgresql.org/docs/17/release-17.html) po pełną listę nowych i zmienionych funkcji.

### O PostgreSQL

[PostgreSQL](https://www.postgresql.org) to najbardziej zaawansowana baza danych open source na świecie, posiadająca globalną społeczność tysięcy użytkowników, współtwórców, firm i organizacji. Opierając się na ponad 35 latach inżynierii, rozpoczętej na Uniwersytecie Kalifornijskim w Berkeley, PostgreSQL rozwija się w niespotykanym tempie. Dojrzały zestaw funkcji PostgreSQL nie tylko dorównuje wiodącym systemom baz danych o zamkniętym kodzie, ale często je przewyższa w zakresie zaawansowanych funkcji baz danych, rozszerzalności, bezpieczeństwa i stabilności.

### Linki

* [Pobierz](https://www.postgresql.org/download/)
* [Informacje o wydaniu](https://www.postgresql.org/docs/17/release-17.html)
* [Zestaw prasowy](https://www.postgresql.org/about/press/)
* [Strona bezpieczeństwa](https://www.postgresql.org/support/security/)
* [Polityka wersjonowania](https://www.postgresql.org/support/versioning/)
* [Śledź @postgresql](https://twitter.com/postgresql)
* [Wpłać darowiznę](https://www.postgresql.org/about/donate/)

## Więcej o funkcjach

Wyjaśnienia powyższych funkcji i innych można znaleźć w następujących zasobach:

* [Informacje o wydaniu](https://www.postgresql.org/docs/17/release-17.html)
* [Macierz funkcji](https://www.postgresql.org/about/featurematrix/)

## Gdzie pobrać

Istnieje kilka sposobów pobrania PostgreSQL 17, w tym:

* Strona [Oficjalnych pobrań](https://www.postgresql.org/download/), która zawiera instalatory i narzędzia dla [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/) i innych.
* [Kod źródłowy](https://www.postgresql.org/ftp/source/v17.0)

Inne narzędzia i rozszerzenia są dostępne na [PostgreSQL Extension Network](http://pgxn.org/).

## Dokumentacja

PostgreSQL 17 zawiera dokumentację w formacie HTML oraz man pages, a także można przeglądać dokumentację online zarówno w formatach [HTML](https://www.postgresql.org/docs/17/) jak i [PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-US.pdf).

## Licencja

PostgreSQL używa [licencji PostgreSQL](https://www.postgresql.org/about/licence/), podobnej do licencji BSD. Ta [certyfikowana przez OSI](http://www.opensource.org/licenses/postgresql/) licencja jest powszechnie doceniana za swoją elastyczność i przyjazność dla biznesu, ponieważ nie ogranicza korzystania z PostgreSQL w aplikacjach komercyjnych i zastrzeżonych. Wraz z wielofirmowym wsparciem i publiczną własnością kodu, nasza licencja sprawia, że PostgreSQL jest bardzo popularny wśród dostawców, którzy chcą włączyć bazę danych do swoich produktów bez obawy o opłaty, zamknięcie przez dostawcę lub zmiany w warunkach licencjonowania.

## Kontakty

Strona internetowa

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-mail

Polski kontakt dla prasy:

* [pl@postgresql.org](mailto:pl@postgresql.org)

Główny kontakt dla prasy (angielski):

* [press@postgresql.org](mailto:press@postgresql.org)

## Obrazy i logo

Postgres, PostgreSQL i logo Słonik są zarejestrowanymi znakami towarowymi [PostgreSQL Community Association](https://www.postgres.ca). Jeśli chcesz używać tych znaków, musisz przestrzegać [polityki znaków towarowych](https://www.postgresql.org/about/policies/trademarks/).

## Wsparcie korporacyjne i darowizny

PostgreSQL cieszy się wsparciem wielu firm, które sponsorują programistów, zapewniają zasoby hostingowe i udzielają wsparcia finansowego. Zobacz naszą stronę [sponsorów](https://www.postgresql.org/about/sponsors/) dla listy tych wspierających projekt.

Jest także duża społeczność [firm oferujących wsparcie dla PostgreSQL](https://www.postgresql.org/support/professional_support/), od indywidualnych konsultantów po międzynarodowe korporacje.

Jeśli chcesz wesprzeć finansowo PostgreSQL Global Development Group lub jedną z uznanych organizacji non-profit wspólnoty, odwiedź naszą stronę [darowizn](https://www.postgresql.org/about/donate/).
