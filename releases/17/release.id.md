﻿26 September 2024 - [PostgreSQL Global Development Group](https://www.postgresql.org) hari ini mengumumkan perilisan [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html), versi terbaru dari database Open Source paling canggih di dunia.

PostgreSQL 17 dibangun berdasarkan puluhan tahun pengembangan Open Source dengan peningkatan performa dan skalabilitas, serta penyesuaian terhadap pola akses dan penyimpanan data terbaru. Rilis [PostgreSQL](https://www.postgresql.org) ini menambahkan peningkatan performa yang signifikan, termasuk implementasi manajemen memori yang lebih efisien untuk vacuum, optimasi akses penyimpanan, dan peningkatan untuk beban kerja dengan tingkat concurrency tinggi. Selain itu, ada peningkatan kecepatan dalam pemrosesan bulk loading dan ekspor data, serta perbaikan dalam eksekusi query untuk indeks.. PostgreSQL 17 memiliki fitur yang menguntungkan, baik untuk beban kerja baru maupun sistem bermisi kritis, seperti penambahan perintah SQL/JSON JSON\_TABLE, dan peningkatan replikasi logis yang menyederhanakan manajemen beban kerja High Availability (HA) dan peningkatan versi utama.

"PostgreSQL 17 menunjukkan bagaimana komunitas Open Source global, yang memimpin pengembangan PostgreSQL, terus menghadirkan inovasi yang membantu pengguna di berbagai tahap perjalanan database mereka," ucap Jonathan Katz, anggota tim inti PostgreSQL. "Baik itu peningkatan untuk mengoperasikan database dalam skala besar atau fitur baru yang membangun pengalaman pengembang yang menyenangkan, PostgreSQL 17 akan meningkatkan pengalaman pengelolaan data Anda."

PostgreSQL, sistem manajemen data yang inovatif yang dikenal karena keandalannya, ketangguhannya, dan ekstensibilitasnya, mendapat manfaat dari lebih dari 25 tahun pengembangan Open Source oleh komunitas pengembang global dan telah menjadi database relasional Open Source pilihan bagi berbagai ukuran organisasi. 
### Peningkatan Kinerja Secara Menyeluruh
Proses [vacuum](https://www.postgresql.org/docs/17/routine-vacuuming.html) PostgreSQL sangat penting untuk operasi yang sehat, membutuhkan sumber daya server untuk beroperasi. PostgreSQL 17 memperkenalkan struktur memori internal baru untuk vacuum yang mengonsumsi hingga 20x lipat lebih sedikit memori. Hal ini meningkatkan kecepatan vacuum dan juga mengurangi penggunaan sumber daya bersama sehingga lebih banyak yang tersedia untuk beban kerja Anda.

PostgreSQL 17 terus meningkatkan kinerja lapisan I/O-nya. Beban kerja dengan concurrency tinggi dapat melihat throughput penulisan hingga 2x lebih baik berkat peningkatan dalam pemrosesan [write-ahead log](https://www.postgresql.org/docs/17/wal-intro.html) ([WAL](https://www.postgresql.org/docs/17/wal-intro.html)). Selain itu, antarmuka streaming I/O yang baru mempercepat pemindaian berurutan (membaca semua data dari tabel) dan mempercepat pembaruan statistik perencana melalui perintah [ANALYZE](https://www.postgresql.org/docs/17/sql-analyze.html).

PostgreSQL 17 juga memperluas peningkatan kinerjanya ke eksekusi query. PostgreSQL 17 meningkatkan kinerja query dengan klausa IN yang menggunakan indeks [B-tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE), metode indeks default di PostgreSQL. Selain itu, indeks [BRIN](https://www.postgresql.org/docs/17/brin.html) sekarang mendukung pembuatan paralel. PostgreSQL 17 mencakup beberapa peningkatan untuk perencanaan query, termasuk optimasi untuk constraint NOT NULL, dan peningkatan dalam pemrosesan [common table expressions](https://www.postgresql.org/docs/17/queries-with.html) ([WITH](https://www.postgresql.org/docs/17/queries-with.html)[ queries](https://www.postgresql.org/docs/17/queries-with.html)). Rilis ini menambahkan lebih banyak dukungan SIMD (Single Instruction/Multiple Data) untuk mempercepat perhitungan, termasuk menggunakan AVX-512 untuk fungsi [bit_count](https://www.postgresql.org/docs/17/functions-bitstring.html).

### Ekspansi Lebih Lanjut Pengalaman Pengembang yang Kuat
PostgreSQL adalah [database relasional pertama yang menambahkan dukungan JSON (2012)](https://www.postgresql.org/about/news/postgresql-92-released-1415/), dan PostgreSQL 17 menambahkan pada implementasinya standar SQL/JSON. [JSON_TABLE](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE) sekarang tersedia di PostgreSQL 17 yang memungkinkan pengembang mengonversi data JSON menjadi tabel PostgreSQL standar. PostgreSQL 17 sekarang mendukung [konstruktor SQL/JSON](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE) (JSON, JSON\_SCALAR, JSON\_SERIALIZE) dan [fungsi query](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS) (JSON\_EXISTS, JSON\_QUERY, JSON\_VALUE), memberikan pengembang cara lain untuk berinteraksi dengan data JSON mereka. Rilis ini menambahkan lebih banyak ekspresi [jsonpath](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS), dengan penekanan pada pengonversian data JSON ke tipe data asli PostgreSQL, termasuk tipe numerik, boolean, string, dan tanggal/waktu.

PostgreSQL 17 menambahkan lebih banyak fitur ke [MERGE](https://www.postgresql.org/docs/17/sql-merge.html), yang digunakan untuk pembaruan bersyarat, termasuk klausa RETURNING dan kemampuan untuk memperbarui [views](https://www.postgresql.org/docs/17/sql-createview.html). Selain itu, PostgreSQL 17 memiliki kemampuan baru untuk pemuatan massal dan ekspor data, termasuk peningkatan kinerja hingga 2x saat mengekspor baris besar menggunakan perintah [COPY](https://www.postgresql.org/docs/17/sql-copy.html). Kinerja COPY juga mengalami peningkatan ketika encoding sumber dan tujuan cocok, dan mencakup opsi baru, ON\_ERROR, yang memungkinkan impor untuk terus berlanjut bahkan jika ada kesalahan saat menyisipkan.

Rilis ini memperluas fungsionalitas baik untuk mengelola data dalam partisi maupun data yang didistribusikan di antar-instance PostgreSQL jarak jauh. PostgreSQL 17 mendukung penggunaan kolom identitas dan constraint pengecualian pada [tabel yang dipartisi](https://www.postgresql.org/docs/17/ddl-partitioning.html). [PostgreSQL foreign data wrapper](https://www.postgresql.org/docs/17/postgres-fdw.html) ([postgres_fdw](https://www.postgresql.org/docs/17/postgres-fdw.html)), yang digunakan untuk menjalankan query di instance PostgreSQL jarak jauh, sekarang dapat mendorong subquery EXISTS dan IN ke server jarak jauh untuk pemrosesan yang lebih efisien.

PostgreSQL 17 juga mencakup penyedia kolasi built-in, platform independent, yang dijamin immutable dan menyediakan semantik pengurutan yang mirip dengan kolasi C kecuali dengan encoding UTF-8 daripada SQL\_ASCII. Menggunakan penyedia kolasi baru ini menjamin bahwa query berbasis teks Anda akan mengembalikan hasil yang diurutkan sama di mana pun PostgreSQL dijalankan.

### Peningkatan Logical Replication untuk Ketersediaan Tinggi (High Availability) dan Peningkatan Versi Utama

[Replikasi logis](https://www.postgresql.org/docs/17/logical-replication.html) digunakan untuk mengalirkan data secara real-time di berbagai kasus penggunaan. Namun, sebelum rilis ini, pengguna yang ingin melakukan peningkatan versi utama harus menghapus [slot replikasi logis](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT), yang memerlukan sinkronisasi ulang data ke pelanggan setelah peningkatan. Mulai dengan peningkatan dari PostgreSQL 17, pengguna tidak perlu menghapus slot replikasi logis, menyederhanakan proses peningkatan saat menggunakan replikasi logis.

PostgreSQL 17 sekarang menyertakan kontrol failover untuk replikasi logis, membuatnya lebih tangguh ketika digunakan dalam lingkungan ketersediaan tinggi. Selain itu, PostgreSQL 17 memperkenalkan alat baris perintah [pg_createsubscriber](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html) untuk mengonversi replika fisik menjadi replika logis baru.

### Lebih Banyak Opsi untuk Mengelola Keamanan dan Operasi
PostgreSQL 17 lebih memperluas bagaimana pengguna dapat mengelola siklus hidup keseluruhan sistem basis data mereka. PostgreSQL memiliki opsi TLS baru, ‘sslnegotiation’, yang memungkinkan pengguna melakukan jabat tangan TLS langsung saat menggunakan [ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation) (terdaftar sebagai postgresql dalam direktori ALPN). PostgreSQL 17 juga menambahkan pg\_maintain [peran yang telah ditentukan](https://www.postgresql.org/docs/17/predefined-roles.html), yang memberikan pengguna izin untuk melakukan operasi pemeliharaan.

[pg_basebackup](https://www.postgresql.org/docs/17/app-pgbasebackup.html), utilitas cadangan yang disertakan dalam PostgreSQL, sekarang mendukung cadangan inkremental dan menambahkan utilitas [pg_combinebackup](https://www.postgresql.org/docs/17/app-pgcombinebackup.html) untuk merekonstruksi cadangan penuh. Selain itu, [pg_dump](https://www.postgresql.org/docs/17/app-pgdump.html) menyertakan opsi baru yang disebut--filter yang memungkinkan Anda memilih objek apa yang akan disertakan saat menghasilkan file dump.

PostgreSQL 17 juga menyertakan peningkatan fitur pemantauan dan analisis. [EXPLAIN](https://www.postgresql.org/docs/17/sql-explain.html) sekarang menunjukkan waktu yang dihabiskan untuk pembacaan dan penulisan blok I/O lokal, dan menyertakan dua opsi baru: SERIALIZE dan MEMORY, berguna untuk melihat waktu yang dihabiskan dalam konversi data untuk transmisi jaringan, dan berapa banyak memori yang digunakan. PostgreSQL 17 sekarang melaporkan [kemajuan vakum indeks](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING), dan menambahkan tampilan sistem [pg_wait_events](https://www.postgresql.org/docs/17/view-pg-wait-events.html) yang, bila dikombinasikan dengan [pg_stat_activity](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW), memberikan wawasan lebih lanjut tentang mengapa sesi aktif sedang menunggu.

### Fitur Tambahan
Banyak fitur dan peningkatan baru lainnya telah ditambahkan ke PostgreSQL 17 yang mungkin juga berguna untuk kasus penggunaan Anda. Silakan lihat [catatan rilis](https://www.postgresql.org/docs/17/release-17.html) untuk daftar lengkap fitur baru dan fitur yang diubah.

### Tentang PostgreSQL
[PostgreSQL](https://www.postgresql.org) adalah basis data Open Source paling canggih di dunia, dengan komunitas global ribuan pengguna, kontributor, perusahaan, dan organisasi. Dibangun di atas lebih dari 35 tahun rekayasa, dimulai di Universitas California, Berkeley, PostgreSQL terus berlanjut dengan kecepatan pengembangan yang tak tertandingi. Set fitur yang sudah matang dari PostgreSQL tidak hanya cocok dengan sistem basis data proprieter teratas, tetapi melebihi mereka dalam fitur basis data canggih, ekstensibilitas, keamanan, dan stabilitas.

### Tautan
- [Unduh](https://www.postgresql.org/download/)
- [Catatan Rilis](https://www.postgresql.org/docs/17/release-17.html)
- [Kit Pers](https://www.postgresql.org/about/press/)
- [Halaman Keamanan](https://www.postgresql.org/support/security/)
- [Kebijakan Versi](https://www.postgresql.org/support/versioning/)
- [Ikuti @postgresql](https://twitter.com/postgresql)
- [Donasi](https://www.postgresql.org/about/donate/)

## Lebih Lanjut Tentang Fitur-fitur
Untuk penjelasan tentang fitur-fitur lebih lanjut di atas dan lainnya, silakan lihat sumber berikut:

- [Catatan Rilis](https://www.postgresql.org/docs/17/release-17.html)
- [Matriks Fitur](https://www.postgresql.org/about/featurematrix/)

## Tempat Mengunduh
Terdapat beberapa cara yang dapat Anda gunakan untuk mengunduh PostgreSQL 17, yaitu:

- Halaman [Unduhan Resmi](https://www.postgresql.org/download/), yang berisi installer dan alat untuk [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/), dan lainnya.
- [Kode Sumber](https://www.postgresql.org/ftp/source/v17.0)

Alat dan ekstensi lainnya tersedia di [Jaringan Ekstensi PostgreSQL](http://pgxn.org/).

## Dokumentasi
PostgreSQL 17 hadir dengan dokumentasi HTML serta halaman manual, dan Anda juga dapat menjelajahi dokumentasi secara online dalam format [HTML](https://www.postgresql.org/docs/17/) dan [PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-US.pdf).

## Lisensi
PostgreSQL menggunakan [Lisensi PostgreSQL](https://www.postgresql.org/about/licence/), sebuah lisensi BSD Like "permisif". Lisensi [bersertifikat OSI](http://www.opensource.org/licenses/postgresql/) ini sangat dihargai karena fleksibel dan ramah bisnis, karena tidak membatasi penggunaan PostgreSQL dengan aplikasi komersial dan proprietary. Bersama dengan dukungan multi-perusahaan dan kepemilikan publik atas kode, lisensi kami membuat PostgreSQL sangat populer di kalangan vendor yang ingin menanamkan database dalam produk mereka sendiri tanpa takut akan biaya, vendor lock-in, atau perubahan dalam ketentuan lisensi.

## Kontak
Situs Web
- [https://www.postgresql.org/](https://www.postgresql.org)

Email
- [press@postgresql.org](mailto:press@postgresql.org)

## Gambar dan Logo
Postgres, PostgreSQL, dan Logo Gajah (Slonik) semuanya adalah merek dagang terdaftar dari [Asosiasi Komunitas PostgreSQL](https://www.postgres.ca/). Jika Anda ingin menggunakan merek ini, Anda harus mematuhi [kebijakan merek dagang](https://www.postgresql.org/about/policies/trademarks/).

## Dukungan Perusahaan dan Donasi
PostgreSQL mendapat dukungan dari berbagai perusahaan, yang mensponsori pengembang, menyediakan sumber daya hosting, dan memberi kami dukungan finansial. Lihat halaman [sponsor](https://www.postgresql.org/about/sponsors/) kami untuk beberapa pendukung proyek ini.

Terdapat juga komunitas besar [perusahaan yang menawarkan Dukungan PostgreSQL](https://www.postgresql.org/support/professional_support/), mulai dari konsultan individu hingga perusahaan multinasional.

Jika Anda ingin memberikan kontribusi finansial kepada PostgreSQL Global Development Group atau salah satu organisasi nirlaba komunitas yang diakui, silakan kunjungi halaman [donasi](https://www.postgresql.org/about/donate/) kami.
