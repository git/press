2024 年 9 月 26 日 - [PostgreSQL Global Development Group](https://www.postgresql.org) は本日、世界で最も高度かつ進化したオープンソースデータベースの最新バージョンである [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html) を発表しました。

PostgreSQL 17 は、数十年にわたるオープンソース開発の成果を基に構築され、そのパフォーマンスとスケーラビリティがさらに強化され、新しいデータアクセスおよびストレージパターンに適応しています。
 [PostgreSQL](https://www.postgresql.org) のこのリリースでは、全体的なパフォーマンスの大幅な向上が図られており、特にバキューム処理のメモリ管理の実装を全面的に見直し、ストレージアクセスの最適化や高い並行処理ワークロードへの対応が改善され、大量データの読み込みやエクスポートの速度が大幅に向上し、インデックスに対するクエリ実行の効率も改善されています。
PostgreSQL 17 は、全く新しいワークロードにも、重要なシステムにもメリットをもたらし、開発者体験をさらに強化する SQL/JSON の `JSON_TABLE` コマンドの追加や、高可用性ワークロードの管理とメジャーバージョンアップを簡素化する論理レプリケーションの強化など、多くの重要な新機能が追加されています。

「PostgreSQL 17 は、PostgreSQL の開発を推進するグローバルなオープンソースコミュニティが、あらゆる段階のデータベース利用者に対して大きな改善をもたらしていることを示しています」と、PostgreSQL のコアチームメンバーである Jonathan Katz 氏は述べています。
「大規模データベースの運用性の大幅な向上から、開発者体験を劇的に向上させる新機能に至るまで、PostgreSQL 17 はデータ管理の体験を改善します。」

PostgreSQL は、その高い信頼性、堅牢性、そして拡張性で知られる革新的なデータ管理システムであり、25 年以上にわたるオープンソース開発の恩恵を受けています。グローバルな開発者コミュニティの尽力により、PostgreSQL はあらゆる規模の組織にとって最も信頼されるオープンソースのリレーショナルデータベースとなっています。

### システム全体の大幅なパフォーマンス向上

PostgreSQL の [vacuum](https://www.postgresql.org/docs/17/routine-vacuuming.html) プロセスは、健全な運用に不可欠であり、サーバインスタンスのリソースを必要とします。
PostgreSQL 17 は、バキュームのための新しい内部メモリ構造を導入しており、最善のケースでは従来の 1 / 20 のメモリで済むようになります。
これにより、バキュームの速度が向上し、共有リソースの使用が削減され、ワークロードに利用できるリソースが増加します。

PostgreSQL 17 は、I/O レイヤのパフォーマンスを引き続き向上させています。
高並行処理ワークロードでは、[先行書き込みログ](https://www.postgresql.org/docs/17/wal-intro.html)（[WAL](https://www.postgresql.org/docs/17/wal-intro.html)）処理の改善により、書き込みスループットが最大で 2 倍に向上する可能性があります。
さらに、新しいストリーミング I/O インターフェースにより、シーケンシャルスキャン（テーブルの全データの読み込み）や、[`ANALYZE`](https://www.postgresql.org/docs/17/sql-analyze.html) がプランナ統計を更新する速度が向上します。

PostgreSQL 17 は、クエリ実行においてもパフォーマンスの向上を実現しています。
PostgreSQL 17 では、PostgreSQL のデフォルトのインデックス メソッドである [B-Tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE) インデックスを使用する `IN` 句を含むクエリのパフォーマンスが向上しています。
さらに、[BRIN](https://www.postgresql.org/docs/17/brin.html) インデックスは並列ビルドをサポートするようになりました。
PostgreSQL 17 には、`NOT NULL` 制約の最適化や、[共通テーブル式](https://www.postgresql.org/docs/17/queries-with.html)（[`WITH` クエリ](https://www.postgresql.org/docs/17/queries-with.html)）の処理改善を含む、クエリプランニングの複数の改善が含まれています。
また、このリリースでは、計算を高速化するためにさらに多くの SIMD（単一命令・複数データ）サポートが追加され、[`bit_count`](https://www.postgresql.org/docs/17/functions-bitstring.html) 関数で AVX-512 を使用できるようになりました。

### 堅牢な開発者体験のさらなる拡充

PostgreSQL は [JSON サポートを追加した最初のリレーショナルデータベース（2012 年）](https://www.postgresql.org/about/news/postgresql-92-released-1415/) であり、PostgreSQL 17 は SQL/JSON 標準の実装をさらに強化しています。
PostgreSQL 17 では、[`JSON_TABLE`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE) が利用可能となり、開発者は JSON データを標準の PostgreSQL テーブルに変換できるようになりました。
また、PostgreSQL 17 は [SQL/JSON コンストラクタ](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE)（`JSON`、`JSON_SCALAR`、`JSON_SERIALIZE`）や [クエリ関数](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS)（`JSON_EXISTS`、`JSON_QUERY`、`JSON_VALUE`）をサポートし、開発者が JSON データと対話するためのさらなる手段を提供しています。
このリリースでは、[`jsonpath` 式](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS) が追加されており、JSON データを数値、ブール値、文字列、日付/時刻型などのネイティブな PostgreSQL データ型に変換することに重点を置いています。

PostgreSQL 17 では、条件付きの更新に使用される [`MERGE`](https://www.postgresql.org/docs/17/sql-merge.html) にさらに多くの機能が追加され、`RETURNING` 句や [ビュー](https://www.postgresql.org/docs/17/sql-createview.html) を更新する機能が含まれています。
さらに、PostgreSQL 17 では、大規模な行をエクスポートする際に [`COPY`](https://www.postgresql.org/docs/17/sql-copy.html) コマンドを使用すると、パフォーマンスが最大で 2 倍向上するなど、大量データのロードやエクスポートに関する新機能も追加されています。
`COPY` のパフォーマンスは、ソースと宛先のエンコーディングが一致する場合にも改善され、新しいオプションとして、挿入エラーが発生してもインポートを継続できる `ON_ERROR` が追加されています。

このリリースでは、パーティション化されたデータやリモートの PostgreSQL インスタンスに分散されたデータの管理機能が拡張されています。
PostgreSQL 17 は、[パーティションテーブル](https://www.postgresql.org/docs/17/ddl-partitioning.html) で識別列と排他制約を使用することをサポートしています。
リモートの PostgreSQL インスタンスでクエリを実行するために使用される [PostgreSQL 外部データラッパ](https://www.postgresql.org/docs/17/postgres-fdw.html)（[`postgres_fdw`](https://www.postgresql.org/docs/17/postgres-fdw.html)）は、より効率的な処理のために `EXISTS` や `IN` サブクエリをリモートサーバにプッシュできるようになりました。

PostgreSQL 17 には、プラットフォームに依存しないビルトインのイミュータブルコレーションプロバイダが含まれており、`SQL_ASCII` ではなく `UTF-8` エンコーディングを使用しながら、`C` コレーションに類似した並べ替えセマンティクスを提供します。
この新しいコレーションプロバイダを使用することで、PostgreSQL をどこで実行しても、テキストベースのクエリが同じ並べ替え結果を返すことが保証されます。

### 高可用性とメジャーバージョンアップのための論理レプリケーションの強化

[論理レプリケーション](https://www.postgresql.org/docs/17/logical-replication.html) は、さまざまなユースケースでデータをリアルタイムにストリーミングするために使用されます。
しかし、このリリース以前は、メジャーバージョンアップを行う際に [論理レプリケーションスロット](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT) を削除する必要があり、アップグレード後にデータをサブスクライバに再同期する必要がありました。
PostgreSQL 17 以降のアップグレードでは、論理レプリケーションスロットを削除する必要がなくなり、論理レプリケーションを使用する際のアップグレードプロセスが簡素化されます。

PostgreSQL 17 には、論理レプリケーションのフェイルオーバ制御が追加されており、高可用性環境での耐障害性が向上しています。
さらに、PostgreSQL 17 では、物理レプリカを新しい論理レプリカに変換するためのコマンドラインツール [`pg_createsubscriber`](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html) が導入されています。

### セキュリティと運用管理のためのオプションの追加

PostgreSQL 17 は、ユーザがデータベースシステムのライフサイクル全体を管理する方法をさらに拡張しています。
PostgreSQL には新しい TLS オプション `sslnegotiation` が追加され、[ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation)（ALPN ディレクトリに `postgresql` として登録済み）を使用して直接 TLS ハンドシェイクを行うことが可能です。
PostgreSQL 17 では、メンテナンス操作を実行するための権限をユーザに付与する `pg_maintain` [事前定義ロール](https://www.postgresql.org/docs/17/predefined-roles.html) も追加されています。

PostgreSQL に含まれるバックアップユーティリティ [`pg_basebackup`](https://www.postgresql.org/docs/17/app-pgbasebackup.html) は、増分バックアップをサポートし、フルバックアップを再構築するための [`pg_combinebackup`](https://www.postgresql.org/docs/17/app-pgcombinebackup.html) ユーティリティが追加されました。
さらに、[`pg_dump`](https://www.postgresql.org/docs/17/app-pgdump.html) には `--filter` という新しいオプションが追加され、ダンプファイルを生成する際に含めるオブジェクトを選択できるようになっています。

PostgreSQL 17 には、監視と分析機能の強化も含まれています。
[`EXPLAIN`](https://www.postgresql.org/docs/17/sql-explain.html) は、ローカル I/O ブロックの読み書きにかかる時間を表示するようになり、ネットワーク伝送用のデータ変換にかかる時間を確認できる `SERIALIZE` オプションと、使用メモリ量を確認できる `MEMORY` オプションが追加されました。
PostgreSQL 17 は、[インデックスのバキューム進行状況](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING) を報告し、[`pg_wait_events`](https://www.postgresql.org/docs/17/view-pg-wait-events.html) システムビューを追加しました。
これにより、[`pg_stat_activity`](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW) と組み合わせて使用することで、アクティブなセッションがなぜ待機しているのかをより詳細に把握できます。

### 追加機能

PostgreSQL 17 には、他にも多くの新機能や改善が追加されており、さまざまなユースケースで役立つ可能性があります。
新機能および変更点の完全な一覧については、[リリースノート](https://www.postgresql.org/docs/17/release-17.html) をご覧ください。

### PostgreSQL について

[PostgreSQL](https://www.postgresql.org) は、世界で最も高度なオープンソースデータベースであり、数千人のユーザー、コントリビューター、企業、組織からなるグローバルコミュニティによって支えられています。
カリフォルニア大学バークレー校で始まり 35 年以上にわたるエンジニアリングの成果を基に構築された PostgreSQL は、比類のない開発ペースで進化を続けています。
PostgreSQL の成熟した機能セットは、主要な商用データベースシステムと同等であるだけでなく、高度なデータベース機能、拡張性、セキュリティ、安定性の面でそれらを凌駕しています。

### リンク

* [ダウンロード](https://www.postgresql.org/download/)
* [リリースノート](https://www.postgresql.org/docs/17/release-17.html)
* [プレスキット](https://www.postgresql.org/about/press/)
* [セキュリティページ](https://www.postgresql.org/support/security/)
* [バージョンポリシー](https://www.postgresql.org/support/versioning/)
* [@postgresql をフォロー](https://twitter.com/postgresql)
* [寄付](https://www.postgresql.org/about/donate/)

## 機能の詳細

上記の機能やその他の説明については、以下のリソースをご覧ください：

* [リリースノート](https://www.postgresql.org/docs/17/release-17.html)
* [機能マトリックス](https://www.postgresql.org/about/featurematrix/)

## ダウンロード方法

PostgreSQL 17 をダウンロードする方法はいくつかあります：

* [公式ダウンロード](https://www.postgresql.org/download/) ページには、[Windows](https://www.postgresql.org/download/windows/)、[Linux](https://www.postgresql.org/download/linux/)、[macOS](https://www.postgresql.org/download/macosx/) などのインストーラやツールが含まれています。
* [ソースコード](https://www.postgresql.org/ftp/source/v17.0)

その他のツールや拡張機能は [PostgreSQL Extension Network](http://pgxn.org/) で利用可能です。

## ドキュメント

PostgreSQL 17 には、HTML ドキュメントおよび man ページが付属しており、[HTML](https://www.postgresql.org/docs/17/) 形式および [PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-US.pdf) 形式の両方でオンラインでも閲覧できます。

## ライセンス

PostgreSQL は、BSD に似た「許容的」なライセンスである [PostgreSQL ライセンス](https://www.postgresql.org/about/licence/) を使用しています。
この [OSI 認定ライセンス](http://www.opensource.org/licenses/postgresql/) は、商業的およびプロプライエタリなアプリケーションと PostgreSQL を併用する際に制限がないため、柔軟でビジネスフレンドリーなものとして広く評価されています。
複数の企業によるサポートとコードの公開所有権と相まって、このライセンスは、ベンダーがデータベースを自社製品に組み込む際に、料金、ベンダーロックイン、ライセンス条件の変更を心配することなく利用できるため、PostgreSQL を非常に人気のある選択肢にしています。

## 連絡先

Web サイト

* [https://www.postgresql.org/](https://www.postgresql.org/)

E メール

* [press@postgresql.org](mailto:press@postgresql.org)

## 画像およびロゴ

Postgres、PostgreSQL、および象のロゴ（Slonik）はすべて [PostgreSQL Community Association](https://www.postgres.ca) の登録商標です。これらのマークを使用する場合は、[商標ポリシー](https://www.postgresql.org/about/policies/trademarks/) に従う必要があります。

## コーポレートサポートと寄付

PostgreSQL は、多くの企業からサポートを受けており、これらの企業は開発者をスポンサーし、ホスティングリソースを提供し、財政的な支援を行っています。これらのプロジェクト支援者の一部については、[スポンサー](https://www.postgresql.org/about/sponsors/) ページをご覧ください。

また、個人コンサルタントから多国籍企業に至るまで、[PostgreSQL サポートを提供する企業](https://www.postgresql.org/support/professional_support/) の大規模なコミュニティも存在します。

PostgreSQL Global Development Group または認定されたコミュニティの非営利団体への寄付を希望される方は、[寄付](https://www.postgresql.org/about/donate/) ページをご覧ください。
