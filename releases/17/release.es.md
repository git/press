26 de septiembre de 2024 - El [Grupo Global de Desarrollo de PostgreSQL](https://www.postgresql.org)
ha anunciado hoy el lanzamiento de [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html),
la versión más reciente de la base de datos de código abierto más avanzada del mundo.

PostgreSQL 17 es el resultado de décadas de desarrollo de código abierto, que ha mejorado su rendimiento y escalabilidad y le ha permitido adaptarse a los nuevos patrones de acceso y almacenamiento de datos. Esta versión de [PostgreSQL](https://www.postgresql.org) añade mejoras significativas en su rendimiento general, que incluyen una implementación revisada de la gestión de memoria para vacuum, optimizaciones en el acceso al almacenamiento y mejoras en las cargas de trabajo de alta concurrencia, mayor velocidad en la exportación y carga masiva de datos, y mejoras en la ejecución de consultas para índices. PostgreSQL 17 contiene características que benefician tanto a las nuevas cargas de trabajo como a los sistemas críticos, tales como una experiencia enriquecida para los desarrolladores gracias al comando `JSON_TABLE` de SQL/JSON, y mejoras en la replicación lógica que simplifican la gestión de las cargas de trabajo de alta disponibilidad y las actualizaciones a versiones principales.

"PostgreSQL 17 demuestra cómo la comunidad global de código abierto, que impulsa el desarrollo de PostgreSQL, realiza mejoras que ayudan a los usuarios a lo largo de todas las etapas de su viaje en el mundo de las bases de datos", dijo Jonathan Katz, miembro del Core Team de PostgreSQL. "Sea que se trate de mejoras en el funcionamiento de bases de datos a gran escala o de nuevas características que refuerzan la agradable experiencia de los desarrolladores, PostgreSQL 17 potenciará su experiencia en la administración de datos". 

PostgreSQL es un innovador sistema de gestión de datos conocido por su confiabilidad, robustez y extensibilidad. Cuenta con más de 25 años de desarrollo de código abierto por parte de una comunidad global de desarrolladores y se ha convertido en la base de datos relacional de código abierto preferida por organizaciones de todos los tamaños.

### Aumento del rendimiento en todo el sistema

El proceso de [vacuum](https://www.postgresql.org/docs/17/routine-vacuuming.html)
en PostgreSQL es fundamental para el buen estado de las operaciones, ya que requiere recursos de instancias del servidor para funcionar. PostgreSQL 17 introduce una nueva estructura de memoria interna para vacuum que consume hasta 20 veces menos memoria. Esto mejora la velocidad de vacuum y reduce el uso de recursos compartidos, ofreciendo una mayor cantidad de memoria disponible para las cargas de trabajo.

PostgreSQL 17 continúa mejorando el rendimiento de su capa de E/S. Las cargas de trabajo de alta concurrencia pueden obtener hasta el doble de rendimiento en las operaciones de escritura debido a mejoras en el procesamiento del [registro de escritura anticipada](https://www.postgresql.org/docs/17/wal-intro.html)
([WAL](https://www.postgresql.org/docs/17/wal-intro.html)).
Además, la nueva interfaz de E/S en flujo acelera los escaneos secuenciales (al leer todos los datos de una tabla) y la rapidez con la que[`ANALYZE`](https://www.postgresql.org/docs/17/sql-analyze.html) puede actualizar las estadísticas del planificador.

PostgreSQL 17 también extiende sus mejoras de rendimiento a la ejecución de consultas. De hecho, mejora el rendimiento de las consultas con cláusulas `IN` que utilizan índices
[B-tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE)
el método de índice predeterminado en PostgreSQL. Además, los índices
[BRIN](https://www.postgresql.org/docs/17/brin.html) ahora soportan construcciones paralelas. PostgreSQL 17 incluye varias mejoras para la planificación de consultas, como optimizaciones para restricciones `NOT NULL` y mejoras en el procesamiento de [expresiones de tabla comunes](https://www.postgresql.org/docs/17/queries-with.html)
([consultas `WITH`](https://www.postgresql.org/docs/17/queries-with.html)). Esta versión ofrece un mayor soporte para SIMD (Single Instruction/Multiple Data) con el fin de acelerar las operaciones de cálculo, incluyendo el uso de AVX-512 para la función
[`bit_count`](https://www.postgresql.org/docs/17/functions-bitstring.html).

### Ampliación ulterior de una sólida experiencia de los desarrolladores

PostgreSQL fue la [primera base de datos relacional en añadir soporte para JSON (2012)](https://www.postgresql.org/about/news/postgresql-92-released-1415/),
y PostgreSQL 17 amplía su implementación del estándar SQL/JSON.
[`JSON_TABLE`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE)
ya está disponible en PostgreSQL 17, lo cual permite a los desarrolladores convertir datos JSON en una tabla PostgreSQL estándar. PostgreSQL 17 ahora soporta [constructores SQL/JSON](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE)
(`JSON`, `JSON_SCALAR`, `JSON_SERIALIZE`) y
[funciones de consulta](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS)
(`JSON_EXISTS`, `JSON_QUERY`, `JSON_VALUE`) ofreciendo a los desarrolladores otras formas de interactuar con sus datos JSON. Esta versión añade más
[expresiones `jsonpath`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS),
con énfasis en la conversión de datos JSON a un tipo de datos PostgreSQL nativo, incluidos los tipos numérico, booleano, de cadena y de fecha/hora.

PostgreSQL 17 añade más características a [`MERGE`](https://www.postgresql.org/docs/17/sql-merge.html),
que se utiliza para actualizaciones condicionales, incluyendo la cláusula `RETURNING` así como la posibilidad de actualizar [vistas](https://www.postgresql.org/docs/17/sql-createview.html).
Además, PostgreSQL 17 cuenta con nuevas capacidades para la exportación y carga masiva de datos, incluida una mejora de rendimiento de hasta el doble al exportar registros de gran tamaño mediante el comando [`COPY`](https://www.postgresql.org/docs/17/sql-copy.html). El rendimiento de
`COPY` también ha mejorado en los casos en que las codificaciones de origen y destino coinciden, e incluye la nueva opción `ON_ERROR`, que hace posible continuar un proceso de importación incluso si se produce un error de inserción.

Esta versión incrementa la funcionalidad tanto para el manejo de datos en particiones como para datos distribuidos a través de instancias remotas de PostgreSQL. PostgreSQL 17 soporta el uso de columnas de identidad y restricciones de exclusión en
[tablas particionadas](https://www.postgresql.org/docs/17/ddl-partitioning.html).
El [conector de datos externos de PostgreSQL](https://www.postgresql.org/docs/17/postgres-fdw.html)
([`postgres_fdw`](https://www.postgresql.org/docs/17/postgres-fdw.html)), usado para ejecutar consultas en instancias remotas de PostgreSQL, ahora puede enviar subconsultas `EXISTS` e
`IN` al servidor remoto permitiendo un procesamiento más eficiente.

PostgreSQL 17 incorpora también un proveedor de intercalación inmutable, sin dependencia de plataforma, que garantiza su inmutabilidad y proporciona una semántica de ordenación similar a la de la intercalación aunque con codificación `UTF-8` en lugar de
`SQL_ASCII`. El uso de este nuevo proveedor de intercalación garantiza que las consultas en formato texto devuelvan los mismos resultados ordenados independientemente de dónde se ejecute PostgreSQL.

### Mejoras en la replicación lógica para alta disponibilidad y actualizaciones de versiones principales

La [replicación lógica](https://www.postgresql.org/docs/17/logical-replication.html)
se utiliza para enviar datos en tiempo real en distintos escenarios de uso. Sin embargo, antes de esta versión, los usuarios que querían realizar una actualización a una versión principal tenían que eliminar los [slots de replicación lógica](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT), 
y tras la actualización, volver a sincronizar los datos con los suscriptores. Para las actualizaciones realizadas desde PostgreSQL 17, los usuarios ya no tendrán que eliminar los slots de replicación lógica, lo cual simplifica el proceso de actualización al utilizar este tipo de replicación.

PostgreSQL 17 ahora dispone de control de failover para la replicación lógica, lo que la hace más resiliente cuando se implementa en entornos de alta disponibilidad. Además, PostgreSQL 17 introduce la herramienta de línea de comando
[`pg_createsubscriber`](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html)
para convertir una réplica física en una nueva réplica lógica.

### Más opciones para gestionar la seguridad y las operaciones

PostgreSQL 17 amplía aún más la forma en que los usuarios pueden gestionar el ciclo de vida completo de sus sistemas de bases de datos. PostgreSQL cuenta con la nueva opción TLS, `sslnegotiation`, que permite a los usuarios realizar un handshake TLS directo cuando se utiliza
[ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation)
(registrado como `postgresql` en el directorio ALPN). PostgreSQL 17 también añade el [rol predefinido](https://www.postgresql.org/docs/17/predefined-roles.html) `pg_maintain`,
que otorga a los usuarios permiso para realizar operaciones de mantenimiento.

[`pg_basebackup`](https://www.postgresql.org/docs/17/app-pgbasebackup.html), la utilidad incluida en PostgreSQL que permite realizar respaldos, ahora soporta respaldos incrementales y agrega la utilidad [`pg_combinebackup`](https://www.postgresql.org/docs/17/app-pgcombinebackup.html) 
para reconstruir un respaldo completo. Además,
[`pg_dump`](https://www.postgresql.org/docs/17/app-pgdump.html) incluye una nueva opción llamada `--filter` que permite seleccionar qué objetos incluir al generar un archivo de volcado.

PostgreSQL 17 también incluye mejoras en las funciones de monitoreo y análisis.
[`EXPLAIN`](https://www.postgresql.org/docs/17/sql-explain.html) ahora muestra el tiempo empleado para lecturas y escrituras de bloques de E/S locales, e incluye dos nuevas opciones:
`SERIALIZE` y `MEMORY`, útiles para visualizar el tiempo empleado en la conversión de datos destinados a la transmisión por red, así como la cantidad de memoria utilizada. PostgreSQL 17 ahora reporta el [progreso del proceso de vacuum en índices](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING),
y añade la vista del sistema [`pg_wait_events`](https://www.postgresql.org/docs/17/view-pg-wait-events.html)
que, combinada con [`pg_stat_activity`](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
proporciona más información sobre las razones por las que una sesión activa se encuentra en estado de espera.

### Características adicionales

Se han añadido numerosas nuevas características y mejoras a PostgreSQL 17 que podrían ser útiles para sus casos de uso. Consulten las
[notas de la versión](https://www.postgresql.org/docs/17/release-17.html) para obtener una lista completa de las características nuevas y modificadas.

### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) es la base de datos de código abierto más avanzada del mundo, que cuenta con una comunidad global de miles de usuarios, contribuidores, empresas y organizaciones. Basada en más de 35 años de ingeniería, que comenzaron en la Universidad de Berkeley en California, PostgreSQL ha continuado con un ritmo de desarrollo inigualable. El maduro conjunto de características de PostgreSQL no sólo iguala a los principales sistemas de bases de datos propietarios, sino que los supera en términos de características avanzadas, extensibilidad, seguridad y estabilidad.

### Enlaces

* [Descargas](https://www.postgresql.org/download/)
* [Notas de la versión](https://www.postgresql.org/docs/17/release-17.html)
* [Kit de prensa](https://www.postgresql.org/about/press/)
* [Información de seguridad](https://www.postgresql.org/support/security/)
* [Política de versiones](https://www.postgresql.org/support/versioning/)
* [Sigan @postgresql](https://twitter.com/postgresql)
* [Donaciones](https://www.postgresql.org/about/donate/)

## Más información sobre las características
Para más información sobre las características antes mencionadas y otras más, consulten los siguientes recursos:

* [Notas de la versión](https://www.postgresql.org/docs/17/release-17.html)
* [Matriz de características](https://www.postgresql.org/about/featurematrix/)

## Dónde descargarlo

Hay varias maneras de descargar PostgreSQL 17, entre ellas:

* La [página oficial de descargas](https://www.postgresql.org/download/) que contiene instaladores y herramientas para [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/), etc.
* [Código fuente](https://www.postgresql.org/ftp/source/v17.0)

Otras herramientas y extensiones están disponibles en el
[PostgreSQL Extension Network](http://pgxn.org/).

## Documentación

PostgreSQL 17 incluye documentos HTML y páginas de manual. Es posible también consultar la documentación en línea en formato [HTML](https://www.postgresql.org/docs/17/) y [PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-US.pdf).

## Licencia

PostgreSQL utiliza la [PostgreSQL License](https://www.postgresql.org/about/licence/),
una licencia "permisiva" de tipo BSD. Esta
[licencia certificada por la OSI](http://www.opensource.org/licenses/postgresql/) es ampliamente apreciada por ser flexible y adecuada para las empresas, ya que no limita el uso de PostgreSQL con aplicaciones comerciales y propietarias. Junto con el soporte para múltiples empresas y la propiedad pública del código, nuestra licencia hace que PostgreSQL sea muy popular entre los proveedores que desean integrar una base de datos en sus propios productos sin tener que preocuparse por tarifas, dependencia de un único proveedor o cambios en los términos de la licencia.

## Contactos

Sitio web

* [https://www.postgresql.org/](https://www.postgresql.org/)

Correo electrónico

* [press@postgresql.org](mailto:press@postgresql.org)

## Imágenes y logotipos

Postgres, PostgreSQL y el logo del elefante (Slonik) son todas marcas registradas de la [PostgreSQL Community Association](https://www.postgres.ca).
Quien desee utilizar estas marcas, deberá cumplir con la [política de marca](https://www.postgresql.org/about/policies/trademarks/).

## Soporte corporativo y donaciones

PostgreSQL cuenta con el soporte de numerosas empresas, que patrocinan a los desarrolladores, ofrecen recursos de alojamiento y nos dan apoyo financiero. Consulten nuestra página de
[patrocinadores](https://www.postgresql.org/about/sponsors/) para conocer algunos de los que dan soporte al proyecto.

Existe también una gran comunidad de
[empresas que ofrecen soporte para PostgreSQL](https://www.postgresql.org/support/professional_support/),
desde consultores individuales hasta empresas multinacionales.

Si desean hacer una contribución financiera al Grupo Global de Desarrollo de PostgreSQL o a una de las organizaciones sin fines de lucro reconocidas por la comunidad, visiten nuestra página de [donaciones](https://www.postgresql.org/about/donate/) page.
