26 September 2024 - [PostgreSQL Global Development Group](https://www.postgresql.org)
presenterade idag [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html),
den senaste versionen av världens mest avancerade databas byggd med öppen källkod.

PostgreSQL 17 bygger vidare på årtionden av utveckling med öppen källkod, och
fortsätter förbättra prestandan och skalbarheten samtidigt som databasen
anpassas för nya användningsmönster och trender inom databashantering. Denna
versionen av [PostgreSQL](https://www.postgresql.org) har betydligt förbättrad
systemprestanda, bland annat genom optimerad minneshantering för vacuum,
optimering av I/O operationer och parallellisering, förbättringar för
import av data samt bättre utnyttjande av index vid
databasfrågor.  PostgreSQL 17 passar de flesta typer av användningsmönster, och
adderar nya funktioner för utvecklare som SQL/JSON `JSON_TABLE` och
förbättringar av logisk replikering för att ge högre tillgänglighet och
förenklad uppgradering.

"PostgreSQL 17 visar hur den globala gruppen utvecklare, vilka ligger bakom 
utvecklingen av PostgreSQL, fortsätter att förbättra och förenkla för alla
databasanvändare", säger Jonathan Katz, ledamot i PostgreSQL core team.
"Oavsett om det är förenkling av storskalig databasdrift eller nya funktioner
för förenkla för utvecklare, så kommer PostgreSQL 17 förbättra din organisations
datahantering."

PostgreSQL är känt för pålitlighet, stabilitet och tillförlitlighet. Med mer än
25 års utveckling med öppen källkod av en global grupp av utvecklare har
PostgreSQL blivit den mest populära relationsdatabasen byggd på öppen källkod
för organisationer av alla storlekar.

### Prestandaförbättringar

[Vacuum](https://www.postgresql.org/docs/17/routine-vacuuming.html) är en
kritisk komponent för PostgreSQL i produktion.  I PostgreSQL 17 har
minneshanteringen för vacuum effektiviserats och använder nu upp till 20 gånger
mindre minne. Detta gör vacuum snabbare samtidigt som det frigör mer 
systemresurser för databasfrågor.

PostgreSQL 17 har också förbättrad prestanda för I/O-operationer, upp till
fördubblad prestanda vid skrivning tack vare förbättringar av
[write-ahead log](https://www.postgresql.org/docs/17/wal-intro.html)
([WAL](https://www.postgresql.org/docs/17/wal-intro.html)) hantering.

Utöver det finns också ny funktionalitet för strömmande I/O vilket gör
sekventiell läsning av tabeller snabbare samt effektiviserar uppdatering av
statistik med [`ANALYZE`](https://www.postgresql.org/docs/17/sql-analyze.html).

Prestanda för databasfrågor har också förbättrats i PostgreSQL 17. Frågor med
`IN` satser som använder
[B-tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE)
index, vilket är standard indexmetod i PostgreSQL, är nu snabbare.
[BRIN](https://www.postgresql.org/docs/17/brin.html) index kan nu byggas med
parallella processer och frågeplanering har förbättrats avsevärt. Bland annat
har `NOT NULL` begränsningar och 
[common table expressions](https://www.postgresql.org/docs/17/queries-with.html)
([`WITH` frågor](https://www.postgresql.org/docs/17/queries-with.html)) optimerats.
Denna versionen utökar också användningen av CPU-acceleration med SIMD (Single
Instruction/Multiple Data), bland annat med AVX-512 för
[`bit_count`](https://www.postgresql.org/docs/17/functions-bitstring.html)
funktionen.

### Utvecklarupplevelse

PostgreSQL var 2012
[den första relationsdatabasen att addera stöd för JSON](https://www.postgresql.org/about/news/postgresql-92-released-1415/),
och PostgreSQL 17 fortsätter att utöka SQL/JSON stödet.
[`JSON_TABLE`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE)
låter utvecklare konvertera JSON data till en normal databas tabell. PostgreSQL
17 utökar stöd för att skapa
[SQL/JSON](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE)
med standardiserade funktioner (`JSON`, `JSON_SCALAR`, `JSON_SERIALIZE`), och
skriva databasfrågor med
[SQL/JSON funktioner](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS)
(`JSON_EXISTS`, `JSON_QUERY`, `JSON_VALUE`). Detta ger utvecklare större
flexibilitet i hur data i JSON format kan användas.  Fler
[`jsonpath` operationer](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS),
stöds också, speciellt för konvertering av JSON data till PostgreSQL's datatyper
för numeriska värden, datum/tid, strängar m.fl.

Stödet för [`MERGE`](https://www.postgresql.org/docs/17/sql-merge.html), vilken
används för villkorade uppdateringar, utökas med en `RETURNING` sats samt
funktion för att uppdatera
[databasvyer](https://www.postgresql.org/docs/17/sql-createview.html).
PostgreSQL 17 har också nya funktioner för import eller export av stora volymer
med upp fördubblad prestanda i
[`COPY`](https://www.postgresql.org/docs/17/sql-copy.html) operationen. `COPY`
kan också känna av om datakällan har samma kodning som destinationen och kan
optimera för detta. Funktionen `ON_ERROR` medger fortsatt import av data även om
det uppstår fel vid inläsning.


PostgreSQL 17 innehåller också förbättringar för hantering av data som fördelats
i partitioner samt data distribuerad över flera PostgreSQL instanser på
fjärrservrar.
Identity kolumner och exclusion constraints stöds i och med PostgreSQL 17 för
[partitionerade tabeller](https://www.postgresql.org/docs/17/ddl-partitioning.html).
[PostgreSQL foreign data wrapper](https://www.postgresql.org/docs/17/postgres-fdw.html)
([`postgres_fdw`](https://www.postgresql.org/docs/17/postgres-fdw.html)), vilken
används för att exekvera frågor på andra PostgreSQL instanser, kan nu flytta
hantering av `EXISTS` och `IN` satser till fjärrservern för effektivare
databasfrågor.

PostgreSQL 17 inkluderar också en inbyggd plattformsoberoende sorteringsordning
som garanterar konsekvent och oförändrad sortering med `UTF-8` stöd liknande
hur `C` sortering fungerar för `SQL_ASCII`. Med denna kommer
databasfrågor över text-data returnera samma sorteringsordning oavsett var
PostgreSQL körs.

### Förbättring av logisk replikering för hög tillgänglighet och systemuppgradering

[Logisk replikering](https://www.postgresql.org/docs/17/logical-replication.html)
kan användas för att strömma data i realtid för en mängd användningsområden. Före
denna versionen behövde användare ta bort
[slots för logisk replikering](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT)
vid uppgradering mellan huvudversioner, vilket krävde omsynkronisering av data
mellan prenumeranter efter uppgraderingen. I och med uppgraderingar från 
PostgreSQL 17 behöver dessa inte längre tas bort, vilket förenklar processen.

PostgreSQL 17 inkluderar kontroll vid failover för logisk replikering vilket
gör det enklare att använda i miljöer vilka kräver hög tillgänglighet.
Kommandoradsverktyget
[`pg_createsubscriber`](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html)
har också lagts till vilket används för att konvertera en fysiskt replikerad
nod till en logiskt replikerad nod.


### Utökad funktionalitet för hantering av säkerhet och drift

PostgreSQL 17 har vidareutvecklats för att ge utökad flexibilitet vid drift.
En ny TLS parameter, `sslnegotiation`,
har adderats vilken låter användare direkt utföra en TLS anslutning vid användning av
[ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation)
(registrerat som `postgresql` i ALPN katalogen).
PostgreSQL 17 har också utökats med den 
[fördefinierade rollen](https://www.postgresql.org/docs/17/predefined-roles.html),
`pg_maintain` vilken ger användare rättigheter att utföra olika former av
underhållsarbeten.

[`pg_basebackup`](https://www.postgresql.org/docs/17/app-pgbasebackup.html),
säkerhetskopieringsverktyget som ingår i PostgreSQL, stödjer nu inkrementell
säkerhetskopiering och har utökats med
[`pg_combinebackup`](https://www.postgresql.org/docs/17/app-pgcombinebackup.html),
ett verktyg för att rekonstruera en full säkerhetskopia. Utöver det har
[`pg_dump`](https://www.postgresql.org/docs/17/app-pgdump.html) en ny funktion,
`--filter`, vilken ger större kontroll över vilka objekt som ska ingå i en
databasdump.

PostgreSQL 17 har också förbättrats gällande övervakning och systemanalys.
[`EXPLAIN`](https://www.postgresql.org/docs/17/sql-explain.html) visar nu tid
spenderad för läsning och skrivning; och har två nya parametrar `SERIALIZE` och
`MEMORY` vilka är användbara för att se hur mycket tid som spenderats med att
konvertera och skicka data över nätverket, samt hur mycket minne som använts.
I PostgreSQL 17 visas nu hur långt
[vacuum av index](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING)
har kommit, och den nya systemvyn
[`pg_wait_events`](https://www.postgresql.org/docs/17/view-pg-wait-events.html)
kan i kombination med
[`pg_stat_activity`](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW)
användas för att ta reda på varför aktiva sessioner väntar.

### Mer förbättringar

Mycket fler förbättringar och fler nya funktion har också adderats till
PostgreSQL 17 för att bättre hantera alla användningsfall.  Se 
[listan över nyheter](https://www.postgresql.org/docs/17/release-17.html) för
en överblick över alla nya och förbättrade funktioner.

### Om PostgreSQL

[PostgreSQL](https://www.postgresql.org) är världens mest avancerade databas
byggd på öppen källkod, med tusentals användare, utvecklare, företag och 
organisationer världen över. Med över 35 års utveckling sedan starten på
University of California, Berkeley, har PostgreSQL fortsatt utvecklas med
en enastående fart. PostgreSQL:s väl etablerade funktioner är inte bara
jämförbara med proprietära databassystem, utan överträffar dem när det gäller
avancerade databasfunktioner, utbyggbarhet och modularitet, säkerhet och
stabilitet.

### Länkar

* [Nerladdning](https://www.postgresql.org/download/)
* [Nyheter i version 17](https://www.postgresql.org/docs/17/release-17.html)
* [Press Kit](https://www.postgresql.org/about/press/)
* [Säkerhet](https://www.postgresql.org/support/security/)
* [Versionspolicy](https://www.postgresql.org/support/versioning/)
* [Följ @postgresql](https://twitter.com/postgresql)
* [Donationer](https://www.postgresql.org/about/donate/)

### Fördjupad information

För ingående förklaringar och dokumentation av de ovan nämnda funktionerna, och
mer, se följande resurser:

* [Nyheter i version 17](https://www.postgresql.org/docs/17/release-17.html)
* [Funktioner per version](https://www.postgresql.org/about/featurematrix/)

### Nerladdning

PostgreSQL 17 kan laddas ner på ett flertal olika sätt, bland annat:

* De [officiella nerladdningarna](https://www.postgresql.org/download/),
  vilka tillhandahåller installationspaket för
  [Windows](https://www.postgresql.org/download/windows/),
  [Linux](https://www.postgresql.org/download/linux/),
  [macOS](https://www.postgresql.org/download/macosx/) med flera.
* [Källkod](https://www.postgresql.org/ftp/source/v17.0)

Andra verktyg och tilläggsmoduler finns tillgängliga via
[PostgreSQL Extension Network](http://pgxn.org/).

### Dokumentation

PostgreSQL 17 levereras med dokumentation i HTML-format samt man-sidor, och
dokumentationen kan också läsas online i både
[HTML-format](https://www.postgresql.org/docs/17/) och som
[PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-A4.pdf).

### Licens

PostgreSQL är licensierad med [PostgreSQL Licensen](https://www.postgresql.org/about/licence/),
en BSD-liknande "tillåtande" licens.
Denna [OSI-certifierade licens](http://www.opensource.org/licenses/postgresql/)
anses flexibel och företagsvänlig eftersom den inte begränsar användningen av
PostgreSQL i kommersiella eller proprietära applikationer. Licensen,
tillsammans med brett stöd från många företag och ett publikt ägande av koden,
gör att PostgreSQL är väldigt populär bland tillverkare som vill bygga in en
databas i sin produkt utan att riskera avgifter, inlåsning eller förändrade
licensvillkor.

### Kontaktinformation

Hemsida

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-post

* [press@postgresql.org](mailto:press@postgresql.org)

### Bilder och logotyper

Postgres, PostgreSQL, och elefantlogotypen (Slonik) är av
[PostgreSQL Community Association](https://www.postgres.ca) registrerade
varumärken. Användning av dess varumärken måste följa dess
[varumärkespolicy](https://www.postgresql.org/about/policies/trademarks/).

### Kommersiell support

PostgreSQL projektet stöttas av ett stort antal företag som sponsrar utvecklare,
erbjuder infrastruktur och ger finansiellt stöd. Se listan över PostgreSQL:s
[sponsors](https://www.postgresql.org/about/sponsors/) för mer information.

Det finns också många
[företag som säljer tjänster kring PostgreSQL](https://www.postgresql.org/support/professional_support/),
allt från små konsultbolag till multinationella företag.

För att ge ett ekonomiskt bidrag till PostgreSQL Global Development Group eller
till en av de officiellt erkända ideella organisationerna, se sidan för
[donationer](https://www.postgresql.org/about/donate/) för mer information.
