26 de Setembro de 2023 - O [Grupo de Desenvolvimento Global do PostgreSQL](https://www.postgresql.org)
anunciou hoje o lançamento do [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html),
a versão mais recente do banco de dados de código aberto mais avançado do mundo.

PostgreSQL 17 se baseia em décadas de desenvolvimento de código aberto,
melhorando seu desempenho e escalabilidade ao mesmo tempo em que se adapta ao
padrões emergentes de acesso a armazenamento de dados. Esta versão do
[PostgreSQL](https://www.postgresql.org) adiciona ganhos significativos de
performance em geral, incluindo uma revisão da implementação do gerenciamento
de memória do vacuum, otimizações no acesso ao armazenamento e melhorias para
cargas de trabalho de alta concorrência, melhorias no carregamento e exportação
de dados e melhorias na execução de consultas com índices. PostgreSQL 17 tem
funcionalidades que beneficiam cargas de trabalho totalmente novas e sistemas
críticos, como adições à experiência do desenvolvedor com comando `JSON_TABLE`
do SQL/JSON, e melhorias na replicação lógica que simplificam o gerenciamento
de cargas de trabalho de alta disponibilidade e atualizações de versões.

"PostgreSQL 17 ressalta como uma comunidade global de código aberto, que
impulsiona o desenvolvimento do PostgreSQL, realiza melhorias que ajudam
usuários em todos os estágios da jornada no universo do banco de dados", disse
Jonathan Katz, um membro da equipe principal do PostgreSQL. "Quer se trate de
melhorias para operação de bancos de dados em escala ou de novas
funcionalidades que se baseiam em uma experiência agradável para o
desenvolvedor, isso aprimorará sua experiência em gerenciamento de dados".

PostgreSQL, um sistema de gerenciamento de dados inovador conhecido pela sua
confiabilidade, robustez e extensibilidade, se beneficia de mais de 25 anos de
desenvolvimento de código aberto de uma comunidade global de desenvolvedores e
se tornou o banco de dados relacional de código aberto preferido pelas
organizações de todos os tamanhos.

### Ganhos de performance em todo o sistema

O processo de [vacuum](https://www.postgresql.org/docs/17/routine-vacuuming.html)
do PostgreSQL é crítico para operações saudáveis, exigindo recursos da
instância do servidor para operar. O PostgreSQL 17 introduz uma nova estrutura
de memória interna para vacuum que consome até 20 vezes menos memória. Isso
melhora a velocidade de execução do vacuum e também reduz o uso de recursos
compartilhados, tornando-os mais disponíveis para sua carga de trabalho.

O PostgreSQL 17 continua melhorando a performance da sua camada de I/O. Cargas
de trabalho de alta concorrência podem ter uma taxa de escrita até duas vezes
melhor por causa de melhorias no processamento do
[registro de escrita prévia](https://www.postgresql.org/docs/17/wal-intro.html)
([WAL](https://www.postgresql.org/docs/17/wal-intro.html)). Adicionalmente, uma
nova interface de transmissão de I/O acelera buscas sequenciais (lendo todos os
dados de uma tabela) e com que rapidez o
[`ANALYZE`](https://www.postgresql.org/docs/17/sql-analyze.html) pode atualizar
as estatísticas do planejador.

O PostgreSQL 17 também estende seus ganhos de performance para execução de
consultas. O PostgreSQL 17 melhora a performance de consultas com cláusulas
`IN` que usam índices
[B-tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE),
o método padrão de indexação no PostgreSQL. Adicionalmente, índices
[BRIN](https://www.postgresql.org/docs/17/brin.html) agora suportam construção
em paralelo. O PostgreSQL 17 inclui várias melhorias para planejamento de
consultas, incluindo otimizações em restrições `NOT NULL` e melhorias no
processamento de
[common table expressions](https://www.postgresql.org/docs/17/queries-with.html)
([consultas `WITH`](https://www.postgresql.org/docs/17/queries-with.html)). Esta
versão adiciona mais suporte a SIMD (Single Instruction/Multiple Data) para
acelerar cálculos, incluindo uso de AVX-512 para a função
[`bit_count`](https://www.postgresql.org/docs/17/functions-bitstring.html).


### Expansão adicional de uma sólida experiência dos desenvolvedores

O PostgreSQL foi o [primeiro banco de dados relacional a adicionar suporte a JSON (2012)](https://www.postgresql.org/about/news/postgresql-92-released-1415/),
e o PostgreSQL 17 adiciona à sua implementação do padrão SQL/JSON.
[`JSON_TABLE`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE)
agora está disponível no PostgreSQL 17, permitindo que desenvolvedores
convertam dados JSON em uma tabela padrão do PostgreSQL. O PostgreSQL 17 agora
suporta [construtores SQL/JSON](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE)
(`JSON`, `JSON_SCALAR`, `JSON_SERIALIZE`) e
[funções de consulta](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS)
(`JSON_EXISTS`, `JSON_QUERY`, `JSON_VALUE`), fornecendo a desenvolvedores
outras formar de interagir com seus dados JSON. Esta versão adiciona mais
[expressões `jsonpath`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS),
com uma ênfase na conversão de dados JSON em um tipo de dados nativo do
PostgreSQL, incluindo tipos numérico, booleano, cadeia de caracteres e
data/hora.

O PostgreSQL 17 adiciona mais funcionalidades ao [`MERGE`](https://www.postgresql.org/docs/17/sql-merge.html),
que é utilizado para atualizações condicionais, incluindo uma cláusula
`RETURNING` e a capacidade de atualizar
[visões](https://www.postgresql.org/docs/17/sql-createview.html).
Adicionalmente, o PostgreSQL 17 possui novos recursos para carregamento em
massa e exportação de dados, incluindo uma melhoria de desempenho de até duas
vezes ao exportar registros grandes utilizando o comando
[`COPY`](https://www.postgresql.org/docs/17/sql-copy.html). A performance do
`COPY` também tem melhorias quando a codificação de caracteres é a mesma na
origem e no destino, e inclui uma nova opção, `ON_ERROR`, que permite uma
importação continuar mesmo se há uma erro de inserção.

Esta versão expande a funcionalidade para gerenciar dados em partições e dados
distribuídos entre instâncias remotas do PostgreSQL. O PostgreSQL 17 suporta
utilizar colunas identidade e restrições de exclusão em
[tabelas particionadas](https://www.postgresql.org/docs/17/ddl-partitioning.html).
O [adaptador de dados externos do PostgreSQL](https://www.postgresql.org/docs/17/postgres-fdw.html)
([`postgres_fdw`](https://www.postgresql.org/docs/17/postgres-fdw.html)),
utilizado para executar consultas em instâncias remotas do PostgreSQL, agora
pode enviar subconsultas `EXISTS` e `IN` para o servidor remoto para um
processamento mais eficiente.

O PostgreSQL 17 também inclui um provedor de ordenações embutido, independente
de plataforma e imutável que é garantidamente imutável e fornece semântica de
ordenação semelhante à ordenação `C`, exceto pela codificação `UTF-8` em vez de
`SQL_ASCII`. Utilizar este novo provedor de ordenação garante que suas
consultas baseadas em texto retornarão os mesmo resultados ordenados,
independentemente de onde você executar o PostgreSQL.

### Melhorias na replicação lógica para alta disponibilidade e atualizações de versão

[Replicação lógica](https://www.postgresql.org/docs/17/logical-replication.html)
é utilizada para enviar dados em tempo real em muitos casos de uso. Contudo,
antes desta versão, usuários que queriam fazer uma atualização de versão tinham
que remove [slots de replicação lógica](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT), que requer ressincronizar dados para os assinantes após uma
atualização. Iniciando com atualizações do PostgreSQL 17, usuários não tem que
remover os slots de replicação lógica, simplificando o processo de atualização
quando estiver utilizando replicação lógica.

PostgreSQL 17 agora inclui controle de failover para replicação lógica,
tornando-o mais resiliente quando implantado em ambientes de alta
disponibilidade. Adicionalmente, PostgreSQL 17 introduz uma ferramenta de linha
de comando [`pg_createsubscriber`](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html)
para converter uma réplica física em uma nova réplica lógica.

### Mais opções para gerenciamento de segurança e operações

O PostgreSQL 17 amplia ainda mais como os usuários gerenciam o ciclo de vida
geral de seus sistemas de bancos de dados. O PostgreSQL tem uma nova opção TLS,
`sslnegotiation`, que permite aos usuários realizar handshakes TLS diretos
quando utilizar
[ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation)
(registrado como `postgresql` no diretório ALPN). O PostgreSQL 17 também
adiciona uma
[role predefinida](https://www.postgresql.org/docs/17/predefined-roles.html)
`pg_maintain`, que dá aos usuários permissão para realizar operações de
manutenção.

O [`pg_basebackup`](https://www.postgresql.org/docs/17/app-pgbasebackup.html),
a ferramenta de cópia de segurança incluída no PostgreSQL, agora suporta cópias
de segurança incrementais e adiciona a ferramenta
[`pg_combinebackup`](https://www.postgresql.org/docs/17/app-pgcombinebackup.html) 
para reconstruir uma cópia de segurança completa. Adicionalmente, o
[`pg_dump`](https://www.postgresql.org/docs/17/app-pgdump.html) inclui uma nova
opção chamada `--filter` que permite selecionar quais objetos incluir ao gerar
o arquivo.

O PostgreSQL 17 também inclui melhorias nas funcionalidades de monitoramento e
análise. [`EXPLAIN`](https://www.postgresql.org/docs/17/sql-explain.html) agora
mostra o tempo gasto para leituras e escritas de blocos de I/O locais, e inclui
duas novas opções: `SERIALIZE` e `MEMORY`, úteis para visualizar o tempo gasto
em conversão de dados para transmissão pela rede, e quanto de memória foi
utilizada. O PostgreSQL 17 agora relata o
[progresso de limpeza dos índices](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING),
e adiciona a visão do sistema
[`pg_wait_events`](https://www.postgresql.org/docs/17/view-pg-wait-events.html)
que, quando combinada com
[`pg_stat_activity`](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
fornece mais informações sobre por que uma sessão ativa está esperando.

### Funcionalidades Adicionais

Muitas outras novas funcionalidades e melhorias foram adicionadas ao PostgreSQL
17 que também podem ser úteis para os seus casos de uso. Veja as
[notas de lançamento](https://www.postgresql.org/docs/17/release-17.html) para
a lista completa de funcionalidades novas e modificadas.

### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) é o banco de dados mais avançado do
mundo, com uma comunidade global de milhares de usuários, colaboradores,
empresas e organizações. O Projeto PostgreSQL baseia-se em mais de 35 anos de
engenharia, iniciando na Universidade da Califórnia, Berkeley, e continua em um
ritmo inigualável de desenvolvimento. Conjunto de funcionalidades maduras do
PostgreSQL não só se igualam aos principais sistemas de bancos de dados
proprietários, mas os supera em funcionalidades avançadas, extensibilidade,
segurança e estabilidade.

### Links

* [Download](https://www.postgresql.org/download/)
* [Notas de Lançamento](https://www.postgresql.org/docs/17/release-17.html)
* [Kit à Imprensa](https://www.postgresql.org/about/press/)
* [Informação sobre Segurança](https://www.postgresql.org/support/security/)
* [Política de Versionamento](https://www.postgresql.org/support/versioning/)
* [Siga @postgresql](https://twitter.com/postgresql)
* [Doação](https://www.postgresql.org/about/donate/)

## Mais Sobre as Funcionalidades

Para explicação sobre as funcionalidades acima e outras, consulte os seguintes links:

* [Notas de Lançamento](https://www.postgresql.org/docs/17/release-17.html)
* [Matriz de Funcionalidades](https://www.postgresql.org/about/featurematrix/)

## Onde Baixar

Há várias maneiras de fazer uma cópia do PostgreSQL 17, incluindo:

* A página [oficial de Downloads](https://www.postgresql.org/download/) que contém instaladores e ferramentas para [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/linux/), [macOS](https://www.postgresql.org/download/macosx/) e mais.
* [Código Fonte](https://www.postgresql.org/ftp/source/v17.0)

Outras ferramentas e extensões estão disponíveis na [PostgreSQL Extension
Network](http://pgxn.org/).

## Documentação

O PostgreSQL 17 vem com documentação em HTML bem como páginas man, e você
também pode navegar na documentação online nos formatos
[HTML](https://www.postgresql.org/docs/17/) e
[PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-US.pdf).

## Licença

O PostgreSQL usa a [PostgreSQL
License](https://www.postgresql.org/about/licence/), uma licença "permissiva"
do tipo BSD. Esta [licença certificada pela
OSI](http://www.opensource.org/licenses/postgresql/) é amplamente apreciada
como flexível e amigável aos negócios, uma vez que não restringe o uso do
PostgreSQL com aplicações comerciais e proprietárias. Juntamente com o suporte
de múltiplas empresas e a propriedade pública do código fonte, nossa licença
torna o PostgreSQL muito popular entre os fornecedores que desejam incorporar
um banco de dados em seus produtos sem o medo de taxas, dependência de
fornecedor ou alterações nos termos de licenciamento.

## Contatos

Página Web

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

## Imagens e Logotipos

Postgres, PostgreSQL e o Logotipo do Elefante (Slonik) são todas marcas
registradas da [PostgreSQL Community Association](https://www.postgres.ca). Se
você deseja utilizar estas marcas, você deve estar em conformidade com a
[política de marcas
registradas](https://www.postgresql.org/about/policies/trademarks/).


## Suporte Corporativo e Doações

O PostgreSQL conta com o apoio de inúmeras empresas, que financiam
desenvolvedores, fornecem recursos de hospedagem e nos dão suporte financeiro.
Veja nossa página de
[patrocinadores](https://www.postgresql.org/about/sponsors/) para alguns desses
apoiadores do projeto.

Há também uma grande comunidade de [empresas que oferecem suporte ao
PostgreSQL](https://www.postgresql.org/support/professional_support/), de
consultores individuais a empresas multinacionais.

Se você deseja fazer uma contribuição financeira para o Grupo de
Desenvolvimento Global do PostgreSQL ou uma das organizações comunitárias sem
fins lucrativos reconhecidas, visite nossa página de
[doações](https://www.postgresql.org/about/donate/).
