26. September 2024 - Die [PostgreSQL Global Development Group](https://www.postgresql.org) gab heute die Veröffentlichung von [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html) bekannt, der neuesten Version der weltweit fortschrittlichsten Open-Source-Datenbank.

Aufbauend auf jahrzehntelanger Open Source Entwicklung wird in PostgreSQL 17 die Leistung und Skalierbarkeit verbessert, während es weiter an die sich verändernden Anforderungen in der Datenspeicherung und -nutzung angepasst wird.

Diese Version von [PostgreSQL](https://www.postgresql.org) bringt deutliche Performancezuwächse mit, unter anderem durch eine überarbeitete Speicherverwaltung für VACUUM, Optimierungen beim Festplattenzugriff und Verbesserungen bei hoher Parallelität, beschleunigte Bulk Be- und entladung von Daten und Verbesserungen bei der Nutzung von Indexen.

Sowohl für neue Nutzungsarten, als auch vorhandene, kritische Systeme kommt PostgreSQL 17 mit neuen Features, wie z.B. eine neue Developer Experience in Form des SQL/JSON "JSON_TABLE" Kommandos oder auch Verbesserungen bei der logischen Replikation, die sowohl dem Betrieb hochverfügbarer Umgebungen als auch Major-Upgrades zugute kommen.

“PostgreSQL 17 zeigt anschaulich, wie die weltweite Open Source Gemeinschaft, die die Entwicklung von PostgreSQL vorantreibt, für jeden Benutzer Verbesserungen implementiert, egal wo diese gerade auf ihrem Weg in der Datenbankwelt sind”, so Jonathan Katz, Mitglied des PostgreSQL Kern-Teams. “Ob es Verbesserungen für den Betrieb ganz großer Umgebungen sind oder einfach ein angenehmeres Entwickeln, PostgreSQL 17 wird dein persönliches Erlebnis verbessern.”

PostgreSQL, ein innovatives Datenverwaltungssystem, welches für seine Zuverlässigkeit und Robustheit bekannt ist, profitiert von über 25 Jahren Open-Source-Entwicklung einer globalen Entwicklergemeinschaft und hat sich zur bevorzugten relationalen Open-Source-Datenbank für Organisationen jeder Größe entwickelt.

### Systemweite Performance-Zuwächse

Der PostgreSQL [VACUUM-Prozess](https://www.postgresql.org/docs/17/routine-vacuuming.html) ist von entscheidender Bedeutung für einen zuverlässigen und effektiven Betrieb, benötigt aber auch Server-Ressourcen im laufenden Betrieb. PostgreSQL 17 führt hierfür eine neue interne Speicherstruktur ein, die bis zu 20 mal weniger RAM belegt. Dies beschleunigt nicht nur den VACUUM-Prozess an sich, sondern verringert auch den Fußabdruck bei Ressourcen, die somit für andere Aufgaben zur Verfügung stehen.

PostgreSQL 17 verbessert erneut den Durchsatz beim I/O. Verbesserungen beim Schreiben des Write-Ahead Log ([WAL](https://www.postgresql.org/docs/17/wal-intro.html)) führen vor allem bei sehr vielen gleichzeitigen Anfragen zu zum Teil doppeltem Schreibdurchsatz.
Die Einführung des Streaming I/O Interface beschleunigt das sequentielle Lesen
(das Lesen aller Daten einer Tabelle) und die Arbeitsgeschwindigkeit von 
["ANALYZE"](https://www.postgresql.org/docs/17/sql-analyze.html) beim Aktualisieren der Statistiken für den Query-Planner.

PostgreSQL 17 verbessert die Geschwindigkeit der Ausführung von Abfragen.
Außerdem wurde die Leistung von Abfragen mit "IN"-Klauseln, die [B-Tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE) Indizes verwenden, die Standardindexmethode in PostgreSQL, gesteigert. Darüber hinaus unterstützen [BRIN](https://www.postgresql.org/docs/17/brin.html)-Indizes jetzt die parallele Erstellung.

PostgreSQL 17 enthält mehrere Verbesserungen für die Abfrageplanung, darunter Optimierungen für "NOT NULL"-Einschränkungen und Verbesserungen bei der Verarbeitung von [common table expressions](https://www.postgresql.org/docs/17/queries-with.html) ("WITH"-Abfragen).

Diese Version fügt mehr SIMD-Unterstützung (Single Instruction/Multiple Data) zur Beschleunigung von Berechnungen hinzu, einschließlich der Verwendung von AVX-512 für die ["bit_count"](https://www.postgresql.org/docs/17/functions-bitstring.html) Funktion.

### Weiterer Ausbau einer robusten Developer Experience

PostgreSQL war die [erste relationale Datenbank mit JSON-Unterstützung (2012)](https://www.postgresql.org/about/news/postgresql-92-released-1415/), und PostgreSQL 17 ergänzt die Implementierung des SQL/JSON-Standards.
["JSON_TABLE"](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE) ist jetzt in PostgreSQL 17 verfügbar und ermöglicht Entwicklern die Konvertierung von JSON-Daten in eine Standard-PostgreSQL-Tabelle. Außerdem unterstützen [SQL/JSON-Konstruktoren](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE) jetzt ("JSON", "JSON_SCALAR", "JSON_SERIALIZE") und [Abfragefunktionen](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS) ("JSON_EXISTS", "JSON_QUERY", "JSON_VALUE"), was Entwicklern alternative Möglichkeiten bietet, mit den JSON-Daten zu arbeiten. Diese Version fügt weitere ["jsonpath"-Ausdrücke](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS) hinzu, mit dem Schwerpunkt auf der Konvertierung von JSON-Daten in einen nativen PostgreSQL-Datentyp, einschließlich numerischer, boolescher, Zeichenfolgen- und Datums-/Zeittypen.

PostgreSQL 17 fügt ["MERGE"](https://www.postgresql.org/docs/17/sql-merge.html) weitere Funktionen hinzu, die für bedingte Aktualisierungen verwendet werden, darunter eine "RETURNING"-Klausel und die Möglichkeit, [Views](https://www.postgresql.org/docs/17/sql-createview.html) zu aktualisieren.
Darüber hinaus verfügt es über neue Funktionen für das Massenladen und Exportieren von Daten, darunter eine bis zu zweifache Leistungssteigerung beim Exportieren großer Zeilen mit dem ["COPY"](https://www.postgresql.org/docs/17/sql-copy.html) Befehl.
Die "COPY"-Leistung wurde auch verbessert, wenn die Quell- und Zielkodierungen übereinstimmen, und enthält eine neue Option, "ON_ERROR", die es ermöglicht, einen Import auch dann fortzusetzen wenn beim Einfügen Fehler auftreten.

Die neue Version von PostgreSQL erweitert die Funktionalität sowohl für die Verwaltung von Daten in Partitionen als auch für Daten, die über Remote-PostgreSQL-Instanzen verteilt sind. PostgreSQL 17 unterstützt die Verwendung von Identity Columns und Exclusion Constraints für [partitionierte Tabellen](https://www.postgresql.org/docs/17/ddl-partitioning.html).
Der [PostgreSQL Foreign Data Wrapper](https://www.postgresql.org/docs/17/postgres-fdw.html) (["postgres_fdw"](https://www.postgresql.org/docs/17/postgres-fdw.html)), der zum Ausführen von Abfragen auf Remote-PostgreSQL-Instanzen verwendet wird, kann jetzt "EXISTS"- und "IN"-Unterabfragen zur effizienteren Verarbeitung an den Remote-Server übertragen.

PostgreSQL 17 enthält außerdem einen integrierten, plattformunabhängigen, unveränderlichen Collation Provider, der garantiert unveränderlich ist und eine ähnliche Sortiersemantik wie die "C"-Sortierung bietet, allerdings mit "UTF-8"-Kodierung statt "SQL_ASCII". Die Verwendung dieses neuen Collation Provider garantiert, dass textbasierte Abfragen unabhängig davon, wo du PostgreSQL ausführst, dieselben sortierten Ergebnisse zurückgeben.

### Verbesserungen der logischen Replikation für hohe Verfügbarkeit und Major Version Upgrades

[Logische Replikation](https://www.postgresql.org/docs/17/logical-replication.html) wird verwendet, um in verschiedenen Anwendungsfällen Daten in Echtzeit zu streamen. Vor dieser Version mussten Benutzer, die ein Major Version Upgrade durchführen wollten, [Slots für logische Replikation](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT) löschen, was eine neue Synchronisierung der Daten mit Replicas nach einem Upgrade erforderte. Ab PostgreSQL 17 müssen Benutzer keine Slots für logische Replikation mehr löschen, was den Upgrade-Prozess bei Verwendung logischer Replikation vereinfacht.

PostgreSQL 17 enthält jetzt eine Failover-Steuerung für die logische Replikation, wodurch es widerstandsfähiger wird, wenn es in Umgebungen mit hoher Verfügbarkeit eingesetzt wird. Darüber hinaus führt PostgreSQL 17 das ["pg_createsubscriber"](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html) Befehlszeilentool zum Konvertieren einer physischen Replikation in eine neue logische Replik ein.

### Mehr Optionen zur Verwaltung von Sicherheit und Betrieb

PostgreSQL 17 erweitert die Möglichkeiten der Benutzer, den gesamten Lebenszyklus ihrer Datenbanksysteme zu verwalten. PostgreSQL verfügt über eine neue TLS-Option, "sslnegotiation", mit der Benutzer direkte TLS-Handshakes durchführen können, wenn sie ALPN verwenden (registriert als "postgresql" im ALPN-Verzeichnis). Es fügt außerdem die vordefinierte Rolle "pg_maintain" hinzu, die Benutzern die Berechtigung erteilt, Wartungsvorgänge durchzuführen.

["pg_basebackup"](https://www.postgresql.org/docs/17/app-pgbasebackup.html), das in PostgreSQL enthaltene Backup-Dienstprogramm, unterstützt jetzt inkrementelle Backups und fügt das Dienstprogramm ["pg_combinebackup"](https://www.postgresql.org/docs/17/app-pgcombinebackup.html) hinzu, um ein vollständiges Backup zu rekonstruieren. Darüber hinaus enthält ["pg_dump"](https://www.postgresql.org/docs/17/app-pgdump.html) eine neue Option namens "--filter", mit der ausgewählt werden kann, welche Objekte beim Generieren einer Dump-Datei eingeschlossen werden sollen.

PostgreSQL 17 enthält außerdem Verbesserungen bei den Überwachungs- und Analysefunktionen. ["EXPLAIN"](https://www.postgresql.org/docs/17/sql-explain.html) zeigt jetzt die für lokales Lesen und Schreiben von I/O-Blöcken aufgewendete Zeit an und enthält zwei neue Optionen: "SERIALIZE" und "MEMORY", die nützlich sind, um die für die Datenkonvertierung für die Netzwerkübertragung aufgewendete Zeit und den verwendeten Speicher anzuzeigen. Außerdem wird jetzt der [Fortschritt beim Bereinigen von Indizes](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING) angezeigt und die Systemansicht ["pg_wait_events"](https://www.postgresql.org/docs/17/view-pg-wait-events.html) hinzugefügt, die in Kombination mit ["pg_stat_activity"](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW) mehr Einblick darin gibt, warum eine aktive Sitzung wartet.

### Zusätzliche Funktionen

In PostgreSQL 17 wurden viele weitere neue Funktionen und Verbesserungen hinzugefügt, die auch für deine Anwendungsfälle hilfreich sein können. Eine vollständige Liste der neuen und geänderten Funktionen findest du in den [Versionshinweisen](https://www.postgresql.org/docs/17/release-17.html).

### Über PostgreSQL

[PostgreSQL](https://www.postgresql.org) ist das führende Open-Source Datenbanksystem, mit einer weltweiten Community bestehend aus Tausenden von Nutzern und Mitwirkenden sowie Dutzenden von Firmen und Organisationen. Das PostgreSQL Projekt baut auf über 35 Jahre Erfahrung auf, beginnend an der University of California, Berkeley, und hat heute eine nicht zu vergleichende Performance bei der Entwicklung. PostgreSQL's ausgereiftes Feature Set ist nicht nur mit den führenden proprietären Datenbanksystemen vergleichbar, sondern übertrifft diese in erweiterten Datenbankfunktionen, Erweiterbarkeit, Sicherheit und Stabilität.

### Links

* [Download](https://www.postgresql.org/download/)
* [Versionshinweise](https://www.postgresql.org/docs/17/release-17.html)
* [Pressemitteilung](https://www.postgresql.org/about/press/)
* [Sicherheit](https://www.postgresql.org/support/security/)
* [Versionierungsrichtlinie](https://www.postgresql.org/support/versioning/)
* [Folge @postgresql auf Twitter](https://twitter.com/postgresql)
* [Spende](https://www.postgresql.org/about/donate/)

## Mehr über die Funktionen

Erläuterungen zu den oben genannten und anderen Funktionen finden Sie in den folgenden Quellen:

* [Versionshinweise](https://www.postgresql.org/docs/17/release-17.html)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/)

## Wo Herunterladen

Es gibt mehrere Möglichkeiten, PostgreSQL 17 herunterzuladen, darunter:

* Die Seite [Offizielle Downloads](https://www.postgresql.org/download/) enthält Installationsprogramme und Tools für [Windows](https://www.postgresql.org/download/windows/), [Linux ](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) und weitere.
* [Quellcode](https://www.postgresql.org/ftp/source/v17.0)

Weitere Tools und Erweiterungen sind über das [PostgreSQL Extension Network](http://pgxn.org/) verfügbar.

## Dokumentation

PostgreSQL 17 wird mit einer HTML-Dokumentation sowie Manpages geliefert. Sie können die Dokumentation auch online unter [HTML](https://www.postgresql.org/docs/17/) aufrufen und als [PDF](https://www.postgresql.org/files/documentation/pdf/17/postgresql-17-US.pdf) Datei herunterladen.

## Lizenz

PostgreSQL verwendet die [PostgreSQL-Lizenz](https://www.postgresql.org/about/licence/), eine BSD-artige "permissive" Lizenz. Diese [OSI-zertifizierte Lizenz](http://www.opensource.org/licenses/postgresql/) wird  allgemein als flexibel und geschäftsfreundlich geschätzt, da die Verwendung von PostgreSQL mit kommerziellen und proprietären Anwendungen nicht eingeschränkt wird. Zusammen mit unternehmensübergreifender Unterstützung und öffentlichem Quellcode macht diese Lizenz PostgreSQL sehr beliebt bei Anbietern die eine Datenbank in ihre eigene Anwendungen einbetten möchten, ohne Einschränkugen bei Gebühren, Herstellerbindung oder Änderungen der Lizenzbedingungen.

## Kontakte

Website

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

## Bilder und Logos

Postgres und PostgreSQL und das Elefanten Logo (Slonik) sind registrierte Marken der [PostgreSQL Community Association](https://www.postgres.ca). Wenn Sie diese Marken verwenden möchten, müssen Sie die [Markenrichtlinie](https://www.postgresql.org/about/policies/trademarks/) einhalten.

## Professioneller Support

PostgreSQL genießt die Unterstützung zahlreicher Unternehmen, die Entwickler sponsern, Hosting-Ressourcen bereitstellen und finanzielle Unterstützung leisten. Unsere [Sponsorenliste](https://www.postgresql.org/about/sponsors/) listet einige Unterstützer des Projekts auf.

Es gibt eine große Anzahl von [Unternehmen, die PostgreSQL-Support anbieten](https://www.postgresql.org/support/professional_support/), von einzelnen Beratern bis hin zu multinationalen Unternehmen.

Wenn Sie einen finanziellen Beitrag zur PostgreSQL Development Group leisten möchten oder eine der anerkannten gemeinnützigen Organisationen der Community unterstützen möchten, besuchen Sie bitte unsere [Spenden Seite](https://www.postgresql.org/about/donate/).
