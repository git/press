26 settembre 2024 - Il [PostgreSQL Global Development Group](https://www.postgresql.org)
ha annunciato oggi il rilascio di [PostgreSQL 17](https://www.postgresql.org/docs/17/release-17.html),
l'ultima versione del database open source più avanzato al mondo.

PostgreSQL 17 si basa su decenni di sviluppo open source, migliorando
prestazioni e scalabilità e adattandosi al tempo stesso ai nuovi modlli di accesso e archiviazione dei dati.
Questa versione di [PostgreSQL](https://www.postgresql.org) aggiunge
significativi miglioramenti delle prestazioni complessive, inclusa una revisione dell'implementazione della 
gestione della memoria per le operazioni di vacuum, ottimizzazioni per l'accesso allo storage e miglioramenti per
carichi di lavoro ad alta concorrenza, velocizzazioni nel caricamento ed esportazione massive dei dati e 
miglioramenti nell'esecuzione degli indici delle query. PostgreSQL 17 dispone di funzionalità che apportano vantaggi
sui nuovi carichi di lavoro e sui sistemi critici, come ad esempio, aggiunte all'esperienza
degli sviluppatori con il comando SQL/JSON "JSON_TABLE" e miglioramenti alla
replica logica che semplifica la gestione dei carichi di lavoro ad alta disponibilità e
importanti aggiornamenti della versione.

"PostgreSQL 17 evidenzia come la comunità open source globale, che guida lo
sviluppo di PostgreSQL, crea miglioramenti che aiutano gli utenti in tutte le fasi 
di utilizzo del database", ha affermato Jonathan Katz, membro del core team di PostgreSQL.
"Che si tratti di miglioramenti per il funzionamento dei database su larga scala o
nuove funzionalità che si basano su una piacevole esperienza per gli sviluppatori, PostgreSQL 17
migliorerà la tua esperienza di gestione dei dati."

PostgreSQL, un innovativo sistema di gestione dei dati noto per la sua affidabilità,
robustezza ed estensibilità trae vantaggio da oltre 25 anni di svilupo open source
ad opera di una comunità di sviluppatori globale ed è diventato il 
database relazionale open source preferito per organizzazioni di tutte le dimensioni.

### Miglioramenti delle prestazioni a livello di sistema

Il processo [vacuum] di PostgreSQL(https://www.postgresql.org/docs/17/routine-vacuuming.html)
è fondamentale per garantire l'integrità delle operazioni richiedendo risorse dell'istanza server per operare. 
PostgreSQL 17 introduce una nuova struttura di memoria interna per il proceso di vacuum
che consuma fino a 20 volte meno memoria. Ciò migliora la velocità del processo di vacuum e
riduce inoltre l'utilizzo delle risorse condivise, rendendone più disponibili per il tuo carico di lavoro.

PostgreSQL 17 continua a migliorare le prestazioni del suo livello I/O. 
Carichi di lavoro ad alta concorrenza possono ottenere un throughput di scrittura fino a 2 volte superiore grazie ai
miglioramenti sulle elaborazioni dei [log write-ahead](https://www.postgresql.org/docs/17/wal-intro.html)
([WAL](https://www.postgresql.org/docs/17/wal-intro.html)).
Inoltre, la nuova interfaccia I/O di streaming accelera le scansioni sequenziali
(leggendo tutti i dati da una tabella) e determina quanto velocemente
[`ANALYZE`](https://www.postgresql.org/docs/17/sql-analyze.html) può aggiornare
statistiche del pianificatore.

PostgreSQL 17 estende i miglioramenti prestazionali anche all'esecuzione delle query.
PostgreSQL 17 migliora le prestazioni delle query con clausole "IN" che utilizzano indici basati su 
[B-tree](https://www.postgresql.org/docs/17/indexes-types.html#INDEXES-TYPES-BTREE), 
il metodo di indice predefinito in PostgreSQL. Inoltre,
gli indici [BRIN](https://www.postgresql.org/docs/17/brin.html) ora supportano
costruzioni parallele. PostgreSQL 17 include diversi miglioramenti per la pianificazione delle query,
incluse ottimizzazioni per i vincoli `NOT NULL` e miglioramenti in
elaborazione delle [espressioni di tabella comuni](https://www.postgresql.org/docs/17/queries-with.html)
([Query `WITH`](https://www.postgresql.org/docs/17/queries-with.html)). Questa
versione aggiunge ulteriore supporto SIMD (Single Instruction/Multiple Data) per
accelerando i calcoli, incluso l'utilizzo di AVX-512 per la funzione
[`bit_count`](https://www.postgresql.org/docs/17/functions-bitstring.html).

### Ulteriore espansione di una solida esperienza di sviluppo

PostgreSQL è stato il [primo database relazionale ad aggiungere il supporto JSON (2012)](https://www.postgresql.org/about/news/postgresql-92-released-1415/),
e PostgreSQL 17 aggiunge alla sua implementazione lo standard SQL/JSON.
[`JSON_TABLE`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-TABLE)
è ora disponibile in PostgreSQL 17, consentendo agli sviluppatori di convertire i dati JSON in una
tabella PostgreSQL standard. PostgreSQL 17 ora supporta i [costruttori SQL/JSON](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE)
(`JSON`, `JSON_SCALAR`, `JSON_SERIALIZE`) e
[funzioni di query](https://www.postgresql.org/docs/17/functions-json.html#SQLJSON-QUERY-FUNCTIONS)
(`JSON_EXISTS`, `JSON_QUERY`, `JSON_VALUE`), offrendo agli sviluppatori altri modi di
interfacciarsi con i propri dati JSON. Questa versione aggiunge altre
espressioni [`jsonpath`](https://www.postgresql.org/docs/17/functions-json.html#FUNCTIONS-SQLJSON-PATH-OPERATORS),
con particolare attenzione alla conversione dei dati JSON in un tipo di dati PostgreSQL nativo,
inclusi i tipi numerico, booleano, stringa e data/ora.

PostgreSQL 17 aggiunge altre funzionalità a [`MERGE`](https://www.postgresql.org/docs/17/sql-merge.html),
che viene utilizzato per gli aggiornamenti condizionali, tra cui una clausola `RETURNING` e la
capacità di aggiornare le [viste](https://www.postgresql.org/docs/17/sql-createview.html).
Inoltre, PostgreSQL 17 ha nuove funzionalità per il caricamento in blocco e l'esportazione
di dati, tra cui un miglioramento delle prestazioni fino a 2 volte durante l'esportazione di righe di grandi dimensioni
utilizzando il comando [`COPY`](https://www.postgresql.org/docs/17/sql-copy.html).
Anche le prestazioni di `COPY` presentano miglioramenti quando le codifiche di origine e destinazione
corrispondono e include una nuova opzione, `ON_ERROR`, che consente a un'importazione di
continuare anche se si verifica un errore di inserimento.

Questa versione amplia le funzionalità sia per la gestione dei dati nelle partizioni sia per i dati distribuiti tra istanze remote di PostgreSQL. PostgreSQL 17 supporta
l'utilizzo di colonne di identità e vincoli di esclusione su
[tabelle partizionate](https://www.postgresql.org/docs/17/ddl-partitioning.html).
Il [wrapper dati esterno PostgreSQL](https://www.postgresql.org/docs/17/postgres-fdw.html)
([`postgres_fdw`](https://www.postgresql.org/docs/17/postgres-fdw.html)), utilizzato
per eseguire query su istanze remote di PostgreSQL, ora può inviare le sottoquery `EXISTS` e
`IN` al server remoto per un'elaborazione più efficiente.

PostgreSQL 17 include anche un provider di collazione immutabile, indipendente dalla piattaforma e incorporato che è garantito essere immutabile 
e fornisce una semantica di ordinamento simile alla collazione `C`, tranne che con la codifica `UTF-8` anziché
`SQL_ASCII`. L'utilizzo di questo nuovo provider di collazione garantisce che le query basate su testo
restituiranno gli stessi risultati ordinati indipendentemente da dove si esegue
PostgreSQL.

### Miglioramenti della replica logica per alta disponibilità e aggiornamenti di versione principali

[La replica logica](https://www.postgresql.org/docs/17/logical-replication.html)
viene utilizzata per trasmettere dati in tempo reale in molti casi d'uso. Tuttavia, prima di
questa versione, gli utenti che desideravano eseguire un aggiornamento di versione principale dovevano
eliminare [slot di replica logica](https://www.postgresql.org/docs/17/logical-replication-subscription.html#LOGICAL-REPLICATION-SUBSCRIPTION-SLOT), il che richiedeva la risincronizzazione dei dati
con gli abbonati dopo un aggiornamento. A partire dagli aggiornamenti da PostgreSQL 17,
gli utenti non devono eliminare slot di replica logica, semplificando il processo di aggiornamento
quando si utilizza la replica logica.

PostgreSQL 17 ora include il controllo del failover per la replica logica, rendendola
più resiliente quando distribuita in ambienti ad alta disponibilità. Inoltre,
PostgreSQL 17 introduce lo
[`pg_createsubscriber`](https://www.postgresql.org/docs/17/app-pgcreatesubscriber.html)
strumento da riga di comando per convertire una replica fisica in una nuova replica logica.

### Ulteriori opzioni per la gestione della sicurezza e delle operazioni

PostgreSQL 17 amplia ulteriormente il modo in cui gli utenti possono gestire il ciclo di vita complessivo dei
loro sistemi di database. PostgreSQL ha una nuova opzione TLS, `sslnegotiation`, che
consente agli utenti di eseguire handshake TLS diretti quando si utilizza
[ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation)
(registrato come `postgresql` nella directory ALPN). PostgreSQL 17 aggiunge anche il
[ruolo predefinito](https://www.postgresql.org/docs/17/predefined-roles.html)
`pg_maintain`, che fornisce agli utenti l'autorizzazione per eseguire operazioni di manutenzione.

[`pg_basebackup`](https://www.postgresql.org/docs/17/app-pgbasebackup.html), l'utility di backup inclusa in PostgreSQL, ora supporta i backup incrementali e aggiunge
l'utility [`pg_combinebackup`](https://www.postgresql.org/docs/17/app-pgcombinebackup.html)
per ricostruire un backup completo. Inoltre,
[`pg_dump`](https://www.postgresql.org/docs/17/app-pgdump.html) include una nuova
opzione chiamata `--filter` che consente di selezionare quali oggetti includere quando
si genera un file di dump.

PostgreSQL 17 include anche miglioramenti alle funzionalità di monitoraggio e analisi. [`EXPLAIN`](https://www.postgresql.org/docs/17/sql-explain.html) ora mostra il
tempo impiegato per le letture e le scritture dei blocchi I/O locali e include due nuove opzioni:
`SERIALIZE` e `MEMORY`, utili per vedere il tempo impiegato nella conversione dei dati
per la trasmissione in rete e quanta memoria è stata utilizzata. PostgreSQL 17 ora
segnala il [progresso degli indici di vacuuming](https://www.postgresql.org/docs/17/progress-reporting.html#VACUUM-PROGRESS-REPORTING),
e aggiunge la vista di sistema [`pg_wait_events`](https://www.postgresql.org/docs/17/view-pg-wait-events.html) che, 
se combinata con [`pg_stat_activity`](https://www.postgresql.org/docs/17/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
fornisce maggiori informazioni sul motivo per cui una sessione attiva è in attesa.

### Funzionalità aggiuntive

Molte altre nuove funzionalità e miglioramenti che sono stati aggiunti a PostgreSQL 17
potrebbero tornare utili per i tuoi casi d'uso. Si prega di consultare le
[note sulla versione](https://www.postgresql.org/docs/17/release-17.html) per un
elenco completo delle funzionalità nuove e modificate.

### Informazioni su PostgreSQL

[PostgreSQL](https://www.postgresql.org) è il database open source più avanzato al mondo, con una comunità globale di migliaia di utenti, collaboratori,
aziende e organizzazioni. Costruito su oltre 35 anni di ingegneria, a partire da
l'Università della California, Berkeley, PostgreSQL ha continuato con un
ritmo di sviluppo senza pari. Il set di funzionalità mature di PostgreSQL non solo corrisponde
migliori sistemi di database proprietari, ma li supera in funzionalità di database avanzato, estensibilità, sicurezza e stabilità.

### Collegamenti

* [Download](https://www.postgresql.org/download/)
* [Note sulla versione](https://www.postgresql.org/docs/17/release-17.html)
* [Kit per la stampa](https://www.postgresql.org/about/press/)
* [Pagina sulla sicurezza](https://www.postgresql.org/support/security/)
* [Politica di versione](https://www.postgresql.org/support/versioning/)
* [Segui @postgresql su Twitter](https://twitter.com/postgresql)
* [Donazioni](https://www.postgresql.org/about/donate/)

## Maggiori informazioni sulle funzionalità

Per le spiegazioni delle funzioni di cui sopra e altre, consultare le seguenti risorse:

* [Note sulla versione](https://www.postgresql.org/docs/17/release-17.html)
* [Matrice delle funzioni](https://www.postgresql.org/about/featurematrix/)

## Dove scaricare

Esistono diversi modi per scaricare PostgreSQL 17, tra cui:

* La pagina di [Download ufficiale](https://www.postgresql.org/download/), con contiene programmi di installazione e strumenti per [Windows](https://www.postgresql.org/download/windows/), [Linux ](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) e altro ancora.
* Il [Codice sorgente](https://www.postgresql.org/ftp/source/v17.0)

Altri strumenti ed estensioni sono disponibili sulla
[Rete di estensioni per PostgreSQL](http://pgxn.org/).

## Documentazione

PostgreSQL 17 viene fornito con documentazione HTML e pagine man e puoi anche sfogliare la documentazione online in [HTML](https://www.postgresql.org/docs/17/) e [PDF](https:// www.postgresql.org/files/documentation/pdf/17/postgresql-17-US.pdf).

## Licenza

PostgreSQL utilizza la [Licenza PostgreSQL](https://www.postgresql.org/about/licence/), una licenza "permissiva" simile a BSD. Questa
[Licenza certificata OSI](http://www.opensource.org/licenses/postgresql/) è ampiamente apprezzata come flessibile e business-friendly, poiché non limita
l'uso di PostgreSQL con applicazioni commerciali e proprietarie. Insieme con il supporto multi-aziendale e la proprietà pubblica del codice, la nostra licenza fa si che
PostgreSQL sia molto popolare tra i fornitori che desiderano incorporare un database nel proprio prodotti senza timore di commissioni, vincoli del fornitore o modifiche ai termini di licenza.

## Contatti

Sito web

* [https://www.postgresql.org/](https://www.postgresql.org/)

E-mail

* [press@postgresql.org](mailto:press@postgresql.org)

## Immagini e loghi

Postgres e PostgreSQL e Elephant Logo (Slonik) sono tutti marchi registrati di [PostgreSQL Community Association](https://www.postgres.ca).
Se desideri utilizzare questi marchi, devi rispettare la [politica sui marchi](https://www.postgresql.org/about/policies/trademarks/).

## Supporto aziendale e donazioni

PostgreSQL gode del supporto di numerose aziende che sponsorizzano sviluppatori e forniscono risorse di hosting e supporto finanziario. 
Consulta la nostra pagina [sponsor](https://www.postgresql.org/about/sponsors/) per l'elenco dei sostenitori del progetto.

C'è anche una grande comunità di [aziende che offrono supporto PostgreSQL](https://www.postgresql.org/support/professional_support/), dai singoli consulenti alle multinazionali.

Se desideri dare un contributo finanziario al PostgreSQL Global Development Group o ad una delle organizzazioni non profit riconosciute della comunità,
puoi visitare la nostra pagina delle [donazioni](https://www.postgresql.org/about/donate/).
