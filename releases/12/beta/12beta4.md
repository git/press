PostgreSQL 12 Beta 4 Released
=============================

The PostgreSQL Global Development Group announces that the fourth beta release
of PostgreSQL 12 is now available for download. This release contains previews
of all features that will be available in the final release of PostgreSQL 12,
though some details of the release could change before then.

This is likely the final beta release of PostgreSQL 12 before a release
candidate is made available.

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 12 in your database systems to help us
eliminate any bugs or other issues that may exist.

Upgrading to PostgreSQL 12 Beta 4
---------------------------------

To upgrade to PostgreSQL 12 Beta 4 from Beta 3 or an earlier version of
PostgreSQL 12, you will need to use a strategy similar to upgrading between
major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`).
For more information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/12/static/upgrading.html).

Changes Since 12 Beta 3
-----------------------

There have been many bug fixes for PostgreSQL 12 reported during the Beta 3
period and applied to the Beta 4 release. This release also includes other bug
fixes that were reported for supported versions of PostgreSQL that also affected
PostgreSQL 12.

These changes include:

* Fix for crash that could occur with nested queries that contain locks
* Only allow chained transactions to work in an explicitly set transaction
block, otherwise error
* Changed the log level for several types of messages around partitioning to
`DEBUG1`
* Add the `default_table_access_method` configuration parameter to the sample
configuration kept in `postgresql.sample.conf`
* Fix handling of duplicate entries in `postgresql.auto.conf` when
`ALTER SYSTEM` is called. Now, when a configuration parameter with duplicate
entries is updated, PostgreSQL will remove all entries and append the new entry
to the end
* Ensure `ALTER SYSTEM` makes case-insensitive comparisons when adjusting
entries in the configuration

For a detailed list of fixes, please visit the
[open items](https://wiki.postgresql.org/wiki/PostgreSQL_12_Open_Items#resolved_before_12beta4)
page.

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 12. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_12_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

https://www.postgresql.org/account/submitbug/

Beta Schedule
-------------

This is the fourth beta release of version 12. In all likelihood, this is the
final beta release of PostgreSQL 12 before one or more release candidates are
made available. For further information please see the
[Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 12 Beta Release Notes](https://www.postgresql.org/docs/devel/release-12.html)
* [PostgreSQL 12 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_12_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
