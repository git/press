# Выпущен PostgreSQL 12!

Всемирная группа разработки PostgreSQL объявила сегодня о выходе PostgreSQL 12, новейшей версии лидирующей реляционной системы управления базами данных (СУБД) с открытым исходным кодом.

В PostgreSQL 12 значительно улучшена производительность запросов – особенно это касается работы с большими объёмами данных – и произведена оптимизация использования дискового пространства в целом. Разработчики приложений получают такие новые возможности как:
- реализация языка запросов JSON Path (важнейшей части стандарта SQL/JSON),
- оптимизация исполнения общих табличных выражений ("WITH"),
- поддержка генерируемых столбцов.

Кроме этого, PostgreSQL-сообщество продолжает работу над расширяемостью и надёжностью PostgreSQL, развивая поддержку интернационализации, возможностей аутентификации, предоставляя более простые пути администрирования системы. В этот релиз также вошла реализация интерфейса подключаемых движков хранения, что отныне позволяет разработчикам создавать свои собственные методы хранения данных.

«Сообщество разработчиков PostgreSQL создало богатые возможности для PostgreSQL 12, которые включают улучшения производительности и управления пространством, что наши пользователи могут достичь за счёт минимальных усилий, а также развитие вариантов аутентификации в крупных предприятиях, возможностей администрирования и поддержки SQL/JSON», – говорит Дейв Пейдж (Dave Page), член основной команды Всемирной группы разработки PostgreSQL. – «Выход новой версии продолжает тенденцию на упрощение администрирования баз данных с большими и малыми нагрузками, опираясь на репутацию PostgreSQL в отношении гибкости, надежности и стабильности в производственных средах».

Используя плоды более чем 20 лет открытой разработки, PostgreSQL стал предпочитаемой реляционной системой управления базами данных с открытым исходным кодом в организациях любого масштаба. Проект продолжает получать признание в индустрии: PostgreSQL получил звание «СУБД года» от проекта DB-Engines второй год подряд и премию для открытых проектов «Lifetime Achievement» на конференции OSCON 2019.


## Общие улучшения производительности

PostgreSQL 12 включает существенные улучшения производительности и процедур обслуживания для систем индексирования и секционирования.

Индексы B-tree — стандартный тип индексирования в PostgreSQL — были оптимизированы в версии 12 для нагрузок, предполагающих частые модификации индексов. Использование эталонного теста TPC-C для PostgreSQL 12 продемонстрировало сокращение использования пространства в среднем на 40% и общий прирост производительности запросов.

Запросы к секционированным таблицам также получили заметные улучшения, особенно для таблиц, состоящих из тысяч секций, предполагающих работу только с ограниченными частями массивов данных. В PostgreSQL 12 также улучшена производительность добавления данных в секционированные таблицы с помощью «INSERT» и «COPY», а также возможность подсоединения новой секции без блокирования выполняемых запросов.

В PostgreSQL 12 произведены дополнительные усовершенствования в индексировании, которые влияют на общую производительность, включая:
- снижение накладных расходов при генерации WAL для типов индексов GiST, GIN и SP-GiST,
- возможность создавать так называемые покрывающие индексы (covering indexes, предложение «INCLUDE») на GiST-индексы,
- возможность выполнять запросы «К ближайших соседей» (k-NN search) с помощью оператора расстояния ("<->") с использованием индексов SP-GiST,
- поддержку сбора статистики наиболее распространенных значений (most-common value, MCV) с помощью «CREATE STATISTICS», что помогает получать лучшие планы выполнения запросов при использовании столбцов, значения для которых распределены неравномерно.

JIT-компиляция с использованием LLVM, появившаяся в PostgreSQL 11, теперь включена по умолчанию. JIT-компиляция позволяет повышать производительность при работе с выражениями в предложениях WHERE, целевых списках, агрегатах и некоторых внутренних операциях. Она доступна, если вы скомпилировали PostgreSQL с LLVM или используете пакет PostgreSQL, который был создан с включённым LLVM.

## Улучшения возможностей языка SQL и совместимости со стандартом

PostgreSQL известен своим соответствием стандарту SQL - одна из причин, по которой он был переименован из «POSTGRES» в «PostgreSQL». В PostgreSQL 12 добавлены несколько функций для развития реализации стандарта SQL с расширенными функциональными возможностями.

В PostgreSQL 12 появилась возможность выполнять запросы к документам JSON с использованием выражений пути JSON, определенных в стандарте SQL/JSON. Такие запросы могут использовать существующие механизмы индексации для документов, хранящихся в формате JSONB, для эффективного извлечения данных.

Общие табличные выражения, также известные как запросы с «WITH», в PostgreSQL 12 теперь могут автоматически исполняться с применением подстановки, что, в свою очередь, может помочь повысить производительность многих существующих запросов. В новой версии часть запроса WITH может выполняться с подстановкой, только если она не является рекурсивной, не имеет побочных эффектов и на неё ссылаются только один раз в последующей части запроса.

В PostgreSQL 12 появляется поддержка «генерируемых столбцов». Описанный в стандарте SQL, этот тип столбца вычисляет значение на основе содержимого других столбцов в той же таблице. В этой версии PostgreSQL поддерживает «хранимые генерируемые столбцы», где вычисленное значение хранится на диске.

## Интернационализация

PostgreSQL 12 расширяет поддержку ICU-сопоставлений, разрешая пользователям определять «недетерминированные сопоставления», которые могут, например, позволять сравнения без учёта регистра или без учёта ударения.

## Аутентификация

PostgreSQL расширяет свою поддержку надёжных методов аутентификации с помощью нескольких улучшений, которые обеспечивают дополнительную безопасность и функциональность. В этом выпуске представлено шифрование на стороне клиента и на стороне сервера для аутентификации через интерфейсы GSSAPI, а также возможность PostgreSQL обнаруживать серверы LDAP, если PostgreSQL скомпилирован с OpenLDAP.

Кроме того, PostgreSQL 12 теперь поддерживает вариант многофакторной аутентификации. Теперь серверу PostgreSQL может затребовать у клиента предоставление валидного SSL-сертификата с соответствующим именем пользователя с использованием «clientcert = verify-full», и комбинировать это с отдельным требованием метода аутентификации (например, «scram-sha-256»).

## Администрирование

В PostgreSQL 12 появилась возможность выполнять неблокирующее перестроение индексов с помощью команды «REINDEX CONCURRENTLY». Это позволяет пользователям избегать простоя в работе СУБД при длительном перестроении индексов.

Кроме того, в PostgreSQL 12 можно включать или отключать контрольные суммы страниц в кластере, находящемся в выключенном состоянии, с помощью команды «pg_checksums». Ранее контрольные суммы страниц — функция, помогающая проверить целостность данных, хранящихся на диске, — можно было включить только в момент инициализации кластера PostgreSQL с помощью «initdb».

Полный список новых возможностей, вошедших в данный релиз, можно найти по ссылке (англ.): https://www.postgresql.org/docs/12/release-12.html.

## О PostgreSQL

PostgreSQL является ведущей СУБД с открытыми исходными текстами, с глобальным сообществом из тысяч пользователей и разработчиков, объединяющим множество компаний и организаций. Проект PostgreSQL базируется на более чем 30-летнем опыте проектирования и разработки, начавшихся в Калифорнийском университете Беркли, и в настоящее время продолжает развиваться беспрецедентными темпами. Богатый набор возможностей PostgreSQL не только не уступает ведущим коммерческим СУБД, но и превосходит их развитой функциональностью, расширяемостью, безопасностью и стабильностью.

Узнайте больше о PostgreSQL и примите участие в жизни сообщества на PostgreSQL.org.
