29 SEPTEMBER 2016: PostgreSQL 9.6, die neueste Version der weltweit führenden Open-Source-SQL-Datenbank,  wurde heute von der PostgreSQL Global Development Group freigegeben. Diese Version ermöglicht es Benutzern, ihre High Performance Datenbank-Workloads sowohl vertikal ("scale-up") als auch horizontal ("scale-out") zu skalieren. Die wichtigsten neuen Features umfassen parallelisierte Abfragen, Phrasensuche und Verbesserungen bei synchroner Replikation, sowie generelle verbesserte Performance und Benutzerfreundlichkeit.

Scale Up mit parallelen Abfragen
--------------------------------

Version 9.6 ermöglicht es erstmals, bestimmte Abfrageoperationen parallel auszuführen und gestattet so die Nutzung mehrerer oder aller CPU-Kerne, was die Laufzeit von Abfragen deutlich reduzieren kann. Die unterstützten Operationen sind: parallele sequenzielle (Tabellen-) Scans, Aggregationen und JOINs. Je nach Anwendungsfall und Hardware wurden hierbei Beschleunigungen um das bis zu 32-fache beobachtet.

"Ich habe unsere gesamte Genomdatenplattform - alle 25 Milliarden Datensätze der bisherigen MySQL Datenbank - in eine einzige Postgres-Datenbank migriert und dabei bereits von der Zeilenkomprimierung des JSONB-Datentyps und den hervorragenden GIN-, BRIN- und B-Tree-Indexierungsmöglichkeiten profitiert. Jetzt mit Version 9.6 erwarte ich durch die Parallelisierung von Abfragen sogar eine noch besserere Skalierbarkeit für Abfragen gegen unsere doch ziemlich großen Tabellen", so Mike Sofen, Chief Database Architect, Synthetic Genomics.

Scale-Out mit Synchronous Replication und postgres_fdw
------------------------------------------------------

Zwei neue Optionen wurden PostgreSQLs synchroner Replikation hinzugefügt, die es ermöglichen, Lesevorgänge clusterweit konsistent zu halten. Erstens ermöglicht die Replikation jetzt, Gruppen synchroner Replikas, also mehrere synchrone Zielserver zu konfigurieren. Zweitens schafft der "remote_apply"-Modus eine konsistente Sicht der Daten. Zusammen erlaubt dies, mit der integrierten Replikation eine Menge "identischer" Knoten für das Load-Balancing lesender Workloads vorzuhalten.

Der PostgreSQL-to-PostgreSQL Data Federation-Treiber, "postgres_fdw", hat neue Funktionen erhalten, die es ihm erlauben, noch mehr Tätigkeiten an Remote-Server zu delegieren. Mit "push down" von SORTs, JOINs und Massen-Updates können Nutzer Arbeitslast auf mehrere PostgreSQL-Server verteilen. Diese Funktionalitäten dürften in Zukunft auch durch anderen FDW-Treiber adaptiert werden.

"Mit den Möglichkeiten von Remote-JOIN, -UPDATE und -DELETE sind Foreign Data Wrappers jetzt eine komplette Lösung für den Austausch von Daten zwischen anderen
Datenbanken und PostgreSQL. So können diese beispielsweise dafür verwendet werden, Datenflüsse, die für mehrere unterschiedliche Zieldatenbanksysteme gedacht sind, durch PostgreSQL zu 'tunneln'", so Julyanto Sutandang, Director Business Solutions bei Equnix.

Bessere Textsuche mit Phrasen
-----------------------------

PostgreSQLs Volltextsuche unterstützt jetzt "Phrasensuche". So kann nach exakten Phrasen oder nach Wörtern in bestimmter Nähe zueinander gesucht werden. Unterstützt werden solche Suchen auch weiterhin durch GIN-Indizes und sind dadurch sehr schnell. Zusammen mit neuen Möglichkeiten für die Feinabstimmung der Textsuche ist PostgreSQL eine überlegene Option für "Hybrid-Suche", die Suchen über relationale Daten, JSON und die Volltextsuche kombiniert.

Besser, schneller und einfacher zu bedienen
-------------------------------------------

Dank Rückmeldungen und Tests durch PostgreSQL-Nutzer mit sehr großen Produktionsdatenbanken konnte das Projekt in dieser Version viele Verbesserungen bei Performance und Nutzbarkeit einfließen lassen. Replikation, Aggregation, Indexierung, Sortierung und Stored Procedures wurden effizienter gemacht, und PostgreSQL nutzt mit neueren Linux-Kerneln die Ressourcen effektiver. Der interne Verwaltungsaufwand für große Tabellen und komplexe Workloads wurde reduziert, insb. durch Verbesserungen an VACUUM.

Links
-----

* Downloads:
  https://www.postgresql.org/downloads
* Pressemappe:
  https://www.postgresql.org/about/press/presskit96
* Release Notes:
  https://www.postgresql.org/docs/current/static/release-9-6.html
* Was ist neu in 9.6:
  https://wiki.postgresql.org/wiki/NewIn96

Kontakt
-------

Für weitere Informationen kontaktieren Sie bitte:

Deutschland:
Andreas Scherbaum
+49 33056 406869
de@postgresql.org

Über PostgreSQL
---------------

PostgreSQL ist das führende Datenbanksystem, mit einer weltweiten Community bestehend aus Tausenden von Nutzern und Mitwirkenden sowie Dutzenden von Firmen und Organisationen. Das PostgreSQL Projekt baut auf über 30 Jahre Erfahrung auf, beginnend an der University of California, Berkeley, und hat heute eine nicht zu vergleichende Performance bei der Entwicklung. PostgreSQL's ausgereiftes Feature Set ist nicht nur mit den führenden proprietären Datenbanksystemen vergleichbar, sondern übertrifft diese in erweiterten Datenbankfunktionen, Erweiterbarkeit, Sicherheit und Stabilität. Lerne mehr über PostgreSQL und nimm an unserer Community teil, unter: http://www.postgresql.org.
