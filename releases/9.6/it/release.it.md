29 SETTEMBRE 2016: PostgreSQL 9.6, l'ultima versione del più avanzato database SQL open source, è stato rilasciato oggi dal PostgreSQL Global Development Group. Questa release migliora la scalabilità verticale e orizzontale di database con alti requisiti di performance. Le nuove funzionalità comprendono query parallele, maggior controllo nella replica sincrona, ricerche di frasi, oltre ai consueti miglioramenti in ambito di prestazioni e di usabilità.

Scalabilità verticale tramite query parallele
---------------------------------------------

PostgreSQL 9.6 introduce il supporto per la parallelizzazione delle query, consentendo di sfruttare più processori di uno stesso server e di velocizzarne l'esecuzione. Questa release include il supporto per *sequential scan* di una tabella, *aggregazioni* e *join*.
A seconda del numero di processori a disposizione, la parallelizzazione può ridurre i tempi di esecuzione di query in ambito big data fino a 32 volte.

"Abbiamo migrato la totalità dei genomi immagazzinati nella nostra piattaforma - complessivamente 25 miliardi di righe in MySQL - su di un singolo database PostgreSQL, sfruttando la possibilità di compressione dello spazio occupato dalle righe grazie al tipo di dato `JSONB` e alle eccellenti indicizzazioni `GIN`, `BRIN` e `B-tree`. Adesso, con la versione 9.6, mi aspetto di sfruttare a pieno le query parallele ed aumentare così la scalabilità del nostro database, composto da tabelle piuttosto grandi", ha detto Mike Sofen, Chief Database Architect di Synthetic Genomics.

Scalabilità orizzontale tramite replica sincrona e `postgres_fdw`
-----------------------------------------------------------------

Sono state aggiunte due nuove opzioni alla replica sincrona di PostgreSQL che permettono di scalare le letture su più nodi di un cluster, assicurandone la consistenza fra loro. Finora non era possibile mantenere più repliche sincrone. Adesso, con l'introduzione della modalità `remote_apply`, è possibile mantenere una vista consistente su più nodi. Tramite la replica sincrona, si potranno avere diversi nodi "identici" ed effettuare *load balancing* in lettura.

In aggiunta, `postgres_fdw`, il driver per la federazione dei dati tra database PostgreSQL, permette adesso la propagazione (in gergo tecnico *"push down"*) di ordinamenti, *join* e aggiornamenti batch di dati sul server remoto. In questo modo, gli utenti potranno distribuire il carico su diversi nodi PostgreSQL. Presto queste operazioni saranno estese anche ad altri driver FDW.

"Grazie all'esecuzione remota di JOIN, UPDATE e DELETE, i *Foreign Data Wrapper* rappresentano adesso una soluzione completa per la distribuzione dei dati tra altri database e PostgreSQL. Per esempio, PostgreSQL può essere usato per gestire dati in ingresso verso due o più differenti tipi di database", ha detto Julyanto Sutandang, Direttore *Business Solution* di Equnix.

Miglior ricerca testuale con le frasi
-------------------------------------

Il *full text search* di PostgreSQL supporta adesso la ricerca a livello di "frase". Gli utenti saranno in grado di ricercare con *match* esatto intere frasi, oppure parole con una certa prossimità fra loro, usando la veloce indicizzazione GIN. Combinata con le nuove funzionalità di maggior controllo delle opzioni di ricerca testuale, PostgreSQL si conferma essere la prima scelta per "ricerche ibride" che includono l'approccio relazionale, NOSQL (col supporto JSON), e *full text*. 

Più armonioso, veloce e semplice da usare
-----------------------------------------

Grazie al feedback ricevuto dagli utenti che utilizzano in produzione database PostgreSQL su grandi volumi, questa nuova versione ha visto notevoli miglioramenti in termini di prestazioni e usabilità. Replica, aggregazioni, indicizzazioni, ordinamenti e procedure sono stati resi più efficienti. PostgreSQL adesso riesce a sfruttare meglio le risorse delle recenti versioni di kernel Linux. Anche l'impatto di amministrazione ordinaria di tabelle di grandi dimensioni e in presenza di carichi di lavoro complessi è stato ridotto,  soprattutto attraverso il potenziamento del `VACUUM`.

Link utili
----------

* Download: <https://www.postgresql.org/downloads>
* Press Kit: <https://www.postgresql.org/about/press/presskit96>
* Release Note: <https://www.postgresql.org/docs/current/static/release-9-6.html>
* Le novità di PostgreSQL 9.6 (in inglese): <https://wiki.postgresql.org/wiki/NewIn96>
* Articoli di blog su funzionalità di PostgreSQL 9.6 (in italiano, a cura di 2ndQuadrant Italia): <http://blog.2ndquadrant.it/tag/9-6/>

Contattaci
----------

Contatto per la stampa:

    Gabriele Bartolini
    it@postgresql.org
    Ufficio: +39 0574 159 3000
    Mobile: +39 338 1566217

Su PostgreSQL
-------------

PostgreSQL è il principale sistema di gestione di database open source, con una comunità internazionale costituita da migliaia di utenti e sviluppatori nonché decine di aziende ed enti provenienti da tutte le parti del mondo. Il progetto PostgreSQL si porta dietro oltre 30 anni di attività di ingegneria del software, a partire dal campus di Berkeley dell'Università di California, ed oggi può vantare un ritmo di sviluppo senza uguali. La gamma di funzionalità mature messe a disposizione da PostgreSQL non soltanto è in grado di competere con quelle offerte da sistemi di database proprietari, ma le migliora in termini  di funzionalità avanzate, estensibilità, sicurezza e stabilità. Scopri maggiori informazioni su PostgreSQL e partecipa attivamente alla nostra comunità su [PostgreSQL.org](https://www.postgresql.org) e, per l'Italia, [ITPUG](http://www.itpug.org).
