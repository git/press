#!/usr/bin/perl -w
#use strict;

use DBI ;
my $user = "josh" ;
my $passwd = "" ;

my $pdbh = DBI->connect("dbi:Pg:dbname=contacts host=127.0.0.1 port=5432", $user, $passwd);

$action = $pdbh->prepare("SELECT name, pgemail, office_phone, cell_phone, xtra_line, UPPER(continent) as con, region,
                         url FROM contacts WHERE verified ORDER BY continent, region, name;") ;
$action->execute() ;
$action->bind_columns( undef, \$name, \$pgemail, \$office, \$cell, \$xtra, \$continent, \$region, \$url );


open (CONTACTS, ">", $ARGV[0]) ;
print CONTACTS "List As Of: ". `date`;


my $last_region;
my $last_continent;

while ( $action->fetch ) {
    if ( $continent ne $last_continent ) {
        print CONTACTS "\n$continent\n";
        $last_continent = $continent;
    }
    if ( $region ne $last_region ) {
        print CONTACTS "$region\n";
        $last_region = $region;
    }
    print CONTACTS "$name\n";
    print CONTACTS "$pgemail\n";
    $office and print CONTACTS "Phone: $office\n";
    $cell and print CONTACTS "Cell: $cell\n";
    $url and print CONTACTS "$url\n";
    $xtra and print CONTACTS "$xtra\n";
    print CONTACTS "\n";
}

$pdbh->disconnect;
close CONTACTS;

exit(0);
