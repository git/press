#!/usr/bin/python

import os
import re
import argparse
import string
import shutil

parser = argparse.ArgumentParser(description="Copy presskits from press repo to pgweb repo")

parser.add_argument('--pressdir', default=".")
parser.add_argument('--pgweb', default='/home/josh/git/pgweb')
parser.add_argument('--release', default='9.3')
args = parser.parse_args()

pgwebdir = args.pgweb + '/templates/pages/about/press/presskit' + string.replace(args.release,'.','')
pkitname = 'presskit' + string.replace(args.release,'.','') + '.html'

for root, dirs, files in os.walk(args.pressdir):
    print root
    redir = re.search(r'/(\w{2}(_\w{2})?)$', root, re.UNICODE | re.I)
    if not redir:
        continue

    dgrp = redir.groups()
    langd = dgrp[0]
    if pkitname in files:
        shutil.copyfile(root + '/' + pkitname, pgwebdir + '/' + langd + '.html')
        print langd








