\documentclass[11pt]{article}
\title{The Business Case for PostgreSQL}

\begin{document}
\maketitle
\begin{center}
Whitepaper\\
2006
\end{center}
\newpage
\tableofcontents

\newpage


\section{Abstract}

Database management systems present two costs to the owners of the
data: the cost of the management software and the cost of managing and
interacting with the data.

This white paper examines the PostgreSQL database management system
(DBMS). It considers how PostgreSQL can reduce software licensing costs
and allow businesses to get more out of their data for less.

\section{The importance and cost of data}

Businesses are increasingly defined by the availability data to
employees. With storage capabilities expanding and costs reducing,
companies can retain significantly more data than previously thought
possible.

Databases allow companies to consolidate, manage, manipulate and
understand the data they've collected. Successful companies must do this
well and must be \textit{able} to do this at a reasonable price.

To achieve this, a database must provide its users with features which
allow them to manage the data efficiently and effectively; and, it must
do this as inexpensively as possible.

%% consider footnote citing massive growth in storage industry

\subsection{The commoditised database market}

Open source software has ushured in an age of commoditisation within the
software industry, particularly in the server environment. The
operating system, middleware, development tools, deployment tools and
systems management software can all obtained from the open source
community. This has lead to great cost and efficiency gains. Businesses
are able to look to traditional software vendors for customisation,
support and services but they are not locked into a particular vendor's
product.

Open source database options are now mature enough for businesses to
explore and exploit alternatives to commercial databases.


\section{PostgreSQL --- another open source success}

The operating system market has been transformed by Linux --- the first
open source product to gain widespread acceptance and deliver enormous
benefits to business. Following in this tradition, PostgreSQL
has emerged as a leading open source contender to reshape the database
market.

PostgreSQL is developed in largely the same way as Linux is. Expert
programmers world wide contribute features, bug fixes and improvements.
They either volunteer their time or are sponsored by software and
hardware vendors who support PostgreSQL\footnote{A full list of
signficant supporters can be found at the following address:
http://www.postgresql.org/about/sponsors}.

Being open source means that PostgreSQL attracts very experienced and
knowledgeable contributors who care about the quality of the product.

\subsection{Licensing costs with PostgreSQL}

PostgreSQL is distributed under the BSD license\footnote{For example see
http://www.opensource.org/licenses/bsd-license.php}. This license is very
business friendly, as it does not impose distribution
restrictions on its users. This means that businesses can bundle
PostgreSQL with their applications, be they open or closed source, and
not incur a licensing fee.

\section{Why choose PostgreSQL?}

PostgreSQL has the features, robust architecture, sound development
history and mission-critical capabilities which make it ideal for the
full range of business use.

\subsection{PostgreSQL --- the world’s most advanced open source
database}

Currently, PostgreSQL is widely used at all levels of business and is
renowned for its reliability, stability, high performance, ease of use,
rich feature set and SQL compliance. This alone makes it a serious
player in the database market, but combined with its transparent open
source development process, PostgreSQL ensures that users can control
the future of their database and avoid lock-in to expensive middleware,
tools and upgrade paths.

The 'Berkeley' (or BSD) license under which PostgreSQL is distributed 
gives users the freedom to distribute PostgreSQL as they see fit. This
makes businesses immune to over-deployment --- you need never pay a 
database license fee again.

\subsection{Standards compliance}

PostgreSQL is highly ANSI compliant. This is the universally recognised
technical standard that prescribes a range of features and functionality
that ANSI compliant databases must conform to. PostgreSQL has a very
high level of compliance with ANSI SQL 92, ANSI SQL 99 and ANSI SQL 2003
with documented extensions and enhancements. This reduces the risk of 
deploying on PostgreSQL.

Also, it means existing employees with SQL know-how can begin working
with PostgreSQL straight away instead of having to learn database
specific extensions to the query language.

\subsection{Features you need and want}

% list headline features.

\subsection{Extensive support options}

PostgreSQL is supported by a large number of organisations globally,
ranging from small contractor groups to world leading IT service
providers such as Fujitsu and Sun Microsystems. 
Assistance with everything from system
administration and software engineering through to performance
optimisation and high-level database-modelling is available.

\subsection{Advantages over big name proprietary databases}

\subsubsection{Avoid `lock-in' and reduce costs}

The database market has largely been dominated by a small
number of proprietary database systems with high brand-name recognition.
This lack of competition has been bad for users. It gives the vendors
too much control of cost, direction, upgrade path and in many cases the
application stack running on top of the database.

PostgreSQL does not lock in its users in this way. It runs on all major
platforms, with all major languages and middleware. Better yet, because
it is open source, the future of PostgreSQL is secure. Any user of
PostgreSQL can have direct input upon the design and direction of the
software. Technological merit, not marketing jargon, is the guiding
force of PostgreSQL.

\subsubsection{PostgreSQL has all the features others have, and more}

\paragraph{Designed for systems big and small}

PostgreSQL is compact in size when installed, and highly
memory-efficient, making it ideal for use in smaller size systems and
devices. It is also an Enterprise-grade database which can cater to
applications requiring many terabytes of data storage.

PostgreSQL supports all major operating systems including most types of
UNIX/Linux and Microsoft Windows, as well as all major hardware
platforms.

\paragraph{Ease of administration}

% much more info required here.

Administration and maintenance requirements for PostgreSQL are minimal.
PostgreSQL ships with all the tools necessary to install, configure,
maintain, backup and upgrade a database system.

In fact, PostgreSQL is widely recognised\footnote{For example, see
Lewis R. Cunningham's assessment of PostgreSQL against other leading
database systems:
http://blogs.ittoolbox.com/oracle/guide/archives/oracle-10g-vs-postgresql-8-vs-mysql-5-5452} as amongst the most simple databases to get up and running with --- regardless of platform.

\subsubsection{Architectural soundness and integrity}

Software Development companies are often pressurised into implementing
product features and enhancements which can compromise the software’s
underlying structure or impede the ability to make major changes to the
software in the future. This can be motivated by an objective of meeting
a short term goal but the disadvantages that this can entail can far
outweigh the benefits. Because PostgreSQL is not controlled and owned by
one organisation, the community overseeing its development represent a
wider cross-section of viewpoints in regard to shaping the future
direction of the product. In this way, modifications and enhancements to
PostgreSQL are undertaken with a strong intention to preserve the
underlying system structure and with longer term outcomes in mind, thus
ensuring a better, architecturally sound database.

\subsubsection{Future roadmap}

Organisations adopting PostgreSQL are no longer at the mercy of vendor’s
choosing to discontinue a particular database product line or feature
set. Nor will they run the risk of the vendor ceasing its entire
business operations. As PostgreSQL is entirely developed by a mainly
voluntary global community, it is not subject to the vagaries of the
marketplace to the same extent as commercial organisations responsible
for proprietary databases. Organisations can even directly establish a
stake in the future roadmap of PostgreSQL thanks to the unrestricted
participation model that is operated by the community.

\subsection{PostgreSQL has advantages over other open source database solutions}

\subsubsection{The more flexible BSD licensing model}

% discuss business friendliness

\subsubsection{The most advanced open source database}

From the outset, PostgreSQL was constructed to meet the goals of active
businesses which could rely on it as a core element of their
mission-critical IT infrastructure. The heritage of PostgreSQL is
therefore one of measuring up to the ongoing pressures and operational
demands of business, particularly those requiring complex transactional
capabilities. Forged in this environment, PostgreSQL has the features,
sophistication and reliability that have identified it as the serious
big-hitter among the open source databases.

\subsubsection{More features}

The range of features available with PostgreSQL is the most
comprehensive of all open source databases. Although other databases do
add features from time to time, none can match the record of technology
innovation and adoption of new standards that PostgreSQL has displayed
in recent years. Also, as probably the best architected database among
these, PostgreSQL’s ability to expand its range of features and
functionality far outweighs that of comparable products.

\section{Who should consider switching to PostgreSQL?}

\subsection{Independent software vendors (ISVs)}

The ISV sector has traditionally been highly responsive in adopting new
and emerging open source technologies. PostgreSQL provides ISVs with a
reliable database, which they can control and deploy as they see fit. As
with other open source software, ISVs can easily value-add to
PostgreSQL.

\subsubsection{Better sales margins}

ISVs replacing an existing bundled proprietary database with PostgreSQL,
can enjoy receiving a healthier margin on every single additional
application licence sold. No more leaking of valuable revenue in
database vendor licence fees. In the competitive world of software
sales, this is the kind of business benefit that can make all the
difference to hard-pressed ISVs striving to maintain or increase market
share.

\subsubsection{Future-proof your bundled database}

ISVs who in the past, for costs reasons, selected a
bundled database from a smaller-sized vendor which has not survived or
has not been able to provide a satisfactory level of on-going product
development in keeping with the ISVs changing needs. Now at last, ISVs
can make a vital strategic decision to eliminate dependencies they may
have on outdated technology and move to a standards-based big-hitting
database with an assured future, such as PostgreSQL.

\subsection{Organisations seeking lower total cost of ownership (TCO)}

\subsubsection{Big brand databases often charge high licence and support
costs}

When confronted with a limited range of database solution options in the
past, organisations have often been influenced by arbitrary factors such
as brand recognition more so than the suitability of a particular
database to best serve their individual business needs. This approach,
while understandable, has driven many organisations down a path which
has led to high ongoing licence and support costs. In such cases, the
database vendor may enjoy a healthy profit margin on a product which
could be easily replaced by a functionally equivalent lower cost
product. This situation often results from a lack of awareness of the
degree to which more cost-effective products such as PostgreSQL can
match and exceed the capabilities of commonly-used proprietary
databases.

\subsubsection{Be ambitious --- grow your business without incurring
additional database costs}

PostgreSQL has no licence cost and therefore delivers zero marginal cost
of scaleability to those businesses wishing to expand their IT
infrastructure. If your IT network consists of 20 or 200 systems
utilising PostgreSQL, the total licence costs attributable to the
database, remains nil. This represents a significant and direct benefit
to organisations seeking to extend their IT capability in keeping with
ambitious business growth plans. That’s quite an incentive particularly
in circumstances where a high risk or exploratory business venture is
only feasible if costs can be kept to a minimum.

\subsubsection{Stop paying for software modules you don’t need}

While a multitude of colourfully-labelled CDs may appear impressive when
you open your proprietary database software pack, it’s appropriate to
consider the actual tangible benefits of having such an array of
components and modules. A large percentage of these will likely never be
used during the lifetime of operation of the database. If anything, as
any software developer will attest, the more superfluous components
within a system, the greater the likelihood of an error or problem
arising.

PostgreSQL is first and foremost a powerful database solution and its
continued development focus is to remain just that. While easily
integrated with all commonly available technology components, PostgreSQL
is not weighed down by the excessive overhead of having to interface to
large numbers of rarely-used niche software modules. 

It’s no surprise therefore that many long-established PostgreSQL
business users commonly report that they have never experienced an
occurrence of the database going down, “not even once, ever”.

\subsubsection{Existing administration costs are too high}

A number of proprietary databases are sufficiently complex to operate
and run in a business environment that they require dedicated system
administration personnel to oversee these tasks on an ongoing basis.
This represents an additional and unwanted cost against the IT budget in
most organisations. In contrast, PostgreSQL has been designed to
minimise administration overheads, and its easy-to-use approach means
that many of the IT technical team in your organisation can install,
operate and manage multiple instances of the database, saving on the
need for specialist administrators.

\subsection{Organisations constrained by their current choice}

\subsubsection{Need a feature in a hurry – where’s the source code?}

If there is a business critical requirement that your database currently
can’t fulfil, it’s not much use if your database vendor will not have
this feature available to you in a timeframe that suits your business.
With PostgreSQL, you can organise an in-house or out-sourced enhancement
to PostgreSQL by suitably qualified personnel and in this way, meet your
own needs. 

Furthermore by actively participating in the development of PostgreSQL,
you have a stake in its future, and can influence the direction it takes

\subsubsection{No more costly enforced upgrades}

Businesses can expend a lot of effort and resources integrating a
particular version of a database into their core IT system or product,
only to find that the database vendor makes a decision to stop
supporting or increases support fees for that version of the database.
Businesses in this situation are often left with no choice but to accede
to the vendors demands for a greater slice of your budget. If you make
the switch to PostgreSQL, as well as giving your IT spend a healthy
boost, no third party will ever have this kind of strangle-hold on your
business ambitions again.

\subsubsection{Trapped using a database that has a limited future --- or none
at all?}

Is your database holding you back from moving your business application
to a new operating system? Has what was once a cheap integrated database
solution, now become a technology millstone around your neck? Perhaps,
your database vendor has ceased all future product enhancements, or, the
technology itself is outdated and has no future. Switching to PostgreSQL
will eliminate all of these concerns, as well provide all of the
benefits of the most advanced open source database available.

\subsubsection{There is only one organisation who supports my database}

Perhaps you’re dissatisfied with the support arrangements provided by
your current database vendor, but there are no other suitable options
you can pursue as the vendor or partners have an effective monopoly on
support arrangements for the product.

That’s a situation that can never exist with PostgreSQL. There are
thousands of organisations that can provide PostgreSQL support
worldwide. This creates a competitive climate which ensures that not
only are support costs kept low due to market pressures, but there is a
wider degree of choice of support provider. 

\section{Hear it from someone who’s made the switch to PostgreSQL}

% add prominent users

\subsection{Other successful businesses using PostgreSQL}
See PostgreSQL List of Reference Sites:-
http://www.postgresql.org/about/casestudies/

\section{Free commercial databases}

% point out that express editions lack features we have for by default

\section{More information}

% web site, etc.

\newpage
\section{Credit}

\subsection{Original authors}
Liam O'Duibhir <liamod@fast.fujitsu.com.au>
Gavin Sherry <gavin@alcove.com.au>

Prepared on behalf of Fujitsu Australia Software Technology.

\end{document}
